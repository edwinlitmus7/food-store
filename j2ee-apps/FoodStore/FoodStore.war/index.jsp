<%@ page language="java" contentType="text/html;"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:page>
	<%@ include file="./includes/header_v2.jsp" %>
	<%@ include file="./includes/navbar_v2.jsp" %>

	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
    <dsp:importbean bean="/atg/userprofiling/Profile" />
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean bean="/atg/targeting/TargetingForEach" />
    <dsp:importbean bean="/atg/commerce/catalog/custom/FoodStore" />
    <dsp:importbean bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler" />
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
    <dsp:importbean bean="/atg/commerce/catalog/custom/FacetedSearchFormHandler" />

		
		
		
		<div class="section">
		<!-- container -->
		<div class="container">
		<div class="row">
		<div id="aside" class="col-md-3">
			<dsp:include page="browse/gadgets/facets.jsp"></dsp:include>
		</div>
		<div id="main" class="col-md-9">
		<div class="store">
				<!-- home slick -->


			<%-- The jsps for faceted search, product display, product search are included here --%>

                <!-- menu items-->
                        <dsp:droplet name="IsNull">
                            <dsp:param bean="FoodStoreSearchFormHandler.searchResults" name="value" />
                            <dsp:oparam name="true">
                                <dsp:droplet name="IsNull">
                                    <dsp:param bean="FacetedSearchFormHandler.results" name="value" />
                                    <dsp:oparam name="true">
                                        <dsp:droplet name="ForEach">
                                            <dsp:param name="array" bean="FoodStore.rootCategory" />
                                            <dsp:param name="elementName" value="category" />
                                            <dsp:oparam name="output">
                                                <dsp:getvalueof id="index" idtype="java.lang.String" param="index" />
                                                <c:if test="${index < 2}">

                                                <!-- row -->
												<div class="row">
												<!-- section title -->
												<div class="col-md-12">
													<div class="section-title">
														<h2 class="title"> <dsp:valueof param="category.displayName" /> </h2>
													</div>
												</div>
												<!-- section title -->

												</div>
												<!-- /row -->



                                                    <div class="row">
																											
                                                        <dsp:include page="browse/gadgets/category_navigation.jsp">
                                                        	<dsp:param name="prev_index" value="${index}"/>
                                                        	 </dsp:include>
                                                    </div>
                                                </c:if>
                                            </dsp:oparam>
                                        </dsp:droplet>

                                    </dsp:oparam>
                                    <dsp:oparam name="false">
                                        <dsp:include page="browse/gadgets/faceted_results.jsp"></dsp:include>
                                    </dsp:oparam>
                                </dsp:droplet>
                                </dsp:oparam>
                                <dsp:oparam name="false">
                                    <dsp:include page="browse/gadgets/search_results.jsp"></dsp:include>
                                </dsp:oparam>

                        </dsp:droplet>
				
				<!-- /home slick -->
				</div>
				</div>
			</div>



                <!-- /menu items -->

			</div>
	<!-- /section -->

		</div>
		<!-- /container -->

				<!-- /content inside wrap -->
	  <%@ include file="./includes/footer_v2.jsp" %>
</dsp:page>
