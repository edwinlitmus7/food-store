<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<%@ page isELIgnored="false" %>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
    <head>
        <title> FoodStore </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <link href="css/style.css" rel="stylesheet" type="text/css">

    </head>
    <dsp:importbean bean="/atg/dynamo/droplet/Switch" />
    <dsp:importbean bean="/atg/userprofiling/Profile" />
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean bean="/atg/targeting/TargetingForEach" />
    <dsp:importbean bean="/atg/commerce/catalog/custom/FoodStore" />
    <dsp:importbean bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler" />
    <dsp:importbean bean="/atg/dynamo/droplet/IsNull" />
    <dsp:importbean bean="/atg/commerce/catalog/custom/FacetedSearchFormHandler" />

    <%@ include file="./includes/navbar.jsp" %>

        <%-- The jsps for faceted search, product display, product search are included here --%>
            <div class="row">
                <div class="col-sm-4">
                    <dsp:include page="browse/gadgets/facets.jsp"></dsp:include>
                </div>
                <div class="col-sm-6">
                    <div class="product-container">
                        <dsp:droplet name="IsNull">
                            <dsp:param bean="FoodStoreSearchFormHandler.searchResults" name="value" />
                            <dsp:oparam name="true">
                                <dsp:droplet name="IsNull">
                                    <dsp:param bean="FacetedSearchFormHandler.results" name="value" />
                                    <dsp:oparam name="true">
                                        <dsp:droplet name="ForEach">
                                            <dsp:param name="array" bean="FoodStore.rootCategory" />
                                            <dsp:param name="elementName" value="category" />
                                            <dsp:oparam name="output">
                                                <dsp:getvalueof id="index" idtype="java.lang.String" param="index" />
                                                <c:if test="${index < 2}">
                                                    <div class="food-category-name">
                                                        <dsp:valueof param="category.displayName" />
                                                        
                                                    </div>
                                                    <div class="row">
                                                        <dsp:include page="browse/gadgets/category_navigation.jsp"></dsp:include>
                                                    </div>
                                                </c:if>
                                            </dsp:oparam>
                                        </dsp:droplet>

                                    </dsp:oparam>
                                    <dsp:oparam name="false">
                                        <dsp:include page="browse/gadgets/faceted_results.jsp"></dsp:include>
                                    </dsp:oparam>
                                </dsp:droplet>
                                </dsp:oparam>
                                <dsp:oparam name="false">
                                    <dsp:include page="browse/gadgets/search_results.jsp"></dsp:include>
                                </dsp:oparam>
                            
                        </dsp:droplet>
                    </div>
                </div>
            </div>
            <%@ include file="./includes/footer.jsp" %>
                <!-- jQuery library -->
                <!-- Latest compiled JavaScript -->
                
                <script type="text/javascript" src="./js/script.js"></script>
</dsp:page>