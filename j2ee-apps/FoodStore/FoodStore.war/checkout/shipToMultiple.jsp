<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

	<dsp:page>
		<dsp:include page="/includes/header_v2.jsp" />
		<dsp:include page="/includes/navbar_v2.jsp" />

	<div class="container">
			<div class="row">
				<div class="col-md-7">
					<h1>Ship To Multiple Addresses</h1>
					<dsp:importbean
						bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
					<dsp:importbean
						bean="/atg/commerce/order/purchase/ShippingGroupDroplet" />
					<dsp:importbean
						bean="atg/commerce/order/purchase/ShippingGroupFormHandler" />
					<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
					<dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst" />
					<dsp:importbean bean="/atg/commerce/ShoppingCart" />
					<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
					<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
					<dsp:getvalueof var="contextPath"
						bean="/OriginatingRequest.contextPath" />
					<dsp:setvalue param="order" beanvalue="ShoppingCart.current" />
				<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Name</th>
									<th>Qty</th>
									<th>Quantity to move</th>
									<th>Shipping Address</th>
									<th>Save Changes</th>
								</tr>
							</thead>
							<tbody>
								<dsp:droplet name="ShippingGroupDroplet">
									<dsp:param name="createOneInfoPerUnit" value="false" />
									<dsp:param name="clearShippingInfos" param="init" />
									<dsp:param name="clearShippingGroups" value="false" />
									<dsp:param name="shippingGroupTypes" value="hardgoodShippingGroup" />
									<dsp:param name="initShippingGroups" param="init" />
									<dsp:param name="initBasedOnOrder" param="init" />
									<dsp:oparam name="output">
										<dsp:droplet name="ForEach">
											<dsp:param
												bean="ShippingGroupFormHandler.allHardgoodCommerceItemShippingInfos"
												name="array" />
											<dsp:param name="elementName" value="cisiItem" />
											<dsp:oparam name="output">
												<%-- <dsp:setvalue bean="ShippingGroupFormHandler.allHardgoodCommerceItemShippingInfos[param:index].splitQuantity" paramvalue="cisiItem.commerceItem.quantity" /> --%>
												<dsp:getvalueof var="cisiItem" param="cisiItem" />
												<tr>
													<div class="form-group">
														<dsp:form name="shipToMultiple"
															id="shipToMultiple_${serialNumber}" method="post">
															<td><dsp:valueof
																	param="cisiItem.commerceItem.auxiliaryData.catalogRef.displayName" />
															</td>
															<td><dsp:valueof param="cisiItem.commerceItem.quantity" /></td>
															<td> <dsp:input bean="ShippingGroupFormHandler.allHardgoodCommerceItemShippingInfos[param:index].splitQuantity" name="quantity" paramvalue="cisiItem.quantity" /> </td>
															<td>
																<dsp:select iclass="form-control"
																	bean="ShippingGroupFormHandler.allHardgoodCommerceItemShippingInfos[param:index].splitShippingGroupName"
																	name="shipping" id="shipping_${serialNumber}">
																	<dsp:droplet name="MapToArrayDefaultFirst">
																		<dsp:param name="defaultKey"
																			bean="ShippingGroupDroplet.shippingGroupMapContainer.defaultShippingGroupName" />
																		<dsp:param name="map" param="shippingGroups" />
																		<dsp:param name="sortByKeys" value="true" />
																		<dsp:oparam name="output">
																			<dsp:getvalueof var="sortedArray"
																				vartype="java.lang.Object" param="sortedArray" />
																			<dsp:droplet name="ForEach">
																				<dsp:param name="array" param="sortedArray" />
																				<dsp:param name="elementName" value="shippingGroup" />
																				<dsp:oparam name="output">
																					<dsp:getvalueof var="shippingGroup" param="shippingGroup" />
																					<dsp:option value="${shippingGroup.key}"selected="${shippingGroup.key eq cisiItem.shippingGroupName }">
																						<dsp:valueof param="shippingGroup.key" />
																                     </dsp:option> 
																				</dsp:oparam>
																			</dsp:droplet>
																		</dsp:oparam>
																	</dsp:droplet>
																</dsp:select>
															</td>
															<td><dsp:input
																	bean="ShippingGroupFormHandler.splitShippingInfosSuccessURL"
																	type="hidden" value="shipToMultiple.jsp?init=false" />
																<dsp:input
																	bean="ShippingGroupFormHandler.splitShippingInfos" iclass="primary-btn"
																	id="shipSubmit_${serialNumber}" type="submit" value="Save" />
															</td>
														</dsp:form>
													</div>
												</tr>
											</dsp:oparam>
										</dsp:droplet>
									</dsp:oparam>
								</dsp:droplet>
							</tbody>
						</table>
					</div>
					<dsp:form action="credit_card.jsp" method="post">
						<dsp:input
							bean="ShippingGroupFormHandler.applyShippingGroupsSuccessURL"
							type="hidden" value="credit_card.jsp" />
						<dsp:input bean="ShippingGroupFormHandler.applyShippingGroups"
							type="submit" iclass="primary-btn" value="Continue To Payment" />
					</dsp:form>
				</div>
			</div>
		</div>
		
		<dsp:include page="/includes/footer_v2.jsp" />
		<script>
			// temporary fix for page refreshing.
			window.onload = function() {
			    if(!window.location.hash) {
			        window.location = window.location + '#loaded';
			        window.location.reload();
			    }
			}
		</script>
	</dsp:page>
