<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="./../css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>
	<%@ include file="/includes/navbar.jsp"%>
	<div class="row">
		<div class="col-xs-6">
			<h1>Shipping address</h1>
		</div>
		<div class="col-xs-6">
			<h3>
				<dsp:a title="New Billing Address"
					page="gadgets/billingAddressEditForm_1.jsp"
					iclass="glyphicon glyphicon-plus-sign">
				</dsp:a>
			</h3>
		</div>
	</div>
	
	<%@ include file="/includes/footer.jsp" %>
	<script>
	function hideEditForm(name){
		$("#editShippingAddressFormPart_"+name).hide();
	}
	function showEditForm(name){
		$("#editShippingAddressFormPart_"+name).show(1000);
	}
	</script>
	<style> input[type=text]{width:100%} </style>
	</body>
	</html>

</dsp:page>
<%-- @version $Id: //product/DCS/version/11.2/release/DCSSampleCatalog/j2ee-apps/sampleCatalog/web-app/shipping.jsp#2 $$Change: 1179550 $--%>
