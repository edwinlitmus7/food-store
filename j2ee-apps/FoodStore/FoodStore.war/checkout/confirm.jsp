<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
		<dsp:include page="/includes/header_v2.jsp"/>
		<dsp:include page="/includes/navbar_v2.jsp"/>
		<dsp:importbean
			bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
		<dsp:importbean bean="/atg/commerce/ShoppingCart" />

		<%-- Display errors if any --%>
		<div id="atg_store_formValidationError">
			<dsp:include page="/global/gadgets/errorMessage.jsp">
				<dsp:param name="formHandler" bean="CommitOrderFormHandler" />
				<dsp:param name="divid" value="errorMessage" />
			</dsp:include>
		</div>

		<c:set var="stage" value="confirm" />

		<div class="section">
			<div class="container">
				<div class="row">
				<div class="col-xs-7 invoice_container">
					<div class="col-xs-12">
						<div class="section-title">
							<h2 class="title"> Order Confirmation </h2>
						</div>
					</div>
					<div class="col-xs-12">
					<%-- Display cart contents, shipping and billing information. --%>
					<dsp:include page="/global/gadgets/orderSummary.jsp">
						<dsp:param name="order" bean="/atg/commerce/ShoppingCart.current" />
						<dsp:param name="isCurrent" value="true" />
					</dsp:include>
					</div>
				</div>
					<div class="col-xs-5">
						<%-- Order Summary --%>
						<dsp:include page="/checkout/gadgets/checkoutOrderSummary.jsp">
							<dsp:param name="order" bean="ShoppingCart.current" />
							<dsp:param name="currentStage" value="${stage}" />
							<dsp:param name="isConfirmPage" value="true" />
						</dsp:include>
					</div>

				</div>
			
			</div>
		</div>

  <dsp:include page="/includes/footer_v2.jsp"/>
</dsp:page>
