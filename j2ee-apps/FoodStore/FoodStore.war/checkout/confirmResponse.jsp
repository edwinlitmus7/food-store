<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:include page="/includes/header_v2.jsp"></dsp:include>
	<dsp:include page="/includes/navbar_v2.jsp"></dsp:include>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	
	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	
	<div class="section">
	<div class="container">
		<div class="alert alert-success" style="margin:5px">
			<div class="row"  style="padding:5px">
			
			<h4 class="title"> Thank you for shopping with us. </h4>
			<strong>Your order has been placed successfully!</strong>
			<div>
				<b> Order number: </b> <dsp:valueof bean="ShoppingCart.last.id" /> 
			</div>
			<br/>
			<a href="${contextPath}/myaccount/myOrders.jsp" class="btn main-btn"> View previous orders </a>
		</div>
	</div>
	</div>
	</div>
	<dsp:include page="/includes/footer_v2.jsp"></dsp:include>
</dsp:page>