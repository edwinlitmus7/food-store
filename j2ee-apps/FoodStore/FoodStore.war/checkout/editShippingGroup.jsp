<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:include page="/includes/header_v2.jsp"/>
	<dsp:include page="/includes/navbar_v2.jsp"/>
	
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
			
	<dsp:param name="addEditMode" param="addEditMode" />
	<dsp:param name="addressName" param="addressName" />
	
	<div class="section">
		<div class="container">
					
					<dsp:droplet name="Switch">
		<dsp:param name="value" param="addEditMode" />
		<!-- Shipping group edit mode -->
		<dsp:oparam name="edit">
			<dsp:include page="gadgets/addEditShippingGroup.jsp">
				<dsp:param name="formHandlerComponent"
					value="UpdateHardgoodShippingGroupFormHandler" />
				<dsp:param name="addEditMode" param="addEditMode" />
				<dsp:param name="addressName" param="addressName" />
				<dsp:param name="defaultAddress" param="defaultAddress"/>
				<dsp:param name="shippingGroup" value="workingHardgoodShippingGroup"/>
			</dsp:include>
		</dsp:oparam>
		<!-- Shipping Group Add mode -->
		<dsp:oparam name="add">
			<dsp:include page="gadgets/addEditShippingGroup.jsp">
				<dsp:param name="formHandlerComponent"
					value="CreateHardgoodShippingGroupFormHandler" />
				<dsp:param name="addEditMode" param="addEditMode" />
				<dsp:param name="shippingGroup" value="hardgoodShippingGroup" />
			</dsp:include>
		</dsp:oparam>
	</dsp:droplet>
	
		</div>
	</div>
	
<dsp:include page="/includes/footer_v2.jsp"/>
</dsp:page>