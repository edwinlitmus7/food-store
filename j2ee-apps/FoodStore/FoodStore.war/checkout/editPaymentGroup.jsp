<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
<dsp:include page="/includes/header_v2.jsp"/>
	<dsp:include page="/includes/navbar_v2.jsp"/>

	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CreateCreditCardFormHandler" />
	<dsp:importbean
		bean="atg/commerce/order/purchase/UpdateCreditCardFormHandler" />

	<dsp:param name="addEditMode" param="addEditMode" />
	<dsp:param name="creditCardNickname" param="creditCardNickname"/>

	<dsp:droplet name="Switch">
		<dsp:param name="value" param="addEditMode" />
		<!-- Payment group edit mode -->
		<dsp:oparam name="edit">
			<dsp:include page="gadgets/addEditPaymentGroup.jsp">
				<dsp:param name="formHandlerComponent"
					value="UpdateCreditCardFormHandler" />
				<dsp:param name="savToProfile" value="updateProfile"/>
				<dsp:param name="addEditMode" param="addEditMode" />
				<dsp:param name="creditCardNickname" param="creditCardNickname"/>
			</dsp:include>
		</dsp:oparam>
		<!-- Payment group add mode -->
		<dsp:oparam name="default">
			<dsp:include page="gadgets/addEditPaymentGroup.jsp">
				<dsp:param name="formHandlerComponent"
					value="CreateCreditCardFormHandler" />
				<dsp:param name="savToProfile" value="copyToProfile"/>
				<dsp:param name="addEditMode" param="addEditMode" />
			</dsp:include>
		</dsp:oparam>
	</dsp:droplet>


<dsp:include page="/includes/footer_v2.jsp"/>
</dsp:page>
<%-- @version $Id: //product/DCS/version/11.2/release/DCSSampleCatalog/j2ee-apps/sampleCatalog/web-app/shipping.jsp#2 $$Change: 1179550 $--%>
