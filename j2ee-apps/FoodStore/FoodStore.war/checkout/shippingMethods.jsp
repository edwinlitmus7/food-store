<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

	<dsp:page>
		<dsp:include page="/includes/header_v2.jsp" />
		<dsp:include page="/includes/navbar_v2.jsp" />
		<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler"/>
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<div class="section">
		<div class="container">
			<div class="row">
				<div class="section-title">
					<h2 class="title">  Shipping Methods</h2>
				</div>
			</div>
			<div class="row">
				
			
				
					<dsp:form action="credit_card.jsp">
					 <dsp:droplet name="/atg/commerce/pricing/AvailableShippingMethods">
							<dsp:param bean="CartModifierFormHandler.shippingGroup" name="shippingGroup"/>
							<dsp:oparam name="output">
 								<dsp:select bean="CartModifierFormHandler.shippingGroup.shippingMethod">
 									<dsp:droplet name="ForEach">
 										<dsp:param param="availableShippingMethods" name="array"/>
 										<dsp:param value="method" name="elementName"/>
 										<dsp:oparam name="output">
   											<dsp:getvalueof id="option16" param="method" idtype="java.lang.String">
											<dsp:option value="<%=option16%>"/>
											</dsp:getvalueof><dsp:valueof param="method"/>
 										</dsp:oparam>
 									</dsp:droplet>
 								</dsp:select>
							</dsp:oparam>
						</dsp:droplet> 
						<input value="SUBMIT" type="submit"></input>
						</dsp:form>
			</div>
					
				
			
		</div>
		</div>
	</dsp:page>