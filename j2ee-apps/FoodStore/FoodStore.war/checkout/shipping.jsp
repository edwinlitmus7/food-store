<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>
<dsp:page>

	
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />

	<!--  Displays shipping groups of the user. And an option to add new shipping address -->
	<dsp:include page="/includes/header_v2.jsp"/>
	<dsp:include page="/includes/navbar_v2.jsp"/>
	<div class="section">
	<div class="container">
	<div class="row">
		<div class="section-title">
				<h2 class="title">  Shipping </h2>
		</div>
		<div class="col-xs-6">
			<h3>
				<dsp:a title="New Shipping Address"
					page="editShippingGroup.jsp"
					iclass="fa fa-plus">
					Add address
					<dsp:param name="addEditMode" value="add"/>
				</dsp:a>
			</h3>
		</div>
		<div class="col-xs-6">
			<dsp:a href="${originatingRequest.contextPath}/checkout/shipToMultiple.jsp?init=true" iclass="btn main-btn">
				Ship To Multiple Address
			</dsp:a>
		</div>
	</div>
	<dsp:include page="gadgets/shippingSingleForm.jsp" />
	
	<script>
	function hideEditForm(name){
		$("#editShippingAddressFormPart_"+name).hide();
	}
	function showEditForm(name){
		$("#editShippingAddressFormPart_"+name).show();
	}
	</script>
	
	</div>
	</div>
	<dsp:include page="/includes/footer_v2.jsp"/>
</dsp:page>
<%-- @version $Id: //product/DCS/version/11.2/release/DCSSampleCatalog/j2ee-apps/sampleCatalog/web-app/shipping.jsp#2 $$Change: 1179550 $--%>
