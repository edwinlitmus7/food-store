<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="atg/commerce/order/purchase/ShippingGroupDroplet" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CreateHardgoodShippingGroupFormHandler" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/UpdateHardgoodShippingGroupFormHandler" />

	<!-- Parameters from the page  -->
	<dsp:getvalueof var="formHandlerComponent" param="formHandlerComponent" />
	<dsp:getvalueof var="shippingGroup" param="shippingGroup" />
	<dsp:getvalueof var="defaultAddress" param="defaultAddress" />
	<dsp:getvalueof var="addressName" param="addressName" />
	<dsp:getvalueof var="mode" param="addEditMode" />
	<!-- Sets the bean with edit address details -->
	<c:if test="${mode == 'edit'}">
		<dsp:setvalue bean="${formHandlerComponent}.${shippingGroup}"
			beanvalue="ShippingGroupDroplet.ShippingGroupMapContainer.ShippingGroupMap.${addressName}" />
	</c:if>
	<%-- Iterate form errors --%>
	<dsp:droplet name="Switch">
		<dsp:param bean="${formHandlerComponent}.formError" name="value" />
		<dsp:oparam name="true">
			<div class="row">

				<div class="col-xs-7">
					<div class="alert alert-danger">
						<font color=fffcfc><STRONG><UL>
									<dsp:droplet name="ErrorMessageForEach">
										<dsp:param bean="${formHandlerComponent}.formExceptions"
											name="exceptions" />
										<dsp:oparam name="output">
											<LI><dsp:valueof param="message" />
										</dsp:oparam>
									</dsp:droplet>
								</UL></STRONG></font>
					</div>
				</div>

			</div>
		</dsp:oparam>
	</dsp:droplet>
	<!-- Form which handles the Shipping Address details -->
	<dsp:form method="post">

				<!-- Shipping group name -->
				<div class="form-group">
					<label for="shippingGroupName" class="control-label">Address
						Name</label>

						<dsp:input id="shippingGroupName" type="text"
							bean="${formHandlerComponent}.hardgoodShippingGroupName"
							maxlength="40" value="${addressName}"
							iclass="form-control"
							readonly="${mode == 'edit' ? 'true' : 'false'}">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Nickname for address" />
						</dsp:input>

				</div>

				<!-- Shipping Address Nickname -->
				<div class="form-group">
					<label for="newNickName" class=" control-label">Nick
						Name</label>

						<dsp:input id="newNickName" type="text"
							iclass="form-control"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.nickname"
							maxlength="40">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="inlineIndicator" value="NicknameAlert" />
							<dsp:tagAttribute name="placeholder" value="Nickname" />
						</dsp:input>

				</div>
				<!-- Shipping Address first name -->
				<div class="form-group">
					<label for="firstName" class=" control-label">First
						Name</label>

						<dsp:input id="firstName"
							iclass="form-control"
							type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.firstName"
							maxlength="40">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="First Name" />
						</dsp:input>

				</div>

				<!-- Shipping Address Last name -->
				<div class="form-group">
					<label for="LastName" class=" control-label">Last
						Name</label>
						<dsp:input id="LastName" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.lastName"
							maxlength="40"
							iclass="form-control"
							>
							<dsp:tagAttribute name="placeholder" value="Last Name" />
						</dsp:input>

				</div>
				<!-- Type of address -->
				<div class="form-group">
					<label for="select" class=" control-label">Type Of
						Address</label>

						<dsp:select
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.typeOfAddress"
							id="typeOfAddress" iclass="form-control">
							<dsp:option value="Home">Home</dsp:option>
							<dsp:option value="Office">Office</dsp:option>
						</dsp:select>

				</div>
				<!-- Address line 1 -->
				<div class="form-group">
					<label for="address1" class=" control-label">Address
						Line 1</label>

						<dsp:input id="address1"
							iclass="form-control"
							 type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.address1"
							maxlength="50">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Address Line 1" />
						</dsp:input>

				</div>
				<!-- Address line 2 -->
				<div class="form-group">
					<label for="address2" class=" control-label">Address
						Line 2</label>

						<dsp:input id="address2" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.address2"
							maxlength="50"
							iclass="form-control"
							>
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Address Line 2" />
						</dsp:input>

				</div>
				<!-- Address line 3 -->
				<div class="form-group">
					<label for="address3" class=" control-label">Address
						Line 3</label>

						<dsp:input id="address3" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.address3"
							maxlength="50"
							iclass="form-control"
							>
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Address Line 3" />
						</dsp:input>

				</div>

			<!-- City -->

				<div class="form-group">
					<label for="city" class=" control-label">City</label>

						<dsp:input id="city" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.city"
							maxlength="30"
							iclass="form-control"
							>
							<dsp:tagAttribute name="placeholder" value="Thiruvalla" />
						</dsp:input>

				</div>
				<!-- State -->
				<div class="form-group">
					<label for="state" class=" control-label">State</label>

						<dsp:input id="state" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.state"
							iclass="form-control"
							maxlength="30">
							<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*" />
							<dsp:tagAttribute name="title" value="First letter as Capital" />
							<dsp:tagAttribute name="placeholder" value="State" />
						</dsp:input>

				</div>
				<!-- Postal Code -->
				<div class="form-group">
					<label for="postalCode" class=" control-label">Postal
						Code</label>

						<dsp:input id="postalCode" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.postalCode"
							maxlength="10"
							iclass="form-control"
							>
							<dsp:tagAttribute name="pattern" value="[0-9]{6}" />
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="PostalCode" />
						</dsp:input>

				</div>
				<!-- Country -->
				<div class="form-group">
					<label for="country" class=" control-label">Country</label>

						<dsp:input id="country" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.country"
							iclass="form-control"
							maxlength="30">
							<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*" />
							<dsp:tagAttribute name="title" value="First letter as Capital" />
							<dsp:tagAttribute name="placeholder" value="Country" />
						</dsp:input>

				</div>
				<!-- Mobile -->
				<div class="form-group">
					<label for="mobile" class=" control-label">Mobile</label>

						<dsp:input id="mobile" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.mobile"
							maxlength="15"
							iclass="form-control"
							>
							<dsp:tagAttribute name="pattern" value="[+][9][1][-][0-9]{10}" />
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="title" value="+91-XXXXXXXXXX" />
							<dsp:tagAttribute name="placeholder" value="+91-XXXXXXXXXX" />
						</dsp:input>

				</div>
				<!-- Fax number -->
				<div class="form-group">
					<label for="faxNumber" class=" control-label">Fax
						Number</label>

						<dsp:input id="faxNumber" type="text"
							bean="${formHandlerComponent}.${shippingGroup}.shippingAddress.faxNumber"
							maxlength="15"
							iclass="form-control"
							>
							<dsp:tagAttribute name="placeholder" value="FaxNumber" />
						</dsp:input>

				</div>

				<dsp:droplet name="/atg/dynamo/droplet/Switch">
					<dsp:param name="value" param="addEditMode" />
					<!--
						The form for creating a new shipping group. This will invoke handleNewHardgoodShippingGroup
						method of CreateHardgoodShippingGroupFormHandler.

					 -->
					<dsp:oparam name="add">
					<!--
						The property assignNewShippingGroupAsDefault of CreateHardgoodShippingGroupFormHandler when set as true,
						sets the shipping group as default shipping group.
					  -->

							<div class="form-group">
								
									 <dsp:input type="checkbox"
										
											bean="${formHandlerComponent}.assignNewShippingGroupAsDefault"
											value="true" />
									
									<b>Default Address</b>
									
								
							</div>

						<!--
							The property addToProfile of CreateHardgoodShippingGroupFormHandler when set as true,
							 adds the shipping group to user's profile address.
						  -->
						<div class="form-group">

								
									 <dsp:input type="checkbox"
										
											bean="${formHandlerComponent}.addToProfile" value="true" />
											
											<b>Add
											to Profile</b>
									
								

						</div>
						<!-- Submit button for creating new Shipping Address, also contains Success and Error URL-->
						<div class="form-group">

								<div>
									<dsp:input
										bean="${formHandlerComponent}.newHardgoodShippingGroupSuccessURL"
										type="hidden" value="../shipping.jsp" />
									<dsp:input
										bean="${formHandlerComponent}.newHardgoodShippingGroupErrorURL"
										type="hidden" value="../editShippingGroup.jsp?addEditMode=add" />
									<dsp:input
										bean="${formHandlerComponent}.newHardgoodShippingGroup"
										priority="<%=(int) -10%>" type="submit"
										iclass="primary-btn" value="Add New Shipping Address" />
								</div>
							</div>

					</dsp:oparam>
					<!--
						Already created shipping groups can be edited here. This will invoke handleUpdateHardgoodShippingGroup
						of UpdateHardgoodShippingGroupFormHandler.
					 -->
					<dsp:oparam name="edit">
						<div class="form-group">

								
									<c:if test="${defaultAddress eq false}">
										<dsp:input
										
											bean="${formHandlerComponent}.setDefaultShippingGroup"
											type="checkbox" value="true" checked="true" />
										<b>Set As Default</b>
									</c:if>
								

						</div>
						<!-- Checks whether the entered Address is in Profile -->
						<dsp:getvalueof var="transientAddress" value="true" scope="page" />
						<dsp:droplet name="ForEach">
							<dsp:param name="array" bean="Profile.secondaryAddresses" />
							<dsp:param name="elementName" value="contactInfo" />
							<dsp:oparam name="output">
								<dsp:droplet name="/atg/dynamo/droplet/Compare">
									<dsp:param name="obj1" param="contactInfo.id" />
									<dsp:param name="obj2" param="shippingAddress.id" />
									<dsp:oparam name="equal">
										<%-- el variable transientAddress is set false ie., address exists in users secondary address --%>
										<dsp:getvalueof var="transientAddress" value="false"
											scope="page" />
									</dsp:oparam>
								</dsp:droplet>
							</dsp:oparam>
						</dsp:droplet>
						<!-- Checks whether the Address is Transient or not -->
						<dsp:droplet name="Switch">
							<dsp:param name="value" value="${transientAddress}" />
							<dsp:oparam name="true">
								<%-- address is not in address list --%>
								<div class="form-group">

										
											 <dsp:input
												iclass=""
													bean="UpdateHardgoodShippingGroupFormHandler.addToProfile"
													type="checkbox" checked="false" /> <b>Add to Profile</b>
											
											<dsp:input
												bean="UpdateHardgoodShippingGroupFormHandler.updateProfile"
												type="hidden" value="false" />
										
								</div>
							</dsp:oparam>
						</dsp:droplet>
						<!-- Submit button for update Shipping group, also Success and Error URL -->
						<div class="form-group">

								<dsp:input
									bean="${formHandlerComponent}.updateHardgoodShippingGroupSuccessURL"
									type="hidden" value="../shipping.jsp" />
								<dsp:input
									bean="${formHandlerComponent}.updateHardgoodShippingGroupErrorURL"
									type="hidden"
									value="../editShippingGroup.jsp?addressName=${addressName}&addEditMode=edit" />
								<dsp:input
									bean="${formHandlerComponent}.updateHardgoodShippingGroup"
									priority="<%=(int) -10%>" type="submit"
									iclass="primary-btn" value="Update this shipping group" />

						</div>
					</dsp:oparam>
				</dsp:droplet>

		
	</dsp:form>
</dsp:page>
