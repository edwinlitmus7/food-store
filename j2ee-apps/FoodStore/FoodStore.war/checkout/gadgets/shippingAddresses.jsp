<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/ShippingGroupDroplet" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CreateHardgoodShippingGroupFormHandler" />
	<c:set var="buttonFlag" value="false"></c:set>
	<%-- Check for errors  --%>
	<dsp:droplet name="Switch">
		<dsp:param bean="ShippingGroupFormHandler.formError" name="value" />
		<dsp:oparam name="true">
			<div class="row">

				<div class="col-xs-7">
					<font color=fffcfc><STRONG><UL>
								<dsp:droplet name="ErrorMessageForEach">
									<dsp:param bean="ShippingGroupFormHandler.formExceptions"
										name="exceptions" />
									<dsp:oparam name="output">
										<LI><dsp:valueof param="message" />
									</dsp:oparam>
								</dsp:droplet>
							</UL></STRONG></font>
				</div>

			</div>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="Switch">
		<dsp:param bean="CreateHardgoodShippingGroupFormHandler.formError"
			name="value" />
		<dsp:oparam name="true">
			<div class="row">

				<div class="col-xs-7">
					<div class="alert alert-danger">
						<font color=cc0000><STRONG><UL>
									<dsp:droplet name="ErrorMessageForEach">
										<dsp:param
											bean="CreateHardgoodShippingGroupFormHandler.formExceptions"
											name="exceptions" />
										<dsp:oparam name="output">
											<LI><dsp:valueof param="message" />
										</dsp:oparam>
									</dsp:droplet>
								</UL></STRONG></font>
					</div>
				</div>

			</div>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:form>
		<dsp:getvalueof var="contextPath"
			bean="/OriginatingRequest.contextPath" />
		<dsp:getvalueof var="transient" bean="Profile.transient" />
		<dsp:input
			bean="ShippingGroupFormHandler.applyShippingGroupsSuccessURL"
			type="hidden" value="${contextPath }/checkout/credit_card.jsp" />
		<dsp:input bean="ShippingGroupFormHandler.applyShippingGroupsErrorURL"
			type="hidden"
			value="${contextPath }/global/gadgets/errorMessage.jsp" />
		<dsp:input bean="ShippingGroupFormHandler.applyDefaultShippingGroup"
			type="hidden" value="true" />
		<dsp:droplet name="ShippingGroupDroplet">
			<dsp:param name="createOneInfoPerUnit" value="false" />
			<dsp:param name="clearShippingInfos" value="true" />
			<dsp:param name="clearShippingGroups" value="false" />
			<dsp:param name="shippingGroupTypes" value="hardgoodShippingGroup" />
			<dsp:param name="initShippingGroups" value="true" />
			<dsp:param name="initBasedOnOrder" value="true" />
			<dsp:oparam name="output">
				<dsp:droplet name="MapToArrayDefaultFirst">
					<dsp:param name="defaultKey"
						bean="ShippingGroupDroplet.shippingGroupMapContainer.defaultShippingGroupName" />
					<dsp:param name="map" param="shippingGroups" />
					<dsp:param name="sortByKeys" value="true" />
					<dsp:oparam name="empty">
						<c:set var="buttonFlag" value="true"></c:set>
						<div align="center">
							<h4>There are no Addresses linked to this account</h4>
						</div>
					</dsp:oparam>
					<dsp:oparam name="output">
						<dsp:getvalueof var="sortedArray" vartype="java.lang.Object"
							param="sortedArray" />
						<dsp:droplet name="ForEach">
							<dsp:param name="array" param="sortedArray" />
							<dsp:oparam name="output">
								<dsp:droplet name="Switch">
									<dsp:param name="value" param="count" />
									<dsp:oparam name="1">
										<dsp:getvalueof var="panel" value="panel panel-danger" />
										<dsp:getvalueof var="radio" value="true" />
										<c:set var="defaultAddress" value="true" />
									</dsp:oparam>
									<dsp:oparam name="default">
										<dsp:getvalueof var="panel" value="panel panel-default" />
										<dsp:getvalueof var="radio" value="false" />
										<c:set var="defaultAddress" value="false" />
									</dsp:oparam>
								</dsp:droplet>
								<div class="container-fluid">
									<div class="row">
										<div class="col-xs-6">
											<div class="${panel }">
												<div class="panel-heading">
													<dsp:getvalueof var="addrName" param="element.key" />
													<c:choose>
														<c:when test="${not empty addrName}">
															<dsp:getvalueof var="nickname" param="element.key" />
														</c:when>
														<c:otherwise>
															<dsp:getvalueof var="nickname"
																param="element.value.shippingAddress.nickname" />
														</c:otherwise>
													</c:choose>
													<dsp:valueof value="${nickname}" />
													-
													<dsp:valueof
														param="element.value.shippingAddress.typeOfAddress" />
													<div align="right">
														Ship To Address
														<dsp:input
															bean="ShippingGroupDroplet.shippingGroupMapContainer.defaultShippingGroupName"
															paramvalue="element.key" type="radio" checked="${radio}" />
													</div>
												</div>
												<div class="panel-body" align="left">
													<!-- Address information -->
													<dsp:include page="/global/util/displayAddress.jsp">
														<dsp:param name="address"
															param="element.value.shippingAddress" />
													</dsp:include>
												</div>
												<div class="panel-footer" align="right">
													<ul class="list-inline">
														<c:if test="${defaultAddress eq false}">
															<li><dsp:a title="Remove Address"
																	bean="ShippingGroupFormHandler.removeShippingAddress"
																	page="/${contextPath }/checkout/shipping.jsp"
																	iclass="fa fa-trash" value="">
																	<dsp:param name="removeShippingAddressSuccessURL"
																		value="${contextPath }/checkout/shipping.jsp" />
																	<dsp:param name="removeShippingAddressErrorURL"
																		value="${contextPath }/checkout/shipping.jsp" />
																	<dsp:param name="removeShippingAddressNickname"
																		param="element.key" />
																</dsp:a></li>
														</c:if>
														<li>
															<dsp:a title="Edit Shipping Address"
																page="../editShippingGroup.jsp"
																iclass="fa fa-edit">
																<dsp:param name="addEditMode" value="edit" />
																<dsp:param name="defaultAddress"
																	value="${defaultAddress}" />
																<dsp:param name="addressName" param="element.key" />
															</dsp:a>
														</li>
													</ul>
												</div>
											</div>
										</div>
									</div>
								</div>
							</dsp:oparam>
						</dsp:droplet>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
		<c:choose>
			<c:when test="${not buttonFlag }">
				<div class="container">
					<div class="col-xs-6" align="right">
						<dsp:input type="submit" value="Ship Here" iclass="primary-btn"
							bean="ShippingGroupFormHandler.applyShippingGroups"
							>
						</dsp:input>
					</div>
				</div>
			</c:when>
		</c:choose>
	</dsp:form>
</dsp:page>
