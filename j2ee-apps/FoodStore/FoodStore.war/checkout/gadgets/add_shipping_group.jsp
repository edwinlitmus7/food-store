<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="./../css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	</head>
	<%@ include file="/includes/navbar.jsp"%>
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CreateHardgoodShippingGroupFormHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<h3 style="text-align: center;">Add New Shipping Address</h3>
	<%-- Check for errors  --%>
	<dsp:droplet name="Switch">
		<dsp:param bean="ShippingGroupFormHandler.formError" name="value" />
		<dsp:oparam name="true">
			<font color=cc0000><STRONG><UL>
						<dsp:droplet name="ErrorMessageForEach">
							<dsp:param bean="ShippingGroupFormHandler.formExceptions"
								name="exceptions" />
							<dsp:oparam name="output">
								<LI><dsp:valueof param="message" />
							</dsp:oparam>
						</dsp:droplet>
					</UL></STRONG></font>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:droplet name="Switch">
		<dsp:param bean="CreateHardgoodShippingGroupFormHandler.formError"
			name="value" />
		<dsp:oparam name="true">
			<font color=cc0000><STRONG><UL>
						<dsp:droplet name="ErrorMessageForEach">
							<dsp:param
								bean="CreateHardgoodShippingGroupFormHandler.formExceptions"
								name="exceptions" />
							<dsp:oparam name="output">
								<LI><dsp:valueof param="message" />
							</dsp:oparam>
						</dsp:droplet>
					</UL></STRONG></font>
		</dsp:oparam>
	</dsp:droplet>
	<dsp:form action="add_shipping_group.jsp" method="post">

		<div class="row">
			<div class="col-lg-6">
				<div class="form-group">
					<label for="shippingGroupName" class="col-xs-3 control-label">Address
						Name</label>
					<div class="col-xs-4">
						<dsp:input id="shippingGroupName" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroupName"
							maxlength="40">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Shipping Group Name" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="nickName" class="col-xs-3 control-label">Nick
						Name</label>
					<div class="col-xs-4">
						<dsp:input id="newNickName" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.nickname"
							maxlength="40">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="inlineIndicator" value="NicknameAlert" />
							<dsp:tagAttribute name="placeholder" value="Nickname" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="firstName" class="col-xs-3 control-label">First
						Name</label>
					<div class="col-xs-4">
						<dsp:input id="firstName" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.firstName"
							maxlength="40" beanvalue="Profile.firstName">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="First Name" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="LastName" class="col-xs-3 control-label">Last
						Name</label>
					<div class="col-xs-4">
						<dsp:input id="LastName" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.lastName"
							maxlength="40" beanvalue="Profile.lastName">
							<dsp:tagAttribute name="placeholder" value="Last Name" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="select" class="col-xs-3 control-label">Type Of
						Address</label>
					<div class="col-xs-4">
						<dsp:select
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.typeOfAddress"
							id="typeOfAddress" iclass="form-control">
							<dsp:option value="Home">Home</dsp:option>
							<dsp:option value="Office">Office</dsp:option>
						</dsp:select>
					</div>
				</div>
				<div class="form-group">
					<label for="address1" class="col-xs-3 control-label">Address
						Line 1</label>
					<div class="col-xs-4">
						<dsp:input id="address1" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.address1"
							maxlength="50"
							beanvalue="Profile.derivedShippingAddress.address1">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Address Line 1" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="address2" class="col-xs-3 control-label">Address
						Line 2</label>
					<div class="col-xs-4">
						<dsp:input id="address2" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.address2"
							maxlength="50"
							beanvalue="Profile.derivedShippingAddress.address2">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Address Line 2" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="address3" class="col-xs-3 control-label">Address
						Line 3</label>
					<div class="col-xs-4">
						<dsp:input id="address3" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.address3"
							maxlength="50"
							beanvalue="Profile.derivedShippingAddress.address3">
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="Address Line 3" />
						</dsp:input>
					</div>
				</div>
			</div>
			<div class="col-xs-6">
				<div class="form-group">
					<label for="city" class="col-xs-3 control-label">City</label>
					<div class="col-xs-4">
						<dsp:input id="city" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.city"
							maxlength="30" beanvalue="Profile.derivedShippingAddress.city">
							<dsp:tagAttribute name="placeholder" value="City" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="state" class="col-xs-3 control-label">State</label>
					<div class="col-xs-4">
						<dsp:input id="state" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.state"
							maxlength="30" beanvalue="Profile.derivedShippingAddress.state">
							<dsp:tagAttribute name="placeholder" value="State" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="postalCode" class="col-xs-3 control-label">Postal
						Code</label>
					<div class="col-xs-4">
						<dsp:input id="postalCode" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.postalCode"
							maxlength="10" iclass="required"
							beanvalue="Profile.derivedShippingAddress.postalCode">
							<dsp:tagAttribute name="pattern" value="[0-9]{6}" />
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="placeholder" value="PostalCode" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="country" class="col-xs-3 control-label">Country</label>
					<div class="col-xs-4">
						<dsp:input id="country" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.country"
							maxlength="30" beanvalue="Profile.derivedShippingAddress.country">
							<dsp:tagAttribute name="placeholder" value="Country" />
						</dsp:input>
					</div>
				</div>

				<div class="form-group">
					<label for="mobile" class="col-xs-3 control-label">Mobile</label>
					<div class="col-xs-4">
						<dsp:input id="mobile" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.mobile"
							maxlength="15" beanvalue="Profile.derivedShippingAddress.mobile">
							<dsp:tagAttribute name="pattern" value="[+][9][1][-][0-9]{10}" />
							<dsp:tagAttribute name="required" value="true" />
							<dsp:tagAttribute name="title" value="+91-XXXXXXXXXX" />
							<dsp:tagAttribute name="placeholder" value="MobileNumber" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<label for="faxNumber" class="col-xs-3 control-label">Fax
						Number</label>
					<div class="col-xs-4">
						<dsp:input id="faxNumber" type="text"
							bean="CreateHardgoodShippingGroupFormHandler.hardgoodShippingGroup.shippingAddress.faxNumber"
							maxlength="15"
							beanvalue="Profile.derivedShippingAddress.faxNumber">
							<dsp:tagAttribute name="placeholder" value="FaxNumber" />
						</dsp:input>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-6">
						<div class="checkbox" align="right">
							<dsp:input
								bean="ShippingGroupFormHandler.specifyDefaultShippingGroupSuccessURL"
								type="hidden" value="../shipping.jsp" />
							<label> <dsp:input type="checkbox"
									bean="CreateHardgoodShippingGroupFormHandler.assignNewShippingGroupAsDefault"
									value="true" /><b>Default Address</b>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-6">
						<div class="checkbox" align="right">
							<label> <dsp:input type="checkbox"
									bean="CreateHardgoodShippingGroupFormHandler.addToProfile"
									value="true" /><b>Add to Profile</b>
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-xs-6">
						<div>
							<dsp:input
								bean="CreateHardgoodShippingGroupFormHandler.newHardgoodShippingGroupSuccessURL"
								type="hidden" value="../shipping.jsp" />
							<dsp:input
								bean="CreateHardgoodShippingGroupFormHandler.newHardgoodShippingGroupErrorURL"
								type="hidden" value="add_shipping_group.jsp" />
							<dsp:input
								bean="CreateHardgoodShippingGroupFormHandler.newHardgoodShippingGroup"
								priority="<%=(int) -10%>" type="submit" iclass="btn btn-primary"
								value="Add New Shipping Address" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</dsp:form>
</dsp:page>
<%-- @version $Id: //product/DCS/version/11.2/release/DCSSampleCatalog/j2ee-apps/sampleCatalog/web-app/hardgood_sg_b2c.jsp#1 $$Change: 946917 $--%>
