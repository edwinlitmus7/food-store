<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
	<dsp:getvalueof var="paymentGroups" vartype="java.lang.Collection" param="order.paymentGroups" />
	<dsp:getvalueof var="isCurrent" param="isCurrent" />
	<dsp:getvalueof var="expressCheckout" param="expressCheckout" />
	
	<%-- Default value for expressCheckout is false. --%>
	<c:if test="${empty expressCheckout}">
		<c:set var="expressCheckout" value="false" />
	</c:if>
	
	<%-- Then display all available payment groups (they may be Store Credits or Credit Card). --%>
	<c:forEach var="group" items="${paymentGroups}">
		<dsp:param name="paymentGroup" value="${group}" />
			<dsp:include page="/checkout/gadgets/paymentGroupRenderer.jsp">
				<dsp:param name="isCurrent" param="isCurrent" />
				<dsp:param name="paymentGroup" param="paymentGroup" />
				<dsp:param name="isExpressCheckout" value="${expressCheckout}" />
			</dsp:include>
	</c:forEach>
</dsp:page>