<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:form method="post" formid="specifyDefaultShippingGroup${element.key}" id="specifyDefaultShippingGroup">
<dsp:importbean 
		bean="/atg/commerce/order/purchase/ShippingGroupFormHandler"/>
		<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
		<dsp:getvalueof var="element" param="element" />
			<dsp:input bean="ShippingGroupFormHandler.specifyDefaultShippingGroupSuccessURL" 
			value="${contextPath }/checkout/shipping.jsp?error=false&workingShippingGroup=${element.key}" type="hidden" />
		<dsp:input bean="ShippingGroupFormHandler.specifyDefaultShippingGroupErrorURL" 
			value="${contextPath }/checkout/shipping.jsp?error=true&workingShippingGroup=${element.key}" type="hidden" />
			
		
		<dsp:input id="${element.key}_checkbox" bean="ShippingGroupFormHandler.defaultShippingGroupName"
		type="checkbox" value="${element.key}" /><b>Set as Default Address</b><br>
		<dsp:input id="${element.key}_submit" bean="ShippingGroupFormHandler.specifyDefaultShippingGroup" type="submit"
			value="Update" />
			<script>
			$(function(){
				$("#${element.key}_submit").hide();
				$("#${element.key}_checkbox").change(function() {
				    
				    	$("#${element.key}_submit").click();
				    
				});
			});
			</script>
	
	</dsp:form>
	