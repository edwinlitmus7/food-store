<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<dsp:page>

	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupDroplet" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CreateCreditCardFormHandler" />
	<dsp:importbean
		bean="atg/commerce/order/purchase/UpdateCreditCardFormHandler" />
	<!-- Getting Parameters from the page -->
	<dsp:getvalueof var="creditCardNickname" param="creditCardNickname" />
	<dsp:getvalueof var="formHandlerComponent" param="formHandlerComponent" />
	<dsp:getvalueof var="saveToProfile" param="saveToProfile" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<!-- Selecting credit card parameter according to the addEditMode -->
	<dsp:droplet name="Switch">
		<dsp:param name="value" param="addEditMode" />
		<dsp:oparam name="edit">
			<dsp:getvalueof var="creditCardParam" value="workingCreditCard" />
			<dsp:getvalueof var="pageTitle" value="Upadte Credit card details" />
			<dsp:setvalue bean="${formHandlerComponent}.${creditCardParam}"
				beanvalue="PaymentGroupDroplet.PaymentGroupMapContainer.PaymentGroupMap.${creditCardNickname}" />
		</dsp:oparam>
		<dsp:oparam name="add">
			<dsp:getvalueof var="creditCardParam" value="creditCard" />
			<dsp:getvalueof var="pageTitle" value="Add a credit card" />
		</dsp:oparam>
	</dsp:droplet>
	<div class="section">
	<div class="container">
	<div class="row">
	<%-- Iterate form errors --%>
	<dsp:droplet name="Switch">
		<dsp:param bean="${formHandlerComponent}.formError" name="value" />
		<dsp:oparam name="true">
			<div class="row">

				<div class="col-xs-7">
					<div class="alert alert-danger">
						<font color=fffcfc> <STRONG>
								<UL>
									<dsp:droplet name="ErrorMessageForEach">
										<dsp:param bean="${formHandlerComponent}.formExceptions"
											name="exceptions" />
										<dsp:oparam name="output">
											<LI><dsp:valueof param="message" />
										</dsp:oparam>
									</dsp:droplet>
								</UL>
						</STRONG>
						</font>
					</div>
				</div>

			</div>
		</dsp:oparam>
	</dsp:droplet>
</div>
	<!-- Form which contains credit card and billing address details -->
	<div class="row">

		<div class="row">
		<div class="col-md-12">
					<div class="section-title">
														<h2 class="title"> ${pageTitle}</h2>
													</div>
												</div>

			</div>

		<dsp:form method="post" iclass="form-horizontal">
			<fieldset>
				<div class="col-md-8">
					<div class="panel panel-default">
						<div class="panel-body">
							<legend> Credit Card Details </legend>
							<!-- Credit card nickname -->
							<div class="form-group">
								<label for="cc_nickname" class="control-label col-md-2">
									Nickname </label>
								<div class="col-md-3">
									<dsp:input type="text" id="cc_nickname"
										bean="${formHandlerComponent}.creditCardName"
										iclass="form-control" value="${creditCardNickname}">
										<dsp:tagAttribute name="required" value="true" />
									</dsp:input>
								</div>
								<!-- Name on credit card -->
								<label for="cc_card_name" class="control-label col-md-3">
									Name on card </label>
								<div class="col-md-3">
									<dsp:input type="text" id="cc_card_name"
										bean="${formHandlerComponent}.${creditCardParam}.nameOnCard"
										iclass="form-control">
										<dsp:tagAttribute name="required" value="true" />
									</dsp:input>
								</div>
							</div>
							<!-- Credit card number -->
							<div class="form-group">
								<label for="cc_number" class="control-label col-md-2">
									Card Number </label>
								<div class="col-md-4">
									<dsp:input type="text" id="cc_number"
										bean="${formHandlerComponent}.${creditCardParam}.creditCardNumber"
										iclass="form-control" maxlength="16">
										<dsp:tagAttribute name="required" value="true" />
									</dsp:input>
								</div>
								<!-- Credit card type : Master Card/Visa -->
								<label for="cc_type" class="control-label col-md-2">
									Card Type </label>
								<div class="col-md-2">
									<dsp:select id="cc_type"
										bean="${formHandlerComponent}.${creditCardParam}.creditCardType"
										iclass="form-control">
										<dsp:option value="MasterCard">Master Card</dsp:option>
										<dsp:option value="Visa">Visa</dsp:option>
									</dsp:select>
								</div>
							</div>
							<!-- The expiry month of the credit card -->
							<div class="form-group">
								<label for="cc_expiry_month" class="control-label col-md-2">
									Expiration Month </label>
								<div class="col-md-1">
									<dsp:select id="cc_expiry_month"
										bean="${formHandlerComponent}.${creditCardParam}.expirationMonth"
										iclass="form-control">
										<dsp:option value="1"> 1 </dsp:option>
										<dsp:option value="2"> 2 </dsp:option>
										<dsp:option value="3"> 3 </dsp:option>
										<dsp:option value="4"> 4 </dsp:option>
										<dsp:option value="5"> 5 </dsp:option>
										<dsp:option value="6"> 6 </dsp:option>
										<dsp:option value="7"> 7 </dsp:option>
										<dsp:option value="8"> 8 </dsp:option>
										<dsp:option value="9"> 9 </dsp:option>
										<dsp:option value="10"> 10 </dsp:option>
										<dsp:option value="11"> 11 </dsp:option>
										<dsp:option value="12"> 12 </dsp:option>
									</dsp:select>
								</div>
								<!-- The expiry year of the credit card -->
								<label for="cc_expiry_year" class="control-label col-md-2">
									Expiration Year </label>
								<div class="col-md-2">
									<dsp:input type="text" id="cc_expiry_year"
										bean="${formHandlerComponent}.${creditCardParam}.expirationYear"
										iclass="form-control">
										<dsp:tagAttribute name="required" value="true" />
										<dsp:tagAttribute name="minlength" value="4" />
										<dsp:tagAttribute name="maxlength" value="4" />
									</dsp:input>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Billing address details -->
				<div class="col-md-8">
					<div class="panel panel-default">
						<div class="panel-body">
							<legend> Billing Address Details </legend>
							<div class="form-group">
								<!-- Billing  address nick name -->
								<div class="form-group">
									<label for="address_nickname" class="control-label col-md-2">
										Nickname </label>
									<div class="col-md-4">
										<dsp:input type="text" id="address_nickname"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.nickname"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
									<!-- Billing address first name -->
									<label for="address_firstname" class="control-label col-md-2">
										Firstname </label>
									<div class="col-md-3">
										<dsp:input type="text" id="address_firstname"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.firstName"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
								</div>
								<!-- Billing address middle name -->
								<div class="form-group">
									<label for="address_middlename" class="control-label col-md-2">
										Middlename </label>
									<div class="col-md-3">
										<dsp:input type="text" id="address_middlename"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.middleName"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="false" />
										</dsp:input>
									</div>
									<!-- Billing address last name -->
									<label for="address_lastname" class="control-label col-md-2">
										Lastname </label>
									<div class="col-md-3">
										<dsp:input type="text" id="address_lastname"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.lastName"
											iclass="form-control" />
									</div>
								</div>
								<!-- Type of address -->
								<div class="form-group">
									<label for="address_typeofaddress"
										class="control-label col-md-2"> Type Of Address </label>
									<div class="col-md-2">
										<dsp:select id="cc_type"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.typeOfAddress"
											iclass="form-control">
											<dsp:option value="Home">Home</dsp:option>
											<dsp:option value="Office">Office</dsp:option>
										</dsp:select>
									</div>
									<!-- Address line 1  -->
									<div class="form-group">
										<label for="address_1" class="control-label col-md-2">
											Address line 1 </label>
										<div class="col-md-2">
											<dsp:input type="text" id="address_1"
												bean="${formHandlerComponent}.${creditCardParam}.billingAddress.address1"
												iclass="form-control">
												<dsp:tagAttribute name="required" value="true" />
											</dsp:input>
										</div>
									</div>
									<!-- Address line 2 -->
									<label for="address_2" class="control-label col-md-2">
										Address line 2 </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_2"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.address2"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
									<!-- Address line 3 -->
									<label for="address_3" class="control-label col-md-2">
										Address line 3 </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_3"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.address3"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
								</div>
								<!-- City -->
								<div class="form-group">
									<label for="address_city" class="control-label col-md-2">
										City </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_city"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.city"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
									<!-- State -->
									<label for="address_state" class="control-label col-md-2">
										State </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_state"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.state"
											iclass="form-control">
											<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*" />
											<dsp:tagAttribute name="title"
												value="First letter as Capital" />
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
									<!-- Country -->
									<label for="address_country" class="control-label col-md-2">
										Country </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_country"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.country"
											iclass="form-control">
											<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*" />
											<dsp:tagAttribute name="title"
												value="First letter as Capital" />
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
								</div>
								<!-- Postal Code -->
								<div class="form-group">
									<label for="address_pin" class="control-label col-md-2">
										Pin </label>
									<div class="col-md-2">
										<dsp:input type="number" id="address_pin"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.postalCode"
											iclass="form-control">
											<dsp:tagAttribute name="maxlength" value="6" />
											<dsp:tagAttribute name="minlength" value="6" />
											<dsp:tagAttribute name="required" value="true" />
										</dsp:input>
									</div>
								</div>
								<!-- Phone number -->
								<div class="form-group">
									<label for="address_phoneNumber" class="control-label col-md-2">
										phone Number </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_mobile"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.phoneNumber"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="true" />
											<dsp:tagAttribute name="pattern" value="[0-9]{4}[-][0-9]{7}" />
											<dsp:tagAttribute name="required" value="true" />
											<dsp:tagAttribute name="title" value="XXXX-XXXXXXX" />
											<dsp:tagAttribute name="placeholder" value="XXXX-XXXXXXX" />
										</dsp:input>
									</div>
									<!-- Mobile -->
									<label for="address_mobile" class="control-label col-md-2">
										Mobile </label>
									<div class="col-md-3">
										<dsp:input type="text" id="address_mobile"
											bean="${formHandlerComponent}.${creditCardParam}.billingAddress.mobile"
											iclass="form-control">
											<dsp:tagAttribute name="required" value="true" />
											<dsp:tagAttribute name="pattern" value="+91[-][0-9]{8}" />
											<dsp:tagAttribute name="required" value="true" />
											<dsp:tagAttribute name="title" value="+91-xxxxxxxx" />
											<dsp:tagAttribute name="placeholder" value="+91-xxxxxxxx" />
										</dsp:input>
									</div>
								</div>
								<!-- Set Addess as Default -->
								<div class="col-md-3">
									<div class="form-group ">
										<dsp:droplet name="Switch">
											<dsp:param name="value" param="addEditMode" />
											<dsp:oparam name="edit">
												<dsp:input bean="${formHandlerComponent}.assignAsDefault"
													type="checkbox" checked="true" />
											Set Card As Default<br />
											</dsp:oparam>
											<dsp:oparam name="default">
												<dsp:input
													bean="${formHandlerComponent}.assignNewCreditCardAsDefault"
													type="checkbox" checked="true" />
											Set Card As Default<br />
											</dsp:oparam>
										</dsp:droplet>
									</div>
								</div>
							</div>
						</div>
					</div>
					<dsp:droplet name="Switch">
						<dsp:param name="value" param="addEditMode" />
						<!--
							In the edit mode, already added credit card details can be updated. This will invoke handleUpdateCreditCard
							method of UpdateCreditCardFormHandler
						-->
						<dsp:oparam name="edit">
							<dsp:input
								bean="${formHandlerComponent}.updateCreditCardSuccessURL"
								value="${contextPath}/checkout/credit_card.jsp" type="hidden" />
							<dsp:input
								bean="${formHandlerComponent}.updateCreditCardErrorURL"
								value="${contextPath}/checkout/editPaymentGroup.jsp?success=false&addEditMode=edit&workingAction=%20editing%20card%20&creditCardNickname=${creditCardNickname}"
								type="hidden" />
							<dsp:input id="submit"
								iclass="btn btn-primary pull-right disable-submit"
								value="Update" type="submit"
								bean="${formHandlerComponent}.updateCreditCard" />
						</dsp:oparam>
						<!--
							In the add mode, new credit card can be added. And this will invoke handleNewCreditCard
							method of CreateCreditCardFormHandler.
						 -->
						<dsp:oparam name="default">
							<dsp:input bean="${formHandlerComponent}.newCreditCardSuccessURL"
								type="hidden" value="${contextPath}/checkout/credit_card.jsp" />
							<dsp:input bean="${formHandlerComponent}.newCreditCardErrorURL"
								type="hidden"
								value="${contextPath}/checkout/editPaymentGroup.jsp?success=false&workingAction=%20adding%20card%20&addEditMode=add" />
							<dsp:input id="submit"
								iclass="btn btn-primary pull-right disable-submit" value="Add"
								type="submit" name="addAction"
								bean="${formHandlerComponent}.newCreditCard" />
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</fieldset>
		</dsp:form>
	</div>
</div>
</div>
</dsp:page>
