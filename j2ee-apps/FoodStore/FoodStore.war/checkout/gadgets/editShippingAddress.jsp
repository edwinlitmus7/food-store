<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<dsp:page>
	<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Shipping Address</title>
	</head>
	<dsp:importbean bean="/atg/commerce/order/purchase/ShippingGroupFormHandler" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="shippingGroupName" param="shippingGroupName"/>
	<dsp:form>
		<dsp:input bean="ShippingGroupFormHandler.specifyDefaultShippingGroupSuccessURL" 
			value="${contextPath }/checkout/gadgets/success.jsp" type="hidden" />
		<dsp:input bean="ShippingGroupFormHandler.specifyDefaultShippingGroupErrorURL" 
			value="${contextPath }/checkout/gadgets/shippingAddresses.jsp" type="hidden" />
			
		
		<dsp:input bean="ShippingGroupFormHandler.defaultShippingGroupName"
			type="checkbox" value="${shippingGroupName}" /><b>Default Address</b><br>
		<dsp:input bean="ShippingGroupFormHandler.specifyDefaultShippingGroup" type="submit"
			value="Update" />
	</dsp:form>
</dsp:page>