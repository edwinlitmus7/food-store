<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
	<dsp:getvalueof var="contextroot"
		bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="isCurrent" param="isCurrent" />
	
	<div class="panel panel-default col-xs-12">
		<div class="panel-heading">
			<h4>
				Payment details
			</h4>
		</div>
		<div class="panel-body">
			<h5> Credit card details </h5>
			<div class="row">
				<div class="col-md-3">
					<dsp:valueof param="paymentGroup.creditCardNumber" />
				</div>
				<div class="col-md-4">
					<dsp:valueof param="paymentGroup.creditCardType" />
				</div>
				<div class="col-md-4">
					<dsp:getvalueof var="expirationMonth" param="paymentGroup.expirationMonth" />
					<dsp:getvalueof var="expirationYear" param="paymentGroup.expirationYear" />
					<c:if test="${empty expirationMonth}">
						<c:set var="expirationMonth" value="00"/>
					</c:if>
					<c:if test="${empty expirationYear}">
						<c:set var="expirationYear" value="0000" />
					</c:if>
					<fmt:formatNumber minIntegerDigits="2" value="${expirationMonth}" var="formattedMonthValue"/>
					${formattedMonthValue }/${expirationYear }
				</div>
			</div>
			<h5> Billing Address </h5>
			<%-- First, display billing address for the credit card specified. --%>
			<dsp:include page="/global/util/displayAddress.jsp">
				<dsp:param name="address" param="paymentGroup.billingAddress" />
			</dsp:include>
		</div>
	</div>

</dsp:page>
