<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/PaymentGroupDroplet" />
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-4">
				<ul class="list-unstyled">

					<li>Type:</li>
					<li>Nick Name:</li>
					<li>Name on card:</li>
					<li>Card Number:</li>
					<li>Expiration Date:</li>
				</ul>
			</div>

			<div class="col-xs-4">
				<ul class="list-unstyled">
					<li>&nbsp;<dsp:valueof param="element.value.creditCardType" />
					</li>

					<li>&nbsp;<dsp:valueof param="element.key" /></li>
					<li>&nbsp;<dsp:valueof param="element.value.nameOnCard"></dsp:valueof></li>
					<li>&nbsp;<dsp:droplet
							name="/com/foodstore/droplet/HideString">
							<dsp:param name="value" param="element.value.creditCardNumber" />
							<dsp:oparam name="output">
								<dsp:valueof param="element" />
							</dsp:oparam>
						</dsp:droplet></li>
					<li>&nbsp;<dsp:valueof param="element.value.expirationMonth" />/<dsp:valueof
							param="element.value.expirationYear" /></li>
				</ul>
			</div>
			<div class="col-xs-4">
				<ul class="list-unstyled">
					<li><dsp:valueof
							param="element.value.billingAddress.firstName" />&nbsp; <dsp:valueof
							param="element.value.billingAddress.middleName"></dsp:valueof>&nbsp;
						<dsp:valueof param="element.value.billingAddress.lastName" /></li>
					<li><dsp:valueof param="element.value.billingAddress.email"></dsp:valueof></li>
					<li><dsp:valueof
							param="element.value.billingAddress.phoneNumber"></dsp:valueof></li>
					<li><dsp:valueof param="element.value.billingAddress.address1"></dsp:valueof>,&nbsp;
						<dsp:valueof param="element.value.billingAddress.address2"></dsp:valueof>,</li>
					<li><dsp:valueof param="element.value.billingAddress.city"></dsp:valueof>,&nbsp;
						<dsp:valueof param="element.value.billingAddress.state"></dsp:valueof>,</li>
					<li><dsp:valueof param="element.value.billingAddress.country"></dsp:valueof></li>
					<li><dsp:valueof
							param="element.value.billingAddress.postalCode"></dsp:valueof></li>
					<li><dsp:valueof param="element.value.billingAddress.mobile" /></li>

				</ul>
			</div>
		</div>
	</div>
</dsp:page>