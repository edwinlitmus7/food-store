
<dsp:page>
	<dsp:importbean bean="/atg/store/profile/RequestBean" />

	<dsp:getvalueof var="defaultError" vartype="java.lang.String"
		value="false" scope="request" />
	<dsp:getvalueof var="formExceptions" vartype="java.lang.Object"
		param="formHandler.formExceptions" />

	<%-- Draw error messages pane only if we have something to display. --%>
	<c:if test="${not empty formExceptions}">
		<ul>
			<c:forEach var="formException" items="${formExceptions}">
				<li> - <b> <dsp:valueof param="formException.errorCode" /> </b> <dsp:valueof param="formException.errorMessage" /> - </li> 
			</c:forEach>
		</ul>
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.2/Storefront/j2ee/store.war/checkout/gadgets/checkoutErrorMessages.jsp#1 $$Change: 946917 $--%>
