<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<dsp:page>
	<dsp:setvalue param="order"
		beanvalue="/atg/commerce/ShoppingCart.current" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<%-- <dsp:importbean
		bean="/atg/commerce/order/purchase/ExpressCheckoutFormHandler" /> --%>
	<style>
.invoice_container {
	float: left;
	box-shadow: 0 0 3px #aaa;
	height: auto;
	color: #666;
}

.invoice_title {
	width: 100%;
	padding: 10px;
	float: left;
	background: black;
	color: #fff;
	font-size: 30px;
	text-align: center;
}

.invoice_header {
	width: 100%;
	padding: 0px 0px;
	border-bottom: 1px solid rgba(0, 0, 0, 0.2);
	float: left;
}

/* For material button */
@import url(https://fonts.googleapis.com/css?family=Roboto:500);

h1,p {
	font-family: 'Roboto', sans-serif;
	text-align: center;
}

button {
	font-family: 'Roboto', sans-serif;
	text-transform: uppercase;
}

button:focus {
	outline: none !important;
}

.btn-checkout {
	border-radius: 2px;
	border: 0;
	transition: .2s ease-out;
	color: #fff;
	margin: 6px;
	box-shadow: 0 2px 5px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0
		rgba(0, 0, 0, 0.12);
}

.btn-checkout:hover {
	color: #fff;
	box-shadow: 0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0
		rgba(0, 0, 0, 0.15);
}

.btn-checkout:active,.btn-checkout:focus,.btn-checkout.active {
	outline: 0;
	color: #fff;
}

/*Link*/
.btn-link {
	background-color: transparent;
	color: #000;
}

.btn-link:hover,.btn-link:focus {
	background-color: transparent;
	color: #000;
}
</style>
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	
	<dsp:getvalueof var="items" bean="ShoppingCart.current.commerceItems"></dsp:getvalueof>
	
	<c:if test="${fn:length(items) > 0 }">
		<div class="invoice_container"
			style="width: 400px;">
			<dsp:include page="/global/gadgets/errorMessage.jsp">
				<dsp:param name="formHandler" bean="CartModifierFormHandler" />
			</dsp:include>
			<div class="invoice_title">Order Summary</div>
	
			<div class="invoice_header">
	
	
				<div style="width: 100%; float: right; padding:">
					<dsp:getvalueof var="specialOrderCloseness" param="order.specialOrderCloseness" vartype="java.lang.Boolean">
						
						<c:if test ="${specialOrderCloseness eq true}">
							<span style="background-color:FFFF00">
								Shipping Charge is free if the order amount > $200
							</span>
						</c:if>
						</dsp:getvalueof>
					<span
						style="font-size: 14px; float: left; padding: 10px; text-align: right;">
						
						<small>Merchandise Subtotal : </small> <b> <dsp:getvalueof
								var="rawSubtotal" vartype="java.lang.Double"
								param="order.priceInfo.rawSubtotal" /> <dsp:include
								page="/global/gadgets/formattedPrice.jsp">
	
								<dsp:param name="price" value="${rawSubtotal}" />
							</dsp:include>
					</b>
					</span>
					<span style="font-size: 14px; float: left; padding: 10px; text-align: right;">
						
						<dsp:importbean bean="/com/foodstore/pricing/HotDeliveryUpdator"/>
						<small><b>Extra <dsp:valueof converter="currency" bean="HotDeliveryUpdator.hotDeliveryCharge"></dsp:valueof> for HotDelivery will be charged</b></small>
					</span>
				</div>
			</div>
	
			<div style="width: 100%; padding: 0px; float: left;">
	
				<div style="width: 100%; float: left; background: #efefef;">
					<span
						style="float: left; text-align: left; padding: 10px; width: 50%; color: #888; font-weight: 600;">
						Decription </span> <span
						style="font-weight: 600; float: left; padding: 10px; width: 40%; color: #888; text-align: right;">
						Amount </span>
				</div>
				<dsp:getvalueof var="shipping" vartype="java.lang.Double"
					param="order.priceInfo.shipping" />
				<%-- <c:if test="${shipping > 0}"> --%>
					<%-- <div style="width: 100%; float: left;">
						<span
							style="float: left; text-align: left; padding: 10px; width: 50%; color: #888;">
							Shipping and handling </span> <span
							style="font-weight: normal; float: left; padding: 10px; width: 40%; color: #888; text-align: right;">
							<dsp:include page="/global/gadgets/formattedPrice.jsp">
								<dsp:param name="price" value="${shipping}" />
							</dsp:include>
						</span>
					</div> --%>
				<%-- </c:if> --%>
				<dsp:getvalueof var="tax" vartype="java.lang.Double"
					param="order.priceInfo.tax" />
				<c:if test="${shipping > 0}">
					<div style="width: 100%; float: left;">
						<span
							style="float: left; text-align: left; padding: 10px; width: 50%; color: #888;">
							Taxes </span> <span
							style="font-weight: normal; float: left; padding: 10px; width: 40%; color: #888; text-align: right;">
							<dsp:include page="/global/gadgets/formattedPrice.jsp">
								<dsp:param name="price" value="${tax}" />
							</dsp:include>
						</span>
					</div>
				</c:if>
				<dsp:getvalueof var="discountAmount" vartype="java.lang.Double"
					param="order.priceInfo.discountAmount" />
				<c:if test="${discountAmount > 0}">
					<div style="width: 100%; float: left;">
						<span
							style="float: left; text-align: left; padding: 10px; width: 50%; color: #888;">
							Applied discounts </span> <span
							style="font-weight: normal; float: left; padding: 10px; width: 40%; color: #888; text-align: right;">
							<dsp:include page="/global/gadgets/formattedPrice.jsp">
	
								<dsp:param name="price" value="${-discountAmount}" />
							</dsp:include>
						</span>
	
					</div>
				</c:if>
				<div style="font-size: 30px; padding: 10px;">
					<small> Estimated total : </small>
					<dsp:getvalueof var="total" vartype="java.lang.Double"
						param="order.priceInfo.total" />
					<dsp:include page="/global/gadgets/formattedPrice.jsp">
	
						<dsp:param name="price" value="${total}" />
					</dsp:include>
					<br /> <span style="font-size: 10px; float: left; width: 100%;">
						(includes shipping and taxes calculated ) </span>
				</div>
				<%-- It's not an Order Details page, hence some buttons should be displayed. --%>
				<dsp:droplet name="Switch">
					<dsp:param name="value" param="isShoppingCart" />
					<dsp:oparam name="true">
						<div class="row">
						<div class="col-xs-6">
						<dsp:form method="get">
					
						<dsp:droplet name="ForEach">
							<dsp:param name="array" bean="ShoppingCart.current.CommerceItems" />
							<dsp:param name="elementName" value="commerceItem"/>
							<dsp:oparam name="output">
								<dsp:getvalueof var="commerceId" param="commerceItem.id" vartype="java.lang.String"></dsp:getvalueof>
								<dsp:getvalueof var="commerceQuantity" param="commerceItem.quantity" vartype="java.lang.String"></dsp:getvalueof>
								<input type="hidden" name="${commerceId}" value="${commerceQuantity}"/>
							</dsp:oparam>
						</dsp:droplet>
					
					<dsp:input type="hidden"
						bean="CartModifierFormHandler.moveToPurchaseInfoErrorURL"
						value="${originatingRequest.contextPath}/cart/orderFailed.jsp" />
					<dsp:input type="hidden"
						bean="CartModifierFormHandler.moveToPurchaseInfoSuccessURL"
						value="${originatingRequest.contextPath}/checkout/shipping.jsp" />
					<dsp:input id="atg_store_checkout" type="submit"
						iclass="btn main-btn"
						bean="CartModifierFormHandler.moveToPurchaseInfoByCommerceId" value="Checkout" >
							<dsp:tagAttribute name="style" value="margin:5px"/>
						</dsp:input>
				</dsp:form>
				 	</div>
					</div>
					<%-- 	<dsp:a href="${originatingRequest.contextPath}/checkout/shipping.jsp" iclass="btn btn-checkout btn-lg btn-link">Proceed to Checkout</dsp:a> --%>
				
							
					</dsp:oparam>
				</dsp:droplet>
		 <%-- <dsp:form method="get">
	
					<dsp:input type="hidden"
						bean="CartModifierFormHandler.moveToPurchaseInfoErrorURL"
						value="${originatingRequest.contextPath}/cart/orderFailed.jsp" />
					<dsp:input type="hidden"
						bean="CartModifierFormHandler.moveToPurchaseInfoSuccessURL"
						value="${originatingRequest.contextPath}/cart/orderSuccess.jsp" />
					<dsp:input id="atg_store_checkout" type="submit"
						iclass="btn btn-lg btn-link"
						bean="CartModifierFormHandler.moveToPurchaseInfoByCommerceId" value="Checkout" />
				</dsp:form> --%>
				
				<%-- <dsp:form method="POST">
					<dsp:input
						bean="ExpressCheckoutFormHandler.expressCheckoutSuccessURL"
						type="hidden"
						value="${originatingRequest.contextPath}/checkout/shipping.jsp" />
					<dsp:input
						bean="ExpressCheckoutFormHandler.moveToPurchaseInfoErrorURL"
						type="hidden"
						value="${originatingRequest.contextPath}/cart/orderFailed.jsp" />
					<dsp:input bean="ExpressCheckoutFormHandler.expressCheckout"
						type="submit" iclass="btn btn-lg btn-link" value="Checkout" />
				</dsp:form> --%>
	
				<c:if test="${empty submittedOrder}">
					<dsp:droplet name="Switch">
						<dsp:param name="value" param="isConfirmPage" />
						<dsp:oparam name="true">
							<%-- It's Confirmation page, display its buttons. --%>
							<dsp:include page="/checkout/gadgets/confirmControls.jsp">
								<dsp:param name="expressCheckout" param="expressCheckout" />
							</dsp:include>
						</dsp:oparam>
					</dsp:droplet>
				</c:if>
	
			</div>
			<%-- Coupon Code section --%>
			 <div style="padding:5px 0px 15px 0px;">
	      <div style="width: 100%; float: left; background: #efefef;    border-bottom: 1px solid rgba(0, 0, 0, 0.2);">
					<span
						style="float: left; text-align: left; padding: 10px; width: 50%; color: #888; font-weight: 600;">
						Apply Coupon </span> 
				</div>
<div class="col-sm-12" style="margin:5px">
 <dsp:form id="CouponFormhandler" method="post" iclass="form-inline">
<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler"/>
  <% /* Where to go to on success or failure */%>

  <dsp:input bean="CouponFormHandler.claimCouponSuccessURL"
      value="${originatingRequest.contextPath}/cart/cart.jsp" type="hidden"/>
  <dsp:input bean="CouponFormHandler.claimCouponErrorURL"
      value="${originatingRequest.contextPath}/global/gadgets/errorMessage.jsp" type="hidden"/>

   <div class="form-group">
    <label for="coupon_code">Coupon code:</label>
  	<dsp:input id="coupon_code" iclass="input" bean="CouponFormHandler.couponClaimCode" type="text">
  		<dsp:tagAttribute name="style" value="width:180px"/>
  	</dsp:input>
  </div>

  <dsp:input bean="CouponFormHandler.claimCoupon" type="submit" iclass="btn main-btn pull-right" value="Claim it"/>
</dsp:form>
      </div>
			
		</div>
		</div>
	</c:if>
	     
</dsp:page>