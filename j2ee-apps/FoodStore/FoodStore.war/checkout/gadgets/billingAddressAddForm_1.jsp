<%--
  This gadget renders an 'Edit address' form with all necessary input fields and buttons.

  Required parameters:
    successURL
      The user will be redirected to this URL after he successfully changed address.
    nickName
      Nickname of address to be edited.

  Optional parameters:
    selectedAddress
      Nickname of address to be edited.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<dsp:page>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CreateCreditCardFormHandler" />

	<%-- Iterate form errors --%>
	<dsp:droplet name="Switch">
		<dsp:param value="${param.error}" name="value" />
		<dsp:oparam name="true">
			<div class="alert alert-info">
				<center>
					<h3>[Error] - processing ${param.workingBillingGroup}</h3>
				</center>
				<br />
				<center>
					<dsp:include page="/global/gadgets/errorMessage.jsp">
						<dsp:param name="formHandler" bean="CreateCreditCardFormHandler" />
					</dsp:include>
				</center>
			</div>
		</dsp:oparam>
		<dsp:oparam name="false">
			<div class="alert alert-success">
				<center>
					<h3>[Success] - processing ${param.workingBillingGroup}</h3>
				</center>
			</div>
		</dsp:oparam>
	</dsp:droplet>


	<dsp:importbean bean="/atg/userprofiling/Profile" />

	<hr>
	<p>
		Enter new CreditCard information

		<dsp:form method="post">

			<br>CreditCard NickName:<dsp:input
				bean="CreateCreditCardFormHandler.creditCardName" size="30"
				type="text" value="" />

			<br>CreditCardNumber:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.CreditCardNumber"
				maxsize="20" size="20" type="text" value="4111111111111111" />

			<br>CreditCardType:
<dsp:select bean="CreateCreditCardFormHandler.creditCard.creditCardType"
				required="<%=true%>">
				<dsp:option value="Visa" />Visa
<dsp:option value="MasterCard" />Master Card
<dsp:option value="American Express" />American Express
</dsp:select>

			<br>ExpirationMonth: <dsp:select
				bean="CreateCreditCardFormHandler.creditCard.ExpirationMonth">
				<dsp:option value="1" />January
<dsp:option value="2" />February
<dsp:option value="3" />March
<dsp:option value="4" />April
<dsp:option value="5" />May
<dsp:option value="6" />June
<dsp:option value="7" />July
<dsp:option value="8" />August
<dsp:option value="9" />September
<dsp:option value="10" />October
<dsp:option value="11" />November
<dsp:option value="12" />December
</dsp:select>

			<br>expirationYear:Year: <dsp:select
				bean="CreateCreditCardFormHandler.creditCard.expirationYear">
				<dsp:option value="2002" />2002
<dsp:option value="2003" />2003
<dsp:option value="2004" />2004
<dsp:option value="2005" />2005
<dsp:option value="2006" />2006
<dsp:option value="2007" />2007
<dsp:option value="2008" />2008
<dsp:option value="2009" />2009
<dsp:option value="2010" />2010
</dsp:select>

			<br>FirstName:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.firstName"
				beanvalue="Profile.firstName" size="30" type="text" />
			<br>MiddleName:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.middleName"
				beanvalue="Profile.middleName" size="30" type="text" />
			<br>LastName:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.lastName"
				beanvalue="Profile.lastName" size="30" type="text" />
			<br>Nick name:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.nickname"
				beanvalue="Profile.firstName" size="30" type="text" />
			<br>Type of Address:&nbsp;
			<dsp:select
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.typeOfAddress"
				id="typeOfAddress" iclass="form-control">
				<dsp:option value="Home">Home</dsp:option>
				<dsp:option value="Office">Office</dsp:option>
			</dsp:select>
			<br>Mobile number:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.mobile"
				beanvalue="Profile.firstName" size="30" type="text" />
			<br>EmailAddress:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.email"
				beanvalue="Profile.email" size="30" type="text" />
			<br>PhoneNumber:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.phoneNumber"
				beanvalue="Profile.daytimeTelephoneNumber" size="30" type="text" />
			<br>Address:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.address1"
				beanvalue="Profile.defaultCreditCard.address1" size="30" type="text" />
			<br>Address (line 2):<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.address2"
				beanvalue="Profile.defaultCreditCard.address2" size="30" type="text" />
			<br>City:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.city"
				beanvalue="Profile.defaultCreditCard.city" size="30" type="text" />
			<br>State:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.state"
				beanvalue="Profile.defaultCreditCard.state" size="30" type="text" />
			<br>PostalCode:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.postalCode"
				beanvalue="Profile.defaultCreditCard.postalCode" size="30"
				type="text" />
			<br>Country:<dsp:input
				bean="CreateCreditCardFormHandler.creditCard.billingAddress.country"
				beanvalue="Profile.defaultCreditCard.country" size="30" type="text" />
			<br/>
			<dsp:input bean="CreateCreditCardFormHandler.copyToProfile"
				type="checkbox" checked="true" /> Copy to profile.<br/>
				<dsp:input bean="CreateCreditCardFormHandler.assignNewCreditCardAsDefault"
				type="checkbox" checked="true" /> Assign New Credit Card As Default<br/>
			<dsp:input bean="CreateCreditCardFormHandler.newCreditCardSuccessURL"
				type="hidden" value="${contextPath}/checkout/gadgets/billingAddressAddForm_1.jsp?success=true&workingBillingGroup" />
			<dsp:input bean="CreateCreditCardFormHandler.newCreditCardErrorURL"
				type="hidden" value="${contextPath}/checkout/gadgets/billingAddressAddForm_1.jsp?error=true&workingBillingGroup" />
			<dsp:input bean="CreateCreditCardFormHandler.newCreditCard"
				type="submit" value="Enter Credit Card"/>
		</dsp:form>
		
</dsp:page>
<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.2/Storefront/j2ee/store.war/checkout/gadgets/shippingAddressEdit.jsp#1 $$Change: 946917 $--%>
