<%--
  This gadget renders an 'Edit Shipping address' form with all necessary input fields and buttons.

  Required parameters:
    successURL
      The user will be redirected to this URL after he successfully changed address.
    element
      The object of shippingGroup to be edited.
     defaultAddress
     	shipping group name of default address

--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<dsp:page>
<dsp:importbean bean="/atg/userprofiling/Profile" />
			<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean
		bean="/atg/commerce/order/purchase/UpdateHardgoodShippingGroupFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="element" param="element" />
	<dsp:getvalueof var="defaultAddress" param="defaultAddress" />
	<div id="editShippingAddressFormPart_${element.key}"
		style="display: none">
		<h3>Edit this address</h3>

		<dsp:form method="post" formid="editShippingAddressForm"
			id="editShippingAddressForm">
			<br>ShippingGroup NickName:
		 
		 &nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.hardgoodShippingGroupName"
				type="text" value="${element.key}">

				<dsp:tagAttribute name="required" value="true" />
				<dsp:tagAttribute name="readonly" value="true" />

			</dsp:input>
			<br>User's NickName: 
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.nickname"
				type="text" value="${element.value.shippingAddress.nickname}">
				<dsp:tagAttribute name="required" value="true" />
			</dsp:input>
			<br>First:
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.firstName"
				type="text" value="${element.value.shippingAddress.firstName}">
				<dsp:tagAttribute name="required" value="true" />
			</dsp:input>
			<br>
		Middle:
		&nbsp;<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.middleName"
				type="text" value="${element.value.shippingAddress.middleName}">
			</dsp:input>
			<br>Last:
		&nbsp;<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.lastName"
				type="text" value="${element.value.shippingAddress.lastName}" />
			<br>Address:
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.address1"
				value="${element.value.shippingAddress.address1}" type="text">
				<dsp:tagAttribute name="required" value="true" />
			</dsp:input>
			<br>Address (line 2):
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.address2"
				value="${element.value.shippingAddress.address2}" type="text" />
			<br>City:
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.city"
				value="${element.value.shippingAddress.city}" type="text"
				required="<%=true%>">
				<dsp:tagAttribute name="required" value="true" />
			</dsp:input>
			<br>State:
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.state"
				maxsize="2" value="${element.value.shippingAddress.state}" size="2"
				type="text" required="<%=true%>">
				<dsp:tagAttribute name="required" value="true" />
			</dsp:input>
			<br>Postal Code:
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.postalCode"
				value="${element.value.shippingAddress.postalCode}" size="10"
				type="text" required="<%=true%>">
				<dsp:tagAttribute name="required" value="true" />
			</dsp:input>
			<br>Country:
		&nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.country"
				value="${element.value.shippingAddress.country}" size="10"
				type="text">
				<dsp:tagAttribute name="required" value="true" />
			</dsp:input>
			<br>Type of Address:
		&nbsp;
		<dsp:select
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.typeOfAddress"
				id="typeOfAddress" iclass="form-control">
				<dsp:option value="Home">Home</dsp:option>
				<dsp:option value="Office">Office</dsp:option>
			</dsp:select>
			<br>Mobile : &nbsp;
		<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.workingHardgoodShippingGroup.shippingAddress.mobile"
				size="10" type="text" required="<%=true%>"
				value="${element.value.shippingAddress.mobile}">
				<dsp:tagAttribute name="pattern" value="[+][9][1][-][0-9]{10}" />
				<dsp:tagAttribute name="required" value="true" />
				<dsp:tagAttribute name="title" value="+91-XXXXXXXXXX" />

			</dsp:input>
			<br>
			<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.updateHardgoodShippingGroupSuccessURL"
				type="hidden"
				value="${contextPath }/checkout/shipping.jsp?error=false&workingShippingGroup=${element.key}" />
			<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.updateHardgoodShippingGroupErrorURL"
				type="hidden"
				value="${contextPath }/checkout/shipping.jsp?error=true&workingShippingGroup=${element.key}" />
			<br />


			<c:if test="${defaultAddress eq false}">
				<dsp:input
					bean="UpdateHardgoodShippingGroupFormHandler.setDefaultShippingGroup"
					type="checkbox" checked="true" />&nbsp; Set As Default
		</c:if>
			<br />
			<br />
			<%-- See if the address is transient by searching the users secondary address --%>
			<%-- el variable transientAddress is used as a flag --%>
			<dsp:getvalueof var="transientAddress" value="true" scope="page" />
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="Profile.secondaryAddresses" />
				<dsp:param name="elementName" value="contactInfo" />
				<dsp:oparam name="output">
					<dsp:droplet name="/atg/dynamo/droplet/Compare">
						<dsp:param name="obj1" param="contactInfo.id" />
						<dsp:param name="obj2" param="element.value.shippingAddress.id" />
						<dsp:oparam name="equal">
							<%-- el variable transientAddress is set false ie., address exists in users secondary address --%>
							<dsp:getvalueof var="transientAddress" value="false" scope="page" />
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
			<%-- Below check if the flag is set to show the add to user address or update user address button --%>
			<dsp:droplet name="Switch">
				<dsp:param name="value" value="${transientAddress}" />
				<dsp:oparam name="true">
					<%-- address is not in address list --%>
					<dsp:input
						bean="UpdateHardgoodShippingGroupFormHandler.addToProfile"
						type="checkbox" checked="false" />&nbsp;Add to Profile
						&nbsp;&nbsp;&nbsp;
					<dsp:input
						bean="UpdateHardgoodShippingGroupFormHandler.updateProfile"
						type="hidden" value="false" />
      			</dsp:oparam>
				
			</dsp:droplet>
			<br />
			<dsp:input
				bean="UpdateHardgoodShippingGroupFormHandler.updateHardgoodShippingGroup"
				priority="<%=(int) -10%>" type="submit"
				value="Update this shipping group" />
		</dsp:form>
	</div>

</dsp:page>
<%-- @version $Id: //product/DCS/version/11.2/release/DCSSampleCatalog/j2ee-apps/sampleCatalog/web-app/hardgood_sg_b2c.jsp#1 $$Change: 946917 $--%>
