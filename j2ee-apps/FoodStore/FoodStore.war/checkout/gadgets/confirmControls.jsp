<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CommitOrderFormHandler" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CancelOrderFormHandler" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />

	<dsp:getvalueof bean="/OriginatingRequest.requestURI" var="errorUrl"/>
	<%-- If it's an express checkout case, add expressCheckout parameter to error URL. --%>
    <c:if test="${expressCheckout}">
      <c:set var="errorUrl" value="${errorUrl}?expressCheckout=true"/>
    </c:if>

	<dsp:form>
		<%-- Success/Error URLs. --%>
		<dsp:input bean="CommitOrderFormHandler.commitOrderSuccessURL"
			type="hidden" value="${originatingRequest.contextPath }/checkout/confirmResponse.jsp" />
		<dsp:input bean="CommitOrderFormHandler.commitOrderErrorURL"
			type="hidden" value="${errorUrl}" />


		
	
		<input
			class="btn main-btn "
			title="${cancelLinkTitle}"
			style="margin:10px;cursor:pointer"
			onclick="location.replace('${originatingRequest.contextPath }/cart/orderNotPlaced.jsp');"
			value="Cancel Order"
			>
		<dsp:input iclass="btn primary-btn main-btn" value="Submit Order" type="submit" bean="CommitOrderFormHandler.commitOrder" >
				<dsp:tagAttribute name="style" value="margin:5px" />
		</dsp:input>

		</input>
	</dsp:form>
</dsp:page>
