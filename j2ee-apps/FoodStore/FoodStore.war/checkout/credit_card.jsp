<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
		<dsp:include page="/includes/header_v2.jsp"/>
		<dsp:include page="/includes/navbar_v2.jsp"/>

		<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
		<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
		<dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst" />
		<dsp:importbean
			bean="/atg/commerce/order/purchase/PaymentGroupDroplet" />
		<dsp:importbean
			bean="atg/commerce/order/purchase/UpdateCreditCardFormHandler" />
		<dsp:importbean
			bean="/atg/commerce/order/purchase/PaymentGroupFormHandler" />
		<dsp:importbean
			bean="/atg/commerce/order/purchase/CreateCreditCardFormHandler" />
		<dsp:importbean
			bean="com/foodstore/order/purchase/FoodStoreRemoveCreditCardFormHandler" />
		<dsp:setvalue param="removeCreditCardSuccessURL"
			beanvalue="/OriginatingRequest.requestURI" />
		<div class="section">
			<div class="container">
		<dsp:droplet name="Switch">
			<dsp:param bean="PaymentGroupFormHandler.formError" name="value" />
			<dsp:oparam name="true">
				<div class="alert alert-danger">
				<font color=cc0000><STRONG><UL>
							<dsp:droplet name="ErrorMessageForEach">
								<dsp:param bean="PaymentGroupFormHandler.formExceptions"
									name="exceptions" />
								<dsp:oparam name="output">
									<LI><dsp:valueof param="message" />
								</dsp:oparam>
							</dsp:droplet>
						</UL></STRONG></font>
					</div>
			</dsp:oparam>
		</dsp:droplet>
		<div class="row">
										<!-- section title -->
											<div class="col-md-12">
												<div class="section-title">
													<h2 class="title"> Payments </h2>
												</div>
											</div>
											<!-- section title -->
			<div class="col-xs-6">
				<h3>
					<dsp:a title="New Credit Card" page="editPaymentGroup.jsp"
						iclass="fa fa-plus" style="right: 65px;">
						Add credit card
						<dsp:param name="addEditMode" value="add" />
					</dsp:a>
				</h3>
			</div>
		</div>
		<div class="CreditCardContainer">
			<dsp:form method="post">
				<div id="CardListDisplay">
					<dsp:droplet name="PaymentGroupDroplet">
						<dsp:param name="clearPaymentGroups" value="false" />
						<dsp:param name="clearPaymentInfos" value="true" />
						<dsp:param name="initBasedonOrder" value="true" />
						<dsp:param name="initPaymentGroups" value="true" />
						<dsp:param name="paymentGroupTypes" value="creditCard" />
						<dsp:param name="initOrderPayment" value="true" />
						<dsp:oparam name="output">
							<dsp:droplet name="MapToArrayDefaultFirst">
								<dsp:param name="defaultKey"
									bean="PaymentGroupDroplet.PaymentGroupMapContainer.defaultPaymentGroupName" />
								<dsp:param name="map" param="paymentGroups" />
								<dsp:param name="sortByKeys" value="true" />
								<dsp:oparam name="empty">
									<div align="center">
										<h4>There are no credit cards linked to this account</h4>
									</div>
								</dsp:oparam>
								<dsp:oparam name="output">
									<dsp:getvalueof var="sortedArray" vartype="java.lang.Object"
										param="sortedArray" />
									<dsp:droplet name="ForEach">
										<dsp:param name="array" param="sortedArray" />
										
										<dsp:oparam name="output">
									
								
											<dsp:setvalue bean="PaymentGroupFormHandler.listId"
												paramvalue="order.id" />
											<dsp:droplet name="Switch">
												<dsp:param name="value" param="count" />
												<dsp:oparam name="1">
													<dsp:getvalueof var="radio" value="true" />
													<dsp:getvalueof var="panel" value="panel panel-danger" />
												</dsp:oparam>
												<dsp:oparam name="default">
													<dsp:getvalueof var="radio" value="false" />
													<dsp:getvalueof var="panel" value="panel panel-default" />
												</dsp:oparam>
											</dsp:droplet>

											<div class="container-fluid">
												<div class="row">
													<div class="col-xs-6">
														<div class="${panel }">
															<div class="panel-heading">
																<div align="left">
																	<b><dsp:valueof
																			param="element.value.billingAddress.typeOfAddress" /></b>
																</div>
																<div align="right">
																	<dsp:input bean="PaymentGroupFormHandler.listId"
																		beanvalue="PaymentGroupFormHandler.listId"
																		type="hidden" />
																	<dsp:input
																		bean="PaymentGroupFormHandler.currentList[0].paymentMethod"
																		paramvalue="element.key" type="radio"
																		checked="${radio}" />
																	<b><dsp:valueof
																			param="element.value.creditCardType" /></b>
																</div>
															</div>
															<div class="panel-body" align="left">
																<dsp:include page="gadgets/display_payment_group.jsp" />
															</div>
															<div class="panel-footer" align="right">
																<!-- 'Edit' link -->
																<h8> <dsp:a title="Edit Credit Card"
																	page="editPaymentGroup.jsp"
																	iclass="fa fa-edit">
																	<dsp:param name="creditCardNickname"
																		param="element.key" />
																	<dsp:param name="addEditMode" value="edit" />
																</dsp:a></h8>

																<!-- 'Remove' link -->
																<h8> <dsp:a title="Remove Credit Card"
																	bean="FoodStoreRemoveCreditCardFormHandler.removeCreditCard"
																	page="/checkout/credit_card.jsp"
																	iclass="fa fa-trash" value="">
																	<dsp:param name="removeCreditCardNickname"
																		param="element.key" />
																	<dsp:param name="removeCreditCardSuccessURL"
																		param="removeCreditCardSuccessURL" />
																</dsp:a></h8>
															</div>
														</div>
													</div>
												</div>
											</div>
										</dsp:oparam>
									</dsp:droplet>
									<div class="container">
										<div class="col-xs-6" align="right">
											<dsp:input
												bean="PaymentGroupFormHandler.applyPaymentGroupsSuccessURL"
												type="hidden" value="confirm.jsp" />
											<dsp:input type="submit" value="Continue"
												iclass="primary-btn"
												bean="PaymentGroupFormHandler.applyPaymentGroups">
											</dsp:input>
										</div>
									</div>
								</dsp:oparam>
							</dsp:droplet>
						</dsp:oparam>
					</dsp:droplet>
				</div>
				<!-- End of CardListDisplay -->

			</dsp:form>
		</div>
</div>
	</div>
		<dsp:include page="/includes/footer_v2.jsp" />

</dsp:page>
