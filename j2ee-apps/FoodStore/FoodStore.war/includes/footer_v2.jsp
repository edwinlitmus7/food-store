<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>


<dsp:page>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<!-- FOOTER -->
	<footer id="footer" class="section section-grey">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- footer widget -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<!-- footer logo -->
						<div class="footer-logo">
							<a class="logo" href="${contextPath }">
		            <font size="20" style="color:#F8694A"><b>Food</b></font><font size="20" style="color:black"><b>
							Store</b></font>
		          </a>
						</div>
						<!-- /footer logo -->

						<p>
						Looking for food..?<br/>
						FoodStore is the best choice for a healthy & tasty food.
						Just find your taste and let us do the rest for you.
						</p>

						<!-- footer social -->
						<ul class="footer-social">
							<li><a href="#"><i class="fa fa-facebook"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest"></i></a></li>
						</ul>
						<!-- /footer social -->
					</div>
				</div>
				<!-- /footer widget -->

				<!-- footer widget -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">My Account</h3>
						<ul class="list-links">
								<li><a href="${contextPath}/myaccount/userDashBoard.jsp"> My Account </a></li>
								<li><a href="${contextPath}/myaccount/socialRegistration.jsp"> Update profile</a></li>
								<li><a href="${contextPath}/myaccount/addressBook.jsp"> Address Book</a></li>
								<li><a href="${contextPath}/myaccount/creditcards.jsp"> Saved Cards</a></li>
								<li><a href="${contextPath}/myaccount/myOrders.jsp"> My Orders</a></li>
						</ul>
					</div>
				</div>
				<!-- /footer widget -->

				<div class="clearfix visible-sm visible-xs"></div>

				<!-- footer widget -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">Customer Service</h3>
						<ul class="list-links">
							<li><a href="#">About Us</a></li>
							<li><a href="#">Shiping & Return</a></li>
							<li><a href="#">Shiping Guide</a></li>
							<li><a href="#">FAQ</a></li>
						</ul>
					</div>
				</div>
				<!-- /footer widget -->

				<!-- footer subscribe -->
				<div class="col-md-3 col-sm-6 col-xs-6">
					<div class="footer">
						<h3 class="footer-header">Stay Connected</h3>
						<p> Receive updates on newly added items.</p>
						<form id="news_letter_form">
							<div class="form-group">
								<input class="input" placeholder="Enter Email Address">
							</div>
							<button class="primary-btn">Join Newslatter</button>
						</form>
					</div>
				</div>
				<!-- /footer subscribe -->
			</div>
			<!-- /row -->
			<hr>
			<!-- row -->
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<!-- footer copyright -->
					<div class="footer-copyright">
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
						Copyright &copy;<script>document.write(new Date().getFullYear());  </script> FoodStore All rights reserved | Made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <b> CSC Team</b>
						<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					</div>
					<!-- /footer copyright -->
				</div>
			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</footer>
	<!-- /FOOTER -->

	<!-- jQuery Plugins -->
	
	<script src="${contextPath}/js/bootstrap.min.js"></script>
	<script
  src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
  integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
  crossorigin="anonymous"></script>
	<script src="${contextPath}/js/slick.min.js"></script>
	<script src="${contextPath}/js/nouislider.min.js"></script>
	<script src="${contextPath}/js/jquery.zoom.min.js"></script>
	<script src="${contextPath}/notification_lib/noty.js" type="text/javascript"></script>
	<script src="${contextPath}/js/main.js"></script>
	<script src="${contextPath}/js/autocomplete.js"></script>
	<script src="${contextPath}/js/script.js"></script>
	
	<script src="${contextPath}/js/quickview.js"></script>
	<!-- Slider -->
	<script src="${contextPath}/slider/js/bootstrap-slider.js"></script>
	<!-- Modal -->
	<script src="${contextPath}/js/modal.js"></script>
	
	

</body>

</html>
</dsp:page>
