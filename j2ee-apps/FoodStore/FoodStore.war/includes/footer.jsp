
<footer>
	<div>
		<hr>
		<div class="row" align="center">
			<div class="col-xs-4">
				<h5>Get to know Us</h5>
				<p>About Us</p>
				<p>Careers</p>
				<p>Press Releases</p>
				<p>Gift a Smile</p>
			</div>
			<div class="col-xs-4">
				<h5>Connect with Us</h5>
				<p>Facebook</p>
				<p>Twitter</p>
				<p>Instagram</p>
			</div>
			<div class="col-xs-4">
				<h5>Let Us Help You</h5>
				<p>Your Account</p>
				<p>100% Purchase Protection</p>
				<P>FoodStore Assistant</P>
				<p>Help</p>
			</div>
		</div>
		<hr>
		<div class="row" align="center">
			<div class="col-xs-12">
				<h4>FoodStore</h4>
			</div>
		</div>
		<div class="row" align="center">
			<div class="col-xs-2">Kerala</div>
			<div class="col-xs-2">Tamilnadu</div>
			<div class="col-xs-2">Karnataka</div>
			<div class="col-xs-2">Mumbai</div>
			<div class="col-xs-2">Goa</div>
			<div class="col-xs-2">Hyderabad</div>
		</div>
		<br>
		<div class="bg-primary">
			<div class="row" align="center">
				<div class="col-xs-12">
					<h6>Copyright FoodStore.com</h6>
				</div>
			</div>
		</div>
	</div>
</footer>
<!--  Scripts-->
<script src="${contextPath}/js/script.js"></script>
