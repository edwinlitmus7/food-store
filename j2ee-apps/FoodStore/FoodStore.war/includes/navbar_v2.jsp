<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/FoodStore" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="items" vartype="java.lang.Object" bean="ShoppingCart.current.commerceItems" />

	<!-- HEADER -->
	<header>
		<!-- top Header -->
		<div id="top-header">
			<div class="container">
				<div class="pull-left">
					<span>Welcome to FoodStore !</span>
				</div>
				<div class="pull-right">
					<ul class="header-top-links">
						<li><a href="${contextPath }">Home</a></li>
						<li><a href="#">Newsletter</a></li>
						<li><a href="#">FAQ</a></li>
						<li class="dropdown default-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">ENG <i class="fa fa-caret-down"></i></a>
							<ul class="custom-menu">
								<li><a href="#">English (ENG)</a></li>
								<li><a href="#">Russian (Ru)</a></li>
								<li><a href="#">French (FR)</a></li>
								<li><a href="#">Spanish (Es)</a></li>
							</ul>
						</li>
						<li class="dropdown default-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">USD <i class="fa fa-caret-down"></i></a>
							<ul class="custom-menu">
								<li><a href="#">USD ($)</a></li>
								<li><a href="#">EUR (€)</a></li>
							</ul>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- /top Header -->

		<!-- header -->
		<div id="header">
			<div class="container">
				<div class="pull-left">
					<!-- Logo -->
					<div class="header-logo">
						<a class="logo" href="${contextPath }">
							<font size="20" style="color:#F8694A"><b>Food</b></font><font size="20" style="color:black"><b>
							Store</b></font>
						</a>
					</div>
					<!-- /Logo -->

					<!-- Search -->
					<div class="header-search">

					<dsp:form action="index.jsp" method="post">
					<dsp:input bean="FoodStore.autoComplete" type="hidden"
						name="keywords" id="keywords" />
					</dsp:form>
					<dsp:form
					action="${contextPath }/index.jsp" id="search_form" method="post">

							<dsp:input type="hidden" name="successURL"
								value="${contextPath }/index.jsp"
								bean="FoodStoreSearchFormHandler.successURL" />
							<dsp:input type="hidden" name="errorURL"
								value="${contextPath }/global/gadgets/errorMessage.jsp"
								bean="FoodStoreSearchFormHandler.errorURL" />
							<dsp:input  type="text" id="search-box" iclass="input search-input"
								bean="FoodStoreSearchFormHandler.repositoryKey">
								<dsp:tagAttribute name="placeholder" value="Enter your keyword" />
								<dsp:tagAttribute name="id" value="search-box"/>
							</dsp:input>

							<dsp:input bean="FoodStoreSearchFormHandler.productSearch"
								id="search-submit-button" type="submit" >
									<dsp:tagAttribute name="style" value="display:none"/>
							</dsp:input>

							<button class="search-btn" onclick="document.getElementById('search-submit-button').click();return false;">
							<i class="fa fa-search"></i>
							</button>
				</dsp:form>

					</div>
					<!-- /Search -->
				</div>
				<div class="pull-right">
					<ul class="header-btns">
						<!-- Account -->

						<li class="header-account dropdown default-dropdown">
							<div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
								<div class="header-btns-icon">
									<i class="fa fa-user-o"></i>
								</div>
								<strong class="text-uppercase" id="myaccount_nav_bar_link">My Account <i class="fa fa-caret-down"></i></strong>
							</div>
							<dsp:droplet name="Switch">
							<dsp:param bean="Profile.transient" name="value" />
							<dsp:oparam name="true">
								<script>
								var openMiniLoginForm = function(){

									document.getElementById('myaccount_nav_bar_link').click();

								}
								</script>
								<a class="dropdown-toggle" role="button" data-toggle="dropdown" class="text-uppercase">Login / Join</a>
								<dsp:droplet name="Switch">
									<dsp:param name="value"  bean="/OriginatingRequest.requestURI"/>
									<dsp:oparam name="/foodstore/myaccount/socialLogin.jsp">
									</dsp:oparam>
									<dsp:oparam name="default">
										<ul class="custom-menu" style="width: 500px;">
										<li>
											<dsp:include page="/myaccount/gadgets/navbar_login_fragment.jsp">
											</dsp:include>
										</li>
										</ul>
									</dsp:oparam>
								</dsp:droplet>


							</dsp:oparam>
							<dsp:oparam name="false">
							<ul class="custom-menu">
								<li><a href="${contextPath}/myaccount/userDashBoard.jsp"><i class="fa fa-user-o"></i> My Account </a></li>
								<li><a href="${contextPath}/myaccount/socialRegistration.jsp"><i class="fa fa-edit"></i> Update profile</a></li>
								<li><a href="${contextPath}/myaccount/addressBook.jsp"><i class="fa fa-address-book"></i> Address Book</a></li>
								<li><a href="${contextPath}/myaccount/creditcards.jsp"><i class="fa fa-credit-card"></i> Saved Cards</a></li>
								<li><a href="${contextPath}/myaccount/myOrders.jsp"><i class="fa fa-shopping-cart "></i> My Orders</a></li>
								<li><a href="#" onclick="document.getElementById('logout_form_button').click();return false;"><i class="fa fa-sign-out "></i> Logout</a></li>
							</ul>
										<dsp:form method="post" id="logout_form" formid="logout_form">
												<dsp:input bean="ProfileFormHandler.logoutErrorURL" type="hidden" value="${contextPath}/userDashBoard.jsp" />
												<dsp:input bean="ProfileFormHandler.logoutSuccessURL" type="hidden" value="${contextPath}/index.jsp" />
												<dsp:input bean="ProfileFormHandler.logout" type="submit" value="Logout" >
													<dsp:tagAttribute name="style" value="display:none"/>
													<dsp:tagAttribute name="id" value="logout_form_button"/>
												</dsp:input>
											</dsp:form>

							</dsp:oparam>
							</dsp:droplet>
						</li>
						<!-- /Account -->

						<!-- Cart -->
						<li class="header-cart dropdown default-dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
								<div class="header-btns-icon">
									<i class="fa fa-shopping-cart"></i>
									<span class="qty">${fn:length(items)}</span>
								</div>
								<strong class="text-uppercase">My Cart:</strong>
								<br>
								<!-- <span>35.20$</span> -->
							</a>
							<dsp:droplet name="Switch">
							<dsp:param name="value" bean="/OriginatingRequest.requestURI" />
							<dsp:oparam name="/foodstore/cart/cart.jsp">
							</dsp:oparam>

							<dsp:oparam name="default">
								<dsp:include page="/cart/gadgets/minicart_v2.jsp"/>
							</dsp:oparam>
							</dsp:droplet>


						</li>
						<!-- /Cart -->

						<!-- Mobile nav toggle-->
						<li class="nav-toggle">
							<button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
						</li>
						<!-- / Mobile nav toggle -->
					</ul>
				</div>
			</div>
			<!-- header -->
		</div>
		<!-- container -->
	</header>
	<!-- /HEADER -->

	<!-- NAVIGATION -->
	<div id="navigation">
		<!-- container -->
		<div class="container">
			<div id="responsive-nav">
				<!-- category nav -->

				<div class="category-nav show-on-click">
					<span class="category-header">Categories <i class="fa fa-list"></i></span>
					<ul class="category-list">

						<li>

						<a href="#">View All</a>

						</li>

					</ul>
				</div>

				<!-- /category nav -->

				<!-- menu nav -->
				<div class="menu-nav">
					<span class="menu-header">Menu <i class="fa fa-bars"></i></span>
					<ul class="menu-list">

						<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="FoodStore.rootCategory" />
						<dsp:param name="elementName" value="category" />
						<dsp:oparam name="output">
							<li class="dropdown mega-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true"><dsp:valueof
										param="category.displayName" /><i class="fa fa-caret-down"></i></a>
							<div class="custom-menu">
								<div class="row">


									<div class="col-md-4">
										<ul class="list-links">

											<li>
												<h3 class="list-links-title"><dsp:valueof
										param="category.displayName" /></h3></li>
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="category.childProducts" />
										<dsp:param name="elementName" value="childProduct" />

										<dsp:oparam name="output">
											<li>
												</li>
												<!--  each item link-->
													<li>
													<dsp:a href="${contextPath}/browse/product.jsp">
													<dsp:param name="id" param="childProduct.repositoryId" />
													<dsp:param name="productName"
														param="childProduct.displayName" />
													<dsp:param name="productImageLarge"
														param="childProduct.largeImage.url" />
													<dsp:param name="productDescription"
														param="childProduct.longDescription" />
													<dsp:param name="skuIndex" value="0" />
													<dsp:valueof param="childProduct.displayName" />
												</dsp:a>
												</li>
												<!-- /each item link -->
										</dsp:oparam>
									</dsp:droplet>
								</ul>
								</div>
								</div>
								</div>
								</li>
						</dsp:oparam>
					</dsp:droplet>

					<!--
						<li class="dropdown mega-dropdown"><a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">Women <i class="fa fa-caret-down"></i></a>
							<div class="custom-menu">
								<div class="row">


									<div class="col-md-4">
										<ul class="list-links">
											<li>
												<h3 class="list-links-title">Categories</h3></li>
											<li><a href="#">Women’s Clothing</a></li>
											<li><a href="#">Men’s Clothing</a></li>
											<li><a href="#">Phones & Accessories</a></li>
											<li><a href="#">Jewelry & Watches</a></li>
											<li><a href="#">Bags & Shoes</a></li>
										</ul>
									</div>
								</div>
								<div class="row hidden-sm hidden-xs">
									<div class="col-md-12">
										<hr>
										<a class="banner banner-1" href="#">
											<img src="./img/banner05.jpg" alt="">
											<div class="banner-caption text-center">
												<h2 class="white-color">NEW COLLECTION</h2>
												<h3 class="white-color font-weak">HOT DEAL</h3>
											</div>
										</a>
									</div>
								</div>
							</div>
						</li>
						-->


					</ul>
				</div>
				<!-- menu nav -->
			</div>
		</div>
		<!-- /container -->
	</div>
	<!-- /NAVIGATION -->
</dsp:page>
