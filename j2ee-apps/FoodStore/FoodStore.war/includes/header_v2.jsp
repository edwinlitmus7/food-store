<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false" %>


<dsp:page>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<meta name="google-signin-client_id"
	content="642167561006-qi2sb3n02su1d4csld6cjp2ussecjnfd.apps.googleusercontent.com">
	
	
	<title>FoodStore - Eat healthy </title>

	<!-- Google font -->
	<link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

	<!-- Bootstrap -->
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/bootstrap.min.css" />

	<!-- Slick -->
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/slick.css" />
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/slick-theme.css" />

	<!-- nouislider -->
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/nouislider.min.css" />

	<!-- Font Awesome Icon -->
	<link rel="stylesheet" href="${contextPath}/css/font-awesome.min.css">

	
	<!--  Older css styles (Remove on migration) -->
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/autocomplete.css" />
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/modal.css" />
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/quickview.css" />
	<link type="text/css" rel="stylesheet" href="${contextPath}/css/style.css" />
	
	<!-- Slider -->
	<link type="text/css" rel="stylesheet" href="${contextPath}/slider/css/bootstrap-slider.css" />
	<link href="${contextPath}/notification_lib/noty.css" rel="stylesheet">
	
	
	

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<!-- Jquery -->
	<script src="${contextPath}/js/jquery.min.js"></script>
	<script src="https://apis.google.com/js/api:client.js"></script>
	<script>
	  var globalData = {};
	</script>
	
	

</head>

<body>
</dsp:page>