<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://apis.google.com/js/platform.js" async defer></script>
<meta name="google-signin-client_id"
	content="642167561006-qi2sb3n02su1d4csld6cjp2ussecjnfd.apps.googleusercontent.com">

<dsp:page>
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean
		bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/FoodStore" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />

	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />

	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="items" vartype="java.lang.Object"
		bean="ShoppingCart.current.commerceItems" />


	<link rel="stylesheet" href="${contextPath }/css/autocomplete.css">

	<nav class="navbar navbar-inverse navbar-upper navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<dsp:a iclass="navbar-brand" href="${contextPath}">FoodStore</dsp:a>
			</div>

			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-2">
				<dsp:form action="index.jsp" method="post">
					<dsp:input bean="FoodStore.autoComplete" type="hidden"
						name="keywords" id="keywords" />
				</dsp:form>
				<dsp:form iclass="navbar-form navbar-left navbar-center"
					action="${contextPath }/index.jsp" method="post">
					<div class="form-group">
						<div class="input-group">

							<dsp:input type="hidden" name="successURL"
								value="${contextPath }/index.jsp"
								bean="FoodStoreSearchFormHandler.successURL" />
							<dsp:input type="hidden" name="errorURL"
								value="${contextPath }/global/gadgets/errorMessage.jsp"
								bean="FoodStoreSearchFormHandler.errorURL" />
							<dsp:input type="text" id="search-box" iclass="form-control"
								bean="FoodStoreSearchFormHandler.repositoryKey">
								<dsp:tagAttribute name="placeholder" value="Search..." />
							</dsp:input>

							<dsp:input bean="FoodStoreSearchFormHandler.productSearch"
								id="search-submit-button" type="submit" />
							<span class="input-group-btn">
								<button type="button" id="search-button"
									class="btn btn-default btn-lg">
									<span class="glyphicon glyphicon-search"></span>
								</button>
							</span>
						</div>
					</div>
				</dsp:form>
				<!--  The nav bar menu -->
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown"><a href="${contextPath}/cart/cart.jsp">
							<span class="glyphicon glyphicon-shopping-cart"></span> <span
							class="badge badge-notify"> ${fn:length(items)} </span>
					</a> <dsp:droplet name="Switch">
							<dsp:param name="value" bean="/OriginatingRequest.requestURI" />
							<dsp:oparam name="/foodstore/cart/cart.jsp">
							</dsp:oparam>

							<dsp:oparam name="default">
								<dsp:include page="/cart/gadgets/minicart.jsp"/>
							</dsp:oparam>
						</dsp:droplet>
						</li>
					<li class="dropdown"><dsp:droplet name="Switch">
							<dsp:param bean="Profile.transient" name="value" />
							<dsp:oparam name="true">
								<a href="${contextPath}/myaccount/socialLogin.jsp"> Login <span
									class="caret"> </span>
								</a>
							</dsp:oparam>
							<dsp:oparam name="false">
								<a href="${contextPath}/myaccount/userDashBoard.jsp"> <dsp:valueof
										bean="Profile.firstname">
									</dsp:valueof> <span class="caret"></span>
								</a>
								<ul class="dropdown-menu profile-menu" role="menu">
									<dsp:droplet name="Switch">

										<dsp:param bean="Profile.transient" name="value" />
										<dsp:oparam name="false">
											<li><dsp:a href="${contextPath}/myaccount/myOrders.jsp">My Orders</dsp:a>
											</li>
											<li><dsp:a
													href="${contextPath}/myaccount/addressBook.jsp">Saved Addresses</dsp:a>
											</li>
											<li><dsp:a
													href="${contextPath}/myaccount/creditcards.jsp">Saved cards</dsp:a>
											</li>
											<li><a
												href="${contextPath}/myaccount/socialRegistration.jsp">
													Update profile </a></li>
											<li><a href="#" onclick="submitLogoutForm();">Logout</a>
											</li>
											<dsp:form method="post" id="logout_form" formid="logout_form">
												<dsp:input bean="ProfileFormHandler.logoutErrorURL"
													type="hidden" value="${contextPath}/userDashBoard.jsp" />
												<dsp:input bean="ProfileFormHandler.logoutSuccessURL"
													type="hidden" value="${contextPath}/index.jsp" />
												<dsp:input bean="ProfileFormHandler.logout" type="submit"
													value="Logout" />
											</dsp:form>
										</dsp:oparam>
										<dsp:oparam name="true">
											<li><a href="${contextPath}/index.jsp"> Home </a></li>
											<li><a href="${contextPath}/myaccount/socialLogin.jsp">
													Login </a></li>
											<li><a
												href="${contextPath}/myaccount/socialRegistration.jsp">
													SignUp </a></li>
										</dsp:oparam>
									</dsp:droplet>
								</ul>
							</dsp:oparam>
						</dsp:droplet></li>
				</ul>

			</div>
		</div>
	</nav>
	<nav class="navbar navbar-inverse navbar-lower">
		<div class="container-fluid">
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-2">
				<ul class="nav navbar-nav">

					<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="FoodStore.rootCategory" />
						<dsp:param name="elementName" value="category" />
						<dsp:oparam name="output">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								aria-expanded="false"> <dsp:valueof
										param="category.displayName" /> <span class="caret"></span>
							</a>
								<ul class="dropdown-menu" role="menu">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="category.childProducts" />
										<dsp:param name="elementName" value="childProduct" />
										<dsp:oparam name="output">
											<li><dsp:a href="${contextPath}/browse/product.jsp">
													<dsp:param name="id" param="childProduct.repositoryId" />
													<dsp:param name="productName"
														param="childProduct.displayName" />
													<dsp:param name="productImageLarge"
														param="childProduct.largeImage.url" />
													<dsp:param name="productDescription"
														param="childProduct.longDescription" />
													<dsp:param name="skuIndex" value="0" />
													<dsp:valueof param="childProduct.displayName" />
												</dsp:a></li>
										</dsp:oparam>

									</dsp:droplet>
								</ul></li>
						</dsp:oparam>
					</dsp:droplet>

				</ul>
			</div>
		</div>
	</nav>
	<script src="${contextPath}/js/autocomplete.js"></script>
</dsp:page>
