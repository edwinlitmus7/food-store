<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<dsp:page>
	<!--
 	It displays a summary of the products added to a user's cart. Items can be removed there.
 -->
	<%-- These components are used for removing item from the cart --%>
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean
		bean="/com/foodstore/droplet/order/RemoveUnscheduledCommerceItems" />
		<dsp:importbean bean="/atg/commerce/ShoppingCart" />
		<dsp:importbean bean="/atg/dynamo/droplet/Switch" />

	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />


	<dsp:droplet name="RemoveUnscheduledCommerceItems">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
		</dsp:oparam>
	</dsp:droplet>

	<ul class="dropdown-menu dropdown-menu-right" id="mini-cart">
		<dsp:droplet name="ForEach">
			<dsp:param name="array" bean="ShoppingCart.current.commerceItems" />
			<dsp:param name="elementName" value="item" />

			<dsp:oparam name="empty">
				<div class="text-center">
					<p>
					<h5>Your basket is currently empty.</h5>
					Please <a href="${contextPath}/myaccount/socialLogin.jsp"> sign
						in </a> if you are trying to retrieve a basket created in the past.
					</p>
					<a href="${contextPath}" class="btn btn-primary"> Shop new
						arrivals </a>
				</div>
			</dsp:oparam>

			<dsp:oparam name="outputStart">
				<li><a class="basket-header">
						<h5 class="text-center">Items in basket (${fn:length(items)})
						</h5>
				</a></li>
			</dsp:oparam>

			<dsp:oparam name="output">
				<li class="outer-row"><a href="#">
						<div class="row">
							<dsp:droplet name="ForEach">
								<dsp:param name="array"
									param="item.priceInfo.currentPriceDetails" />
								<dsp:param name="elementName" value="detail" />
								<dsp:oparam name="output">
									<div class="col-md-4" style="padding: 0px;">
										<img
											src='${contextPath }/<dsp:valueof param="item.auxiliaryData.catalogRef.smallImage.url" />'
											style="display: block;">
									</div>
									<div class="col-md-8" style="padding: 0px;">

										<div class="row">
											<div class="col-md-7">
												<strong> <dsp:valueof
														param="item.auxiliaryData.catalogRef.displayName" />
												</strong>
											</div>
											<div class="col-md-3 col-md-offset-1">
												<strong> <dsp:valueof converter="currency"
														param="detail.amount">no price</dsp:valueof>
												</strong>
											</div>
										</div>
										<!-- row -->

										<div class="row">
											<div class="col-md-7">
												<dsp:droplet name="ForEach">
													<dsp:param name="array" param="detail.adjustments" />
													<dsp:param name="elementName" value="adjustment" />
													<dsp:oparam name="output">
														<p>
															<dsp:valueof converter="currency"
																param="adjustment.adjustment">no price</dsp:valueof>
														</p>
													</dsp:oparam>
												</dsp:droplet>
											</div>
										</div>
										<!-- row -->

										<div class="row">
											<div class="col-md-7">
												QTY:
												<dsp:valueof param="detail.quantity" />
											</div>
											<div class="col-md-5">

												<%-- The below form sets the id of the commerceItem to  RemovalCommerceIds map
													and invokes function handleRemoveItemFromOrder of CartModifierFormHandler --%>
												<dsp:getvalueof var="commerceItem" param="item"></dsp:getvalueof>
												<dsp:getvalueof var="serialNumber" param="count"></dsp:getvalueof>
												<dsp:form method="post" id="removal_form${serialNumber}"
													formid="removal_form${serialNumber}">
													<dsp:input type="hidden"
														bean="CartModifierFormHandler.removeItemFromOrderSuccessURL"
														value="${originatingRequest.requestURI}" />
													<dsp:input type="hidden"
														bean="CartModifierFormHandler.removeItemFromOrderErrorURL"
														value="${originatingRequest.requestURI}" />
													<dsp:input type="hidden"
														bean="CartModifierFormHandler.RemovalCommerceIds"
														value="${commerceItem.id}" />
													<dsp:input iclass="btn btn-danger btn-xs"
														bean="CartModifierFormHandler.removeItemFromOrder"
														type="submit" value="Remove" />
												</dsp:form>
											</div>
										</div>
									</div>
								</dsp:oparam>
							</dsp:droplet>
						</div>
				</a></li>
			</dsp:oparam>

		</dsp:droplet>
	</ul>
</dsp:page>
