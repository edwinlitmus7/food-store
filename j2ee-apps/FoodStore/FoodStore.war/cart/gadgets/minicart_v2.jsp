<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<dsp:page>
	<!--
 		It displays a summary of the products added to a user's cart. Items can be removed there.
 	-->
	<%-- These components are used for removing item from the cart --%>
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean
		bean="/com/foodstore/droplet/order/RemoveUnscheduledCommerceItems" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />


	<dsp:droplet name="RemoveUnscheduledCommerceItems">
		<dsp:param name="profile" bean="Profile" />
		<dsp:param name="order" bean="ShoppingCart.current" />
		<dsp:oparam name="output">
		</dsp:oparam>
	</dsp:droplet>

	<div class="custom-menu">
		<div id="shopping-cart">
			<div class="shopping-cart-list">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="ShoppingCart.current.commerceItems" />
					<dsp:param name="elementName" value="item" />
					<dsp:getvalueof var="items"
						bean="ShoppingCart.current.commerceItems" />
					<dsp:oparam name="empty">
						<div class="text-center">

							Your basket is currently empty.<br /> Please <a
								href="${contextPath}/myaccount/socialLogin.jsp"> sign in </a> if
							you are trying to retrieve a basket created in the past. <a
								href="${contextPath}" class="btn btn-primary"> Shop new
								arrivals </a>
						</div>
					</dsp:oparam>
					<dsp:oparam name="outputStart">
						<ul>
							<li><a class="basket-header"> Items in basket
									(${fn:length(items)}) </a></li>
						</ul>
					</dsp:oparam>
					<dsp:oparam name="output">
						<dsp:getvalueof var="item" param="item" />
						<div class="product product-widget">
							<div class="product-thumb">
								<img
									src='${contextPath}/<dsp:valueof param="item.auxiliaryData.catalogRef.smallImage.url" />'
									alt="">
							</div>
							<div class="product-body">
								<h3 class="product-price">
									<dsp:valueof converter="currency"
										param="item.priceInfo.listPrice">--NA--</dsp:valueof>
									<span class="qty">x (<dsp:valueof param="item.quantity" />)
									</span>
								</h3>
								<h2 class="product-name">
									<a href="#"><dsp:valueof
											param="item.auxiliaryData.catalogRef.displayName" /></a>
								</h2>
								<%-- 												<dsp:droplet name="ForEach"> --%>
								<%-- 													<dsp:param name="array" param="detail.adjustments" /> --%>
								<%-- 													<dsp:param name="elementName" value="adjustment" /> --%>
								<%-- 													<dsp:oparam name="output"> --%>
								<%-- 															<dsp:valueof converter="currency" --%>
								<%-- 																param="adjustment.adjustment">no price</dsp:valueof> --%>
								<%-- 													</dsp:oparam> --%>
								<%-- 												</dsp:droplet> --%>

								<dsp:valueof converter="currency" param="item.priceInfo.amount">--NA--</dsp:valueof>
								<%-- The below form sets the id of the commerceItem to  RemovalCommerceIds map
													and invokes function handleRemoveItemFromOrder of CartModifierFormHandler --%>
								<dsp:getvalueof var="commerceItem" param="item"></dsp:getvalueof>
								<dsp:getvalueof var="serialNumber" param="count"></dsp:getvalueof>
								<dsp:form method="post" id="removal_form${serialNumber}"
									formid="removal_form${serialNumber}">
									<dsp:input type="hidden"
										bean="CartModifierFormHandler.removeItemFromOrderSuccessURL"
										value="${originatingRequest.requestURI}" />
									<dsp:input type="hidden"
										bean="CartModifierFormHandler.removeItemFromOrderErrorURL"
										value="${originatingRequest.requestURI}" />
									<dsp:input type="hidden"
										bean="CartModifierFormHandler.RemovalCommerceIds"
										value="${commerceItem.id}" />
									<dsp:input iclass="btn btn-danger btn-xs"
										id="removal_form_button${serialNumber}"
										bean="CartModifierFormHandler.removeItemFromOrder"
										type="submit" value="Remove">
										<dsp:tagAttribute name="style" value="display:none" />
									</dsp:input>
									<button class="cancel-btn"
										onclick="document.getElementById('removal_form_button${serialNumber}').click();return false;">
										<i class="fa fa-trash"></i>
									</button>
								</dsp:form>
							</div>
						</div>
					</dsp:oparam>
				</dsp:droplet> <!-- list -->
			</div>
			<dsp:droplet name="Switch">
				<dsp:param name="value" value="${fn:length(items) >0}" />
				<dsp:oparam name="true">
					<div class="shopping-cart-btns">
						<a href="${originatingRequest.contextPath}/cart/cart.jsp"><button
								class="main-btn">View Cart</button>
							<button class="primary-btn">
								Checkout <i class="fa fa-arrow-circle-right"></i>
							</button>
					</div>

				</dsp:oparam>

			</dsp:droplet>
		</div>
	</div>
</dsp:page>
