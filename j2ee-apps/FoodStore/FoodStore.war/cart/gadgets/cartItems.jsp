<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!--
	The items added in the cart are displayed here. Items can be removed and item quantity can be updated here.
 -->
 <script>
 $(function(){
	 $(".hotDeliveryCheckbox").change(function(){
		 	if($(this).is(':checked')){
		   var commerceId = $(this).attr("id").split("-")[1];
		   $("#checkBoxInput-"+commerceId).val(commerceId);
		   $("#updateHotDelivery-"+commerceId).click();
			
		 	}else{
		 		var commerceId = $(this).attr("id").split("-")[1];
		 		$("#checkBoxInput-"+commerceId).val(commerceId);
				$("#removeHotDelivery-"+commerceId).click();
		 		
		 	} 
	});
	 
 });
 </script> 

<dsp:page>
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean
		bean="/com/foodstore/droplet/catalog/SkuParentProductFinderDroplet" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />

	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:getvalueof var="items" bean="ShoppingCart.current.commerceItems"></dsp:getvalueof>
	<script>
		var openSideSkuModal = function(id) {
			var modal = $("#" + id)[0];
			modal.style.display = "block";
		}
	</script>
	<div class="row">


		<div class="invoice_container col-xs-12" id="cart"
			style="padding-left: 5px">
			<div class="section-title">
				<h2 class="title">Cart (${fn:length(items)})</h2>
			</div>
			<dsp:getvalueof var="schedule"
				bean="/atg/commerce/catalog/custom/FoodStore.schedule" />
			<dsp:droplet name="ForEach">
				<dsp:param name="array" bean="ShoppingCart.current.commerceItems" />
				<dsp:param name="elementName" value="item" />
				<dsp:oparam name="empty">
					<div class="panel-body text-center">
						<p>
						<h5 class="">Your basket is currently empty.</h5>
						Please <a href="${contextPath}/myaccount/socialLogin.jsp">
							sign in </a> if you are trying to retrieve a basket created in the
						past.
						</p>
						<a href="${contextPath}" class="btn btn-primary"> Shop new
							arrivals </a>
					</div>
				</dsp:oparam>

				<dsp:oparam name="output">
					<dsp:getvalueof var="serialNumber" param="count" scope="page" />
					<dsp:getvalueof id="commerceItem" idtype="com.foodstore.order.beans.FoodStoreCommerceItemImpl" param="item" />
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="item.priceInfo.currentPriceDetails" />
						<dsp:param name="elementName" value="detail" />
						<dsp:oparam name="output">
							<dsp:getvalueof var="serialNumberForDetail" param="count" scope="page" />
							<dsp:getvalueof var="serialNumber" value="${serialNumber}${serialNumberForDetail}" scope="page" />
							
							<dsp:droplet name="Switch">
          					<dsp:param name="value" value="${empty commerceItem.parentItem }"/>
          						<dsp:oparam name="true"> 
								<div class="row outer-row" style="padding: 15px;">
								<div class="row">
								<div class="col-md-3">
									<a href="#"> <img
										src='${contextPath }/<dsp:valueof param="item.auxiliaryData.catalogRef.smallImage.url" />'
										class="img-responsive">
									</a>
								</div>
								<div class="col-md-4">
									<p>
										<dsp:valueof param="item.auxiliaryData.catalogRef.displayName" />
									</p>
								<%--	<dsp:droplet name="ForEach">
										<dsp:param name="array" param="detail.adjustments" />
										<dsp:param name="elementName" value="adjustment" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="adjstmt" vartype="java.lang.Object"
												param="adjustment" />
											<p>
												<dsp:valueof converter="currency"
													param="adjustment.adjustment">no price</dsp:valueof>
											</p>
										</dsp:oparam>
									</dsp:droplet>  --%>
									<p>
									<dsp:valueof converter="currency" param="item.priceInfo.listPrice"></dsp:valueof>
									</p>
									<dsp:droplet name="Switch">
          							<dsp:param name="value" value="${empty commerceItem.parentItem }"/>
          								<dsp:oparam name="true"> 
									<center>
									<button class="btn main-btn" style="padding: 5px" onclick="openSideSkuModal('myModal_sideSku${serialNumber}')">Side Dishes</button>
									<dsp:getvalueof var="commerceId" param="item.id" ></dsp:getvalueof>
									<dsp:getvalueof var="isHotDeliveryChecked" param="item.isHotDelivery"></dsp:getvalueof>
									<c:choose>
										<c:when test="${isHotDeliveryChecked}">
											<input type="checkbox" class="hotDeliveryCheckbox" id="hotDeliveryCheckbox-${commerceId}" name="hotDelivery" checked>Hot Delivery
										</c:when>
										<c:otherwise>
											<input type="checkbox" class="hotDeliveryCheckbox" id="hotDeliveryCheckbox-${commerceId}" name="hotDelivery">Hot Delivery
										</c:otherwise>
									</c:choose>
									
									<dsp:importbean bean="com/foodstore/order/item/HotDeliveryFormHandler"/>
									 <dsp:getvalueof var="commerceId" param="item.id" ></dsp:getvalueof>
									 <dsp:form id="hotDeliveryForm-${commerceId}">
										<dsp:input type="hidden" id="checkBoxInput-${commerceId}" bean="HotDeliveryFormHandler.selectedCommerceId"/>
										<dsp:input id="updateHotDelivery-${commerceId}" style="visibility:hidden;" type="submit" bean="HotDeliveryFormHandler.updateHotDelivery"/> 
											<dsp:input id="removeHotDelivery-${commerceId}" style="visibility:hidden;" type="submit" bean="HotDeliveryFormHandler.removeHotDelivery"/>
									</dsp:form> 
									</center>	
									</dsp:oparam>
									</dsp:droplet>
									
									
								</div>
								<%-- The below form updates the quantity of the commerce item
																	and invokes function setOrderByCommerceId of CartModifierFormHandler --%>
								<div class="form-group">
								
									<div class="col-md-3">
									<dsp:droplet name="Switch">
          							<dsp:param name="value" value="${empty commerceItem.parentItem }"/>
          								<dsp:oparam name="true"> 
          												<dsp:form name="quantityUpdate"
											id="quantityForm_${serialNumber}" method="post">
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.updateErrorURL"
												value="${originatingRequest.requestURI}" />
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.updateSuccessURL"
												value="${originatingRequest.requestURI}" />
											<dsp:getvalueof var="itemQuantity" param="detail.quantity" />
											<div class="control-group col-xs-9">
												<dsp:select bean="CartModifierFormHandler.currentQuantity"
													name="selectQuantity" id="select_quantity_${serialNumber}">
													<script>
														$(function() {
															$(
																	"#select_quantity_${serialNumber}")
																	.val(
																			'${itemQuantity}');
														});
													</script>
													<dsp:tagAttribute name="onchange"
														value="valueSelected('submit_${serialNumber}');" />
													<dsp:tagAttribute name="class" value="form-control" />
													<c:forEach var="i" begin="1"
														end="${itemQuantity <= '10' ? '10' : itemQuantity+1}">
														<dsp:option value="${i}">${i}</dsp:option>
													</c:forEach>
												</dsp:select>

											</div>
											<dsp:input bean="CartModifierFormHandler.itemId"
												type="hidden" paramvalue="item.id" />

											<dsp:input
												bean="CartModifierFormHandler.setOrderByCommerceId"
												id="submit_${serialNumber}" type="submit" value="Update">
												<dsp:tagAttribute name="hidden" value="true" />
											</dsp:input>

										</dsp:form>
          								</dsp:oparam>
        							</dsp:droplet>
        							
							


									</div>
									<div class="col-md-2" style="float: right;">
										<p>
											<dsp:valueof converter="currency" param="detail.amount">no price</dsp:valueof>
										</p>
										<%-- The below form sets the id of the commerceItem to RemovalCommerceIds map
																	and invokes function handleRemoveItemFromOrder of CartModifierFormHandler --%>
										<dsp:getvalueof var="commerceItem" param="item"></dsp:getvalueof>
										<dsp:form method="post" id="removal_form${serialNumber}"
											formid="removal_form${serialNumber}">
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.removeItemFromOrderSuccessURL"
												value="${originatingRequest.requestURI}" />
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.removeItemFromOrderErrorURL"
												value="${originatingRequest.requestURI}" />
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.RemovalCommerceIds"
												value="${commerceItem.id}" />
												<%--Removing sideSKU related to commerce Item--%>
											<dsp:droplet name="ForEach">
												<dsp:param name="array" param="item.childItems" />
												<dsp:param name="elementName" value="sideSku" />
												<dsp:getvalueof var="index" param="index" />
												<dsp:oparam name="output">
													<dsp:input type="hidden"
														bean="CartModifierFormHandler.RemovalCommerceIds"
														paramvalue="sideSku.id" />
												</dsp:oparam>
											</dsp:droplet>
											<dsp:input iclass="primary-btn"
												bean="CartModifierFormHandler.removeItemFromOrder"
												type="submit" value="Remove" />

										</dsp:form>

									</div>
								</div>
								</div>
								<div class="row">
										<%-- Display side dishes if any --%>
												<dsp:droplet name="Switch">
          											<dsp:param name="value" value="${not empty commerceItem.childItems}"/>
          											<dsp:oparam name="true">
          												<dsp:droplet name="ForEach">
															<dsp:param name="array" value="${commerceItem.childItems}" />
															<dsp:param name="elementName" value="sideDishCommerceItem" />
															<dsp:oparam name="outputStart">
																	<h2 style="padding:20px"> Side items </h2>
															</dsp:oparam>
															<dsp:oparam name="output">
															<dsp:getvalueof var="sideDishSerialNumber" param="count" scope="page" />
															<dsp:getvalueof var="sideDishCommerceItem" vartype="com.foodstore.order.beans.FoodStoreCommerceItemImpl" param="sideDishCommerceItem" />
          														<dsp:droplet name="ForEach">
																<dsp:param name="array" value="${sideDishCommerceItem.priceInfo.currentPriceDetails}" />
																<dsp:param name="elementName" value="detail" />
																<dsp:oparam name="output">
																	<dsp:getvalueof var="sideDishCurrentPriceDetailSerialNumber" param="count" scope="page" />
																	<dsp:getvalueof var="serialNumberSideSku" value="${serialNumber}${sideDishSerialNumber}${sideDishCurrentPriceDetailSerialNumber}"/>
																	<dsp:include page="./displaySideDish.jsp">
          																<dsp:param name="sideDishCommerceItem" value="${sideDishCommerceItem}"/>
          																<dsp:param name="serialNumber" value="${serialNumberSideSku}"/>
          																<dsp:param name="sideDishDetail" param="detail"/>
          															</dsp:include>
																</dsp:oparam>
          														</dsp:droplet>
          													</dsp:oparam>
          												</dsp:droplet>
          											</dsp:oparam>
          										</dsp:droplet>
										<%-- /Display side dishes if any --%>
								</div>

							</div>
							
							<%--Modal--%>
		<div id="myModal_sideSku${serialNumber}" class="modal" style="z-index: 100">
			<!-- Modal content -->
			<div class="modal-content">
				<span class="close" onclick="closeModal('myModal_sideSku${serialNumber}')">&times;</span>
				<div class="container" style="padding: 45px">
					<h2>Please Select the side dishes.</h2>
					<dsp:form>

						<div class="row">
							<dsp:droplet
								name="/com/foodstore/droplet/catalog/SideSkusDroplet">
								<dsp:oparam name="OUTPUT">
									<dsp:droplet name="/atg/dynamo/droplet/ForEach">
										<dsp:param name="array" param="sideSkus" />
										<dsp:param name="elementName" value="sideSku" />
										<dsp:oparam name="output">
															
															<dsp:droplet name="SkuParentProductFinderDroplet">
																<dsp:param name="skuId" param="sideSku.repositoryId" />
																<dsp:oparam name="OUTPUT">
																	<dsp:getvalueof var="product" param="parentProduct"
																		scope="page" />
																	<input type="hidden" name="add_item_productId"
																		value="${product.repositoryId }" />
																</dsp:oparam>
															</dsp:droplet>
															<%-- Find if the parentItem has a sku of this type as commerceItem  --%>	
															<dsp:getvalueof var="checkBoxChecked" scope="page" value="false"></dsp:getvalueof>
													        <dsp:getvalueof var="checkBoxValue" scope="page" value=""></dsp:getvalueof>
																<dsp:getvalueof var="sideSku" param="sideSku"></dsp:getvalueof>
																<dsp:droplet name="ForEach">
																<dsp:param name="array" bean="ShoppingCart.current.commerceItems" />
																<dsp:param name="elementName" value="allCommerceItem" />
																<dsp:oparam name="output">
																<dsp:getvalueof var="sideCommerceItem" param="item"></dsp:getvalueof>
																<dsp:getvalueof var="allCommerceItem" param="allCommerceItem"></dsp:getvalueof>
																<dsp:droplet name="Switch">
																<dsp:param name="value" value="${allCommerceItem.parentItem  eq commerceItem.id and sideSku.repositoryId eq allCommerceItem.catalogRefId}"/>
          																<dsp:oparam name="true"> 
																				<dsp:getvalueof var="checkBoxChecked" scope="page" value="true"></dsp:getvalueof>
																				<dsp:getvalueof var="checkBoxValue" scope="page" value="checked"></dsp:getvalueof>
																	    </dsp:oparam>
																	   
														
																</dsp:droplet>
																</dsp:oparam>
																</dsp:droplet>
															<%-- End of  checkbox logic --%>
															
											
																

											<dsp:getvalueof id="smallProductImageURL"
												idtype="java.lang.String" param="sideSku.smallImage.url" />


								
											
											<!-- Product Single -->
											<div class="col-xs-3" style="padding: 5px">
												<div class="product product-single">
													<div class="product-thumb">

														<!-- Trigger/Open The Modal -->
														
														<input type="checkbox" class="main-btn quick-view "
															style="width: 30px; height: 30px;opacity:inherit;visibility:visible"
															onchange="this.nextElementSibling.value = this.checked" ${checkBoxValue }/>
														<input name="add_item_checkbox" value="${checkBoxChecked}"
															type="hidden" /> <img
															src="${contextPath}/${smallProductImageURL}"
															style="height: 200px; width: 100%" alt="">
													</div>
													<div class="product-body">
														<h3 class="product-price">

															<dsp:include page="/global/gadgets/formattedPrice.jsp">
																<dsp:param name="price" param="sideSku.listPrice" />
															</dsp:include>
														</h3>

														<h2 class="product-name">

															<dsp:valueof param="sideSku.displayName">ERROR:no product name</dsp:valueof>

														</h2>
														<div class="product-btns">

															<input type="hidden" name="add_item_count" value="1" /> <input
																type="hidden" name="add_item_commerceItemType"
																value="default" /> <input type="hidden"
																name="add_item_catalogRefIds"
																value="<dsp:valueof param='sideSku.repositoryId'/>" />
															
															

														</div>
													</div>
												</div>
											</div>
											<!-- /Product Single -->


										</dsp:oparam>
									</dsp:droplet>
								</dsp:oparam>

							</dsp:droplet>
						</div>
						<div class="row">
							<dsp:input
								bean="CartModifierFormHandler.addSideItemToOrderSuccessURL"
								type="hidden" value="${contextPath}/cart/cart.jsp" />
							<dsp:input bean="CartModifierFormHandler.addSideItemToOrderErrorURL"
								type="hidden"
								value="${contextPath}/global/gadgets/errorMessage.jsp" />
							<input type="hidden" name="adding_side_sku" value="true" />
							<dsp:input bean="CartModifierFormHandler.value.parentItem"
																type="hidden" id="commerceItemId" paramvalue="item.id"></dsp:input>
							<dsp:input bean="CartModifierFormHandler.commerceItemId"
																type="hidden" id="commerceItemId2" paramvalue="item.id">
															</dsp:input>
							<dsp:input bean="CartModifierFormHandler.addSideItemToOrder"
								id="thumbnail_add_to_cart_${index_prev}${index}" type="submit"
								value="Add To Cart" name="addToCart">
								<dsp:tagAttribute name="class" value="primary-btn" />
							</dsp:input>
							

						</div>
					</dsp:form>

				</div>
			</div>
		</div>
							
							<%--/modal --%>
							
								</dsp:oparam>
							</dsp:droplet>
							
					
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>

				<dsp:oparam name="outputEnd">
		</div>
		<!-- End of panel-body -->
		</dsp:oparam>
		</dsp:droplet>
		<script>
			function valueSelected(id) {
				$("#" + id).click();
			}
		</script>


	


	</div>
</dsp:page>