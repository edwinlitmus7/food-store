<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest"/>

	<dsp:form method="post" id="removal_form${serialNumber}"
		formid="removal_form${serialNumber}">
		<dsp:input type="hidden"
			bean="CartModifierFormHandler.removeItemFromOrderSuccessURL"
			value="${originatingRequest.requestURI}" />
		<dsp:input type="hidden"
			bean="CartModifierFormHandler.removeItemFromOrderErrorURL"
			value="${originatingRequest.requestURI}" />
		<dsp:input type="hidden"
			bean="CartModifierFormHandler.RemovalCommerceIds"
			value="${commerceItem.id}" />
		<dsp:input iclass="btn btn-danger"
			bean="CartModifierFormHandler.removeItemFromOrder" type="submit"
			value="Remove" />
	</dsp:form>
</dsp:page>