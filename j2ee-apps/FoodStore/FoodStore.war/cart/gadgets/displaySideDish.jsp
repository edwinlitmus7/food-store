<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<dsp:page>
	<%-- Displays the sidesku details --%>
	<dsp:importbean
		bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:importbean bean="/atg/commerce/ShoppingCart" />
	<dsp:importbean bean="/OriginatingRequest" var="originatingRequest" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
		
	
	<dsp:getvalueof var="sideDishCommerceItem" vartype="com.foodstore.order.beans.FoodStoreCommerceItemImpl" param="sideDishCommerceItem" />
	<dsp:getvalueof id="serialNumber" param="serialNumber" />
	<dsp:getvalueof id="detail" param="sideDishDetail" />
		<div class="row ">
								<div class="col-md-3">
									
								</div>
								<div class="col-md-3">
									<a href="#"> <img
										src='${contextPath }/<dsp:valueof param="sideDishCommerceItem.auxiliaryData.catalogRef.smallImage.url" />'
										class="img-responsive">
									</a>
								</div>
								<div class="col-md-4">
									<p>
										<dsp:valueof param="sideDishCommerceItem.auxiliaryData.catalogRef.displayName" />
									</p>
									<dsp:droplet name="ForEach">
										<dsp:param name="array" value="${detail.adjustments}" />
										<dsp:param name="elementName" value="adjustment" />
										<dsp:oparam name="output">
											<dsp:getvalueof var="adjstmt" vartype="java.lang.Object"
												param="adjustment" />
											<p>
												<dsp:valueof converter="currency"
													param="adjustment.adjustment">no price</dsp:valueof>
											</p>
										</dsp:oparam>
									</dsp:droplet>
									
									
									
									
								</div>
								<div class="form-group">
							
									<div class="col-md-2" style="float:right" >
										<p>
											<dsp:valueof converter="currency" param="detail.amount">no price</dsp:valueof>
										</p>
										<%-- The below form sets the id of the commerceItem to RemovalCommerceIds map
																	and invokes function handleRemoveItemFromOrder of CartModifierFormHandler --%>
										<dsp:getvalueof var="commerceItem" param="item"></dsp:getvalueof>
										<dsp:form method="post" id="removal_form${serialNumber}"
											formid="removal_form${serialNumber}">
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.removeItemFromOrderSuccessURL"
												value="${originatingRequest.requestURI}" />
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.removeItemFromOrderErrorURL"
												value="${originatingRequest.requestURI}" />
											<dsp:input type="hidden"
												bean="CartModifierFormHandler.RemovalCommerceIds"
												value="${sideDishCommerceItem.id}" />
											<dsp:input iclass="primary-btn"
												bean="CartModifierFormHandler.removeItemFromOrder"
												type="submit" value="Remove" />

										</dsp:form>

									</div>
								</div>
								</div>
	

</dsp:page>
