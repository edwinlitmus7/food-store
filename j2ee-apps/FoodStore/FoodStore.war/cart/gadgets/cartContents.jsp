<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
	<%-- 
       	This gadget is used to display cartItems information and relative operations such as delete etc. 
	--%>
    <dsp:include page="cartItems.jsp" />
</dsp:page>