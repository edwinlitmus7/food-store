<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<!--  Displays error message  if any sort of failure occurs in the order. -->
	<dsp:include page="/global/gadgets/errorMessage.jsp">
		<dsp:param name="formHandler"
			bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	</dsp:include>
</dsp:page>