<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:include page="/includes/header_v2.jsp" />
<dsp:include page="/includes/navbar_v2.jsp" />
		<div class="section">
		<div class="container">
		
      
      
    <div class="cart_shopping-cart">
      <!-- Title -->
      <div class="cart_title">
       	Applied Coupons.
      </div>
      
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/commerce/promotion/CouponFormHandler" />
	<dsp:droplet name="/com/foodstore/promotion/CouponsDisplayDroplet">
		<dsp:param name="profile" bean="Profile"/>
		<dsp:oparam name="output">
			<dsp:getvalueof var="coupons" param="coupons"></dsp:getvalueof>
			<dsp:droplet name="/atg/dynamo/droplet/ForEach">
				<dsp:param name="array" value="${coupons}"/>
				<dsp:oparam name="empty">
					<h2>No promotions applied.</h2>
				</dsp:oparam>
				<dsp:oparam name="output">
					<dsp:getvalueof var="coupon" param="key"></dsp:getvalueof>		
<dsp:getvalueof var="promotions" param="element"></dsp:getvalueof>		

	<!-- Product #1 -->
      <div class="cart_item">
        


        <div class="cart_description" style="width:100%;font-size: 30px;">
          <span style="font-size: 30px"><b>${coupon.displayName}</b></span>
   
        </div>

        
        
        <div class="cart_buttons">
          <span class="cart_delete-btn"></span>
          <dsp:form>
          	<dsp:input bean="CouponFormHandler.couponClaimCode" value="${coupon.id}"/>
          	<dsp:input type="submit" bean="CouponFormHandler.revokeCoupon"/>
          	<dsp:input type="hidden" bean="CouponFormHandler.revokeCouponSuccessURL" value="${contextPath }/cart/cart.jsp?success"/>
         	<dsp:input type="hidden" bean="CouponFormHandler.revokeCouponErrorURL" value="${contextPath }/cart/cart.jsp?error"/>
         
          </dsp:form>
         
        </div>
        
      </div>
      
				</dsp:oparam>
			</dsp:droplet>
			
		</dsp:oparam>
	</dsp:droplet>
	
	</div>
	
			

	</div>
</div>			
		<dsp:include page="/includes/footer_v2.jsp" />
</dsp:page>
