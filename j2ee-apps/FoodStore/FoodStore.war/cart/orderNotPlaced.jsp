<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
		<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />

		<dsp:page page="/includes/header_v2.jsp"/>
		<dsp:page page="/includes/navbar_v2.jsp"/>
		<div class="section">
		<div class="container">
			<h3>Your order is not placed!</h3>
			<p>
				<a href="${contextPath }" class="btn btn-primary" > Home </a>
				<a href="${contextPath }/cart/cart.jsp" class="btn btn-primary"> Cart</a>
			</p>
		</div>
		</div>
		<dsp:page page="/includes/footer_v2.jsp"/>
</dsp:page>