<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
<dsp:include page="/includes/header_v2.jsp" />
<dsp:include page="/includes/navbar_v2.jsp" />
		<dsp:importbean
			bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
		<dsp:importbean
			bean="/com/foodstore/droplet/order/RemoveUnscheduledCommerceItems" />

		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:importbean bean="/atg/commerce/ShoppingCart" />

		<dsp:droplet name="RemoveUnscheduledCommerceItems">
			<dsp:param name="profile" bean="Profile" />
			<dsp:param name="order" bean="ShoppingCart.current" />
			<dsp:oparam name="output">
			</dsp:oparam>
		</dsp:droplet>
	
		<%--
		    The number of items currently in the shopping cart. This will determine whether
		    to include an order summary or display a message to the user informing them that
		    their cart is empty.
		  --%>
		<dsp:getvalueof var="commerceItemCount"
			bean="ShoppingCart.current.CommerceItemCount" />
		<div class="section">
		<div class="container">
			<div class="row">
				
				<div class="col-md-7">
					<dsp:include page="/cart/gadgets/cartContents.jsp" />
				</div>

				<div class="col-md-5">
					<dsp:include page="/checkout/gadgets/checkoutOrderSummary.jsp" >
						<dsp:param name="isShoppingCart" value="true" />
					</dsp:include>
				</div>
			</div>
		</div>
		</div>
		<dsp:include page="/includes/footer_v2.jsp" />
</dsp:page>
