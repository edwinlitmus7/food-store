<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>

	<dsp:importbean bean="/atg/commerce/order/OrderLookup" />
	<dsp:importbean bean="/atg/core/i18n/LocaleTools" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/index.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
				<dsp:include page="/includes/header_v2.jsp" />
				<dsp:include page="/includes/navbar_v2.jsp" />
				<div class="section">
				<div class="container">
					<div class="row">
					<div class="col-md-12">
								<div class="section-title">
									<h2 class="title"> Order history</h2>
								</div>
					</div>
				</div>
					<div class="row">
					<div class="col-md-7">
						<dsp:droplet name="OrderLookup">
							<dsp:param name="userId" bean="Profile.id" />
							<dsp:param name="sortBy" value="submittedDate" />
							<dsp:param name="state" value="open" />
							<dsp:param name="viewAll" value="true" />
							<dsp:oparam name="error">
								<dsp:valueof param="errorMsg" />
							</dsp:oparam>
							<dsp:oparam name="empty">
						<b>You have not placed any orders.</b>
					</dsp:oparam>
							<dsp:oparam name="output">
								<dsp:getvalueof var="orders" param="result" />
						Order: <dsp:valueof param="orders" />
								<dsp:getvalueof var="size" param="count" />
								<c:forEach var="order" items="${orders}" varStatus="status">
									<dsp:param name="order" value="${order}" />
									<div class="panel panel-default">
										<div class="panel-heading">
											<div class="row">
												<div class="col-md-3">
													<strong> Order submitted on  </strong>
													<dsp:getvalueof var="dateFormat"
														bean="LocaleTools.userFormattingLocaleHelper.datePatterns.shortWith4DigitYear" />
													<fmt:formatDate value="${order.submittedDate}"
														pattern="${dateFormat}" />
												</div>
												<div class="col-md-3">Number of items :
													${fn:length(order.commerceItems)}</div>
												<div class="col-md-1">
													<dsp:getvalueof var="total" vartype="java.lang.Double"
														param="order.priceInfo.total" />
													<dsp:include page="/global/gadgets/formattedPrice.jsp">
														<dsp:param name="price" value="${total}" />
													</dsp:include>
												</div>
												<div class="col-md-3">
													order: <strong> <dsp:valueof param="order.id" />
													</strong>
													
												</div>
												<div class="col-md-2">
													<dsp:a href="orderDetail.jsp" iclass="btn main-btn">
														
														<dsp:param name="orderId" param="order.id" />
														view details
													</dsp:a>
												</div>
											</div>
										</div>
									</div>
								</c:forEach>
							</dsp:oparam>
						</dsp:droplet>
					</div>
				</div>

				</div>
			</div>
				
				<dsp:include page="/includes/footer_v2.jsp" >
				</dsp:include>
		</dsp:oparam>
	</dsp:droplet>
	
	
</dsp:page>
