<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<%@ include file="/includes/header.jsp"%>
	<body>
		<dsp:getvalueof var="addEditMode" param="addEditMode" />
		<dsp:droplet name="/atg/dynamo/droplet/Switch">
			<dsp:param name="value" param="addEditMode" />
			<dsp:oparam name="edit">
				<dsp:param name="buttonName" value="Update" />
				<dsp:include page="/myaccount/gadgets/addressEdit.jsp">
					<dsp:param name="successURL" param="successURL" />
					<dsp:param name="firstLastRequired" value="true" />
					<dsp:param name="addressKey" param="addressKey" />
					<dsp:param name="buttonName" param="buttonName" />
					<dsp:param name="addEditMode" param="addEditMode" />
					<dsp:param name="restrictionDroplet"
						value="/atg/store/droplet/ShippingRestrictionsDroplet" />
				</dsp:include>
			</dsp:oparam>
			<dsp:oparam name="add">
				<dsp:param name="buttonName" value="Add" />
				<dsp:include page="/myaccount/gadgets/addressEdit.jsp">
					<dsp:param name="successURL" param="successURL" />
					<dsp:param name="firstLastRequired" value="true" />
					<dsp:param name="addressKey" param="addressKey" />
					<dsp:param name="buttonName" param="buttonName" />
					<dsp:param name="addEditMode" param="addEditMode" />
					<dsp:param name="restrictionDroplet"
						value="/atg/store/droplet/ShippingRestrictionsDroplet" />
				</dsp:include>
			</dsp:oparam>
		</dsp:droplet>
	</body>
	<%@ include file="/includes/footer.jsp"%>
</dsp:page>