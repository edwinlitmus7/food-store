<%@ taglib uri="/dspTaglib" prefix="dsp" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<dsp:page>
<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/myaccount/socialLogin.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
	
			<head>
			
					<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
					<link rel="stylesheet" href="./../css/style.css">
					<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
				<title> Credit Card </title>
			</head>
			<body>
				<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
				<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
				<dsp:importbean bean="/atg/commerce/catalog/custom/FoodStore"/>
			    <dsp:importbean bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler"/>
				
				<%@ include file="./../includes/navbar.jsp" %>
				
				
				<!-- List of credit cards -->		
				<div class="container">
					<div class="row">
						<%@ include file="./gadgets/sidemenu_creditcard.jsp"%>
						<div class="col-md-9">
							<%-- Display errors if any --%>
							<dsp:droplet name="Switch">
								<dsp:param bean="ProfileFormHandler.formError" name="value" />
								<dsp:oparam name="true">
									<div class="row">
										<div class="col-xs-4"></div>
										<div class="col-xs-4">
											<div class="alert alert-danger">
												<font color=fffcfc><STRONG><UL>
															<dsp:droplet name="ErrorMessageForEach">
																<dsp:param bean="ProfileFormHandler.formExceptions"
																	name="exceptions" />
																<dsp:oparam name="output">
																	<LI><dsp:valueof param="message" />
																</dsp:oparam>
															</dsp:droplet>
														</UL></STRONG></font>
											</div>
										</div>
										<div class="col-xs-4"></div>
									</div>
								</dsp:oparam>
							</dsp:droplet>
							<%@ include file="./gadgets/addEditCreditCard.jsp"%>
						</div>
					</div>
				</div>
				
				<%@ include file="./../includes/footer.jsp"%>
				
				<!-- Latest compiled JavaScript -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				
				<script src="./../js/script.js"></script>
				
			</body>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>