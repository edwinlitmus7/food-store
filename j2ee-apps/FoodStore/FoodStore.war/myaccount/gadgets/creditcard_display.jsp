<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>

		<div class="panel panel-default col-md-9" style="padding:5px">
			<h4 class="card-title"><i class="fa fa-credit-card"></i>
			<dsp:valueof param="creditCardNickname" />
			<span class="credit-cards-label"></span>
			</h4>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-3">
						<dsp:droplet name="/com/foodstore/droplet/HideString">
							<dsp:param name="value" param="creditCard.creditCardNumber" />
							<dsp:oparam name="output">
								<dsp:valueof param="element" />
							</dsp:oparam>
						</dsp:droplet>
					</div>

					<div class="col-md-4">
						<dsp:valueof param="creditCardNickname" />
					</div>

					<div class="col-md-2">
						<dsp:valueof param="creditCard.creditCardType" />
					</div>

					<div class="col-md-1 col-md-offset-1" data-toggle="tooltip" data-placement="bottom" title="Edit card details">
						<dsp:a href="newcreditcard.jsp" iclass="creditcard-edit-icon">
							<i class=" fa fa-edit"></i>
							<dsp:param name="addEditMode" value="edit" />
							<dsp:param name="nickname" param="creditCardNickname" />
						</dsp:a>
					</div>

					<div class="col-md-1">
					<dsp:getvalueof var="creditCardNickname" param="creditCardNickname"></dsp:getvalueof>
							
						<dsp:form iclass="creditcard-delete-form hidden-form">
							<dsp:input type="hidden" name="nickname"
								bean="ProfileFormHandler.removeCard"
								paramvalue="creditCardNickname" />
							<dsp:input iclass="creditcard-delete-button" type="submit"
								bean="ProfileFormHandler.removeCreditCard" >
								<dsp:tagAttribute name="style" value="display:none"/>
								<dsp:tagAttribute name="id" value="${creditCardNickname}"/>
								</dsp:input>
						</dsp:form>
						<a href="#"  data-toggle="tooltip" data-placement="bottom" title="Remove card"
						onclick="document.getElementById('${creditCardNickname}').click();return false;"
						>
						<i class="fa fa-trash"></i>
						</a>
					</div>
				</div>
			</div>
		</div>
</dsp:page>