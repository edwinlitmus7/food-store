<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
		
		<%-- Dummy login form to send data to the formHandler --%>
		<dsp:form method="post" id="dummy_form" formid="dummy_form">
			<dsp:input bean="ProfileFormHandler.loginErrorURL" type="hidden" value="${contextPath}/myaccount/socialRegistration.jsp" />
			<dsp:input bean="ProfileFormHandler.loginSuccessURL" type="hidden"
				value="${contextPath}" />
			<dsp:input bean="ProfileFormHandler.token" name="token" />
			<br />
			<dsp:input bean="ProfileFormHandler.login" id="submit_login" type="submit"
				value="Login" />
		</dsp:form>
		<%--// End of dummy login form --%>
		
	<!-- Start of form -->
		<!-- javascript -->
	<script>		
					// Script for handling the login form 
					document.getElementById("dummy_form").style.display = "none"; // make the form hidden 
					//document.getElementById("googleSignInButton").style.display = "none";
					function onSignIn(googleUser) {
						
						// The ID token you need to pass to your backend:
						var id_token = googleUser.getAuthResponse().id_token;
						console.log("ID Token: " + id_token);
						document.getElementsByName("token")[0].value = id_token;
						//Hide the sign in button of google display the options
						document.getElementById("all_signin_forms").style.display = "none";
						document.getElementById("processing_signin").style.display = "block";
						atgSignIn();
						googleSessionSignOut();
					}
					function onFailure(error){
						activeSessionOngoogle = false;
						console.log(error);
						document.getElementById("all_signin_forms").style.display = "block";
						document.getElementById("processing_signin").style.display = "none";
						document.getElementById("googleSignInButton").style.display = "block";
						
					}
					function atgSignIn(){
						$("#submit_login").click();
					}
				</script>
				  <script>
  var googleUser = {};
  var startApp = function() {
    gapi.load('auth2', function(){
      // Retrieve the singleton for the GoogleAuth library and set up the client.
      auth2 = gapi.auth2.init({
        client_id: '642167561006-qi2sb3n02su1d4csld6cjp2ussecjnfd.apps.googleusercontent.com',
        cookiepolicy: 'single_host_origin',
        // Request scopes in addition to 'profile' and 'email'
        //scope: 'additional_scope'
      });
      attachSignin(document.getElementById('customBtn'));
    });
  };

  function attachSignin(element) {
    console.log(element.id);
    auth2.attachClickHandler(element, {},
        function(googleUser) {
    		onSignIn(googleUser);
        }, function(error) {
          alert(JSON.stringify(error, undefined, 2));
        });
  }
  </script>
	<dsp:form id="all_signin_forms" iclass="form-horizontal">
		<div class="form-group">
			<label class="control-label col-sm-2" for="email">Email:</label>
			<div class="col-sm-10">
				<dsp:input type="email" required="true" iclass="form-control" id="email"
				bean="ProfileFormHandler.value.login"
					>
					<dsp:tagAttribute name="placeholder" value="Enter email"/>
					</dsp:input>
			</div>
		</div>
		<div class="form-group">
			<label class="control-label col-sm-2" for="pwd">Password:</label>
			<div class="col-sm-10">
				<dsp:input type="password" required="true" iclass="form-control" id="pwd"
					 bean="ProfileFormHandler.value.password">
					<dsp:tagAttribute name="placeholder" value="Enter password"/>
					</dsp:input>
			</div>
		</div>
		
		<dsp:input bean="ProfileFormHandler.loginSuccessURL" type="hidden"
					value="${contextPath }" />
				<dsp:input bean="ProfileFormHandler.loginErrorURL" type="hidden"
					value="${contextPath }/myaccount/socialLogin.jsp" />
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-4">
				<dsp:input type="submit" iclass="primary-btn"   value="Login" bean="ProfileFormHandler.normalLogin"/>
			</div>
			<div class="col-sm-5" id="googleSignInButton">
				<div class="row center" align="center">
					<!-- Displays the button -->
					 <div id="gSignInWrapper">
    				
    				<div id="customBtn" class="primary-btn" style="cursor: pointer;">
      				
     				 <span class="buttonText"> Sign in with <i class="fab fa-google-plus-g"></i> Google</span>
   					 </div>
  					</div>
  					<div id="name"></div>
  					<script>startApp();</script>
				</div>
			</div>
			      
			
		</div>
	</dsp:form>
	<div class="col-sm-12 col-md-12">
	<div id="processing_signin"  style="display:none">
	<h3> Please wait ...</h3>
	<!-- End -->
	</div>
	</div>
	

</dsp:page>
