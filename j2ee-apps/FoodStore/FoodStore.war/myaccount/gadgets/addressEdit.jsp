<%-- 
	This page gives the redirection according to the users mode(Add/Edit/Remove).
 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/myaccount/socialLogin.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">

	<%@ include file="/includes/header_v2.jsp"%>
	<%@ include file="/includes/navbar_v2.jsp"%>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach"/>
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />


	<%-- Get user's mode of operation and SuccessURL --%>
	<dsp:getvalueof var="addEditMode" param="addEditMode" />
	<dsp:getvalueof var="successURL" param="successURL" />
	<dsp:getvalueof var="addressKey" param="addressKey" />
	
	<!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!--  Product Details -->
				<div class="product product-details clearfix">
					<div class="row">
					<%-- Display errors if any --%>
					<dsp:droplet name="Switch">
					<dsp:param bean="ProfileFormHandler.formError" name="value" />
						<dsp:oparam name="true">
								<div class="row">
								<div class="col-xs-7">
									<div class="alert alert-danger">
									<font color=fffcfc><STRONG><UL>
										<dsp:droplet name="ErrorMessageForEach">
											<dsp:param bean="ProfileFormHandler.formExceptions"
												name="exceptions" />
											<dsp:oparam name="output">
												<LI><dsp:valueof param="message" />
											</dsp:oparam>
										</dsp:droplet>
									</UL></STRONG></font>
									</div>
								</div>
								</div>
						</dsp:oparam>
					</dsp:droplet>
					</div>
					<div class="row">
					<%-- Value of successURL would be coming from a previous page --%>
		<dsp:setvalue bean="ProfileFormHandler.newAddressSuccessURL"
			paramvalue="successURL" />
		<dsp:setvalue bean="ProfileFormHandler.updateAddressSuccessURL"
			paramvalue="successURL" />
		
		
		
		<dsp:form id="addressForm" method="post" formid="addressForm">
			
				<legend>
					<dsp:valueof param="addEditMode"></dsp:valueof> adress.
				</legend>
				<dsp:getvalueof id="originatingRequestURL"
					bean="/OriginatingRequest.requestURI" />
				<dsp:getvalueof var="successURL" param="successURL"
					vartype="java.lang.String" />

				<dsp:input type="hidden"
					bean="ProfileFormHandler.newAddressSuccessURL"
					beanvalue="ProfileFormHandler.newAddressSuccessURL" />
				<dsp:input type="hidden"
					bean="ProfileFormHandler.newAddressErrorURL"
					value="${originatingRequestURL}?successURL=${successURL}&addEditMode=Add" />

				<dsp:input type="hidden"
					bean="ProfileFormHandler.updateAddressSuccessURL"
					beanvalue="ProfileFormHandler.updateAddressSuccessURL" />
				<dsp:input type="hidden"
					bean="ProfileFormHandler.updateAddressErrorURL"
					value="${originatingRequestURL}?successURL=${successURL}&addEditMode=Edit&addressKey=${addressKey}" />
			
				<dsp:droplet name="/atg/dynamo/droplet/Switch">
					<dsp:param name="value" param="addEditMode" />

					<%-- When user selects the 'Edit' mode --%>
					<dsp:oparam name="Edit">
						<dsp:include page="/global/gadgets/addressAddEdit.jsp">
							<dsp:param name="successURL" param="successURL" />
							<dsp:param name="firstLastRequired" value="true" />
							<dsp:param name="addressKey" param="addressKey" />
							<dsp:param name="formHandlerComponent"
								value="/atg/userprofiling/ProfileFormHandler.editValue" />
							<dsp:param name="restrictionDroplet"
								value="/atg/store/droplet/ShippingRestrictionsDroplet" />
						</dsp:include>
						
						<div class="form-group"> <!-- Submit Button -->
							<dsp:input bean="ProfileFormHandler.updateAddress"
								name="updateAddress" type="submit" value="Update address"
								iclass="primary-btn" />
						</div> 
						
						  
					</dsp:oparam>

					<%-- When user selects the 'Add' mode --%>
					<dsp:oparam name="Add">
						<dsp:include page="/global/gadgets/addressAddEdit.jsp">
							<dsp:param name="successURL" param="successURL" />
							<dsp:param name="firstLastRequired" value="true" />
							<dsp:param name="formHandlerComponent"
								value="/atg/userprofiling/ProfileFormHandler.editValue" />
							<dsp:param name="restrictionDroplet"
								value="/atg/store/droplet/ShippingRestrictionsDroplet" />
						</dsp:include>
						<div class="form-group"> <!-- Submit Button -->
							<dsp:input bean="ProfileFormHandler.newAddress"
								name="addNewAddress" type="submit" value="Add"
								iclass="primary-btn" />
						</div>
					</dsp:oparam>
				</dsp:droplet>
			
		</dsp:form>
					
					
					
					</div>
					
				
				
				</div>
				<!--  /Product Details -->
			</div>
			<!-- /row -->
		</div>
		<!-- container -->
	</div>
	<!-- section -->
	
	<%@ include file="/includes/footer_v2.jsp"%>
	</html>
	</dsp:oparam>
	</dsp:droplet>
</dsp:page>
