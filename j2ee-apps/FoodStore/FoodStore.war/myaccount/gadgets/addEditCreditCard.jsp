<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<dsp:page>
	<div class="row">
		<dsp:form iclass="form-horizontal">
			<fieldset>
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<legend> Credit Card details </legend>
							<dsp:getvalueof var="nickname"
								bean="ProfileFormHandler.editValue.nickname" />
							<dsp:droplet name="Switch">
								<dsp:param name="value" bean="ProfileFormHandler.addEditMode" />
								<dsp:oparam name="edit">
									<dsp:input type="hidden" name="successURL"
										bean="ProfileFormHandler.updateCardSuccessURL"
										value="creditcards.jsp" />
									<dsp:input type="hidden" name="errorURL"
										bean="ProfileFormHandler.updateCardErrorURL"
										value="newcreditcard.jsp?nickname=${nickname}&addEditMode=edit" />
								</dsp:oparam>
								<dsp:oparam name="default">
									<dsp:input type="hidden" name="successURL"
										bean="ProfileFormHandler.createCardSuccessURL"
										value="creditcards.jsp" />
									<dsp:input type="hidden" name="errorURL"
										bean="ProfileFormHandler.createCardErrorURL"
										value="newcreditcard.jsp" />
								</dsp:oparam>
							</dsp:droplet>


							<!-- Nickname -->
							<div class="form-group">
								<label for="cc_nickname" class="control-label col-md-2 required-field-label">
									Nickname</label>
								<div class="col-md-3">
									<dsp:droplet name="Switch">
										<dsp:param name="value" bean="ProfileFormHandler.addEditMode" />
										<dsp:oparam name="edit">
											<dsp:input type="hidden" value="${nickname }"
												bean="ProfileFormHandler.editValue.nickname"
												iclass="form-control" />
											<dsp:input type="text" id="cc_nickname" value="${nickname }"
												bean="ProfileFormHandler.editValue.newNickname"
												iclass="form-control">
												<dsp:tagAttribute name="required" value="true" />
												<dsp:tagAttribute name="inlineIndicator"
													value="NicknameAlert" />
												<dsp:tagAttribute name="placeholder" value="Nickname" />
											</dsp:input>
										</dsp:oparam>

										<dsp:oparam name="default">
											<dsp:input type="text" id="cc_nickname"
												bean="ProfileFormHandler.editValue.nickname"
												iclass="form-control" >
												<dsp:tagAttribute name="placeholder" value="Unique nickname for the card"/>
											</dsp:input>
										</dsp:oparam>
									</dsp:droplet>
								</div>

								<label for="cc_card_name" class="control-label col-md-3 required-field-label">
									Name on card </label>
								<div class="col-md-3">
									<dsp:input type="text" id="cc_card_name"
										bean="ProfileFormHandler.editValue.nameOnCard"
										iclass="form-control" >
										<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="placeholder" value="Name on the card"/>
									</dsp:input>
								</div>
							</div>

							<!-- Credit card number -->
							<div class="form-group">
								<label for="cc_number" class="control-label col-md-2 required-field-label">
									Card Number </label>
								<div class="col-md-4">
									<dsp:droplet name="Switch">
										<dsp:param name="value" param="creditCardNumberInputType" />
										<dsp:oparam name="readonly">
											<dsp:input type="readonly" id="cc_number"
												bean="ProfileFormHandler.editValue.creditCardNumber"
												iclass="form-control" >
												<dsp:tagAttribute name="required" value="required" />
												<dsp:tagAttribute name="placeholder" value="Credit Card Number"/>
											</dsp:input>
										</dsp:oparam>

										<dsp:oparam name="default">
											<dsp:input type="number" id="cc_number"
												bean="ProfileFormHandler.editValue.creditCardNumber"
												iclass="form-control" >
												<dsp:tagAttribute name="required" value="true" />
												<dsp:tagAttribute name="placeholder" value="Card number" />	
											</dsp:input>
										</dsp:oparam>
									</dsp:droplet>
								</div>

								<label for="cc_type" class="control-label col-md-2">
									Card Type </label>
								<div class="col-md-2">
									<dsp:select id="cc_type"
										bean="ProfileFormHandler.editValue.creditCardType"
										iclass="form-control">
										<dsp:option value="MasterCard">Master Card</dsp:option>
										<dsp:option value="Visa">Visa</dsp:option>
									</dsp:select>
								</div>
							</div>


							<!-- Expiration Date -->
							<div class="form-group">
								<label for="cc_expiry_month" class="control-label col-md-2">
									Expiration Month </label>
								<div class="col-md-1">
									<dsp:select id="cc_expiry_month"
										bean="ProfileFormHandler.editValue.expirationMonth"
										iclass="form-control">
										<dsp:option value="1"> 1 </dsp:option>
										<dsp:option value="2"> 2 </dsp:option>
										<dsp:option value="3"> 3 </dsp:option>
										<dsp:option value="4"> 4 </dsp:option>
										<dsp:option value="5"> 5 </dsp:option>
										<dsp:option value="6"> 6 </dsp:option>
										<dsp:option value="7"> 7 </dsp:option>
										<dsp:option value="8"> 8 </dsp:option>
										<dsp:option value="9"> 9 </dsp:option>
										<dsp:option value="10"> 10 </dsp:option>
										<dsp:option value="11"> 11 </dsp:option>
										<dsp:option value="12"> 12 </dsp:option>
									</dsp:select>
								</div>

								<label for="cc_expiry_year" class="control-label col-md-2 required-field-label">
									Expiration Year </label>
								<div class="col-md-2">
									<dsp:input type="number" id="cc_expiry_year"
										bean="ProfileFormHandler.editValue.expirationYear" iclass="form-control" >
										<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="minlength" value="4"/>
										<dsp:tagAttribute name="maxlength" value="4"/>
										<dsp:tagAttribute name="placeholder" value="yyyy"/>
									</dsp:input>
								</div>
							</div>
							<div class="checkbox" align="right">
								<label> <dsp:input type="checkbox"
										bean="ProfileFormHandler.editValue.newCreditCard" value="true" /><b>Set
										card as default</b>
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<legend> Billing Address Details </legend>

							<div class="form-group">
								<label for="address_firstname" class="control-label col-md-2 required-field-label">
									Firstname </label>
								<div class="col-md-3">
									<dsp:input type="text" id="address_firstname"
										bean="ProfileFormHandler.billingAddress.firstName"
										iclass="form-control" >
										<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="placeholder" value="Firstname"/>
									</dsp:input>
								</div>

								<label for="address_lastname" class="control-label col-md-2">
									Lastname </label>
								<div class="col-md-2">
									<dsp:input type="text" id="address_lastname"
										bean="ProfileFormHandler.billingAddress.lastName" iclass="form-control" >
										<dsp:tagAttribute name="placeholder" value="Lastname"/>
									</dsp:input>
								</div>
							</div>

							<div class="form-group">
								<label for="address_1" class="control-label col-md-2 required-field-label">
									Address line 1 </label>
								<div class="col-md-3">
									<dsp:input type="text" id="address_1"
										bean="ProfileFormHandler.billingAddress.address1"
										iclass="form-control" >
										<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="placeholder" value="Address line 1"/>
									</dsp:input>
								</div>

								<label for="address_2" class="control-label col-md-2">
									Address line 2 </label>
								<div class="col-md-3">
									<dsp:input type="text" id="address_2"
										bean="ProfileFormHandler.billingAddress.address2"
										iclass="form-control" >
										<dsp:tagAttribute name="placeholder" value="Address line 2" />
									</dsp:input>
								</div>
							</div>
							
							<div class="form-group">
									<label for="address_city" class="control-label col-md-2 required-field-label" > City </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_city"
											bean="ProfileFormHandler.billingAddress.city" iclass="form-control" >
											<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*" />
										<dsp:tagAttribute name="title" value="First letter as Capital" />
										<dsp:tagAttribute name="placeholder" value="City"/>	
										</dsp:input>
									</div>
									
									<label for="address_state" class="control-label col-md-2 required-field-label"> State </label>
									<div class="col-md-2">
										<dsp:input type="text" id="address_state"
											bean="ProfileFormHandler.billingAddress.state" iclass="form-control" >
											<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*" />
										<dsp:tagAttribute name="title" value="First letter as Capital" />
										<dsp:tagAttribute name="placeholder" value="State" />
									</dsp:input>
									</div>
									
									<label for="address_country" class="control-label col-md-2 required-field-label"> Country </label>
									<div class="col-md-2">
									<dsp:input type="text" id="address_country"
										bean="ProfileFormHandler.billingAddress.country"
										iclass="form-control">
										<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*" />
										<dsp:tagAttribute name="title" value="First letter as Capital" />
										<dsp:tagAttribute name="placeholder" value="Country" />
									</dsp:input>
								</div>
							</div>

							<div class="form-group">
								<label for="address_pin" class="control-label col-md-2 required-field-label">
									Pin </label>
								<div class="col-md-2">
									<dsp:input type="number" id="address_pin"
										bean="ProfileFormHandler.billingAddress.postalCode"
										iclass="form-control" >
										<dsp:tagAttribute name="maxlength" value="6" />
										<dsp:tagAttribute name="minlength" value="6" />
										<dsp:tagAttribute name="pattern" value="[0-9]{6}" />
										<dsp:tagAttribute name="title" value="Must have 6 digits" />
										<dsp:tagAttribute name="required" value="required" />
										<dsp:tagAttribute name="placeholder" value="xxxxxx" />
									</dsp:input>
								</div>
							</div>
						</div>
					</div>

					<dsp:droplet name="Switch">
						<dsp:param name="value" bean="ProfileFormHandler.addEditMode" />
						<dsp:oparam name="edit">
							<dsp:input id="submit" iclass="primary-btn pull-right disable-submit" value="Update" type="submit"
								name="updateAction" bean="ProfileFormHandler.updateCard" />
						</dsp:oparam>
						<dsp:oparam name="default">
							<dsp:input id="submit" iclass="primary-btn pull-right disable-submit" value="Add" type="submit"
								name="addAction"
								bean="ProfileFormHandler.createNewCreditCardAndAddress" />
						</dsp:oparam>
					</dsp:droplet>
			</fieldset>
		</dsp:form>
	</div>
	
	<script>
		/*$(document).ready(function() {
			$('#submit').attr('disabled', 'disabled');
			$('form').keyup(function() {
				$("#submit").attr('disabled', 'disabled');
				var nickname = $('#cc_nickname').val();
				var cardName = $('#cc_card_name').val();
				var cardNumber = $('#cc_number').val();
				var expiryYear = $('#cc_expiry_year').val();
				var firstname = $('#address_firstname').val();
				var address1 = $('#address_1').val();
				var city = $('#address_city').val();
				var state = $('#address_state').val();
				var country = $('#address_country').val();
				var pin = $('#address_pin').val();
				
				console.log(nickname + " " + cardName + " " + cardNumber + " " + expiryYear + " " + firstname + " " + address1 + " " + city + " " + state + " " + country + " " + pin);
				
				if(nickname && cardName && cardNumber && expiryYear && firstname && address1 && city && state && country && pin) {
					if(pin.length == 6 && expiryYear.length == 4) {
						$('#submit').removeAttr('disabled');	
					}
				}
			});
			
			 $('#submit').click(function() {
				$('#submit').attr('disabled', 'disabled');
			}); 
		});*/
	</script>
</dsp:page>