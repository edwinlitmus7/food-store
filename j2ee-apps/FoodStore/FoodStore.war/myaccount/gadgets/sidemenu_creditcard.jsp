<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
	<dsp:getvalueof var="contextPath" bean="OriginatingRequest.contextPath" />
	<div class="col-md-3">
		<div class="sidemenu well well-lg">
			<h4>My Account</h4>
			<ul>
				<li> <dsp:a href="${contextPath}/myaccount/newcreditcard.jsp"> Add new card </dsp:a> </li>
				<li> <dsp:a href="${contextPath}/myaccount/creditcards.jsp"> View saved cards </dsp:a> </li>
			</ul>
		</div>
	</div>
</dsp:page>