<%-- 
	This page gives the redirection according to the users mode(Add/Edit/Remove).
 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/index.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
		
		<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
		<dsp:importbean bean="/com/foodstore/droplet/profile/creditcard/GetCreditCards" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		
		<%@ include file="/includes/header_v2.jsp"%>
		<%@ include file="/includes/navbar_v2.jsp"%>
		
		<!-- List of credit cards -->
		<div class="section">
		
		<div class="container">
			<div class="row">
				<%@ include file="./gadgets/sidemenu_creditcard.jsp" %>
				<div class="col-md-9">
					<dsp:droplet name="GetCreditCards">
						<dsp:oparam name="outputStart">
							
							<div class="section-title">
								<h2 class="title"> Credit Cards </h2>
							</div>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
						</dsp:oparam>
						<dsp:oparam name="empty">
							<div id="empty-credit-card-message">
								<h4> There are no credit cards linked to this account. </h4>
							</div>
						</dsp:oparam>
						<dsp:oparam name="output">
							<div class="row">
								<%@ include file="./gadgets/creditcard_display.jsp" %>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</div>
		</div>
		</div>
		<%@ include file="/includes/footer_v2.jsp"%>
	<script>
		$(function(){
			if($(".credit-cards-label")[0]){
				var textNode  = document.createTextNode("(Default)");
				$(".credit-cards-label")[0].append(textNode);
			}
		});
	</script>
	</dsp:oparam>
	</dsp:droplet>
</dsp:page>