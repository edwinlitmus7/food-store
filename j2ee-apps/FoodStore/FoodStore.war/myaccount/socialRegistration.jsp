<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/userprofiling/ProfileErrorMessageForEach" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />


	<dsp:include page="/includes/header_v2.jsp"/>
	<dsp:include page="/includes/navbar_v2.jsp"/>
	<div class="section">
	<div class="container">
		<div class="col-xs-6">
		<div class="row">
			<!-- Display form errors to the user  -->
			<dsp:droplet name="Switch">
				<dsp:param bean="ProfileFormHandler.formError" name="value" />
				<dsp:oparam name="true">
					<div class="row">

						<div class="col-xs-7">
							<div class="alert alert-danger">
								<font color=fffcfc><STRONG><UL>
											<dsp:droplet name="ProfileErrorMessageForEach">
												<dsp:param bean="ProfileFormHandler.formExceptions"
													name="exceptions" />
												<dsp:oparam name="output">
													<LI><dsp:valueof param="message" />
												</dsp:oparam>
											</dsp:droplet>
										</UL></STRONG></font>
							</div>
						</div>

					</div>
				</dsp:oparam>
			</dsp:droplet>
			<!-- // End of Display form errors   -->
		</div>
		<div class="row">
			<%-- Show the registration/profile updation form.  --%>
			<dsp:form method="post" id="reg_form" formid="reg_form"
				iclass="form-horizontal">
				<fieldset>
					<legend>
						<dsp:droplet name="Switch">
							<dsp:param name="value" bean="Profile.transient" />
							<dsp:oparam name="true">
								<div class="section-title">
									<h2 class="title"> Registration </h2>
								</div>
      				</dsp:oparam>
							<dsp:oparam name="false">

									<div class="section-title">
										<h2 class="title"> Update profile </h2>
									</div>
      				</dsp:oparam>
						</dsp:droplet>
					</legend>
					<div class="form-group">
						<label for="first_name" class="control-label">First
							Name </label>
						<dsp:input bean="ProfileFormHandler.value.firstName" maxsize="35"
							size="35" id="first_name" type="text" iclass="form-control" >
								<dsp:tagAttribute name="maxlength" value="100"/>
								<dsp:tagAttribute name="required" value="true"/>
						</dsp:input>
					</div>
					<div class="form-group">
						<label for="last_name" class="control-label">Last
							Name </label>
						<dsp:input bean="ProfileFormHandler.value.lastName" maxsize="35"
							size="35" id="last_name" type="text" iclass="form-control" >
								<dsp:tagAttribute name="maxlength" value="100"/>
								<dsp:tagAttribute name="required" value="true"/>
							</dsp:input>
					</div>
					<div class="form-group">
						<label for="login" class=" control-label">login <small>
								(same as email) </small>
						</label>
						<dsp:input bean="ProfileFormHandler.value.login" maxsize="35"
							size="35" id="login" type="email" iclass="form-control" >
							<dsp:tagAttribute name="required" value="true"/>
							<dsp:tagAttribute name="readonly" value="true"/>
							</dsp:input>
					</div>
					<div class="form-group">
						<label for="gender" class="control-label">
							<small>
							Gender
							</small>
						</label>
						<dsp:select bean="ProfileFormHandler.value.gender"
							multiple="false" id="gender" iclass="form-control">
							<dsp:option value="male">male</dsp:option>
							<dsp:option value="female">female</dsp:option>
						</dsp:select>
					</div>
					<div class="form-group">
						<label for="telephone" class=" control-label">
							Telephone </label>
						<dsp:input bean="ProfileFormHandler.value.landphone"
							id="telephone" type="tel" iclass="form-control">
							<dsp:tagAttribute name="pattern" value="^[0-9]{4}-[0-9]{7}$"/>
							<dsp:tagAttribute name="title" value="Phone number should be of format XXXX-XXXXXXX"/>
							</dsp:input>
					</div>
					<div class="form-group">
						<label for="mobile" class=" control-label"> Mobile
						</label>
						<dsp:input bean="ProfileFormHandler.value.mobile" id="mobile"
							type="tel" iclass="form-control" >
								<dsp:tagAttribute name="pattern" value="^+91-[0-9]{8}$"/>
								<dsp:tagAttribute name="required" value="true"/>
								<dsp:tagAttribute name="title" value="Mobile number should be of format +91-XXXXXXXXXX"/>
							</dsp:input>
					</div>
					<dsp:droplet name="Switch">
						<dsp:param name="value" bean="Profile.transient" />
						<dsp:oparam name="true">
							<dsp:input bean="ProfileFormHandler.createSuccessURL"
								type="hidden" value="${contextPath}/myaccount/userDashBoard.jsp" />
							<dsp:input bean="ProfileFormHandler.createErrorURL" type="hidden"
								value="${contextPath}/myaccount/error.jsp" />
							<div class="form-group">
								<label for="password" class=" control-label">
									Password <small>(required for using normal login)</small></label>
								<dsp:input bean="ProfileFormHandler.value.password"
								type="password" value="" id="password" iclass="form-control" />
							</div>
							<div class="form-group">
								<label for="cpassword" class=" control-label">Confirm
									Password </label>
								<dsp:input bean="ProfileFormHandler.value.confirmpassword"
									type="password" value="" id="cpassword" iclass="form-control" />
							</div>
							<dsp:input bean="ProfileFormHandler.token" name="token"
								type="hidden" name="token" />
							<br />
						</dsp:oparam>
						<dsp:oparam name="false">
							<dsp:input bean="ProfileFormHandler.updateSuccessURL"
								type="hidden" value="${contextPath}/myaccount/userDashBoard.jsp" />
							<dsp:input bean="ProfileFormHandler.updateErrorURL" type="hidden"
								value="${contextPath}/myaccount/userDashBoard.jsp" />
							<div class="form-group">
								<label for="password" class=" control-label">
									Password </label>
								<dsp:input bean="ProfileFormHandler.value.password" value=""
									id="password" iclass="form-control" />
							</div>
							<div class="form-group">
								<label for="cpassword" class=" control-label">Confirm
									Password </label>
								<dsp:input bean="ProfileFormHandler.value.confirmpassword"
									value="" id="cpassword" iclass="form-control" />
							</div>
						</dsp:oparam>
					</dsp:droplet>
					<div class="form-group">
						<label for="dob" class=" control-label">Date of
							birth </label>
						<dsp:input bean="ProfileFormHandler.value.dateOfBirth" id="dob"
							type="date" date="yyyy-MM-dd" iclass="form-control" />
					</div>
					<div class="form-group">
						<label for="email" class=" control-label">email</label>
						<dsp:input bean="ProfileFormHandler.value.email" maxsize="35"
							size="35" id="email" type="email" iclass="form-control" >
							<dsp:tagAttribute name="readonly" value="true"/>
							</dsp:input>
					</div>
					<%-- Changes the calling handler according to the current action ie registration
					 will invoke handleCreate and handleUpdate for profile update.
				 	--%>
					<div class="form-group">
						<div class="col-lg-6 col-lg-offset-2">
							<dsp:droplet name="Switch">
								<dsp:param name="value" bean="Profile.transient" />
								<dsp:oparam name="true">
									<dsp:input bean="ProfileFormHandler.create"
										iclass="primary-btn" name="create" type="submit"
										value="Confirm Registration" />
								</dsp:oparam>
								<dsp:oparam name="false">
									<dsp:input iclass="primary-btn"
										bean="ProfileFormHandler.update" name="update" type="submit"
										value="Update profile" />
								</dsp:oparam>
							</dsp:droplet>
						</div>
					</div>
				</fieldset>
			</dsp:form>
		</div>
		</div>
	</div>
	</div>

		<dsp:include page="/includes/footer_v2.jsp"/>

</dsp:page>
