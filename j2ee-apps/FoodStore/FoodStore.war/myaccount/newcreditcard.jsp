<%@ taglib uri="/dspTaglib" prefix="dsp" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<dsp:page>
<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/myaccount/socialLogin.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
	
				<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
				<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
				<dsp:importbean bean="/atg/commerce/catalog/custom/FoodStore"/>
			    <dsp:importbean bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler"/>
				
				<%@ include file="/includes/header_v2.jsp"%>
				<%@ include file="/includes/navbar_v2.jsp"%>
				
				<div class="section">
				
				<!-- List of credit cards -->		
				<div class="container">
					<div class="row">
						<%@ include file="./gadgets/sidemenu_creditcard.jsp"%>
						<div class="col-md-9">
							<%-- Display errors if any --%>
							<dsp:droplet name="Switch">
								<dsp:param bean="ProfileFormHandler.formError" name="value" />
								<dsp:oparam name="true">
									<div class="row">
										
										<div class="col-xs-7">
											<div class="alert alert-danger">
												<font color=fffcfc><STRONG><UL>
															<dsp:droplet name="ErrorMessageForEach">
																<dsp:param bean="ProfileFormHandler.formExceptions"
																	name="exceptions" />
																<dsp:oparam name="output">
																	<LI><dsp:valueof param="message" />
																</dsp:oparam>
															</dsp:droplet>
														</UL></STRONG></font>
											</div>
										</div>
										
									</div>
								</dsp:oparam>
							</dsp:droplet>
							<%@ include file="./gadgets/addEditCreditCard.jsp"%>
						</div>
					</div>
				</div>
				</div>
				
				<%@ include file="/includes/footer_v2.jsp"%>
			
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>