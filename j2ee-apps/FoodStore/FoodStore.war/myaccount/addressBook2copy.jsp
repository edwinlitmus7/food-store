<%--
  Page for managing a user's saved addresses (their address book).
  Displays available addresses for the current profile.
  
  Required parameters:
    None
    
  Optional parameters:
    None 
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url"
					value="${contextPath}/index.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
			<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" href="./../css/style.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</head>

<%@ include file="/includes/navbar.jsp"%>
<body>
	<div class="container-fluid">
		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
		<dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst" />
		<dsp:setvalue param="successURL"
			beanvalue="/OriginatingRequest.requestURI" />

		<!-- Get Profile's default address -->
		<dsp:getvalueof var="defaultAddress" bean="Profile.shippingAddress" />
		<dsp:getvalueof id="requestURL" idtype="java.lang.String"
			bean="/OriginatingRequest.requestURI" />



		<!--  Iterate through all this user's shipping addresses, sorting the array so that the
            default shipping address is first.
            
            Input parameters:
              defaultId
                repository Id of item that will be the first in the array
              map
                Map of repository items that will be converted into array
              sortByKeys
                returning array will be sorted by keys (address nicknames)
                
            Output parameters:
              sortedArray
                array of sorted profile addresses     -->

		<dsp:droplet name="MapToArrayDefaultFirst">
			<dsp:param name="defaultId" value="${defaultAddress.repositoryId}" />
			<dsp:param name="map" bean="Profile.secondaryAddresses" />
			<dsp:param name="sortByKeys" value="true" />


			<!-- In case of no address to display -->
			<dsp:oparam name="empty">
				<div align="center">
					<h4>There are no Addresses linked to this account</h4>
					<p>Use the + sign to Add New Address</p>
				</div>
			</dsp:oparam>

			<dsp:oparam name="output">
				<dsp:getvalueof var="sortedArray" vartype="java.lang.Object"
					param="sortedArray" />

				<!-- Iterate over the array of addresses and display address information
                and edit/remove links  -->

				<dsp:droplet name="ForEach">
					<dsp:param name="array" param="sortedArray" />
					<dsp:setvalue param="shippingAddress" paramvalue="element" />

					<!-- Display address details -->
					<dsp:oparam name="output">
						<div class="container-fluid">
							<div class="row">
								<div class="col-xs-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<dsp:valueof param="shippingAddress.value.nickname"></dsp:valueof>
											-
											<dsp:valueof param="shippingAddress.value.typeOfAddress"></dsp:valueof>
										</div>
										<div class="panel-body" align="left">
											<!-- Address information -->
											<dsp:include page="/global/util/displayAddress.jsp">
												<dsp:param name="address" param="shippingAddress.value" />
												<dsp:param name="private" value="false" />
											</dsp:include>
										</div>
										<!-- Display Edit/Remove links -->
										<div class="panel-footer" align="right">

											<!-- 'Edit' link -->
											<h8> <dsp:a title="Edit Address"
												page="/myaccount/gadgets/addressEdit.jsp"
												iclass="glyphicon glyphicon-pencil">
												<dsp:param name="successURL"
													bean="/OriginatingRequest.requestURI" />
												<dsp:param name="addressKey" param="shippingAddress.key" />
												<dsp:param name="addEditMode" value="Edit" />
											</dsp:a></h8>

											<!-- 'Remove' link -->
											<h8> <dsp:a title="Remove Address"
												bean="ProfileFormHandler.removeAddress"
												page="/myaccount/addressBook.jsp"
												iclass="glyphicon glyphicon-trash" value="">
												<dsp:param name="successURL" param="successURL" />
												<dsp:param name="removeAddressType"
													param="shippingAddress.key" />
											</dsp:a></h8>

										</div>
									</div>
								</div>
							</div>
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</dsp:oparam>
		</dsp:droplet>
		<div class="row">
			<div class="col-xs-6" align="right">
				<h3>
					<dsp:a title="New Address"
						page="/myaccount/gadgets/addressEdit.jsp"
						iclass="glyphicon glyphicon-plus-sign">
						<dsp:param name="successURL" bean="/OriginatingRequest.requestURI" />
						<dsp:param name="addEditMode" value="Add" />
					</dsp:a>
				</h3>
			</div>
		</div>
	</div>
</body>
<%@ include file="/includes/footer.jsp"%>
			</html>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>