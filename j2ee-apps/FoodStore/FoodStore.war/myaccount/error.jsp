<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	
	<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/myaccount/userDashBoard.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="true">
	<%-- //End of redirection code --%>
			
			<html>
			<head>
				<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
				<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
				<!-- Latest compiled JavaScript -->
				<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
				<link rel="stylesheet" href="${contextPath}/css/style.css">
			</head>
			<body>
			<%@ include file="../includes/navbar.jsp"%>
			<div class="container">
				<div class="row">
				<!-- Display form errors to the user  -->
				<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
				<dsp:importbean bean="/atg/userprofiling/ProfileErrorMessageForEach" />
				<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
				<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
				<dsp:droplet name="Switch">

					<%-- Iterate the errors --%>
					<dsp:param bean="ProfileFormHandler.formError" name="value" />
					<dsp:oparam name="true">
						<div class="alert alert-dismissible alert-danger">
						<h1 class="header center black-text"> Please correct these errors.</h1>
  						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<UL>
							<dsp:droplet name="ProfileErrorMessageForEach">
								<dsp:param bean="ProfileFormHandler.formExceptions"
									name="exceptions" />
								<dsp:oparam name="output">
									<LI><dsp:valueof param="message" /></LI>
								</dsp:oparam>
							</dsp:droplet>
						</UL>
						</div>
					</dsp:oparam>
				</dsp:droplet>	
				<!-- // End of Display form errors   -->
				<a href="${contextPath}/myaccount/socialLogin.jsp"> Try again</a>
				</div>
			</div>
			<%@ include file="/includes/footer.jsp"%>
			</body>
			</html>
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>

