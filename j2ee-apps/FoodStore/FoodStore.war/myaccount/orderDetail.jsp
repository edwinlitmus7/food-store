<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>

  <dsp:importbean bean="/atg/commerce/order/OrderLookup"/>
  <dsp:importbean bean="/atg/core/i18n/LocaleTools"/>
  <dsp:importbean bean="/atg/dynamo/droplet/ForEach"/>
  <%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<dsp:include page="/includes/header_v2.jsp">
	</dsp:include>
	<dsp:include page="/includes/navbar_v2.jsp">
	</dsp:include>
	
	<div class="section">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<dsp:droplet name="OrderLookup">
					<dsp:param name="orderId" param="orderId" />

					<dsp:oparam name="error">

						<div class="alert alert-danger">
							<b class="text-center">
								Invalid orderId
							</b>
						</div>

					</dsp:oparam>

					<dsp:oparam name="output">
						<dsp:getvalueof var="order" param="result" />
						<div class="panel panel-default">
							<div class="panel-heading">
								<div class="row">
									<div class="col-md-4">
										<strong> Order submitted on </strong>
										<dsp:getvalueof var="dateFormat"
											bean="LocaleTools.userFormattingLocaleHelper.datePatterns.shortWith4DigitYear" />
										<fmt:formatDate value="${order.submittedDate}"
											pattern="${dateFormat}" />
									</div>

									<div class="col-md-3">Number of items:
										${fn:length(order.commerceItems)}</div>

									<div class="col-md-2">
										<dsp:include page="/global/gadgets/formattedPrice.jsp">
											<dsp:param name="price" value="${order.priceInfo.amount}" />
										</dsp:include>
									</div>
									<div class="col-md-3">
										order #<b>${order.id }</b>
									</div>
								</div>
							</div>
							<div class="panel-body">
								<c:forEach var="items" items="${order.commerceItems}">
									<dsp:param name="item" value="${items }" />
									<dsp:getvalueof var="item" value="${items }" />
									
									<div class="row">
										<div class="col-md-3">
											<a href="#">
												<img src='${contextPath }/<dsp:valueof param="item.auxiliaryData.catalogRef.smallImage.url" />' />
											</a>
										</div>
										<div class="col-md-4">
											<p>
												<dsp:valueof
													param="item.auxiliaryData.catalogRef.displayName" />
											</p>
											<dsp:include page="/global/gadgets/formattedPrice.jsp">
												<dsp:param name="price" value="${item.priceInfo.amount}" />
											</dsp:include>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</div>
		</div>
	</div>
	</div>
	<dsp:include page="/includes/footer_v2.jsp"/>
</dsp:page>

<%-- @version $Id: //hosting-blueprint/B2CBlueprint/version/11.2/Storefront/j2ee/store.war/myaccount/orderDetail.jsp#2 $$Change: 953229 $--%>