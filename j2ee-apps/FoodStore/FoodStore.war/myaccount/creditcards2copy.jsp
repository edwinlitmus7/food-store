<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<dsp:page>
<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/index.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
		
	<head>
		<head>
			<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
			<link rel="stylesheet" href="./../css/style.css">
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		</head>
		
		<title> Credit Cards </title>
	</head>
	<body>
		<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler"/>
		
		<dsp:importbean bean="/com/foodstore/droplet/profile/creditcard/GetCreditCards" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		
		<%@ include file="./../includes/navbar.jsp"%>
		
		<!-- List of credit cards -->		
		<div class="container">
			<div class="row">
				<%@ include file="./gadgets/sidemenu_creditcard.jsp" %>
				<div class="col-md-9">
					<dsp:droplet name="GetCreditCards">
						<dsp:oparam name="outputStart">
							<h4> Credit Cards </h4>
						</dsp:oparam>
						<dsp:oparam name="outputEnd">
						</dsp:oparam>
						<dsp:oparam name="empty">
							<div id="empty-credit-card-message">
								<h4> There are no credit cards linked to this account. </h4>
							</div>
						</dsp:oparam>
						<dsp:oparam name="output">
							<div class="row">
								<%@ include file="./gadgets/creditcard_display.jsp" %>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</div>
			</div>
		</div>
		
		<%@ include file="./../includes/footer.jsp"%>
		
		
		<!-- Latest compiled JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		
		<script src="./../js/script.js"></script>
	</body>
	</dsp:oparam>
	</dsp:droplet>
</dsp:page>