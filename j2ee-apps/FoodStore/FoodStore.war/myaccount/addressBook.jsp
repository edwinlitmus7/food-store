<%--
  Page for managing a user's saved addresses (their address book).
  Displays available addresses for the current profile.
  
  Required parameters:
    None
    
  Optional parameters:
    None 
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url"
					value="${contextPath}/index.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">
			
		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
		<dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst" />
		<dsp:setvalue param="successURL"
			beanvalue="/OriginatingRequest.requestURI" />

		<!-- Get Profile's default address -->
		<dsp:getvalueof var="defaultAddress" bean="Profile.shippingAddress" />
		<dsp:getvalueof id="requestURL" idtype="java.lang.String"
			bean="/OriginatingRequest.requestURI" />

		
			<%@ include file="/includes/header_v2.jsp"%>
			<%@ include file="/includes/navbar_v2.jsp"%>
			<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!--  Product Details -->
				<div class="product product-details clearfix">
					<!-- section title -->
												<div class="col-md-12">
													<div class="section-title">
														<h2 class="title"> Address Book </h2>
													</div>
												</div>
												<!-- section title -->
												
					
					<div class="col-md-12">
						<div class="product-body">

		<!--  Iterate through all this user's shipping addresses, sorting the array so that the
            default shipping address is first.
            
            Input parameters:
              defaultId
                repository Id of item that will be the first in the array
              map
                Map of repository items that will be converted into array
              sortByKeys
                returning array will be sorted by keys (address nicknames)
                
            Output parameters:
              sortedArray
                array of sorted profile addresses     -->

		<dsp:droplet name="MapToArrayDefaultFirst">
			<dsp:param name="defaultId" value="${defaultAddress.repositoryId}" />
			<dsp:param name="map" bean="Profile.secondaryAddresses" />
			<dsp:param name="sortByKeys" value="true" />


			<!-- In case of no address to display -->
			<dsp:oparam name="empty">
				<div align="center">
					<h4>There are no Addresses linked to this account</h4>
					<p>Use the + sign to Add New Address</p>
				</div>
			</dsp:oparam>

			<dsp:oparam name="output">
				<dsp:getvalueof var="sortedArray" vartype="java.lang.Object"
					param="sortedArray" />

				<!-- Iterate over the array of addresses and display address information
                and edit/remove links  -->

				<dsp:droplet name="ForEach">
					<dsp:param name="array" param="sortedArray" />
					<dsp:setvalue param="shippingAddress" paramvalue="element" />

					<!-- Display address details -->
					<dsp:oparam name="output">
					
					<!-- Product Single -->
				<div class="col-md-6 col-sm-12 col-xs-12">
					<div class="product product-single product-hot">
						<div class="product-thumb">
							<div class="product-label addressLabel">
								<span class="sale">
									<dsp:valueof param="shippingAddress.value.typeOfAddress"/>
								</span>
							</div>
							
								
						</div>
						<div class="product-body">
							
							<h2 class="product-name"><a href="#"><dsp:valueof param="shippingAddress.value.nickname"/></a></h2>
							<p>
										<!-- Address information -->
											<dsp:include page="/global/util/displayAddress.jsp">
												<dsp:param name="address" param="shippingAddress.value" />
												<dsp:param name="private" value="false" />
											</dsp:include>
							</p>
							<div class="product-btns">
									
										<dsp:a title="Edit Address"
												page="/myaccount/gadgets/addressEdit.jsp"
												iclass="fa fa-edit">
												<dsp:param name="successURL"
													bean="/OriginatingRequest.requestURI" />
												<dsp:param name="addressKey" param="shippingAddress.key" />
												<dsp:param name="addEditMode" value="Edit" />
											</dsp:a> 
											&nbsp;
											<dsp:a title="Remove Address"
												bean="ProfileFormHandler.removeAddress"
												page="/myaccount/addressBook.jsp"
												iclass="fa fa-trash" value="">
												<dsp:param name="successURL" param="successURL" />
												<dsp:param name="removeAddressType"
													param="shippingAddress.key" />
											</dsp:a>
											
								</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->
					
					
						
										
											
								
					</dsp:oparam>
					
				</dsp:droplet>
				
			</dsp:oparam>
			
		</dsp:droplet>
		<div class="row">
			<div class="col-xs-6" align="right">
				<h3>
					<dsp:a title="New Address"
						page="/myaccount/gadgets/addressEdit.jsp"
						iclass="fa fa-plus">
						<dsp:param name="successURL" bean="/OriginatingRequest.requestURI" />
						<dsp:param name="addEditMode" value="Add" />
					</dsp:a>
				</h3>
			</div>
		</div>
		
		</div>
		</div>
		</div>
		</div>
		</div>
		
		</div>
		

		</dsp:oparam>
		
	</dsp:droplet>
	<dsp:include page="/includes/footer_v2.jsp"></dsp:include>
	<script>
		$(function(){
			if($(".addressLabel")[0]){
				var span = document.createElement("span");
				span.innerHTML = "Default";
				$(".addressLabel")[0].append(span);
			}
		});
	</script>
</dsp:page>