<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />


	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />

	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<%-- Redirect if transient profile --%>
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="true">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url" value="${contextPath}/index.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="false">

			<dsp:include page="/includes/header_v2.jsp" />
			<dsp:include page="/includes/navbar_v2.jsp" />
			<div class="section">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!--  Product Details -->
						<div class="product product-details clearfix">

							<div class="col-md-12">
								<div class="product-body">



									<%-- Droplet call to display the profile values to the dsp page --%>
									<div class="row profile">
										<div class="col-md-3">
											<div class="profile-sidebar">
												<!-- SIDEBAR USERPIC -->
												<div class="profile-userpic">
													<img
														src="http://keenthemes.com/preview/metronic/theme/assets/admin/pages/media/profile/profile_user.jpg"
														class="img-responsive" alt="">
												</div>
												<!-- END SIDEBAR USERPIC -->
												<!-- SIDEBAR USER TITLE -->
												<div class="profile-usertitle">
													<div class="profile-usertitle-name">
														<dsp:valueof bean="Profile.firstName" />
														&nbsp;
														<dsp:valueof bean="Profile.lastName" />
													</div>
													<div class="profile-usertitle-job">
														(<small><b><dsp:valueof bean="Profile.login" />
														</b></small>)
													</div>
												</div>
												<!-- END SIDEBAR USER TITLE -->

												<!-- END SIDEBAR BUTTONS -->
												<!-- SIDEBAR MENU -->
												<div class="profile-usermenu">
													<ul class="nav">
														<li class="active"><a href="#"> <i
																class="fa fa-user"></i> Overview
														</a></li>
														<li><a
															href="${contextPath}/myaccount/socialRegistration.jsp">

																<i class="fa fa-edit"></i> Update profile
														</a></li>

														<li><a
															href="${contextPath}/myaccount/addressBook.jsp"> <i
																class="fa fa-address-book-o"></i> Address Book
														</a></li>
														<li><a
															href="${contextPath}/myaccount/creditcards.jsp"> <i
																class="fa fa-credit-card"></i> Saved Cards
														</a></li>
														<li><a href="${contextPath}/myaccount/myOrders.jsp">
																<i class="fa fa-cart-arrow-down"></i> My Orders
														</a></li>


													</ul>
												</div>
												<!-- END MENU -->
											</div>
										</div>
										<div class="col-md-9">
											<div class="profile-content">
												<div class="section-title">
													<h2 class="title">Profile</h2>
												</div>


												<div class="form-group">
													<label for="userId" class="col-sm-4 control-label">
														user ID : </label>
													<div class="col-sm-8">
														<input class="form-control" id="userId"
															placeholder="<dsp:valueof
										bean="Profile.userId" />"
															type="text" disabled>
													</div>
												</div>


												<div class="form-group">
													<label for="gender" class="col-sm-4 control-label">Gender
														: </label>
													<div class="col-sm-8">
														<input class="form-control" id="gender"
															placeholder="<dsp:valueof
										bean="Profile.gender" />"
															type="text" disabled>
													</div>
												</div>

												<div class="form-group">
													<label for="landphone" class="col-sm-4 control-label">Landline
														Number : </label>
													<div class="col-sm-8">
														<input class="form-control" id="landphone"
															placeholder="<dsp:valueof
										bean="Profile.landphone" />"
															type="text" disabled>
													</div>
												</div>

												<div class="form-group">
													<label for="dateOfBirth" class="col-sm-4 control-label">Date
														of birth : </label>
													<div class="col-sm-8">
														<input class="form-control" id="dateOfBirth"
															placeholder="<dsp:valueof
										bean="Profile.dateOfBirth" />"
															type="text" disabled>
													</div>
												</div>

												<div class="form-group">
													<label for="mobile" class="col-sm-4 control-label">Mobile
														Number : </label>
													<div class="col-sm-8">
														<input class="form-control" id="mobile"
															placeholder="<dsp:valueof
										bean="Profile.mobile" />"
															type="text" disabled>
													</div>
												</div>


											</div>
										</div>
									</div>

								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
			<dsp:include page="/includes/footer_v2.jsp" />
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
