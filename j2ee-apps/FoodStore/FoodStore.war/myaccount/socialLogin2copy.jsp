<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />

	<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url"
					value="${contextPath}/myaccount/userDashBoard.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="true">
			<html>
<head>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<link rel="stylesheet" href="${contextPath}/css/style.css">
<meta name="google-signin-client_id"
	content="642167561006-qi2sb3n02su1d4csld6cjp2ussecjnfd.apps.googleusercontent.com">
</head>
<body>
	<%@ include file="/includes/navbar.jsp"%>
	<div class="container">
		<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
		<div class="row">
				<%-- Display errors if any --%>
							<dsp:droplet name="Switch">
								<dsp:param bean="ProfileFormHandler.formError" name="value" />
								<dsp:oparam name="true">
									<div class="row">
										<div class="col-xs-4"></div>
										<div class="col-xs-4">
											<div class="alert alert-danger">
												<font color=fffcfc><STRONG><UL>
															<dsp:droplet name="ErrorMessageForEach">
																<dsp:param bean="ProfileFormHandler.formExceptions"
																	name="exceptions" />
																<dsp:oparam name="output">
																	<LI><dsp:valueof param="message" />
																</dsp:oparam>
															</dsp:droplet>
														</UL></STRONG></font>
											</div>
										</div>
										<div class="col-xs-4"></div>
									</div>
								</dsp:oparam>
							</dsp:droplet>
		</div>
		<div id="signInOptions" class="row">
			
			<div id="normalLogin" class="col-sm-6">
				<%-- Login form for returning customer --%>
				<dsp:include page="gadgets/login.jsp" />
			</div>
		
</div>
</div>


		<%-- Dummy login form to send data to the formHandler --%>
		<dsp:form method="post" id="dummy_form" formid="dummy_form">
			<dsp:input bean="ProfileFormHandler.loginErrorURL" type="hidden"
				value="${contextPath}/myaccount/socialRegistration.jsp" />
			<dsp:input bean="ProfileFormHandler.loginSuccessURL" type="hidden"
				value="${contextPath}/myaccount/userDashBoard.jsp" />
			<dsp:input bean="ProfileFormHandler.token" name="token" />
			<br />
			<dsp:input bean="ProfileFormHandler.login" id="submit_login" type="submit"
				value="Login" />
		</dsp:form>
		<%--// End of dummy login form --%>
	
	<%@ include file="/includes/footer.jsp"%>
	<!-- javascript -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://apis.google.com/js/platform.js" async defer></script>
	<script>		
					
					
					// Script for handling the login form 
					document.getElementById("dummy_form").style.display = "none"; // make the form hidden 
					//document.getElementById("googleSignInButton").style.display = "none";
					function onSignIn(googleUser) {
						
						// The ID token you need to pass to your backend:
						var id_token = googleUser.getAuthResponse().id_token;
						console.log("ID Token: " + id_token);
						document.getElementsByName("token")[0].value = id_token;
						//Hide the sign in button of google display the options
						document.getElementById("all_signin_forms").style.display = "none";
						document.getElementById("processing_signin").style.display = "block";
						atgSignIn();
						googleSessionSignOut();
					}
					function onFailure(error){
						activeSessionOngoogle = false;
						console.log(error);
						document.getElementById("all_signin_forms").style.display = "block";
						document.getElementById("processing_signin").style.display = "none";
						document.getElementById("googleSignInButton").style.display = "block";
						
					}
					function atgSignIn(){
						$("#submit_login").click();
					}
				</script>
	
</body>
			</html>
		</dsp:oparam>
	</dsp:droplet>

</dsp:page>

