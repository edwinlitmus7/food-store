<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
	<%-- Gets the context to use it in the links --%>
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />

	<%-- Redirect if transient profile --%>
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:droplet name="Switch">
		<dsp:param name="value" bean="Profile.transient" />
		<dsp:oparam name="false">
			<dsp:droplet name="/atg/dynamo/droplet/Redirect">
				<dsp:param name="url"
					value="${contextPath}/myaccount/userDashBoard.jsp" />
			</dsp:droplet>
		</dsp:oparam>
		<dsp:oparam name="true">

	<%@ include file="/includes/header_v2.jsp"%>
	<%@ include file="/includes/navbar_v2.jsp"%>
	
		<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />
		<dsp:importbean bean="/atg/userprofiling/Profile" />
		<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
		<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
		<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />
		<div class="section">
		<!-- container -->
		<div class="container">
		<div class="row">
				<%-- Display errors if any --%>
							<dsp:droplet name="Switch">
								<dsp:param bean="ProfileFormHandler.formError" name="value" />
								<dsp:oparam name="true">
									<div class="row">
										<div class="col-sm-12 col-md-6">
											<div class="alert alert-danger">
												<font color=fffcfc><STRONG><UL>
															<dsp:droplet name="ErrorMessageForEach">
																<dsp:param bean="ProfileFormHandler.formExceptions"
																	name="exceptions" />
																<dsp:oparam name="output">
																	<LI><dsp:valueof param="message" />
																</dsp:oparam>
															</dsp:droplet>
														</UL></STRONG></font>
											</div>
										</div>
										
									</div>
								</dsp:oparam>
							</dsp:droplet>
		</div>
		<div id="signInOptions" class="row">
			<div class="section-title">
						<h2 class="title"> Login </h2>
			</div>
			<div id="normalLogin" class="col-sm-6">
				<%-- Login form for returning customer --%>
				<dsp:include page="gadgets/login.jsp" />
			</div>
		
</div>
</div>
</div>


		
	
	<%@ include file="/includes/footer_v2.jsp"%>
	

		</dsp:oparam>
	</dsp:droplet>
	
</dsp:page>

