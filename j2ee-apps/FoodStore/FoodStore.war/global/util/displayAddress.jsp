<%-- 
	This page simply displays the user's profile address details
 --%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="com/components/DefaultAddressDroplet" />
	<dsp:importbean bean="/atg/commerce/util/MapToArrayDefaultFirst" />

		<dsp:getvalueof var="address" param="address" />
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-3">
					<ul class="list-unstyled">
						<li><b> <dsp:valueof param="address.firstName"></dsp:valueof>
								<dsp:valueof param="address.lastName"></dsp:valueof>
						</b></li>
					</ul>
				</div>
				<div class="col-xs-4">
					<ul class="list-unstyled">
						<li><dsp:valueof param="address.address1"></dsp:valueof></li>

						<li><dsp:valueof param="address.address2"></dsp:valueof></li>

						<li><dsp:valueof param="address.address3"></dsp:valueof></li>

						<li><dsp:valueof param="address.city"></dsp:valueof></li>

						<li><dsp:valueof param="address.state"></dsp:valueof></li>

						<li><dsp:valueof param="address.postalCode"></dsp:valueof></li>

						<li><dsp:valueof param="address.country"></dsp:valueof></li>
					</ul>
				</div>
				<div class="col-xs-4">
					<ul class="list-unstyled">
						<li><dsp:valueof param="address.phoneNumber"></dsp:valueof></li>

						<li><dsp:valueof param="address.mobile"></dsp:valueof></li>

						<li><dsp:valueof param="address.faxNumber"></dsp:valueof></li>
					</ul>
				</div>
			</div>
		</div>
	
</dsp:page>