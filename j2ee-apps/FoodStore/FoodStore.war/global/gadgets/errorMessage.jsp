<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ErrorMessageForEach" />

	<%--
    This droplet iterates over all form handler's exceptions.

    Input parameters:
      exceptions
        Collection of form handler's exceptions.

    Output parameters:
      message
        Current exception's message.

    Open parameters:
      outputStart
        Rendered before iteration is started.
      output
        Rendered for each iteration row.
      outputEnd
        Rendered after iteration is done.
  --%>
	<dsp:droplet name="ErrorMessageForEach">
		<dsp:param param="formHandler.formExceptions" name="exceptions" />
		<dsp:oparam name="outputStart">
		</dsp:oparam>
		<dsp:oparam name="output">
			<p style="color: red;">
				<dsp:valueof param="message" valueishtml="true" />
			</p>
		</dsp:oparam>
		<dsp:oparam name="outputEnd">
		</dsp:oparam>
	</dsp:droplet>
</dsp:page>