<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<dsp:page>
	<dsp:getvalueof var="isCurrent" param="isCurrent" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<c:if test="${empty isCurrent}">
		<c:set var="isCurrent" value="false" />
	</c:if>

	<dsp:getvalueof var="shippingGroups" vartype="java.util.Collection" param="order.shippingGroups"/>

	<!-- section title -->
 	
	<div class="panel panel-default col-xs-12">
		<div class="panel-heading">
			<h4> Shipping Address </h4>
		</div>
		<dsp:droplet name="ForEach">
			<dsp:param name="array" param="order.shippingGroups" />
			<dsp:oparam name="output">
				<dsp:getvalueof var="shippingGroup" vartype="java.lang.Object" param="element" />
				<div class="panel-body">
					<dsp:include page="/global/gadgets/orderSingleShippingInfo.jsp">
						<dsp:param name="isCurrent" value="${isCurrent}" />
						<dsp:param name="shippingAddress" value="${shippingGroup.shippingAddress}" />
					</dsp:include>
				</div>
			</dsp:oparam>
		</dsp:droplet>
	</div>


	<%-- Display payment information here. --%>
	<dsp:include page="/checkout/gadgets/confirmPaymentOptions.jsp">
		<dsp:param name="isCurrent" value="${isCurrent}" />
		<dsp:param name="order" param="order" />
		<dsp:param name="expressCheckout" param="expressCheckout" />
	</dsp:include>

      <div>
        <dsp:getvalueof var="commerceItems" vartype="java.lang.Object" param="order.commerceItems"/>
        <c:choose>
          <c:when test="${not empty commerceItems}">
            <%-- We've display shipping group info already, just display shipping group's items. --%>
            <dsp:getvalueof id="size" value="${fn:length(commerceItems)}"/>

           
              <%-- Do not display availability messages for commerce items in the submitted order case. --%>
              <dsp:include page="/global/gadgets/orderItems.jsp">
                <dsp:param name="order" param="order"/>
                <dsp:param name="commerceItems" value="${commerceItems}"/>
                <dsp:param name="displayProductAsLink" param="displayProductAsLink"/>
                <dsp:param name="displayAvailabilityMessage" value="${isCurrent}"/>
              </dsp:include>
           
          </c:when>
        </c:choose>
      </div>
</dsp:page>
