<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
	<tr>
		<dsp:droplet name="ForEach">
			<dsp:param name="array" param="currentItem.priceInfo.currentPriceDetails" />
			<dsp:param name="elementName" value="detail" />
			<dsp:oparam name="output">
				<%-- <dsp:include page="/cart/gadgets/cartItemImage.jsp">
					<dsp:param name="commerceItem" param="currentItem" />
				</dsp:include> --%>
				
				<%-- Display item-related info. --%>
				<dsp:getvalueof var="productDisplayName"
					param="currentItem.auxiliaryData.catalogRef.displayName" />
				<td colspan="2">
					<a href="#">
						<img src='${contextPath }/<dsp:valueof param="currentItem.auxiliaryData.catalogRef.smallImage.url" />' style="width:105px;height:105px;margin:5px" alt="${currentItem.auxiliaryData.productRef.displayName}" />
					</a>
					<dsp:valueof value="${productDisplayName}" />
			
					<%-- Render SKU-related properties (like color/size/finish). --%>
					<%-- <dsp:include page="/global/util/displaySkuProperties.jsp">
						<dsp:param name="product" param="currentItem.auxiliaryData.productRef" />
						<dsp:param name="sku" param="currentItem.auxiliaryData.catalogRef" />
						<dsp:param name="displayAvailabilityMessage"
							param="displayAvailabilityMessage" />
					</dsp:include> --%>
				</td>
				<td>
					<dsp:valueof param="currentItem.quantity" />
				</td>
				<td>
					<dsp:valueof param="currentItem.priceInfo.listPrice"></dsp:valueof>
				<%--	<dsp:droplet name="ForEach" >
						<dsp:param name="array" param="detail.adjustments" />
						<dsp:param name="elementName" value="adjustment" />
						<dsp:oparam name="output">
							<dsp:valueof converter="currency" param="adjustment.adjustment"> - </dsp:valueof>
						</dsp:oparam>
					</dsp:droplet>  --%>
				</td>
				<td>
					<dsp:valueof param="currentItem.priceInfo.amount" />
				</td>
				
			</dsp:oparam>
		</dsp:droplet>
	</tr>
</dsp:page>