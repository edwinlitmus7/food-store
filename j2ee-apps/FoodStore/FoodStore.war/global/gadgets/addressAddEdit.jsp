
<%--
	This page gives the fields for Adding and Editing user 
	Profile address details.
 --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>
<dsp:page>

	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/userprofiling/ProfileFormHandler" />

	<dsp:getvalueof var="formHandlerComponent" param="formHandlerComponent" />
	<dsp:param name="map" bean="Profile.secondaryAddresses" />

	<dsp:getvalueof var="addressKey" param="addressKey"></dsp:getvalueof>
	<dsp:param name="address" param="map.${addressKey }" />

	
	
			<div class="form-group">
				<dsp:input id="nickname" type="hidden"
					bean="${formHandlerComponent}.oldNickname"
					paramvalue="address.nickname" />
				<label for="nickName" class="control-label">Nick
					Name</label>
					<dsp:input id="newNickName" type="text"
						bean="${formHandlerComponent}.nickname" maxlength="40"
						paramvalue="address.nickName">
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="inlineIndicator" value="NicknameAlert" />
						<dsp:tagAttribute name="placeholder" value="Nickname" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
				</div>
			
			<div class="form-group">
				<label for="firstName" class="control-label">First
					Name</label>
					<dsp:input id="firstName" type="text"
						bean="${formHandlerComponent}.firstName" maxlength="40"
						paramvalue="address.firstName">
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="placeholder" value="First Name" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			<div class="form-group">
				<label for="LastName" class="control-label">Last
					Name</label>
				
					<dsp:input id="LastName" type="text"
						bean="${formHandlerComponent}.lastName" maxlength="40"
						paramvalue="address.lastName">
						<dsp:tagAttribute name="placeholder" value="Last Name" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			<div class="form-group">
				<label for="select" class="control-label">Type Of
					Address</label>
					<dsp:select bean="${formHandlerComponent}.typeOfAddress"
						id="typeOfAddress" iclass="form-control">
						<dsp:option value="Home">Home</dsp:option>
						<dsp:option value="Office">Office</dsp:option>
					</dsp:select>
				</div>
			
			<div class="form-group">
				<label for="address1" class="control-label">Address
					Line 1</label>
					<dsp:input id="address1" type="text"
						bean="${formHandlerComponent}.address1" maxlength="50"
						paramvalue="address.address1">
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="placeholder" value="Street address, P.O. box, company name, c/o" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			<div class="form-group">
				<label for="address2" class="control-label">Address
					Line 2</label>
					<dsp:input id="address2" type="text"
						bean="${formHandlerComponent}.address2" maxlength="50"
						paramvalue="address.address2">
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="placeholder" value="Apartment, suite, unit, building, floor, etc." />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			<div class="form-group">
				<label for="address3" class="control-label">Address
					Line 3</label>
					<dsp:input id="address3" type="text"
						bean="${formHandlerComponent}.address3" maxlength="50"
						paramvalue="address.address3">
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="placeholder" value="Address Line 3" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
		
		
			<div class="form-group">
				<label for="city" class="control-label">City</label>
					<dsp:input id="city" type="text"
						bean="${formHandlerComponent}.city" maxlength="30"
						paramvalue="address.city">
						<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*"/>
						<dsp:tagAttribute name="title" value="First letter as Capital"/>
						<dsp:tagAttribute name="placeholder" value="City Name" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			
			<div class="form-group">
				<label for="state" class="control-label">State</label>
					<dsp:input id="state" type="text"
						bean="${formHandlerComponent}.state" maxlength="30"
						paramvalue="address.state">
						<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*"/>
						<dsp:tagAttribute name="title" value="First letter as Capital"/>
						<dsp:tagAttribute name="placeholder" value="Kerala" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			
			<div class="form-group">
				<label for="postalCode" class="control-label">Postal
					Code</label>
					<dsp:input id="postalCode" type="text"
						bean="${formHandlerComponent}.postalCode" maxlength="10"
						iclass="required" paramvalue="address.postalCode">
						<dsp:tagAttribute name="pattern" value="[0-9]{6}" />
						<dsp:tagAttribute name="title" value="Must have 6 digits"/>
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="placeholder" value="689541" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			
			<div class="form-group">
				<label for="country" class="control-label">Country</label>
					<dsp:input id="country" type="text"
						bean="${formHandlerComponent}.country" maxlength="30"
						paramvalue="address.country">
						<dsp:tagAttribute name="pattern" value="[A-Z]{1}[a-z]*"/>
						<dsp:tagAttribute name="title" value="First letter as Capital"/>
						<dsp:tagAttribute name="placeholder" value="India" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			
			<div class="form-group">
				<label for="phoneNumber" class="control-label">Telephone</label>
					<dsp:input id="phoneNumber" type="text"
						bean="${formHandlerComponent}.phoneNumber" maxlength="15"
						paramvalue="address.phoneNumber">
						<dsp:tagAttribute name="pattern" value="[0-9]{4}[-][0-9]{7}" />
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="title" value="XXXX-XXXXXXX" />
						<dsp:tagAttribute name="placeholder" value="XXXX-XXXXXXX" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			
			<div class="form-group"> <!-- faxNumber -->
				<label for="mobile" class="control-label">Mobile</label>
				
					<dsp:input id="mobile" type="text"
						bean="${formHandlerComponent}.mobile" maxlength="15"
						paramvalue="address.mobile">
						<dsp:tagAttribute name="pattern" value="[+][9][1][-][0-9]{10}" />
						<dsp:tagAttribute name="required" value="true" />
						<dsp:tagAttribute name="title" value="+91-XXXXXXXXXX" />
						<dsp:tagAttribute name="placeholder" value="+91-XXXXXXXXXX" />
						<dsp:tagAttribute name="class" value="form-control" />
					</dsp:input>
			</div>
			
			
			
			<div class="form-group"> <!-- faxNumber -->
			<label for=faxNumber class="control-label">FaxNumber</label>
			<dsp:input  id="faxNumber" 
						type="text"
						bean="${formHandlerComponent}.faxNumber" maxlength="15"
						paramvalue="address.faxNumber">
						<dsp:tagAttribute name="placeholder" value="+1 323 555" />
						<dsp:tagAttribute name="class" value="form-control" />
			</dsp:input>
			</div>
				
			
	
			 <dsp:input type="checkbox" bean="ProfileFormHandler.useShippingAddressAsDefault"
			value="true" >
			<dsp:tagAttribute name="style" value="margin:2px" />
			</dsp:input>
			<b>Default Address</b>
			

</dsp:page>