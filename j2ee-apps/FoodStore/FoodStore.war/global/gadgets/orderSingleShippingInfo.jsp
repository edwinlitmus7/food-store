<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<dsp:page>
	<%-- Display shipping group's shipping address. --%>
	<dsp:include page="/global/util/displayAddress.jsp">
		<dsp:param name="address" param="shippingAddress" />
	</dsp:include>

	<%-- If it's a Confirmation page, display link to 'Change Shipping Info' page. --%>
	<c:if test="${isCurrent}">
		<dsp:a page="/checkout/shipping.jsp" title=""
			iclass="atg_store_editAddress">
			<span>Edit</span>
		</dsp:a>
	</c:if>

	<%-- Display shipping method. --%>
	<dsp:getvalueof var="shippingMethod"
		param="shippingGroup.shippingMethod" />
	<strong>${fn:replace(shippingMethod, ' ', '')}</strong>

	<%-- Display link to 'Change Shipping Info' page when rendering a Confirmation page. --%>
	<%-- <c:if test="${isCurrent}">
		<dsp:a page="/checkout/shippingMethod.jsp" title="">
			<span>Edit</span>
		</dsp:a>
	</c:if> --%>
</dsp:page>