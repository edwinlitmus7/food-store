<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<dsp:page>
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />

	<dsp:getvalueof var="commerceItems" param="commerceItems" />
  <dsp:getvalueof var="commerceItemRelationships" param="commerceItemRelationships" />

  <%-- Display heading of the outer table. Display site indicator's cell only if it's needed. --%>
	<div class="row">
		<div class="col-xs-12">
 <h4> Items in your cart </h4>
            <table class="table table-striped table-hover">
  <thead>
    <tr>
      <th class="item" colspan="2" scope="col"> Item </th>
      <th class="quantity" scope="col"> Quantity </th>
      <th class="price" scope="col"> Price </th>
      <th class="total" scope="col"> Total </th>
    </tr>
  </thead>
  <tbody>
		<c:forEach var="currentItem" items="${commerceItems}"
			varStatus="status">
			<dsp:param name="currentItem" value="${currentItem}" />
			<dsp:include page="/global/gadgets/orderItemRenderer.jsp">
				<dsp:param name="order" param="order" />
				<dsp:param name="currentItem" value="${currentItem}" />
				<dsp:param name="count" value="${status.count}" />
				<dsp:param name="size" value="${size}" />
				<dsp:param name="displayProductAsLink" param="displayProductAsLink" />
			</dsp:include>
		</c:forEach>
	</tbody>
</table>
</div>
</div>
	<div class="row" style="padding:10px">
		<div class="col-xs-12">
	<a href="${contextPath}/cart/cart.jsp" class="primary-btn pull-right">
		Go to cart
	</a>
</div>
</div>
</dsp:page>
