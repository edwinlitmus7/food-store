<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<title> FoodStore </title>
<dsp:page>

    <html>

    <dsp:getvalueof var="port" bean="/OriginatingRequest.serverPort"></dsp:getvalueof>
    <dsp:getvalueof var="protocol" value="//"></dsp:getvalueof>
    <dsp:getvalueof var="serverBase" bean="/OriginatingRequest.serverName"></dsp:getvalueof>
    <dsp:getvalueof var="context" bean="/OriginatingRequest.contextPath"></dsp:getvalueof>
    <dsp:getvalueof var="siteBaseUrl" value="http://${serverBase}:${port}${context}/"></dsp:getvalueof>

    <%@ include file="./includes/navbar.jsp" %>

        <head>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
            <link href="css/style.css" rel="stylesheet" type="text/css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
            <!--  Plugin for slider -->
            <link href="slider/css/bootstrap-slider.css" rel="stylesheet">
            <script src="slider/js/bootstrap-slider.js"></script>
        </head>
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
    <dsp:importbean bean="/atg/commerce/ShoppingCart" />

    <h1>Success!!!</h1>
<dsp:include page="shippingAddresses.jsp"/>
<%-- <dsp:droplet name="ForEach">
    <dsp:param bean="/atg/commerce/ShoppingCart.current.commerceItems" name="array" />
    <dsp:param name="elementName" value="item" />
    <dsp:oparam name="output">
        <dsp:droplet name="ForEach">
            <dsp:param name="array" param="item.priceInfo.currentPriceDetails" />
            <dsp:param name="elementName" value="detail" />
            <dsp:oparam name="output">
                <br/> 1.
                <dsp:valueof param="item.auxiliaryData.productRef.displayName" /> (
                <dsp:valueof param="item.auxiliaryData.productRef.id" />)
                <br/> 2.
                <dsp:valueof param="item.auxiliaryData.catalogRef.displayName" /> (
                <dsp:valueof param="item.auxiliaryData.catalogRef.id" />)
                <br/> 3.
                <dsp:valueof param="item.auxiliaryData.catalogRef" />
                <br/> 4.
                <dsp:valueof param="item.auxiliaryData" />
                <br/> 5.
                <dsp:valueof param="item" />
                <br/> 6.
                <dsp:valueof param="item.auxiliaryData.productRef" />
                <br/> 7.
                <dsp:valueof converter="currency" param="detail.amount">no price</dsp:valueof>
                <br/>8.<dsp:valueof param="detail.quantity"/>
            </dsp:oparam>
        </dsp:droplet>
    </dsp:oparam>
</dsp:droplet>
 --%>
<table>
    <tr>
        <td>Quantity</td>
        <td>Product</td>
        <td>SKU</td>
        <td>Subtotal</td>
        <td>Unit Adjustments</td>
    </tr>

    
        <dsp:droplet name="ForEach">
            <dsp:param bean="ShoppingCart.current.commerceItems" name="array" />
            <dsp:param name="elementName" value="item" />
            <dsp:oparam name="output">
                <dsp:droplet name="ForEach">
                    <dsp:param name="array" param="item.priceInfo.currentPriceDetails" />
                    <dsp:param name="elementName" value="detail" />
                    <dsp:oparam name="output">
                    <tr>
                        <td>
                            <dsp:valueof param="detail.quantityDerivedAsFloatingPoint" />
                        </td>
                        <td>
                            <dsp:valueof param="item.auxiliaryData.productRef.displayName" /> (
                            <dsp:valueof param="item.auxiliaryData.productRef.id" />)</td>
                        <td>
                            <dsp:valueof param="item.auxiliaryData.catalogRef.displayName" /> (
                            <dsp:valueof param="item.auxiliaryData.catalogRef.id" />)</td>
                        <td>
                            <dsp:valueof converter="currency" param="detail.amount">no price</dsp:valueof>
                        </td>
                        <td>
                            <dsp:droplet name="ForEach">
                                <dsp:param name="array" param="detail.adjustments" />
                                <dsp:param name="elementName" value="adjustment" />
                                <dsp:oparam name="output">
                                    <dsp:valueof param="adjustment.pricingModel.repositoryId" /> adjusted by
                                    <dsp:valueof converter="currency" param="adjustment.adjustment">no price</dsp:valueof>
                                    <BR>
                                </dsp:oparam>
                            </dsp:droplet>
                        </td>
        </tr>
    </dsp:oparam>
    <dsp:oparam name="empty">
        <DD>no detail info available</dsp:oparam>
    </dsp:droplet>
    </dsp:oparam>
    </dsp:droplet>
</table>
</dsp:page>