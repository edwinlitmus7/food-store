$('#search-field').keyup(function() {
	var searchTerm = $(this)[0].value;
	console.log(searchTerm);
	if(searchTerm.length > 0) {
		$('#auto-complete').css("display", "block");
	} else {
		$('#auto-complete').css("display", "none");
	}
});

$('#search-field').focusin(function() {
	var searchTerm = $(this)[0].value;
	console.log(searchTerm);
	if(searchTerm.length > 0) {
		$('#auto-complete').css("display", "block");
	}
});

$('#search-field').focusout(function() {
	$('#auto-complete').css("display", "none");
});

$('#search-button').click(function() {
	$('#search-submit-button').click();
});

$('.creditcard-delete-icon').click(function() {
	console.log("inisde js fun");
	$(this).siblings('.creditcard-delete-form').find('input[type=submit]').click();
	console.log("form submitted");
});

// Javascript below are social Login module specific

// hide the dummy logout form
$(document).ready(function() {
	
	var logoutForm = $("#logout_form");
	if(logoutForm){
		logoutForm.hide();
		console.log("Hiding dummy logout form.");
	}
	
	
});
//
function googleSessionSignOut() {
	
var auth2 = gapi.auth2.getAuthInstance();
auth2.signOut().then(function () {
  console.log('User signed out from google.');
});
}
//Submit the logout form simulating the user click
function submitLogoutForm() {
	
	var element = $("input[value='Logout']");
	
	element.click();
	
}


$(function() {
	 prepareHiddenForm();
    $("#hidden_form").hide();
    $("#cost_slider").bootstrapSlider({});
    $("#cost_slider").on(
        "slide",
        function(sliderValue) {
            $("#custom_lowerlimit").val(
                sliderValue.value[0]);
            $("#custom_upperlimit").val(
                sliderValue.value[1]);

            prepareHiddenForm();
        });

});
$("#custom_lowerlimit").change(function() {
    prepareHiddenForm();
});
$("#custom_upperlimit").change(function() {
    prepareHiddenForm();
});

function submitHiddenForm() {
    $("#submit_form").click();
}

function prepareHiddenForm() {
    $("#lowerlimit").val(
        $("#custom_lowerlimit").val());
    $("#upperlimit").val(
        $("#custom_upperlimit").val());
}

function productViewPrepare(){
	if(typeof globalData === 'undefined'){
		console.log('Required varible globalData is undefined ');
	} else if(false) {
		for (var key in globalData) {
			  if (globalData.hasOwnProperty(key)) {
			    var val = globalData[key];
			    console.log(val);
			    // PRODUCT DETAILS SLICK
			    $('#'+key).slick({
			      infinite: true,
			      speed: 300,
			      dots: false,
			      arrows: true,
			      fade: true,
			      asNavFor: '#'+val,
			    });

			    $('#'+val).slick({
			      slidesToShow: 3,
			      slidesToScroll: 1,
			      arrows: true,
			      centerMode: true,
			      focusOnSelect: true,
			      asNavFor: '#'+key,
			    });

			    // PRODUCT ZOOM
			    $('#'+key+' .product-view').zoom();
			    
			  }
			}
	}
}


$(document).ready(function(){
	$('.slick_images').slick({
	  lazyLoad: 'ondemand',
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  slidesToShow: 3,
	   
	    arrows: true,
	    centerMode: true,
	    focusOnSelect: true,
	});
	
});

		