<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<dsp:page>
	<%@ include file="/includes/header_v2.jsp" %>
	<%@ include file="/includes/navbar_v2.jsp" %>
	<!-- Main  -->
		<dsp:include page="./gadgets/product_view.jsp"/>
		<dsp:include  page="./gadgets/related_products.jsp"/>
	<!-- /Main -->
  <%@ include file="/includes/footer_v2.jsp" %>
</dsp:page>
	