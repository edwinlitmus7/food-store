<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<title>FoodStore</title>

<!--
	The  product display page. The details of the product selected will be displayed here.
 -->
<dsp:page>


	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/DisplaySku" />
	<dsp:importbean bean="/atg/dynamo/droplet/Switch" />
	<dsp:importbean bean="/atg/userprofiling/Profile" />
	<dsp:importbean bean="/atg/targeting/TargetingForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/FoodStore" />
	<dsp:importbean
		bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/SkuLookUpDroplet" />
	<dsp:importbean
		bean="/atg/commerce/catalog/custom/RelatedProductsDroplet" />

	<html>

<dsp:getvalueof var="port" bean="/OriginatingRequest.serverPort"></dsp:getvalueof>
<dsp:getvalueof var="protocol" value="//"></dsp:getvalueof>
<dsp:getvalueof var="serverBase" bean="/OriginatingRequest.serverName"></dsp:getvalueof>
<dsp:getvalueof var="context" bean="/OriginatingRequest.contextPath"></dsp:getvalueof>
<dsp:getvalueof var="siteBaseUrl"
	value="http://${serverBase}:${port}${context}/"></dsp:getvalueof>

<%@ include file="../includes/navbar.jsp"%>

<head>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="${context }/css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
	integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n"
	crossorigin="anonymous"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"
	integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb"
	crossorigin="anonymous"></script>
</head>

<body>
	<dsp:setvalue bean="DisplaySku.productId" paramvalue="id" />
	<dsp:droplet name="SkuLookUpDroplet">
		<dsp:param name="skus" bean="DisplaySku.sku" />
		<dsp:param name="index" param="skuIndex" />
		<dsp:oparam name="output">

			<dsp:getvalueof id="currentSku" param="element"></dsp:getvalueof>

			<dsp:getvalueof id="SkuImageLarge" idtype="java.lang.String"
				param="element.largeImage.url" />


			<div class="container">

				<div class="row">
					<%@ include file="gadgets/productImage.jsp"%>
					<%@ include file="gadgets/product_description.jsp"%>
				</div>
			</div>
		</dsp:oparam>
	</dsp:droplet>
	<br />
	<div class="container-related-products">
		<h4>You may also be interested in:</h4>
		<div class="row">
			<dsp:droplet name="RelatedProductsDroplet">
				<dsp:oparam name="output">
					<dsp:droplet name="ForEach">
						<dsp:param name="array" param="relatedProducts"></dsp:param>
						<dsp:param name="elementName" value="childProduct" />
						<dsp:oparam name="output">
							<div class="col-md-2">
								<div class="panel panel-default">
									<dsp:include page="gadgets/product_fragment.jsp"></dsp:include>
								</div>
							</div>
						</dsp:oparam>
					</dsp:droplet>
				</dsp:oparam>
			</dsp:droplet>
		</div>
	</div>

	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
		integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
		crossorigin="anonymous"></script>
	<script src="${context }/js/modal.js"></script>
	<script src="${contextPath}/js/script.js"></script>

</body>

	</html>
</dsp:page>
