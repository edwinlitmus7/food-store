<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<dsp:page>

	<dsp:importbean bean="/atg/commerce/catalog/custom/DisplaySku" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/SkuLookUpDroplet" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	
	<!-- 
		The hyper link for the product page.
		
		Input parameters:
			id
				The id  of the  product.
			
			productName
				The display name of the  product.
				
			productImageLarge
				The large image url of the product..
				
			productDescription
				The description of the product.
				
			skuIndex
				The index of  the sku.
				
		Output parameters: 
			
			none


	 -->
	 
	 
	 


		
		<dsp:getvalueof id="smallProductImageURL" idtype="java.lang.String"
				param="childProduct.smallImage.url" />
		<dsp:getvalueof id="index" idtype="java.lang.String"
				param="index" />
		<dsp:getvalueof id="index_prev" idtype="java.lang.String"
				param="prev_index" />
				
				
		<!-- Product Single -->
				<div class="col-md-4 col-sm-6 col-xs-6">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
							</div>
							<!-- Trigger/Open The Modal -->
							<button class="main-btn quick-view " onclick="openModal('myModal_${index_prev}${index}')"><i class="fa fa-search-plus"></i> Quick view</button>
							<img src="${contextPath}/${smallProductImageURL}" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">
								<dsp:valueof param="childProduct.childSKUs[0].listPrice"/>
							</h3>
							<div class="product-rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-o empty"></i>
							</div>
							
							<h2 class="product-name">
							<dsp:a href="${contextPath}/browse/product.jsp">
							<dsp:param name="id" param="childProduct.repositoryId" />
							<dsp:param name="productName" param="childProduct.displayName" />
							<dsp:param name="productImageLarge" param="childProduct.largeImage.url" />
							<dsp:param name="productDescription" param="childProduct.longDescription" />
							<dsp:param name="skuIndex" value="0" />
									<dsp:valueof param="childProduct.displayName">ERROR:no product name</dsp:valueof>
							</dsp:a>
							</h2>
							<div class="product-btns">
								<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								<button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
								<button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->
				
		<!-- The Modal -->
		<div id="myModal_${index_prev}${index}" class="modal" style="z-index:100">
			<!-- Modal content -->
			<div class="modal-content">
				<span class="close" onclick="closeModal('myModal_${index}')">&times;</span>
				<dsp:param name="id" param="childProduct.repositoryId" />
				<dsp:param name="productName" param="childProduct.displayName" />
				<dsp:param name="productImageLarge"
					param="childProduct.largeImage.url" />
				<dsp:param name="productDescription"
					param="childProduct.longDescription" />
				<dsp:param name="skuIndex" value="0" />
				<dsp:setvalue bean="DisplaySku.productId" paramvalue="id" />
				<dsp:droplet name="SkuLookUpDroplet">
					<dsp:param name="skus" bean="DisplaySku.sku" />
					<dsp:param name="index" param="skuIndex" />
					<dsp:oparam name="output">

						<dsp:getvalueof id="currentSku" param="element"></dsp:getvalueof>

						<dsp:getvalueof id="SkuImageLarge" idtype="java.lang.String"
							param="element.largeImage.url" />


						<div class="container">
							<div class="row">
								<%@ include file="productImage.jsp" %>
								<%@ include file="product_description.jsp" %>
							</div>
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</div>

		</div>



</dsp:page>
