<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<dsp:page>

	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />


	<dsp:importbean
		bean="/atg/commerce/catalog/custom/FacetedSearchFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />


	<!-- 
    
    Faceted search : User can select the price range within which the products are to be searched.
    
    Input ids:
    	
    	upperlimit
    		The upper limit of the price selected by the user.
    		
    	lowerlimit
    		The lower limit of the price selected.
     -->

	
			<div class=" card" style="min-height: 100%;padding-left:5px">
				<!--  Sidebar content -->
				<div class="card-block">
					<!-- aside widget -->
					<div class="aside">
						<h3 class="aside-title">Filter by Price</h3>
						<p class="card-text">
						<b>10</b> <input style="width: 50%" id="cost_slider" type="text"
							value="" data-slider-min="10" data-slider-max="500"
							data-slider-step="5" data-slider-value="[50,200]" /> <b> 500</b>
						<br />
					</p>
						<b>Or a custom price range </b> <br />
						<!-- Start of form -->
						<form class="form-inline">
							<div class="form-group">
								 &nbsp; <input id="custom_lowerlimit"
									style="max-width: 50px" value="50" /> &nbsp;
							</div>

							<div class="form-group">
								- &nbsp; <input id="custom_upperlimit"
									style="max-width: 50px" value="200" /> &nbsp;
							</div>
						</form>
						<br/>
						<div class="form-group">
							<a href="#" class="primary-btn" onclick="submitHiddenForm();"
								>Filter results</a>
						</div>
					
					</div>
					<!-- aside widget -->
					
					
					<!-- Hidden form -->
					<dsp:form id="hidden_form">
						<dsp:input id="upperlimit"
							bean="FacetedSearchFormHandler.upperLimit" type="hidden"
							value="0" />
						<dsp:input id="lowerlimit"
							bean="FacetedSearchFormHandler.lowerLimit" type="hidden"
							value="0" />
						<dsp:input type="hidden" name="successURL" value="${contextPath }"
							bean="FacetedSearchFormHandler.successURL" />
						<dsp:input type="hidden" name="errorURL"
							value="${contextPath }/global/gadgets/errorMessage.jsp"
							bean="FacetedSearchFormHandler.errorURL" />
						<dsp:input id="submit_form" type="submit" value="Search"
							bean="FacetedSearchFormHandler.facetedSearch" />
					</dsp:form>
					<!-- // Hidden form -->
					
				</div>

			</div>
		

</dsp:page>