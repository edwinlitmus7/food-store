<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<dsp:page>

	<dsp:importbean bean="/atg/commerce/catalog/custom/DisplaySku" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/SkuLookUpDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	
	
	<!-- 
		The hyper link for the product page.
		
		Input parameters:
			id
				The id  of the  product.
			
			productName
				The display name of the  product.
				
			productImageLarge
				The large image url of the product..
				
			productDescription
				The description of the product.
				
			skuIndex
				The index of  the sku.
				
		Output parameters: 
			
			none


	 -->
	 
	 
	 

<dsp:getvalueof id="largeProductImageURL" idtype="java.lang.String"
		param="childProduct.childSKUs[0].largeImage.url" />
		<dsp:getvalueof id="smallProductImageURL" idtype="java.lang.String"
				param="childProduct.childSKUs[0].smallImage.url" />
		<dsp:getvalueof id="index" idtype="java.lang.String"
				param="index" />
		<dsp:getvalueof id="index_prev" idtype="java.lang.String"
				param="prev_index" />
				
		<dsp:getvalueof id="classString" idtype="java.lang.String"
		param="classString" />
		<dsp:importbean bean="/atg/dynamo/droplet/IsEmpty"/>
		<dsp:droplet name="IsEmpty">
			<dsp:param name="value" value="${classString}"/>
			<dsp:oparam name="true">
				<dsp:getvalueof id="classString" idtype="java.lang.String"
					value="col-md-4 col-sm-6 col-xs-6" />
			</dsp:oparam>
		</dsp:droplet>
		
		
				
				
		<!-- Product Single -->
				<div class="${classString}">
					<div class="product product-single">
						<div class="product-thumb">
							<div class="product-label">
								<span>New</span>
							</div>
							<!-- Trigger/Open The Modal -->
							<button class="main-btn quick-view " onclick="openModal('myModal_${index_prev}${index}')"><i class="fa fa-search-plus"></i> Quick view</button>
							<img src="${contextPath}/${smallProductImageURL}" alt="">
						</div>
						<div class="product-body">
							<h3 class="product-price">
								<dsp:include page="/global/gadgets/formattedPrice.jsp">
									<dsp:param name="price" param="childProduct.childSKUs[0].listPrice" />
								</dsp:include>
							</h3>
							<div class="product-rating">
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star"></i>
								<i class="fa fa-star-o empty"></i>
							</div>
							
							<h2 class="product-name">
							<dsp:a href="${contextPath}/browse/product.jsp">
							<dsp:param name="id" param="childProduct.repositoryId" />
							<dsp:param name="productName" param="childProduct.displayName" />
							<dsp:param name="productImageLarge" param="childProduct.childSKUs[0].largeImage.url" />
							<dsp:param name="productDescription" param="childProduct.longDescription" />
							<dsp:param name="skuIndex" value="0" />
									<dsp:valueof param="childProduct.displayName">ERROR:no product name</dsp:valueof>
							</dsp:a>
							</h2>
							<div class="product-btns">
								
								
				 				<dsp:form action="index.jsp" iclass="form-inline" method="post">
                					
                    				<dsp:input bean="CartModifierFormHandler.addItemToCount" type="hidden" value="1" />
                    				<dsp:input bean="CartModifierFormHandler.commerceItemType" type="hidden" value="default" />
                    				<dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" type="hidden" value="${contextPath}/cart/cart.jsp" />
                    				<dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden" value="${contextPath}/global/gadgets/errorMessage.jsp" />
                    				<dsp:input bean="CartModifierFormHandler.productId" paramvalue="childProduct.repositoryId" type="hidden" />
                    				<dsp:input bean="CartModifierFormHandler.catalogRefIds"  paramvalue="childProduct.childSKUs[0].repositoryId" type="hidden" />
                    				<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
								
                    				<input type="number" class="input main-btn icon-btn " value="1" onchange="this.nextElementSibling.value = this.value"/>
                    				<dsp:input bean="CartModifierFormHandler.quantity" type="hidden" value="1"></dsp:input>
									
									
                    				<dsp:input bean="CartModifierFormHandler.addItemToOrder" 
                    						id="thumbnail_add_to_cart_${index_prev}${index}"
                    						type="submit" value="Add To Cart" name="addToCart" style="width: 100%; margin: 10px;" >
                    					<dsp:tagAttribute name="style" value="display:none"/>
                    				</dsp:input>
                    				
                    				<button onclick="document.getElementById('thumbnail_add_to_cart_${index_prev}${index}').click();return false;" class=" add-to-cart main-btn primary-btn "><i class="fa fa-shopping-cart"></i> Add to Cart</button>
               					 
            		</dsp:form>
            
								
								
							</div>
						</div>
					</div>
				</div>
				<!-- /Product Single -->
				
		<!-- The Modal -->
		<div id="myModal_${index_prev}${index}" class="modal" style="z-index:100">

			<!-- Modal content -->
			<div class="modal-content">
				<span class="close" onclick="closeModal('myModal_${index_prev}${index}')">&times;</span>
				<dsp:param name="id" param="childProduct.repositoryId" />
				<dsp:param name="productName" param="childProduct.displayName" />
				<dsp:param name="productImageLarge"
					param="childProduct.childSKUs[0].largeImage.url" />
				<dsp:param name="productDescription"
					param="childProduct.longDescription" />
				<dsp:param name="skuIndex" value="0" />
				<dsp:setvalue bean="DisplaySku.productId" paramvalue="id" />
				<dsp:droplet name="SkuLookUpDroplet">
					<dsp:param name="skus" bean="DisplaySku.sku" />
					<dsp:param name="index" param="skuIndex" />
					<dsp:oparam name="output">

						<dsp:getvalueof id="currentSku" param="element"></dsp:getvalueof>

						<dsp:getvalueof id="SkuImageLarge" idtype="java.lang.String"
							param="element.largeImage.url" />


						<div class="container">
							<div class="row">
								<dsp:include  page="quickViewProductDetails_v2.jsp" >
									<dsp:param name="index_prev" value="${index_prev}"/>
									<dsp:param name="index" value="${index}"/>
								</dsp:include>
								
							</div>
						</div>
					</dsp:oparam>
				</dsp:droplet>
			</div>

		</div>



</dsp:page>
