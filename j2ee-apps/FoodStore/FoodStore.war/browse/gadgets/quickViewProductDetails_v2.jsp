<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>
<dsp:page>
<dsp:importbean bean="/atg/commerce/catalog/custom/DisplaySku" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/SkuLookUpDroplet" />
	<dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
    
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
<dsp:getvalueof id="index_prev" param="index_prev" />
<dsp:getvalueof id="index_cur" param="index" />
<dsp:getvalueof id="SkuImageLarge" idtype="java.lang.String"
				param="element.largeImage.url" />
<!--  Product Details -->
				<dsp:param name="id" param="childProduct.repositoryId" />
				<dsp:param name="productName" param="childProduct.displayName" />
				<dsp:param name="productImageLarge" param="childProduct.largeImage.url" />
				<dsp:param name="productDescription" param="childProduct.longDescription" />
				<dsp:param name="skuIndex" value="0" />
				<dsp:setvalue bean="DisplaySku.productId" paramvalue="id" />
				<dsp:droplet name="SkuLookUpDroplet">
				<dsp:param name="skus" bean="DisplaySku.sku" />
				<dsp:param name="index" param="skuIndex" />
				<dsp:oparam name="output">
				<dsp:getvalueof id="currentSku" param="element"></dsp:getvalueof>
				<dsp:getvalueof id="SkuImageLarge" idtype="java.lang.String" param="element.largeImage.url" />
				<dsp:getvalueof id="SkuImageSmall" idtype="java.lang.String" param="element.smallImage.url" />
				<script type="text/javascript">
								globalData["product-main-view${index_prev}${index_cur}"] = "product-view${index_prev}${index_cur}";
				</script>
				<div class="product product-details clearfix">
					<div class="col-md-6">
							
						<div id="product-main-view${index_prev}${index_cur}" >
							
							<div class="product-view">
								<img src="${contextPath}/${SkuImageLarge}" alt="">
							</div>
						
						
							
						</div>
					
						
					</div>
					<div class="col-md-6">
						<div class="product-body">
							<div class="product-label">
								<!--
								<span>New</span>
								<span class="sale">-20%</span>
								-->
							</div>
							<h2 class="product-name">
								<dsp:valueof param="element.displayName"></dsp:valueof>
							</h2>
							<h3 class="product-price">
								<dsp:include page="/global/gadgets/formattedPrice.jsp">
										<dsp:param name="price" param="element.listPrice" />
								</dsp:include>
							</h3>
							<div>
								<div class="product-rating">
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-o empty"></i>
								</div>
								<a href="#">3 Review(s) / Add Review</a>
							</div>
							<!--<p><strong>Availability:</strong> In Stock</p>
							<p><strong>Brand:</strong> E-SHOP</p> -->
							<p>
								<!-- product description -->
								<dsp:valueof param="productDescription" />
								<!-- /product description -->
							
							</p>
							
						<div class="product-options">
						<dsp:droplet name="ForEach">
						<dsp:param name="array" bean="DisplaySku.sku" />
						<dsp:param name="elementName" value="sku" />
						<dsp:oparam name="output">
							<dsp:a href="${contextPath}/browse/product.jsp">
								<dsp:param name="id" param="id" />
								<dsp:param name="productName" param="childProduct.displayName" />
								<dsp:param name="productImageLarge" param="childProduct.largeImage.url" />
								<dsp:param name="productDescription" param="productDescription" />
								<dsp:param name="skuIndex" param="index" />
								<dsp:getvalueof id="largeSkuImageURL" idtype="java.lang.String" param="sku.largeImage.url" />
								<dsp:getvalueof id="smallSkuImageURL" idtype="java.lang.String" param="sku.smallImage.url" />
								<dsp:getvalueof id="skuName" idtype="java.lang.String" param="sku.displayName" />
									
										<img  src="${contextPath}/${smallSkuImageURL}" alt="${skuName}" 
											 data-toggle="tooltip" data-placement="top" class="img-thumbnail"  
											title="${skuName}"
										/>
								
							</dsp:a>
						</dsp:oparam>
						</dsp:droplet>
						</div>
							
							

							<div class="product-btns">
								
								<dsp:param name="skuIndex" param="skuIndex" />
								<dsp:form action="index.jsp" method="post">
                
                    <dsp:input bean="CartModifierFormHandler.addItemToCount" type="hidden" value="1" />
                    <dsp:input bean="CartModifierFormHandler.commerceItemType" type="hidden" value="default" />
                    <dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" type="hidden" value="${contextPath}/cart/cart.jsp" />
                    <dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden" value="${contextPath}/global/gadgets/errorMessage.jsp" />
                    <dsp:input bean="CartModifierFormHandler.productId" paramvalue="id" type="hidden" />
                    <dsp:input bean="CartModifierFormHandler.catalogRefIds" paramvalue="element.repositoryId" type="hidden" />
                    <dsp:getvalueof var="product_id" param="id"></dsp:getvalueof>
                    <dsp:getvalueof var="sku_id" param="element.repositoryId"></dsp:getvalueof>
                    <div class="qty-input">
									<span class="text-uppercase">QTY: </span>
<!--                     <select class='required_quantity' onchange="this.nextElementSibling.value = this.value" style=" -->
<!--     				width: 32px;"></select> -->
					<input class="input" value="1" onchange="this.nextElementSibling.value = this.value" type="number">
                    <dsp:input bean="CartModifierFormHandler.quantity" type="hidden" value="1"></dsp:input>
					</div>

					<button class="primary-btn add-to-cart" onclick="document.getElementById('preview_add_to_cart_${product_id}${sku_id}').click();return false;"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
								
                    <dsp:input bean="CartModifierFormHandler.addItemToOrder" type="submit" iclass="btn btn-warning" value="Add To Cart" name="addToCart" >
					 <dsp:tagAttribute name="style" value="display:none"/>
					 <dsp:tagAttribute name="id" value="preview_add_to_cart_${product_id}${sku_id}"/>
					</dsp:input>
                	
            </dsp:form>
								
								
								
								<div class="pull-right">
									<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
									<button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
									<button class="main-btn icon-btn"><i class="fa fa-share-alt"></i></button>
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
				</dsp:oparam>
				</dsp:droplet>
				<!-- /Product Details -->
</dsp:page>