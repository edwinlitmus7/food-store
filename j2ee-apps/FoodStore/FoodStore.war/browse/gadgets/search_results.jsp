<%@ taglib uri="/dspTaglib" prefix="dsp" %>
<%@ page isELIgnored="false"%>

<dsp:page>
    <%-- The search results for the product keyword search are displayed here --%>
    
    <dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/FoodStoreSearchFormHandler" />
	<dsp:droplet name="ForEach">
	    <dsp:param name="array" bean="FoodStoreSearchFormHandler.searchResults" />
	    <dsp:param name="elementName" value="product" />
	    <dsp:oparam name="outputStart">

	   								<div class="row">
												<!-- section title -->
												<div class="col-md-12">
													<div class="section-title">
														<h2 class="title"> Search Results for "<i> <dsp:valueof bean="FoodStoreSearchFormHandler.repositoryKey"/></i>"</h2>
													</div>
												</div>
												<!-- section title -->
				
												</div>
	    </dsp:oparam>
	    <dsp:oparam name="output">
	        <dsp:getvalueof id="pval0" param="product">
	           
	                    <dsp:include page="product_fragment_v2.jsp">
	
	                        <dsp:param name="childProduct" value="<%=pval0%>" /></dsp:include>
	                
	        </dsp:getvalueof>
	
	    </dsp:oparam>
	    <h3><dsp:valueof bean="FoodStoreSearchFormHandler.message"></dsp:valueof></h3>
	</dsp:droplet>

</dsp:page>