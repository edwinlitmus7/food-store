<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<dsp:page>
<dsp:importbean bean="/atg/commerce/catalog/custom/RelatedProductsDroplet" />
<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />  
  <!-- section -->
	<div class="section">
		<!-- container -->
		<div class="container">
			<!-- row -->
			<div class="row">
				<!-- section title -->
				<div class="col-md-12">
					<div class="section-title">
						<h2 class="title"> Related Products </h2>
						<div class="pull-right">
							<div class="product-slick-dots-4 custom-dots"></div>
						</div>
					</div>
				</div>
				<!-- section title -->
				<div class="container">
				<div class="row">
				
								<dsp:droplet name="RelatedProductsDroplet">
                        <dsp:oparam name="output">
                            <dsp:droplet name="ForEach">
                                <dsp:param name="array" param="relatedProducts"></dsp:param>
                                <dsp:param name="elementName" value="childProduct" />
                                <dsp:oparam name="output">
                                		
                                            <dsp:include page="./product_fragment_v2.jsp">
                                            	<dsp:param name="classString" value="col-md-3 col-sm-4 col-xs-12"/>
                                            </dsp:include>
                                        
                                </dsp:oparam>
                            </dsp:droplet>
                        </dsp:oparam>
  			</dsp:droplet>
  			</div>
  			
  			</div>
  			
  			
  			
				

			</div>
			<!-- /row -->
		</div>
		<!-- /container -->
	</div>
	<!-- /section -->
</dsp:page>
  