<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<dsp:importbean
		bean="/atg/commerce/catalog/custom/FacetedSearchFormHandler" />
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />

	<!--     
      Results of faceted search
    
      Required parameters:
   
    		childProduct 
    			The id of the product.
    	
     -->
	<%-- The results for the selected range will be displayed here --%>
	<dsp:droplet name="ForEach">
		<dsp:param name="array" bean="FacetedSearchFormHandler.results" />
		<dsp:param name="elementName" value="products" />
		<dsp:oparam name="outputStart">
												<div class="row">
												<!-- section title -->
												<div class="col-md-12">
													<div class="section-title">
														<h2 class="title"> Search Results </h2>
													</div>
												</div>
												<!-- section title -->
				
												</div>
		</dsp:oparam>
		<dsp:oparam name="output">
			
					<dsp:getvalueof id="pval0" param="products">
						<dsp:include page="product_fragment_v2.jsp">

							<dsp:param name="childProduct" value="<%=pval0%>" />
						</dsp:include>
					</dsp:getvalueof>
				
		</dsp:oparam>
		<h3>
			<dsp:valueof bean="FacetedSearchFormHandler.message"></dsp:valueof>
		</h3>
	</dsp:droplet>
</dsp:page>