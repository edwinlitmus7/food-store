<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<dsp:page>
	<dsp:importbean bean="/atg/dynamo/droplet/ForEach" />
	<dsp:importbean bean="/atg/commerce/catalog/custom/DisplaySku" />
	<dsp:getvalueof var="contextPath"
		bean="/OriginatingRequest.contextPath" />
	<div class="col-md-7">
		<div class="well well-lg">
			<h5>
				<dsp:valueof param="element.displayName"></dsp:valueof>
			</h5>
			<h3>
				<dsp:include page="/global/gadgets/formattedPrice.jsp">
					<dsp:param name="price" param="element.listPrice" />
				</dsp:include>

				<!-- 
				The display name, image, product description, skus are displayed here.
				
				Input Parameters:
				
					productName
						The display name of the selected product.
						
					productImageLarge
						The large image url of the selected product.
						
					productDescription
						The description for the selected product.
						
					skuIndex
						The index of the skus of the product.
					
				Output Parameters: 
					
					none
				
				 -->
			</h3>
			<p>
				<dsp:valueof param="productDescription" />
			</p>

			<div class="row">
				<dsp:droplet name="ForEach">
					<dsp:param name="array" bean="DisplaySku.sku" />
					<dsp:param name="elementName" value="sku" />
					<dsp:oparam name="output">
						<dsp:a href="${contextPath}/browse/product.jsp">
							<dsp:param name="id" param="id" />
							<dsp:param name="productName" param="childProduct.displayName" />
							<dsp:param name="productImageLarge"
								param="childProduct.largeImage.url" />
							<dsp:param name="productDescription" param="productDescription" />
							<dsp:param name="skuIndex" param="index" />
							<dsp:getvalueof id="largeSkuImageURL" idtype="java.lang.String"
								param="sku.largeImage.url" />
							<dsp:getvalueof id="smallSkuImageURL" idtype="java.lang.String"
								param="sku.smallImage.url" />

							<dsp:getvalueof id="skuName" idtype="java.lang.String"
								param="sku.displayName" />
							<img src="${contextPath}/${smallSkuImageURL}" alt="${skuName}"
								class="img-thumbnail" data-toggle="tooltip" data-placement="top"
								title="${skuName}" />
						</dsp:a>
					</dsp:oparam>
				</dsp:droplet>
			</div>

		</div>
	</div>
</dsp:page>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
	integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
	crossorigin="anonymous"></script>
