<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<%@ page isELIgnored="false"%>

<!-- 
	The product image, add  to cart option are displayed here.
	
	Input parameters:
	
		none
		
	Output parameters:
	 
	  	none
	
 -->
<dsp:page>
<dsp:getvalueof var="contextPath" bean="/OriginatingRequest.contextPath" />
    <dsp:importbean bean="/atg/commerce/order/purchase/CartModifierFormHandler" />
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body" style="height: 60%;">

                        <img src="${contextPath}/${SkuImageLarge}" id="myImg" class="img-responsive" style="width: 100%; height: 100%;">
                        <!-- The Modal -->
                        <div id="myImageModal" class="image-modal">

                            <!-- The Close Button -->
                            <span class="close" onclick="document.getElementById('myImageModal').style.display='none'">&times;</span>

                            <!-- Modal Content (The Image) -->
                            <img class="image-modal-content" id="img01">

                            <!-- Modal Caption (Image Text) -->
                            <div id="caption"></div>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <dsp:param name="skuIndex" param="skuIndex" />
            <dsp:form action="index.jsp" method="get">
                <div class="col-xs-4">
                    <dsp:input bean="CartModifierFormHandler.addItemToCount" type="hidden" value="1" />
                    <dsp:input bean="CartModifierFormHandler.commerceItemType" type="hidden" value="default" />
                    <dsp:input bean="CartModifierFormHandler.addItemToOrderSuccessURL" type="hidden" value="${contextPath}/cart/cart.jsp" />
                    <dsp:input bean="CartModifierFormHandler.addItemToOrderErrorURL" type="hidden" value="${contextPath}/global/gadgets/errorMessage.jsp" />
                    <dsp:input bean="CartModifierFormHandler.productId" paramvalue="id" type="hidden" />
                    <dsp:input bean="CartModifierFormHandler.catalogRefIds" paramvalue="element.repositoryId" type="hidden" />
                    <b style="font-size:16px;">Quantity: </b>
                    <select class='required_quantity' onchange="this.nextElementSibling.value = this.value" style="
    				width: 32px;"></select>
                    <dsp:input bean="CartModifierFormHandler.quantity" type="hidden" value="1">
                    
                    </dsp:input>

                </div>
                <div class="col-xs-8">
                    <dsp:input bean="CartModifierFormHandler.addItemToOrder" type="submit" iclass="btn btn-warning" value="Add To Cart" name="addToCart" />
                </div>
            </dsp:form>

        </div>

    </div>
</dsp:page>
<script>
    var select = '';
    for (i = 1; i <= 10; i++) {
        select += '<option val=' + i + '>' + i + '</option>';
    }
    $('.required_quantity').html(select);
    $('input[name="addToCart"]').css({
        'width': '100%'
    });
</script>