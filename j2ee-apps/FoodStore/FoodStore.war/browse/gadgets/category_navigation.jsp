<%@ taglib uri="/dspTaglib" prefix="dsp"%>

<%-- The categories and products are displayed here
					
	Input Parameters:
		none
		
	Output Parameters:
		none
			
--%>
<dsp:page>
		
	<dsp:droplet name="/atg/dynamo/droplet/ForEach">
		<dsp:param name="array" param="category.childProducts" />
		<dsp:param name="elementName" value="childProduct" />
		<dsp:oparam name="output">
					
					<dsp:include page="product_fragment_v2.jsp">
					
						<dsp:param name="prev_index" param="prev_index"/>
						
					</dsp:include>

		</dsp:oparam>
	</dsp:droplet>
</dsp:page>
<%-- @version $Id: //product/DCS/version/11.2/release/DCSSampleCatalog/j2ee-apps/sampleCatalog/web-app/category_navigation.jsp#1 $$Change: 946917 $--%>