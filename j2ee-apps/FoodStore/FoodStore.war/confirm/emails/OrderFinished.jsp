<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page isELIgnored="false"%>
<%@ taglib uri="/dspTaglib" prefix="dsp"%>
<dsp:page>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Order</title>
		<link rel="stylesheet"
			href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<script
			src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</head>
	<body>
		<dsp:importbean bean="/atg/commerce/ShoppingCart" />
		
		<div class="container">
			<div class="alert alert-success">
				<h4> Thank you for shopping with us. </h4>
				<strong>Your order has been placed successfully!</strong>
				<%-- <div>
					<dsp:getvalueof var="orderId" vartype="java.lang.Object" bean="ShoppingCart.last" />
					<b> Order number: </b>  <dsp:valueof param="orderId" /> -- ${orderId } 
				</div> --%>
			</div>
		</div>
	</body>
</dsp:page>