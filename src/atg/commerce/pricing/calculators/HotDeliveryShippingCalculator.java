package atg.commerce.pricing.calculators;

import java.util.Locale;
import java.util.Map;

import com.foodstore.interfaces.ParameterConstants;
import com.foodstore.order.FoodStoreOrderImpl;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.FixedPriceShippingCalculator;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.repository.RepositoryItem;

public class HotDeliveryShippingCalculator extends FixedPriceShippingCalculator{

	public void priceShippingGroup(Order pOrder, ShippingPriceInfo pPriceQuote, ShippingGroup pShippingGroup, RepositoryItem pPricingModel, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException
	{
		if (!haveItemsToShip(pShippingGroup)) {
			resetShippingPriceInfo(pPriceQuote);
		}
		else {
			boolean doPricing = performPricing(pShippingGroup);
			if (isLoggingDebug())
				logDebug("performPricing Operation = " + doPricing);
			if (doPricing) {
				double amount = getAmount(pOrder, pPriceQuote, pShippingGroup,
						pPricingModel, pLocale, pProfile, pExtraParameters);
				

				if (isLoggingDebug()) {
					logDebug("Pricing Amount = "+amount);
				}
				String currencyCode = (String) pExtraParameters
						.get(ParameterConstants.CURRENCYCODE_PARAMETER);

				try{
				priceShippingPriceInfo(amount, pPriceQuote, currencyCode);
				amount=amount*pOrder.getCommerceItemCount();
				pPriceQuote.setAmount(amount);
				pPriceQuote.setRawShipping(amount);
				}
				catch(Exception e){
					logDebug(e);
				}
				if (((FoodStoreOrderImpl) pOrder).isSpecialOrder()) {

					pPriceQuote.setAmount(0);
					pPriceQuote.setRawShipping(0);
				}

			}
		}
	}
}
