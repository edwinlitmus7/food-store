package atg.commerce.pricing.calculators;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.foodstore.interfaces.ParameterConstants;
import com.foodstore.order.FoodStoreOrderImpl;

import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.pricing.FixedPriceShippingCalculator;
import atg.commerce.pricing.OrderPriceInfo;
import atg.commerce.pricing.PricingAdjustment;
import atg.commerce.pricing.PricingException;
import atg.commerce.pricing.ShippingPriceInfo;
import atg.commerce.pricing.Constants;
import atg.repository.RepositoryItem;


/***
 * Calculator for shipping price, here fixed price shipping calculator is used.
 * 
 * @author Vineeth
 *
 */
public class FoodStoreFreeShippingCalculator extends FixedPriceShippingCalculator {

	/**
	 * Calculate the Shipping charges for the items purchased.
	 * Shipping price is zero if order is a special order.
	 */
	public void priceShippingGroup(Order pOrder, ShippingPriceInfo pPriceQuote, ShippingGroup pShippingGroup, RepositoryItem pPricingModel, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
			throws PricingException
	{
		if (!haveItemsToShip(pShippingGroup)) {
			resetShippingPriceInfo(pPriceQuote);
		}
		else {
			boolean doPricing = performPricing(pShippingGroup);
			if (isLoggingDebug())
				logDebug("performPricing Operation = " + doPricing);
			if (doPricing) {
				double amount = getAmount(pOrder, pPriceQuote, pShippingGroup,
						pPricingModel, pLocale, pProfile, pExtraParameters);

				if (isLoggingDebug()) {
					logDebug("Pricing Amount = "+amount);
				}
				String currencyCode = (String) pExtraParameters
						.get(ParameterConstants.CURRENCYCODE_PARAMETER);

				priceShippingPriceInfo(amount, pPriceQuote, currencyCode);
				if (((FoodStoreOrderImpl) pOrder).isSpecialOrder()) {

					pPriceQuote.setAmount(0);
					pPriceQuote.setRawShipping(0);
				}

			}
		}
	}

}
