package com.foodstore.promotions;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import atg.nucleus.logging.ApplicationLogging;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;


/**
 * This is a wrapper class for the promotion <code>RepositoryItem</code>, since
 * there is no OOB wrapper. Provides convenience setters and getters when
 * manipulating the custom <code>Promotion</code> properties.
 *
 * @author Mike Ottinger (mottinger@tacitknowledge.com)
 * @author Marques Lee (marques@tacitknowledge.com)
 * @author David Williams (david.williams@sephora.com)
 */
public class CouponClaimable {
    private final MutableRepositoryItem repositoryItem;
    private final ApplicationLogging applicationLogging;
  

    /**
     * Wraps a <code>RepositoryItem</code> with convenient methods for accessing
     * promotion properties. validates that the <code>RepositoryItem</code> is
     * not null and is of the correct item-descriptor.
     *
     * @param repositoryItem the <code>RepositoryItem</code> representing this
     * <code>Promotion</code>
     * @param applicationLogging an instance of ATG's application logging
     * @throws IllegalArgumentException when a <code>null</code> <code>
     * RepositoryItem</code> is passed in
     */
    public CouponClaimable(final RepositoryItem repositoryItem,
            ApplicationLogging applicationLogging)
                    throws IllegalArgumentException {
        if (repositoryItem == null) {
            throw new IllegalArgumentException("RepositoryItem cannot be null");
        }
        this.repositoryItem = (MutableRepositoryItem) repositoryItem;
        this.applicationLogging = applicationLogging;
    }

    /**
     * Returns the promotion's repository ID.
     *
     * @return the promotion's repository ID
     */
    public String getId() {
        return (String) getRepositoryItem().getRepositoryId();
    }
    
    //derivedDisplayName,displayName,expirationDate,hasPromotions,lastModified,maxUses,parentFolder,promotions,redeemableOnPromotionSites,startDate,status,type,uses
    
    private String derivedDisplayName;
    private String displayName;
    
    List<Promotion> promotions;
    
    
   



  

    public String getDerivedDisplayName() {
    	if(this.derivedDisplayName == null) {
    		this.derivedDisplayName = (String) getPropertyValue("derivedDisplayName");
    	}
		return derivedDisplayName;
	}

	public void setDerivedDisplayName(String derivedDisplayName) {
		this.derivedDisplayName = derivedDisplayName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public List<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(List<Promotion> promotions) {
		this.promotions = promotions;
	}

	public MutableRepositoryItem getRepositoryItem() {
        return repositoryItem;
    }

    
    public String getdisplayName() {
    	return (String) getPropertyValue("displayName");
    }
    
public String type;

    public String getType() {
	return (String)getPropertyValue("type");
}



	/**
     * Returns the item descriptor name. Used in validating that the
     * <code>RepositoryItem</code> is actually a promotion.
     *
     * @return the item descriptor name
     * @throws RepositoryException an error with retrieving the item descriptor
     * name
     */
    public String getItemDescriptorName() throws RepositoryException {
        return getRepositoryItem().getItemDescriptor().getItemDescriptorName();
    }

    /**
     * Convenience method to retrieve the property from the <code>
     * RespositoryItem</code>.
     *
     * @param propertyName the name of the property to retrieve
     * @return the property from the <code>RepositoryItem</code>
     */
    protected Object getPropertyValue(final String propertyName) {
        return getRepositoryItem().getPropertyValue(propertyName);
    }

    /**
     * Convenience method to set a property on the <code>RepositoryItem</code>.
     *
     * @param propertyName the name of the property to set
     * @param value the value to set on the property
     */
    protected void setPropertyValue(final String propertyName,
            final Object value) {
        getRepositoryItem().setPropertyValue(propertyName, value);
    }




    


    /** {@inheritDoc} */
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("CouponClaimable[");
        sb.append("id:").append(getRepositoryItem().getRepositoryId())
                .append("; ");
        sb.append("name:").append(getdisplayName()).append("; ");
        sb.append("]");

        return sb.toString();
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object promotion) {
        if (promotion == null || !(promotion instanceof CouponClaimable)) {
            return false;
        }

        final CouponClaimable otherPromotion = (CouponClaimable) promotion;

        final String thisRepositoryId = getRepositoryItem().getRepositoryId();
        final String otherRepositoryId = otherPromotion.getRepositoryItem()
                .getRepositoryId();

        return getdisplayName().equals(otherPromotion.getdisplayName())
                && thisRepositoryId.equals(otherRepositoryId);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return getRepositoryItem().getRepositoryId().hashCode();
    }

    /**
     * @return the applicationLogging
     */
    public ApplicationLogging getApplicationLogging() {
        return applicationLogging;
    }

   
}