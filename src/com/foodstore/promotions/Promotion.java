package com.foodstore.promotions;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import atg.nucleus.logging.ApplicationLogging;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;


/**
 * This is a wrapper class for the promotion <code>RepositoryItem</code>, since
 * there is no OOB wrapper. Provides convenience setters and getters when
 * manipulating the custom <code>Promotion</code> properties.
 *
 * @author Mike Ottinger (mottinger@tacitknowledge.com)
 * @author Marques Lee (marques@tacitknowledge.com)
 * @author David Williams (david.williams@sephora.com)
 */
public class Promotion {
    private final MutableRepositoryItem repositoryItem;
    private final ApplicationLogging applicationLogging;
  

    /**
     * Wraps a <code>RepositoryItem</code> with convenient methods for accessing
     * promotion properties. validates that the <code>RepositoryItem</code> is
     * not null and is of the correct item-descriptor.
     *
     * @param repositoryItem the <code>RepositoryItem</code> representing this
     * <code>Promotion</code>
     * @param applicationLogging an instance of ATG's application logging
     * @throws IllegalArgumentException when a <code>null</code> <code>
     * RepositoryItem</code> is passed in
     */
    public Promotion(final RepositoryItem repositoryItem,
            ApplicationLogging applicationLogging)
                    throws IllegalArgumentException {
        if (repositoryItem == null) {
            throw new IllegalArgumentException("RepositoryItem cannot be null");
        }
        this.repositoryItem = (MutableRepositoryItem) repositoryItem;
        this.applicationLogging = applicationLogging;
    }

    /**
     * Returns the promotion's repository ID.
     *
     * @return the promotion's repository ID
     */
    public String getId() {
        return (String) getRepositoryItem().getRepositoryId();
    }
    
 
    
   

    public Integer getMaxUses() {
        return (Integer) getPropertyValue("maxUses");
    }

    public void setMaxUses(Integer maxUses) {
        setPropertyValue("maxUses",maxUses);
    }
    public Timestamp getLastModified() {
    	return (Timestamp) getPropertyValue("lastModified");
    }
    /**
     * Gets the value of the currentUses from the {@code promotionAuxiliaryData} item persisted in the OrderRepository
     * 
     * @return
     */



    
 

    @SuppressWarnings("unchecked")
    public List<String> getShippingMethodNameList() {
        return (List<String>) getPropertyValue("preSelectShippingList");
    }
    
 

  

    public MutableRepositoryItem getRepositoryItem() {
        return repositoryItem;
    }

    public Date getExpirationDate() {
        return (Date) getPropertyValue("expirationDate");
    }
    public void setExpirationDate(Date date) {
        setPropertyValue("expirationDate",date);
    }


    public Date getStartDate() {
        return (Date) getPropertyValue(
                "startDate");
    }
 
    public Date getEndDate() {
        return (Date) getPropertyValue(
                "");
    }
    public String getdisplayName() {
    	return (String) getPropertyValue("displayName");
    }
    
 

    /**
     * Returns a type of the promotion as <code>java.lang.Integer</code>
     * 
     * @return a type of the promotion as <code>java.lang.Integer</code>:<br>
     * 0 - Order Discount - Amount Off - GWP<br>
     * 1 - Order Discount - Amount Off<br>
     * 2 - Order Discount - Amount Off - MSG<br>
     * etc...<br>
     * (more details in pricingModels.xml)
     */
    public Integer getType() {
        return (Integer) getPropertyValue("type");
    }

    /**
     * Returns the value of "uses" field. Possible values: 1 - promotion can be
     * used only once -1 - promotion can be used infinite times
     *
     * @return the number of allowed uses
     */
    public int getUses() {
        Object usesObj = getPropertyValue("uses");
        if(usesObj == null){
            return 0;
        }
        return (Integer) usesObj;
    }
    public String getStatus() {
    	return  (String) getPropertyValue("status");
    }
    public void setStatus(String status) {
    	setPropertyValue("status",status);
    }

    /**
     * Returns the item descriptor name. Used in validating that the
     * <code>RepositoryItem</code> is actually a promotion.
     *
     * @return the item descriptor name
     * @throws RepositoryException an error with retrieving the item descriptor
     * name
     */
    public String getItemDescriptorName() throws RepositoryException {
        return getRepositoryItem().getItemDescriptor().getItemDescriptorName();
    }

    /**
     * Convenience method to retrieve the property from the <code>
     * RespositoryItem</code>.
     *
     * @param propertyName the name of the property to retrieve
     * @return the property from the <code>RepositoryItem</code>
     */
    public Object getPropertyValue(final String propertyName) {
        return getRepositoryItem().getPropertyValue(propertyName);
    }

    /**
     * Convenience method to set a property on the <code>RepositoryItem</code>.
     *
     * @param propertyName the name of the property to set
     * @param value the value to set on the property
     */
    protected void setPropertyValue(final String propertyName,
            final Object value) {
        getRepositoryItem().setPropertyValue(propertyName, value);
    }




    


    /** {@inheritDoc} */
    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Promotion[");
        sb.append("id:").append(getRepositoryItem().getRepositoryId())
                .append("; ");
        sb.append("name:").append(getdisplayName()).append("; ");
        sb.append("]");

        return sb.toString();
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object promotion) {
        if (promotion == null || !(promotion instanceof Promotion)) {
            return false;
        }

        final Promotion otherPromotion = (Promotion) promotion;

        final String thisRepositoryId = getRepositoryItem().getRepositoryId();
        final String otherRepositoryId = otherPromotion.getRepositoryItem()
                .getRepositoryId();

        return getdisplayName().equals(otherPromotion.getdisplayName())
                && thisRepositoryId.equals(otherRepositoryId);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return getRepositoryItem().getRepositoryId().hashCode();
    }

    /**
     * @return the applicationLogging
     */
    public ApplicationLogging getApplicationLogging() {
        return applicationLogging;
    }

   
}