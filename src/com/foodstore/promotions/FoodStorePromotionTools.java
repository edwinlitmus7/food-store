package com.foodstore.promotions;

import java.util.ArrayList;
import java.util.List;

import atg.commerce.promotion.PromotionTools;
import atg.core.util.StringUtils;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

public class FoodStorePromotionTools extends PromotionTools {
    /**
     * Returns the promotion <code>RepositoryItem</code> from the
     * promotion status <code>RepositoryItem</code> passed in.
     *
     * @param promotionStatusItem the promotion status <code>RepositoryItem
     * </code>
     * @return the promotion item associated with the promotion status
     */
    public MutableRepositoryItem getPromotionFromPromotionStatus(
            final RepositoryItem promotionStatusItem) {
        if (null == promotionStatusItem) {
            return null;
        }
        final String statusId = promotionStatusItem.getRepositoryId();
        final MutableRepositoryItem promotionItem = (MutableRepositoryItem)
                promotionStatusItem.getPropertyValue(
                        getPromoStatusPromoProperty());
        if (null == promotionItem) {
            if(isLoggingWarning())
            {
            	logWarning("PromotionStatus " + statusId+
                        " has a null promotion");
            }
        	
            return null;
        }
        if(isLoggingDebug()) {
        	logDebug("PromotionStatus "+ statusId + " has promotion "+
                    promotionItem.getRepositoryId());
        }
        
        return promotionItem;
    }
    /**
     * Determines if the given promotion is a claimable promotion on the user's
     * profile.
     *
     * @param profile the profile the promotion is on
     * @param promotion the promotion being inspected
     * @return <code>true</code> if the promotion is a claimable promotion, else
     * <code>false</code>
     */
    public boolean isClaimablePromotion(Profile profile, Promotion promotion) {
        RepositoryItem activePromotionItem = getPromotionStatusItem(profile,
                promotion.getId());
        return ((null != activePromotionItem)
                && (null != activePromotionItem.getPropertyValue("coupons")));
    }
    
    protected RepositoryItem getPromotionStatusItem(Profile profile,
            String promotionId) {
        return getPromotionStatusItem(profile.getDataSource(), promotionId);
    }
    /**
     * Inspect a profile for promotion status containing the promotion with the
     * ID passed in and return the <code>promotionStatus</code> item for that
     * promotion from the profile.
     *
     * @param profileItem the profile to inspect
     * @param promotionId the promotion ID to look for
     * @return the <code>promotionStatus</code> item if there was a match, else
     * <code>null</code>
     */
    protected RepositoryItem getPromotionStatusItem(RepositoryItem profileItem,
            String promotionId) {
        List<MutableRepositoryItem> promotionStatusItems =
                getActivePromotionsFromProfile(profileItem);
        for (MutableRepositoryItem promotionStatusItem : promotionStatusItems) {
            RepositoryItem promotionItem = (RepositoryItem) promotionStatusItem
                    .getPropertyValue("promotion"
                            );
            if (promotionId.equals(promotionItem.getRepositoryId())) {
                return promotionStatusItem;
            }
        }
        return null;
    }
    /**
     * Convenience method to extract the promotionStatuses/activePromotions from
     * a profile RepositoryItem.
     *
     * @param profile the profile RepositoryItem
     * @return a list of promotionStatuses
     */
    @SuppressWarnings("unchecked")
    public List<MutableRepositoryItem> getActivePromotionsFromProfile(
            final RepositoryItem profile) {
        final String activePromotionsProperty = getActivePromotionsProperty();
        final List<MutableRepositoryItem> statuses =
                (List<MutableRepositoryItem>) profile
                .getPropertyValue(activePromotionsProperty);
        if (statuses == null) {
    
            return new ArrayList<MutableRepositoryItem>();
        }
        return statuses;
    }
    /**
     * Determines if the promotion status item passed in references a claimable
     * promotion.
     *
     * @param promotionStatusItem a promotion status item
     * @return <code>true</code> if the promotion status item references a
     * claimable promotion, else <code>false</code>
     * @throws RepositoryException a repository error
     * @throws IllegalArgumentException the object passed in the
     * <code>promotionStatusItem</code> parameter is not the correct item type
     */
    public boolean isClaimablePromotion(RepositoryItem promotionStatusItem)
            throws RepositoryException {
        String parameterItemDescriptorName = promotionStatusItem
                .getItemDescriptor().getItemDescriptorName();
        String promotionStatusItemDescriptorName =
                getPromoStatusDescriptorName();
        if (!parameterItemDescriptorName.equals(promotionStatusItemDescriptorName)) {
            String message = "The item passed in is a " + parameterItemDescriptorName + " item type but should be "
                    + "a " + promotionStatusItemDescriptorName + " item type";
            vlogError(message);
            throw new IllegalArgumentException(message);
        }
        List<RepositoryItem> couponClaimableId = getAssociatedCoupons(promotionStatusItem);
        return !(couponClaimableId == null);
    }
    
    @SuppressWarnings("unchecked")
	public List<RepositoryItem> getAssociatedCoupons(RepositoryItem couponPromotionItem) {
        return (List<RepositoryItem>) couponPromotionItem
                .getPropertyValue("coupons");
    }
    
    public boolean doStatusHasCoupon(RepositoryItem promotionStatusItem,String couponId) throws RepositoryException {
    	List<RepositoryItem> coupons = getAssociatedCoupons(promotionStatusItem);
    	if(coupons == null) {
    		return false;
    	}
    	for(RepositoryItem coupon:coupons) {
    		if(coupon.getRepositoryId().equals(couponId)) {
    			return true;
    		}
    	}
    	return false;
    }

}
