package com.foodstore.promotions;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import atg.repository.MutableRepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;


public class CouponsDisplayDroplet extends DynamoServlet {
	private final String ACTIVE_PROMOTIONS_PROPERTY_NAME = "activePromotions";
	public void service(DynamoHttpServletRequest request,
			DynamoHttpServletResponse response) throws ServletException,
			                             IOException {
			Profile profile = (Profile)request.getObjectParameter("profile");
		 	@SuppressWarnings("unchecked")
			List<MutableRepositoryItem> promotionStatusItems = (List<MutableRepositoryItem>) profile.getPropertyValue(ACTIVE_PROMOTIONS_PROPERTY_NAME);
			Map<CouponClaimable,Set<Promotion>> couponsMap = new HashMap<>();
			for(MutableRepositoryItem promotionStatus : promotionStatusItems) {
				@SuppressWarnings("unchecked")
				List<MutableRepositoryItem> coupons = (List<MutableRepositoryItem>) promotionStatus.getPropertyValue("coupons");
				if(coupons != null) {
					for(MutableRepositoryItem coupon : coupons) {
						CouponClaimable claimable = new CouponClaimable(coupon, getLoggingForVlogging());
						if(couponsMap.containsKey(claimable)) {
							Set<Promotion> promotions = couponsMap.get(claimable);
							MutableRepositoryItem promotionItem = (MutableRepositoryItem) promotionStatus.getPropertyValue("promotion");
							Promotion promotion = new Promotion(promotionItem,getLoggingForVlogging());
							if(!promotions.contains(promotion)) {
								promotions.add(promotion);
							}
						} else {
							Set<Promotion> promotions = new HashSet<>();
							MutableRepositoryItem promotionItem = (MutableRepositoryItem) promotionStatus.getPropertyValue("promotion");
							Promotion promotion = new Promotion(promotionItem,getLoggingForVlogging());
							promotions.add(promotion);
							couponsMap.put(claimable, promotions);
						}
					}	
				}
				
				
			}
		 	
			request.setParameter("coupons",couponsMap);
			request.serviceParameter("output",request, response);

			}

}
