package com.foodstore.promotions;

import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;

import com.foodstore.order.purchase.FoodStorePurchaseProcessHelper;

import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderManager;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.commerce.pricing.PricingConstants;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.promotion.CouponFormHandler;
import atg.commerce.util.PipelineErrorHandler;
import atg.commerce.util.PipelineUtils;
import atg.core.util.StringUtils;
import atg.droplet.DropletException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.RequestLocale;
import com.foodstore.promotions.Promotion;


public class FoodStoreCouponFormHandler extends CouponFormHandler implements PipelineErrorHandler{
	private String revokeCouponSuccessURL;
	private String revokeCouponErrorURL;
	private PricingModelHolder userPricingModels;
	private Order order;
	public PricingModelHolder getUserPricingModels() {
		return userPricingModels;
	}



	public void setUserPricingModels(PricingModelHolder userPricingModels) {
		this.userPricingModels = userPricingModels;
	}



	public Order getOrder() {
		return order;
	}



	public void setOrder(Order order) {
		this.order = order;
	}
	private String mRepriceOrderChainId;

	/*      */     public void setRepriceOrderChainId(String pRepriceOrderChainId)
	/*      */     {
		/*  303 */         mRepriceOrderChainId = pRepriceOrderChainId;
	/*      */     }
	/*      */ 
	/*      */ 
	/*      */ 
	/*      */ 
	/*      */     public String getRepriceOrderChainId()
	/*      */     {
		/*  311 */         return mRepriceOrderChainId;
	/*      */     }


	public Locale getUserLocale() {
		return userLocale;
	}



	public void setUserLocale(Locale mUserLocale) {
		this.userLocale = mUserLocale;
	}

	private Locale userLocale;

	public String getRevokeCouponSuccessURL() {
		return revokeCouponSuccessURL;
	}

	public void setRevokeCouponSuccessURL(String revokeCouponSuccessURL) {
		this.revokeCouponSuccessURL = revokeCouponSuccessURL;
	}

	public String getRevokeCouponErrorURL() {
		return revokeCouponErrorURL;
	}

	public void setRevokeCouponErrorURL(String revokeCouponErrorURL) {
		this.revokeCouponErrorURL = revokeCouponErrorURL;
	}
	private boolean useRequestLocale;

	public boolean isUseRequestLocale() {
		return useRequestLocale;
	}



	public void setUseRequestLocale(boolean useRequestLocale) {
		this.useRequestLocale = useRequestLocale;
	}
	static final ParameterName LOCALE_PARAM = ParameterName.getParameterName("locale");
	private Locale mDefaultLocale;
	public void setDefaultLocale(Locale pDefaultLocale)
	{
		mDefaultLocale = pDefaultLocale;
	}

	public Locale getDefaultLocale()
	{
		if(mDefaultLocale != null) {
			return mDefaultLocale;}
		else    {
			return Locale.getDefault();
		}
	}
	public Locale getUserLocale(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException
	{
		Object obj = pRequest.getObjectParameter(LOCALE_PARAM);
		if (obj instanceof Locale)
			return ((Locale) obj);
		if (obj instanceof String)
			return RequestLocale.getCachedLocale((String) obj);
		if (isUseRequestLocale()) {
			RequestLocale requestLocale = pRequest.getRequestLocale();
			if (requestLocale != null) {
				return requestLocale.getLocale();
			}
		}
		return getDefaultLocale();
	}

	public boolean handleRevokeCoupon(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {


		TransactionDemarcation td = new TransactionDemarcation();
		boolean rollbackTransaction = false;
		try {
			td.begin(getTransactionManager(), TransactionDemarcation.REQUIRED);

			removePromoCode(getOrder());
			runProcessRepriceOrder(getOrder(), getUserPricingModels(),getUserLocale(pRequest, pResponse), getProfile(), null);
			updateOrder(getOrder(), "errorUpdatingOrder", pRequest, pResponse);

		} catch (TransactionDemarcationException e) {
			if (isLoggingError()) {
				logError(e);
			}
		} catch (Exception e) {
			rollbackTransaction = true;
			if (isLoggingError()) {
				logError(e);
			}
			addFormException(new DropletException("Remove coupon failed, case by:" + e.getMessage()));
			return checkFormRedirect(getRevokeCouponSuccessURL(), getRevokeCouponErrorURL(), pRequest, pResponse);
		} finally {
			try {
				td.end(rollbackTransaction);
			} catch (TransactionDemarcationException e) {
				if (isLoggingError()) {
					logError(e);
				}
			}

			/*if (couponPromotions != null && !couponPromotions.isEmpty()) {
		for (Promotion couponPromotion : couponPromotions) {

				getPromotionTools().revokePromotion(getProfile().getRepositoryId(), couponPromotion.getId(), false);*/

		}

		return checkFormRedirect(getRevokeCouponSuccessURL(), getRevokeCouponErrorURL(), pRequest, pResponse);
	}

	private PromotionManager promotionManager;

	public PromotionManager getPromotionManager() {
		return promotionManager;
	}



	public void setPromotionManager(PromotionManager promotionManager) {
		this.promotionManager = promotionManager;
	}



	public void removePromoCode(Order pOrder) throws CommerceException, RepositoryException {
		Set<Promotion> couponPromotions = getPromotionManager().getCouponPromotions(getProfile(),getCouponClaimCode());

		if (couponPromotions != null && !couponPromotions.isEmpty()) {
			for (Promotion couponPromotion : couponPromotions) {
				if (!StringUtils.isEmpty(getCouponClaimCode()) && !getCouponClaimCode().equals(couponPromotion.getId())) {
					addFormException(new DropletException("PromotionId " + getCouponClaimCode()
					+ " passed does not match the promotion Id in active promotions of the profile. Neither one will be removed"));
					if (isLoggingDebug()) {
						logDebug("promotionId from page was: " + getCouponClaimCode()
						+ ", but couponPromotion.getId() was: " + couponPromotion.getId() + "; Profile: "
						+ getProfile().getRepositoryId() + ", orderId:" + pOrder.getId()
						+ "; we will remove promotion with id that was set explicitly on the page.");
					}
				} else {

					getPromotionTools().revokePromotion(getProfile().getRepositoryId(), couponPromotion.getId(), false);
				}
			}

		} 
	}
	@SuppressWarnings("rawtypes")
	protected void runProcessRepriceOrder(Order pOrder, PricingModelHolder pPricingModels, Locale pLocale,
			RepositoryItem pProfile, Map pExtraParameters) throws RunProcessException {
		runProcessRepriceOrder(PricingConstants.OP_REPRICE_ORDER_TOTAL, pOrder, pPricingModels, pLocale, pProfile,
				pExtraParameters);
	}
	private OrderManager orderManager;






	public Locale getmDefaultLocale() {
		return mDefaultLocale;
	}



	public void setmDefaultLocale(Locale mDefaultLocale) {
		this.mDefaultLocale = mDefaultLocale;
	}



	public OrderManager getOrderManager() {
		return orderManager;
	}



	public void setOrderManager(OrderManager orderManager) {
		this.orderManager = orderManager;
	}



	public PurchaseProcessHelper getPurchaseProcessHelper() {
		return purchaseProcessHelper;
	}
	@SuppressWarnings("deprecation")
	public void updateOrder(Order pOrder, String pMsgId, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		try {
			getOrderManager().updateOrder(pOrder);
		} catch (Exception exc) {
			if(isLoggingError()) {
				logError("Update order failed.");	
			}

		}
	}


	public void setPurchaseProcessHelper(PurchaseProcessHelper pPurchaseProcessHelper) {
		purchaseProcessHelper = pPurchaseProcessHelper;
	}

	private PurchaseProcessHelper purchaseProcessHelper;

	@SuppressWarnings("rawtypes")
	protected void runProcessRepriceOrder(String pPricingOperation, Order pOrder, PricingModelHolder pPricingModels,
			Locale pLocale, RepositoryItem pProfile, Map pExtraParameters) throws RunProcessException {
		PipelineResult result = runRepricingProcess("repriceOrder", pPricingOperation, pOrder, pPricingModels, pLocale,
				pProfile, pExtraParameters);

		processPipelineErrors(result);
	}
	protected boolean processPipelineErrors(PipelineResult pResult) {
		if (pResult == null)
			return false;
		return PipelineUtils.processPipelineErrors(pResult, this, (PipelineErrorHandler) this);
	}
	@SuppressWarnings("rawtypes")
	protected PipelineResult runRepricingProcess(String pChainId, String pPricingOperation, Order pOrder,
			PricingModelHolder pPricingModels, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
					throws RunProcessException {

		return (((FoodStorePurchaseProcessHelper)getPurchaseProcessHelper()).runRepricingProcess(pChainId,
				pPricingOperation, pOrder, pPricingModels, pLocale, pProfile, pExtraParameters));
	}



	@Override
	public void handlePipelineError(Object paramObject, String paramString) {

		addFormException(new DropletException(paramObject.toString(), paramString));

	}




}
