package com.foodstore.promotions;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import atg.commerce.promotion.PromotionTools;
import atg.nucleus.GenericService;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

public class PromotionManager extends GenericService{
	PromotionTools promotionTools;
	
    public PromotionTools getPromotionTools() {
		return promotionTools;
	}

	public void setPromotionTools(PromotionTools promotionTools) {
		this.promotionTools = promotionTools;
	}
	
	/**
     * Returns all promotions associated with a claimable coupon from the
     * promotion statuses on the customer's profile. 
     * 
     * @param profileItem
     *            the customer's profile item
     * @return the promotion associated with a claimable coupon from the
     *         customer's profile, or <code>null</code> if not found
     */
    public Set<Promotion> getCouponPromotions(RepositoryItem profileItem,String couponId) {
        List<MutableRepositoryItem> promotionStatusItems = ((FoodStorePromotionTools)getPromotionTools()).getActivePromotionsFromProfile(
                profileItem);
        if (promotionStatusItems == null || promotionStatusItems.size() == 0) {
            return Collections.emptySet() ;
        }
        Set<Promotion> couponPromotions = new HashSet<Promotion>();
        for (RepositoryItem promotionStatusItem : promotionStatusItems) {
            try {
                if (((FoodStorePromotionTools)getPromotionTools()).doStatusHasCoupon(promotionStatusItem,couponId)) {
                    RepositoryItem promotionItem = ((FoodStorePromotionTools)getPromotionTools()).getPromotionFromPromotionStatus(
                            promotionStatusItem);
                    if (promotionItem == null) {
                        if (isLoggingError()) {
                            logError("Unable to retrieve promotion item from promotion status item "+
                                    promotionStatusItem.getRepositoryId());
                        }
                        // Keep looking
                        continue;
                    }
                    couponPromotions.add(new Promotion(promotionItem, getLoggingForVlogging()));
                }
            } catch (RepositoryException re) {
                if (isLoggingError()) {
                    logError("Unable to determine if promotion status item " + "contains a claimable promotion", re);
                }
                // Keep looking
                continue;
            } catch (IllegalArgumentException iae) {
                if (isLoggingError()) {
                    logError("Unable to determine if promotion status item " + "contains a claimable promotion", iae);
                }
                // Keep looking
                continue;
            }
        }
        // None found
        return couponPromotions;
    }
}
