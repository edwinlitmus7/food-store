package com.foodstore.droplet;

import java.io.IOException;

import javax.servlet.ServletException;

import com.foodstore.interfaces.ParameterConstants;

import atg.commerce.util.RepeatingRequestMonitor;
import atg.nucleus.naming.ParameterName;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * Droplet to convert first 12 digits into stars and display only the last 4 digits 
 * @author Anil
 *
 */
public class HideStringDroplet extends DynamoServlet {
	

	public static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	public static final String HIDE_STRING_DROPLET = "HideStringDroplet";
	public final String STAR_CHARACTERS = "****-****-****-";	

	@Override
	public void service(DynamoHttpServletRequest req,
			DynamoHttpServletResponse res) throws ServletException, IOException {
		if(isLoggingDebug())
			logDebug("Executing HideStringDroplet");
		// credit card numbers are replaced with stars
		String value = req.getParameter(ParameterConstants.VALUE_PARAMETER);
		String element = null;
		if(value != null) {
			element = STAR_CHARACTERS + value.substring(12);
		}
		logInfo("\n Hidden credit card number : " + value + " -- " + element);
		req.setParameter(ParameterConstants.ELEMENT_PARAMETER, element);
		req.serviceLocalParameter(OUTPUT, req, res);
	}
}
