package com.foodstore.droplet.order;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.commerce.order.Order;
import atg.nucleus.naming.ParameterName;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

import com.foodstore.catalog.FoodStore;
import com.foodstore.interfaces.ParameterConstants;
import com.foodstore.order.FoodStoreOrderTools;
import com.foodstore.userprofiling.FoodStoreProfile;

/***
 * Managing catalog according to the schedule.
 * Remove commerce item from the catalog if the corresponding commerce item
 * is not present in that purticular schedule.
 * 
 * @author akhil
 *
 */
public class RemoveUnscheduledCommerceItems extends DynamoServlet {


	private static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	private static final String REMOVE_UNSCHEDULED_COMMERCE_ITEM = "RemoveUnScheduledCommerceItem";

	FoodStore foodStore;
	FoodStoreOrderTools orderTools;

	public FoodStoreOrderTools getOrderTools() {
		return orderTools;
	}

	public void setOrderTools(FoodStoreOrderTools orderTools) {
		this.orderTools = orderTools;
	}

	public FoodStore getFoodStore() {
		return foodStore;
	}

	public void setFoodStore(FoodStore foodStore) {
		this.foodStore = foodStore;
	}

	@Override
	public void service(DynamoHttpServletRequest req,
			DynamoHttpServletResponse res) throws ServletException, IOException {

		if(isLoggingDebug())
			logDebug("Executing RemoveUnscheduledCommerceItems droplet");
		
		boolean perfCancelled = false;
		PerformanceMonitor.startOperation(REMOVE_UNSCHEDULED_COMMERCE_ITEM, req.getServletPath());
		try {

			FoodStoreProfile currentProfile = (FoodStoreProfile) req.getObjectParameter(ParameterConstants.PROFILE);
			Order currentOrder = (Order) req.getObjectParameter(ParameterConstants.ORDER);

			String scheduleInProfile = currentProfile.getSchedule();
			String currentSchedule = getFoodStore().getSchedule();

			// If currentSchedule not equal to scheduleInProfile,
			// then schedule has changed after the user has logged in.
			if(!scheduleInProfile.equals(currentSchedule)) {
				getOrderTools().removeUnscheduledCommerceItems(currentOrder, currentSchedule);
				currentProfile.setSchedule(currentSchedule);
			}

			req.serviceLocalParameter(OUTPUT, req, res);
		}finally {
			try {
				if (!perfCancelled) {
					PerformanceMonitor.endOperation(REMOVE_UNSCHEDULED_COMMERCE_ITEM, req.getServletPath());
					perfCancelled = true;
				}
			} catch (PerfStackMismatchException e2) {
				if (isLoggingWarning())
					logWarning(e2);
			}
		}

	}
}	
