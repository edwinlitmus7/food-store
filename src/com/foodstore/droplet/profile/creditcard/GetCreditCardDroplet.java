package com.foodstore.droplet.profile.creditcard;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import com.foodstore.interfaces.PropertyConstants;
import com.foodstore.profile.FoodStoreProfileTools;

import atg.nucleus.naming.ParameterName;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
import atg.userprofiling.Profile;

/**
 * Retrieves all the credit cards of logged in user
 * @author Anil
 *
 */
public class GetCreditCardDroplet extends DynamoServlet {
	Profile profile;
	FoodStoreProfileTools profileTools;


	private final String CREDIT_CARD_NICKNAME = "creditCardNickname";

	private static final String GET_CREDITCARD_DROPLET = "GetCreditCardDroplet";
	private static final ParameterName OUTPUT = ParameterName.getParameterName("output");
	private static final ParameterName OUTPUT_START = ParameterName.getParameterName("outputStart");
	private static final ParameterName OUTPUT_END = ParameterName.getParameterName("outputEnd");
	private static final ParameterName EMPTY = ParameterName.getParameterName("empty");

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}


	public FoodStoreProfileTools getProfileTools() {
		return profileTools;
	}

	public void setProfileTools(FoodStoreProfileTools profileTools) {
		this.profileTools = profileTools;
	}


	@Override
	public void service(DynamoHttpServletRequest req,
			DynamoHttpServletResponse res) throws ServletException, IOException {

		RepositoryItem mprofile = getProfile();
		boolean perfCancelled = false;

		if(isLoggingDebug())
			logDebug("Executing GetCreditCardDroplet");
		
		PerformanceMonitor.startOperation(GET_CREDITCARD_DROPLET, req.getServletPath());

		try {
			logInfo("\n Loggged in user : " + mprofile + "\n");
			Map<String, Object> creditCards = profileTools.getCreditCards(mprofile);
			RepositoryItem creditCard;
			if(creditCards.isEmpty()) {
				req.serviceLocalParameter(EMPTY, req, res);
			} else {
				req.serviceLocalParameter(OUTPUT_START, req, res);

				// for each credit card service output parameter
				for(Map.Entry<String, Object> entry: creditCards.entrySet()) {
					creditCard = (RepositoryItem) entry.getValue();
					req.setParameter(PropertyConstants.CREDITCARD_PROPERTY, creditCard);

					req.setParameter(CREDIT_CARD_NICKNAME, entry.getKey());
					req.serviceLocalParameter(OUTPUT, req, res);
				}
				req.serviceLocalParameter(OUTPUT_END, req, res);
			}
		}
		finally {
			try {
				if (!perfCancelled) {
					PerformanceMonitor.endOperation(GET_CREDITCARD_DROPLET, req.getServletPath());
					perfCancelled = true;
				}
			} catch (PerfStackMismatchException e2) {
				if (isLoggingWarning())
					logWarning(e2);
			}
		}

	}
}
