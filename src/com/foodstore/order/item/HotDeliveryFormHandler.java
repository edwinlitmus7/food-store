package com.foodstore.order.item;

import java.io.IOException;

import javax.servlet.ServletException;
import com.foodstore.order.beans.FoodStoreCommerceItemImpl;
import com.foodstore.order.purchase.FoodStoreCartModifierFormHandler;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderHolder;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.droplet.GenericFormHandler;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.pipeline.RunProcessException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.ui.reporting.model.ItemDescriptor;

/**
 * Form Handler is called when hotDelivery checkbox is checked or unchecked
 * 
 * @author Vineeth
 *
 */
public class HotDeliveryFormHandler extends FoodStoreCartModifierFormHandler{
	public String hotDeliveryFailureURL;
	public String getHotDeliveryFailureURL() {
		return hotDeliveryFailureURL;
	}
	public void setHotDeliveryFailureURL(String hotDeliveryFailureURL) {
		this.hotDeliveryFailureURL = hotDeliveryFailureURL;
	}
	public String hotDeliverySuccessURL;
	public String getHotDeliverySuccessURL() {
		return hotDeliverySuccessURL;
	}
	public void setHotDeliverySuccessURL(String hotDeliverySuccessURL) {
		this.hotDeliverySuccessURL = hotDeliverySuccessURL;
	}
	public String selectedCommerceId;
	public String getSelectedCommerceId() {
		return selectedCommerceId;
	}
	public void setSelectedCommerceId(String selectedCommerceId) {
		logDebug("Setting selectedCommerceId");
		logDebug(selectedCommerceId);
		this.selectedCommerceId = selectedCommerceId;
	}
	
	/**
	 * Called when user checks in hotDelivery checkbox
	 * 
	 */
	public boolean handleUpdateHotDelivery(DynamoHttpServletRequest request,DynamoHttpServletResponse response) throws ServletException, IOException{
		OrderHolder order = (OrderHolder)request.resolveName("/atg/commerce/ShoppingCart");
		try {
			FoodStoreCommerceItemImpl item=(FoodStoreCommerceItemImpl) order.getCurrent().getCommerceItem(getSelectedCommerceId());
			item.setIshotDelivery(true);
			try {
				runProcessRepriceOrder("ORDER_TOTAL", (Order) order.getCurrent(), getUserPricingModels(), getUserLocale(), getProfile(),
				        createRepriceParameterMap());
				
					order.getOrderManager().updateOrder(order.getCurrent());
			
				
			} catch (RunProcessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (CommerceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (CommerceItemNotFoundException | InvalidParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return checkFormRedirect(getHotDeliverySuccessURL(),getHotDeliveryFailureURL(),request,response);
	}
	
	
	/**
	 * called when user unchecks in hotDelivery checkbox
	 * 
	 */
	public boolean handleRemoveHotDelivery(DynamoHttpServletRequest request,DynamoHttpServletResponse response) throws ServletException, IOException{
		OrderHolder order = (OrderHolder)request.resolveName("/atg/commerce/ShoppingCart");
		try {
			FoodStoreCommerceItemImpl item=(FoodStoreCommerceItemImpl) order.getCurrent().getCommerceItem(getSelectedCommerceId());
			item.setIshotDelivery(false);
			try {
				runProcessRepriceOrder("ORDER_TOTAL",order.getCurrent(), getUserPricingModels(), getUserLocale(), getProfile(),
				        createRepriceParameterMap());
				
					order.getOrderManager().updateOrder(order.getCurrent());
			
				
			} catch (RunProcessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch (CommerceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (CommerceItemNotFoundException | InvalidParameterException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return checkFormRedirect(getHotDeliverySuccessURL(),getHotDeliveryFailureURL(),request,response);
	}
	
}
