package com.foodstore.order;

import java.util.ArrayList;
import java.util.List;

import com.foodstore.order.beans.FoodStoreCommerceItemImpl;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

public class FoodStoreCommerceItemManager extends CommerceItemManager {
	
	@Override
	protected boolean shouldMergeItems(Order pOrder,
			CommerceItem pExistingItem, CommerceItem pNewItem) {
		String skuId = pNewItem.getCatalogRefId();
		Integer isSideSku = 0;
		try {
			isSideSku = (Integer) ((RepositoryItem) getSku(skuId, null)).getPropertyValue("isSideSku");
			if(isSideSku == null) {
				isSideSku = 0;
			}
		} catch (RepositoryException e) {
			logError("Cannot find sku" + e);
		}
		return (isSideSku == 1)? false: super.shouldMergeItems(pOrder, pExistingItem, pNewItem);
	}

	public void updateCommerceItemChilds(FoodStoreCommerceItemImpl parentItem,
			List<CommerceItem> updatedChildItems) throws RepositoryException {
		parentItem.setChildItems(updatedChildItems);
		((FoodStoreOrderTools) getOrderTools()).updateChildItems(parentItem, updatedChildItems);
	}


	public void findSideSkusToBeAdded(CommerceItem parentItem,
			List<CommerceItem> childItems, List<CommerceItem> itemsToBeAdded, List<CommerceItem> itemsToBeRemoved) {
		List<CommerceItem> existingChildSkus = ((FoodStoreCommerceItemImpl) parentItem).getChildItems();
		if(existingChildSkus != null) {
			for(CommerceItem item: existingChildSkus) {
				if(isItemToBeRemoved(item, childItems)) {
					itemsToBeRemoved.add(item);
				} else {
					itemsToBeAdded.add(item);
				}
			}
		}
		filterDuplicateSideSkus(childItems, existingChildSkus, itemsToBeAdded);
	}

	private void filterDuplicateSideSkus(List<CommerceItem> childItems, List<CommerceItem> existingChildSkus,
			List<CommerceItem> itemsToBeAdded) {
		for(CommerceItem item: childItems) {
			if(!isAlreadyInCart(item, existingChildSkus)) {
				itemsToBeAdded.add(item);
			}
		}
	}

	private boolean isAlreadyInCart(CommerceItem pItem,
			List<CommerceItem> existingChildSkus) {
		if(existingChildSkus != null) {
			for(CommerceItem item: existingChildSkus) {
				if(item.getCatalogRefId().equals(pItem.getCatalogRefId())) {
					if(isLoggingDebug()) {
						logDebug("Sidesku " + item.getCatalogRefId() + " has been added as a child!");
					}
					return true;
				}
			}
		}
		return false;
	}

	private boolean isItemToBeRemoved(CommerceItem pItem, List<CommerceItem> childItems) {
		for(CommerceItem item: childItems) {
			if(item.getCatalogRefId().equals(pItem.getCatalogRefId())) {
				return false;
			}
		}
		return true;
	}

	public void removeItemsFromOrder(Order order,
			List<CommerceItem> itemsToBeRemoved) {
		for(CommerceItem item: itemsToBeRemoved) {
			try {
				if(isLoggingDebug()) {
					logDebug("Removing item " + item.getId() + " from order!");
				}
				removeItemFromOrder(order, item.getId());
			} catch (CommerceException e) {
				logError("Error while removing item " + item.getId());
				e.printStackTrace();
			}
		}
	}
}
