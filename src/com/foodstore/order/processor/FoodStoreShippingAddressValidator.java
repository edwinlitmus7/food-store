package com.foodstore.order.processor;

import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import atg.commerce.order.processor.ShippingAddrValidatorImpl;
import atg.commerce.util.PlaceUtils;

import atg.core.util.Address;

import atg.servlet.RequestLocale;

import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.order.beans.FoodStoreContactInfo;

/***
 * Class to validate the Shipping address of the FoodStore application
 * 
 * @author Anil
 *
 */
public class FoodStoreShippingAddressValidator extends
		ShippingAddrValidatorImpl {
	private boolean validateMobile = false;
	private boolean validateNickName = false;
	private PlaceUtils placeUtils;
	private boolean validateTypeOfAddress = false;
	private boolean validateCountryStateCombination = false;
	private Properties countryMap = null;
	private Properties stateMap = null;
	RequestLocale mRequestLocale;

	public FoodStoreShippingAddressValidator() {

	}

	public boolean isValidateCountryStateCombination() {
		return validateCountryStateCombination;
	}

	public void setValidateCountryStateCombination(
			boolean validateCountryStateCombination) {
		this.validateCountryStateCombination = validateCountryStateCombination;
	}

	public Properties getCountryMap() {
		return countryMap;
	}

	public void setCountryMap(Properties countryMap) {
		this.countryMap = countryMap;
	}

	public Properties getStateMap() {
		return stateMap;
	}

	public void setStateMap(Properties stateMap) {
		this.stateMap = stateMap;
	}

	public boolean isValidateMobile() {
		return validateMobile;
	}

	public void setValidateMobile(boolean validateMobile) {
		this.validateMobile = validateMobile;
	}

	public boolean isValidateNickName() {
		return validateNickName;
	}

	public void setValidateNickName(boolean validateNickName) {
		this.validateNickName = validateNickName;
	}

	public boolean isValidateTypeOfAddress() {
		return validateTypeOfAddress;
	}

	public void setValidateTypeOfAddress(boolean validateTypeOfAddress) {
		this.validateTypeOfAddress = validateTypeOfAddress;
		
	}

	public PlaceUtils getPlaceUtils() {
		return placeUtils;
	}

	public void setPlaceUtils(PlaceUtils placeUtils) {
		this.placeUtils = placeUtils;
	}

	public void setRequestLocale(RequestLocale pRequestLocale) {
		mRequestLocale = pRequestLocale;
	}

	public RequestLocale getRequestLocale() {
		return mRequestLocale;
	}

	/* Validation files */
	private boolean validateMobile(String mobile) {
		if (mobile == null || mobile.isEmpty()) {
			return false;
		}
		return true;
	}

	private boolean validateNickName(String nickname) {
		if (nickname == null || nickname.isEmpty()) {
			return false;
		}
		return true;
	}

	private boolean validateTypeOfAddress(String typeOfAddress) {
		if (typeOfAddress == null || typeOfAddress.isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Returns a map which contains the resources and the associated errors.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected Map compileResourceAndErrorMaps() {
		Map resourceAndErrorMaps = super.compileResourceAndErrorMaps();
		Map resourceMap = (Map) resourceAndErrorMaps.get(ErrorsAndExceptionMessages.RESOURCEMAP_KEY);
		Map errorMap = (Map) resourceAndErrorMaps.get(ErrorsAndExceptionMessages.ERRORMAP_KEY);

		resourceMap.put(ErrorsAndExceptionMessages.NICKNAME_MISSING, ErrorsAndExceptionMessages.SHIP_NICKNAME_MISSING);
		resourceMap.put(ErrorsAndExceptionMessages.TYPE_OF_ADDRESS_MISSING, ErrorsAndExceptionMessages.SHIP_TYPE_OF_ADDRESS_MISSING);
		resourceMap.put(ErrorsAndExceptionMessages.MOBILE_MISSING, ErrorsAndExceptionMessages.SHIP_MOBILE_MISSING);
		resourceMap.put(ErrorsAndExceptionMessages.INVALID_COUNTRY_STATE_COMBINATION,
				ErrorsAndExceptionMessages.SHIP_INVALID_COUNTRY_STATE_COMBINATION);

		errorMap.put(ErrorsAndExceptionMessages.NICKNAME_MISSING, ErrorsAndExceptionMessages.SHIP_NICKNAME_MISSING);
		errorMap.put(ErrorsAndExceptionMessages.TYPE_OF_ADDRESS_MISSING, ErrorsAndExceptionMessages.SHIP_TYPE_OF_ADDRESS_MISSING);
		errorMap.put(ErrorsAndExceptionMessages.MOBILE_MISSING, ErrorsAndExceptionMessages.SHIP_MOBILE_MISSING);
		errorMap.put(ErrorsAndExceptionMessages.INVALID_COUNTRY_STATE_COMBINATION,
				ErrorsAndExceptionMessages.SHIP_INVALID_COUNTRY_STATE_COMBINATION);
		
		resourceMap.put(ErrorsAndExceptionMessages.NICKNAME_INVALID, ErrorsAndExceptionMessages.SHIP_NICKNAME_INVALID);
		resourceMap.put(ErrorsAndExceptionMessages.TYPE_OF_ADDRESS_INVALID, ErrorsAndExceptionMessages.SHIP_TYPE_OF_ADDRESS_INVALID);
		resourceMap.put(ErrorsAndExceptionMessages.MOBILE_INVALID, ErrorsAndExceptionMessages.SHIP_MOBILE_INVALID);
	
		errorMap.put(ErrorsAndExceptionMessages.NICKNAME_INVALID, ErrorsAndExceptionMessages.SHIP_NICKNAME_INVALID);
		errorMap.put(ErrorsAndExceptionMessages.TYPE_OF_ADDRESS_INVALID, ErrorsAndExceptionMessages.SHIP_TYPE_OF_ADDRESS_INVALID);
		errorMap.put(ErrorsAndExceptionMessages.MOBILE_INVALID, ErrorsAndExceptionMessages.SHIP_MOBILE_INVALID);
		
		
		resourceAndErrorMaps.put(ErrorsAndExceptionMessages.RESOURCEMAP_KEY, resourceMap);
		resourceAndErrorMaps.put(ErrorsAndExceptionMessages.ERRORMAP_KEY, errorMap);

		return (Map) resourceAndErrorMaps;
	}
	
	/**
	 * Validates the entered Shipping address.
	 */
	@SuppressWarnings("rawtypes")
	@Override
	protected Map validateAddress(Address pAddress, ResourceBundle pResources,
			Map pResourceAndErrorKeyMaps) {
		if(pResourceAndErrorKeyMaps == null){
			pResourceAndErrorKeyMaps = compileResourceAndErrorMaps();
		}
		Map errors = super.validateAddress(pAddress, pResources,
				pResourceAndErrorKeyMaps);
		if (isLoggingDebug())
			logDebug((new StringBuilder())
					.append(" FoodStore Shipping address Validator . Validating address of type ")
					.append(pAddress.getClass().getName()).toString());
		FoodStoreContactInfo contactInfo = (FoodStoreContactInfo) pAddress;
		if (isValidateMobile()) {
			if (!validateMobile(contactInfo.getMobile())) {
				addError(ErrorsAndExceptionMessages.MOBILE_INVALID, pResources, pResourceAndErrorKeyMaps,
						errors);
		}
		}
		if (isValidateNickName()) {
			if (!validateNickName(contactInfo.getNickname())) {
				addError(ErrorsAndExceptionMessages.NICKNAME_INVALID, pResources,
						pResourceAndErrorKeyMaps, errors);
			}
		}
		if (isValidateTypeOfAddress()) {
			if (!validateTypeOfAddress(contactInfo.getTypeOfAddress())) {
				addError(ErrorsAndExceptionMessages.TYPE_OF_ADDRESS_INVALID, pResources,
						pResourceAndErrorKeyMaps, errors);
			}
		}
		if (isValidateCountryStateCombination()) {

			if (!validateCountryStateCombination(pAddress, pResources,
					pResourceAndErrorKeyMaps, errors)) {

				errors.put(ErrorsAndExceptionMessages.INVALID_COUNTRY_STATE_COMBINATION,
						ErrorsAndExceptionMessages.COUNTRY_STATE_COMBINATION_INVALID);
			}
		}

		return errors;
	}

	/**
	 * Validates the entered country state combination in the shipping address.
	 * 
	 * @param pAddress
	 * @param pResources
	 * @param pResourceAndErrorKeyMaps
	 * @param errors
	 * @return
	 */
	protected boolean validateCountryStateCombination(Address pAddress,
			ResourceBundle pResources, Map pResourceAndErrorKeyMaps, Map errors) {
		// TODO Auto-generated method stub

		if (pAddress != null) {
			if (isLoggingDebug()) {
				logDebug("-----------Checking  Country state Combination------------");
				logDebug(pAddress.toString());
			}

			if (countryMap.containsKey(pAddress.getCountry())) {
				if (stateMap.containsKey(pAddress.getState())) {
					if (isLoggingDebug()) {
						logDebug("----------Valid Country state Combination------------");
					}
					return true;
				}
			}
			if (isLoggingDebug()) {
				logDebug("-----------Wrong  Country state Combination------------");
			}

		}
		return false;
	}

}
