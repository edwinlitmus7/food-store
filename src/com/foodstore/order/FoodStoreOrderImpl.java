package com.foodstore.order;

import atg.commerce.order.OrderImpl;

/***
 * This class implements the Order and ChangedProperties
 * interfaces and provides an implementation for the methods. 
 * 
 * Additional property to indicate that the order is special.
 * 
 * @author akhil
 *
 */
public class FoodStoreOrderImpl extends OrderImpl {
	
	
	private static final long serialVersionUID = 1L;
	
	
	private boolean specialOrder;
	
	private boolean specialOrderCloseness;

	public boolean isSpecialOrderCloseness() {
		return specialOrderCloseness;
	}

	public void setSpecialOrderCloseness(boolean specialOrderCloseness) {
		this.specialOrderCloseness = specialOrderCloseness;
	}

	public boolean isSpecialOrder() {
		return specialOrder;
	}

	public void setSpecialOrder(boolean specialOrder) {
		this.specialOrder = specialOrder;
	}

}
