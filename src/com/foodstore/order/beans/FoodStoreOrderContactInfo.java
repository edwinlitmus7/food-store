package com.foodstore.order.beans;

public class FoodStoreOrderContactInfo extends FoodStoreContactInfo {
	/**
	 * Add additional properties to be copied from repository
	 */
	private static final long serialVersionUID = 1L;
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
