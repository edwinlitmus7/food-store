package com.foodstore.order.beans;

import com.foodstore.interfaces.PropertyConstants;

import atg.commerce.order.RepositoryContactInfo;

/***
 * The customized properties associated with the Foodstore contactinfo object.
 * 
 * @author Anil
 *
 */
public class FoodStoreContactInfo extends RepositoryContactInfo {

	
	private static final long serialVersionUID = 1L;
	
	public String getNickname() {
		return (String) mRepositoryItem.getPropertyValue(PropertyConstants.NICKNAME_PROPERTY);

	}

	public void setNickname(String nickname) {
		mRepositoryItem.setPropertyValue(PropertyConstants.NICKNAME_PROPERTY, nickname);
		addChangedProperty(PropertyConstants.NICKNAME_PROPERTY);
	}

	public String getTypeOfAddress() {
		return (String) mRepositoryItem.getPropertyValue(PropertyConstants.TYPE_OF_ADDRESS_PROPERTY);
	}

	public void setTypeOfAddress(String typeOfAddress) {
		mRepositoryItem.setPropertyValue(PropertyConstants.TYPE_OF_ADDRESS_PROPERTY, typeOfAddress);
		addChangedProperty(PropertyConstants.TYPE_OF_ADDRESS_PROPERTY);
	}

	public String getMobile() {
		return (String) mRepositoryItem.getPropertyValue(PropertyConstants.MOBILE_PROPERTY);

	}

	public void setMobile(String mobile) {
		mRepositoryItem.setPropertyValue(PropertyConstants.MOBILE_PROPERTY, mobile);
		addChangedProperty(PropertyConstants.MOBILE_PROPERTY);
	}
 
	public String toString() {
		StringBuilder address = new StringBuilder();
		address.append("Address ").append("[").append("mAddress1=")
				.append(getAddress1()).append(", ").append("mAddress2=")
				.append(getAddress2()).append(", ").append("mAddress3=")
				.append(getAddress3()).append(", ").append("mCity=")
				.append(getCity()).append(", ").append("mState=")
				.append(getState()).append(", ").append("mCountry=")
				.append(getCountry()).append(", ").append("mCounty=")
				.append(getCounty()).append(", ").append("mFirstName=")
				.append(getFirstName()).append(", ").append("mLastName=")
				.append(getLastName()).append(", ").append("mMiddleName=")
				.append(getMiddleName()).append(", ").append("mOwnerId=")
				.append(getOwnerId()).append(", ").append("mPrefix=")
				.append(getPrefix()).append(", ").append("mSuffix=")
				.append(getSuffix()).append(", ").append("mNickname=")
				.append(getNickname()).append(",").append("mTypeOfAddress=")
				.append(getTypeOfAddress()).append(",").append("mMobile=")
				.append(getMobile()).append("]");
		return address.toString();
	}



}
