package com.foodstore.order.beans;

import java.util.List;

import com.foodstore.interfaces.FoodStorePropertyNameConstants;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemImpl;

public class FoodStoreCommerceItemImpl extends CommerceItemImpl {
	private static final long serialVersionUID = 402831232538635462L;
	private List<CommerceItem> childItems;
	private boolean ishotDelivery;
	
	public boolean isIshotDelivery() {
		return ishotDelivery;
	}

	public void setIshotDelivery(boolean ishotDelivery) {
		this.ishotDelivery = ishotDelivery;
	}

	public String getParentItem() {
		return (String) this.getPropertyValue(FoodStorePropertyNameConstants.PARENT_ITEM);
	}
	
	public void setParentItem(String parentItem) {
		this.setPropertyValue(FoodStorePropertyNameConstants.PARENT_ITEM, parentItem);
	}
	
	public List<CommerceItem> getChildItems() {
		return childItems;
	}
	
	public void setChildItems(List<CommerceItem> strings) {
		this.childItems = strings;
	}
}
