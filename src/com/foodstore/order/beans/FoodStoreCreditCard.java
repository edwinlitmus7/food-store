package com.foodstore.order.beans;

import com.foodstore.interfaces.PropertyConstants;

import atg.commerce.order.CreditCard;

/**
 * Bean class that contains food store specific properties for credit card.
 * @author Anil
 *
 */
public class FoodStoreCreditCard extends CreditCard {
	
	private static final long serialVersionUID = 1L;
	
	public String getNameOnCard() {
		 return (String)getPropertyValue(PropertyConstants.NAME_ON_CARD_PROPERTY);
	}
	public void setNameOnCard(String nameOnCard) {
		setPropertyValue(PropertyConstants.NAME_ON_CARD_PROPERTY, nameOnCard);
	}
}

