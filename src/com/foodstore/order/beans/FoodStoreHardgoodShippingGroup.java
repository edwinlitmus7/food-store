package com.foodstore.order.beans;

import java.util.List;

import com.foodstore.interfaces.PropertyConstants;

import atg.commerce.order.HardgoodShippingGroup;

/***
 * Adding customized properties to hardgood shipping group.
 * 
 * @author Anil
 *
 */
public class FoodStoreHardgoodShippingGroup extends HardgoodShippingGroup {

	private static final long serialVersionUID = 1L;
	private List mPropertyContainerPropertyNames;
	private String hardgoodShippingGroupName;
	
	public String getHardgoodShippingGroupName() {
		return hardgoodShippingGroupName;
		
	}

	public void setHardgoodShippingGroupName(String hardgoodShippingGroupName) {
		this.hardgoodShippingGroupName = hardgoodShippingGroupName;
		
	}

	/**
	 * Adding the newly added shipping address properties to the property container.
	 */
	@SuppressWarnings("unchecked")
	public List initializePropertyContainerPropertyNames() {
		mPropertyContainerPropertyNames = super
				.initializePropertyContainerPropertyNames();
		mPropertyContainerPropertyNames.add(PropertyConstants.SHIPPINGADDRESS_NICKNAME_PROPERTY);
		mPropertyContainerPropertyNames.add(PropertyConstants.SHIPPINGADDRESS_TYPEOFADDRESS_PROPERTY);
		mPropertyContainerPropertyNames.add(PropertyConstants.SHIPPINGADDRESS_MOBILE_PROPERTY);
		
		return mPropertyContainerPropertyNames;
	}
}
