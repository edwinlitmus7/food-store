package com.foodstore.order;
import java.util.ArrayList;


import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import com.foodstore.interfaces.PropertyConstants;
import com.foodstore.order.beans.FoodStoreCommerceItemImpl;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.OrderTools;
import atg.commerce.order.RelationshipNotFoundException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/**
 * Tools class that contains food store specific implemetations for order related functions
 * @author Anil
 *
 */
public class FoodStoreOrderTools extends OrderTools {
	
	/**
	 * Removes items that are not available for the current schedule.
	 * i.e user adds certain items to basket which are specific to the current schedule.
	 * These items are removed from the cart when the schedule changes.
	 * 
	 * Method queries commerce items from order, iterates over each item and checks if each one
	 * of those item is scheduled for the current schedule.
	 * If not, then it is removed from the cart.
	 * @param order
	 * @param schedule
	 */
	public void removeUnscheduledCommerceItems(Order order, String schedule) {
		
		if(isLoggingDebug())
			logDebug("Executing removeUnscheduledCommerceItems method");
		
		List<FoodStoreCommerceItemImpl> commerceItems = order.getCommerceItems();
		Iterator iterator = commerceItems.iterator();
		
		if(isLoggingDebug()) {
			vlogDebug("Commerce items in cart = {0}", commerceItems);
		}
		List<String> unscheduledItems = new ArrayList<>();
		while(iterator.hasNext()) {
			FoodStoreCommerceItemImpl commerceItem = (FoodStoreCommerceItemImpl) iterator.next();
			RepositoryItem product = (RepositoryItem) commerceItem.getAuxiliaryData().getProductRef();
			List<RepositoryItem> ancestorCategories = (List<RepositoryItem>) product.getPropertyValue("ancestorCategories");
			Boolean removeItemFromCart = true;
			
			// if any of the ancestor category for the commerceItem is scheduled for current schedule
			// then removeItemFromCart flag is set to false
			for(RepositoryItem category: ancestorCategories) {
				String categoryScheduleId = (String) category.getPropertyValue(PropertyConstants.SCHEDULEID_PROPERTY);
				if(isLoggingDebug()) {
					vlogDebug("Current schedule = {0}, Commerce item schedule = {1}", schedule, categoryScheduleId);
				}
				if(categoryScheduleId.equals(schedule)) {
					removeItemFromCart = false;
					break;
				}
			}
			
			// if removeItemFromCart is set,
			// commerceItem is added to the list of items to be removed
			if(removeItemFromCart) {
				String commerceItemId = (String) commerceItem.getId();
				if (commerceItem.getParentItem() == null) {
					unscheduledItems.add(commerceItemId);
				}
			}
			
		}
		
		// removes all the items added in the list
		for(String itemId: unscheduledItems) {
			try {
				order.getCommerceItem(itemId).removeAllShippingGroupRelationships();
				order.getCommerceItem(itemId).removeAllPaymentGroupRelationships();
				order.getCommerceItem(itemId).removeAllCostCenterRelationships();
				order.removeCommerceItem(itemId);
			} catch (CommerceItemNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidParameterException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
			
			if(isLoggingDebug())
				logDebug("Removing item " + itemId + " from order " + order);
		}
	}

	public void updateChildItems(CommerceItem parentItem,
			List<CommerceItem> updatedChildItems) throws RepositoryException {
		MutableRepository mutRep = (MutableRepository) getOrderRepository();
		if(isLoggingDebug()) {
			logDebug("Setting childSkus of " + parentItem.getId() + " to " + updatedChildItems);
		}
		RepositoryItem[] repItems;
		String[] items = new String[updatedChildItems.size()];
		for(int i=0; i<updatedChildItems.size(); i++) {
			items[i] = updatedChildItems.get(i).getId();
		}
		repItems = mutRep.getItems(items, getBeanNameToItemDescriptorMap().getProperty(getCommerceItemTypeClassMap().getProperty("default")));
		MutableRepositoryItem mutRepItem = mutRep.getItemForUpdate(parentItem.getId(), getBeanNameToItemDescriptorMap().getProperty(getCommerceItemTypeClassMap().getProperty("default")));
		mutRepItem.setPropertyValue("childItems", Arrays.asList(repItems));
		mutRep.updateItem(mutRepItem);
	}
}
