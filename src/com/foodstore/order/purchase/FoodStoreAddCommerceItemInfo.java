package com.foodstore.order.purchase;

import atg.commerce.order.purchase.AddCommerceItemInfo;

public class FoodStoreAddCommerceItemInfo extends AddCommerceItemInfo{
	
	protected String parentId;
	protected String[] childIds;
	
	public String getParentId() {
		return parentId;
	}
	
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	
	public String[] getChildIds() {
		return childIds;
	}
	
	public void setChildIds(String[] childIds) {
		this.childIds = childIds;
	}
	
}
