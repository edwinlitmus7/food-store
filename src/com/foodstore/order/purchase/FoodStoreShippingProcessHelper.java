package com.foodstore.order.purchase;

import java.util.Map;

import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.profile.CommercePropertyManager;
import atg.core.util.ResourceUtils;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

import com.foodstore.profile.FoodStoreProfileTools;

/***
 * Contains the common functionalities for Shipping group formhandler.
 * 
 * @author Anil
 *
 */
public class FoodStoreShippingProcessHelper extends PurchaseProcessHelper {
	private static final String MY_RESOURCE_NAME = "atg.commerce.order.purchase.PurchaseProcessResources";
	private static java.util.ResourceBundle sResourceBundle = 
			atg.core.i18n.LayeredResourceBundle.getBundle(MY_RESOURCE_NAME, 
					atg.service.dynamo.LangLicense.getLicensedDefault());

	/**
	 * Error messages for logging
	 */
	public static final String MSG_ERROR_REMOVING_PROFILE_REPOSITORY_ADDRESS = 
			"errorRemovingProfileRepositoryAddress";



	//---------------------------------------------------------------------------
	// Utility Methods
	//---------------------------------------------------------------------------

	/**
	 * Removes a shipping group from the container.
	 *
	 * @param pRemoveShippingAddressNickName - Nickname for the address to be removed.
	 * @param pShippingGroupMapContainer - map of all shipping groups for the profile.
	 * @param pProfile - the profile.
	 */
	public void removeShippingAddress(RepositoryItem pProfile,
			String pRemoveShippingAddressNickName,
			ShippingGroupMapContainer pShippingGroupMapContainer)  {

		HardgoodShippingGroup hgsg = (HardgoodShippingGroup) 
				pShippingGroupMapContainer.getShippingGroup(pRemoveShippingAddressNickName);
		if (hgsg != null) {
			pShippingGroupMapContainer.removeShippingGroup(pRemoveShippingAddressNickName);
		}  
	}

	/**
	 * If the shipping address nickname is in the profile's addresses map, remove it from the profile.
	 *
	 * @param pProfile - shopper profile.
	 * @param pRemoveShippingAddressNickName - Nickname for the address to be removed.
	 */
	public void removeShippingAddressFromProfile(RepositoryItem pProfile,
			String pRemoveShippingAddressNickName) {

		if(isLoggingDebug())
			logDebug("Executing removeShippingAddressFromProfile method");
		
		FoodStoreProfileTools profileTools = (FoodStoreProfileTools) 
				getOrderManager().getOrderTools().getProfileTools();

		String secondaryAdressProperty = 
				((CommercePropertyManager)profileTools.getPropertyManager()).getSecondaryAddressPropertyName();

		Map addresses = (Map) pProfile.getPropertyValue(secondaryAdressProperty);
		if (addresses.containsKey(pRemoveShippingAddressNickName)) {
			try {
				profileTools.removeProfileRepositoryAddress(pProfile, pRemoveShippingAddressNickName, true);
			} 
			catch (RepositoryException ex) {
				if (isLoggingError()){
					logError(ResourceUtils.getMsgResource(MSG_ERROR_REMOVING_PROFILE_REPOSITORY_ADDRESS,
							MY_RESOURCE_NAME, sResourceBundle), ex);
				}
			}
		}
	}
}
