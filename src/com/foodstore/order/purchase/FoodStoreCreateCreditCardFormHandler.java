package com.foodstore.order.purchase;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;

import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.PropertyConstants;
import com.foodstore.logging.FoodStorePerformanceLogger;
import com.foodstore.logging.PerformanceStatus;

import atg.commerce.order.CreditCard;
import atg.commerce.order.purchase.CreateCreditCardFormHandler;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.util.AddressValidator;
import atg.core.util.StringUtils;
import atg.droplet.DropletFormException;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;

/***
 * CreditCard formhandler to handle credit card information.
 * 
 * @author akhil
 *
 */
public class FoodStoreCreateCreditCardFormHandler extends
		CreateCreditCardFormHandler {
	private FoodStorePerformanceLogger foodStorePerformanceLogger;
	
	public FoodStorePerformanceLogger getFoodStorePerformanceLogger() {
		return foodStorePerformanceLogger;
	}

	public void setFoodStorePerformanceLogger(FoodStorePerformanceLogger foodStorePerformanceLogger) {
		this.foodStorePerformanceLogger = foodStorePerformanceLogger;
	}
	
	AddressValidator addressValidator;
	
	public AddressValidator getAddressValidator() {
		return addressValidator;
	}
	
	public void setAddressValidator(AddressValidator addressValidator) {
		this.addressValidator = addressValidator;
	}
	
	boolean validateBillingAddress;

	public boolean isValidateBillingAddress() {
		return validateBillingAddress;
	}
	
	public void setValidateBillingAddress(boolean validateBillingAddress) {
		this.validateBillingAddress = validateBillingAddress;
	}
	
	/**
	 * Validating the entered billing and credit card details are correct.
	 */
	@SuppressWarnings("rawtypes")
	private void validateBillingAddress(){
		
		if(isLoggingDebug())
			logDebug("Executing validateBillingAddress method");
		
		DynamoHttpServletRequest req = ServletUtil.getCurrentRequest();
		DynamoHttpServletResponse res = ServletUtil.getCurrentResponse();
		CreditCard creditCard = getCreditCard();
		String name = getCreditCardName();
		if(StringUtils.isBlank(name))
		{
			if(isLoggingDebug())
				logDebug("Billing Group Nickname is NULL. I can't process without name.");

			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.NO_VALID_NICKNAME, propertyPath));
			return;
		}
		if(creditCard == null || creditCard.getBillingAddress() == null)
		{
			if(isLoggingDebug())
				logDebug("Payment Group or Billing Address can't be NULL.");


			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.NO_PAYMENTGROUP_OR_ADDRESS, propertyPath));

			return;
		}
		try
		{
			Collection errors = getAddressValidator().validateAddress(creditCard.getBillingAddress(), getUserLocale(req, res));
			if(errors != null && !errors.isEmpty())
			{
				String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
				if(isLoggingDebug())
					logDebug("Errors in the Shipping address validation.");
				Object entry;
				for(Iterator i$ = errors.iterator(); i$.hasNext(); addFormException(new DropletFormException((String)entry, propertyPath)))
					entry = i$.next();

			}
		}
		catch(ServletException e)
		{
			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.ERROR_VALIDATING_CREDITCARD, e, propertyPath));
		}
		catch(IOException e)
		{
			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.ERROR_VALIDATING_CREDITCARD, e, propertyPath));
		}
	}

	/**
	 * Creating credit card information using the user entered details.
	 */
@Override
public void createCreditCard(DynamoHttpServletRequest pRequest,
		DynamoHttpServletResponse pResponse) {
	
	if(isLoggingDebug())
		logDebug("Executing create credit card method");
	
	CreditCard creditCard = getCreditCard();
    PaymentGroupMapContainer container = getContainer();
    String name = getCreditCardName();
    if(StringUtils.isEmpty(name) && isGenerateNickname())
    {
        name = getCommerceProfileTools().getUniqueCreditCardNickname(creditCard, getProfile(), null);
        setCreditCardName(name);
    }
    
    if(isLoggingDebug())
    	logDebug("CreditCard nickname : "+name);
    try
    {
        if(isValidateCreditCard())
            validateCreditCard(getCreditCard(), pRequest, pResponse);
        if(isValidateBillingAddress()){
        	validateBillingAddress();
        }
    }
    catch(ServletException se)
    {
        try
        {
            String msg = formatUserMessage(ErrorsAndExceptionMessages.ERROR_INVALIDATION, pRequest, pResponse);
            String propertyPath = generatePropertyPath(PropertyConstants.CREDITCARD_PROPERTY);
            addFormException(new DropletFormException(msg, se, propertyPath, ErrorsAndExceptionMessages.ERROR_INVALIDATION));
        }
        catch(Exception exception)
        {
            if(isLoggingError())
                logError(exception);
        }
    }
    catch(IOException ioe)
    {
        try
        {
            String msg = formatUserMessage(ErrorsAndExceptionMessages.ERROR_INVALIDATION, pRequest, pResponse);
            String propertyPath = generatePropertyPath(PropertyConstants.CREDITCARD_PROPERTY);
            addFormException(new DropletFormException(msg, ioe, propertyPath, ErrorsAndExceptionMessages.ERROR_INVALIDATION));
        }
        catch(Exception exception)
        {
            if(isLoggingError())
                logError(exception);
        }
    }
    if(getFormError())
        return;
    if(isAddToContainer())
    {
        if(isAssignNewCreditCardAsDefault())
            container.setDefaultPaymentGroupName(name);
        container.addPaymentGroup(name, creditCard);
    }
    if(isCopyToProfile())
        getCommerceProfileTools().copyCreditCardToProfile(creditCard, getProfile(), name);
}
@Override
public boolean handleNewCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
		throws ServletException, IOException {
	FoodStorePerformanceLogger foodStorePerformanceLogger = getFoodStorePerformanceLogger();
	foodStorePerformanceLogger.Init(HandlerNames.ADD_PAYMENT_GROUP_OPERATION_NAME,getProfile());
	foodStorePerformanceLogger.startMonitoring(HandlerNames.HANDLE_ADD_PAYMENT_GROUP);
	boolean result = super.handleNewCreditCard(pRequest, pResponse);
	
	if(!getFormError()) 
	{
		foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.END);
		foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_ADD_PAYMENT_GROUP, PerformanceStatus.END);
		
	} 
	else {
		foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.FAILED);
		foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_ADD_PAYMENT_GROUP, PerformanceStatus.FAILED);
		}
	if(getCreditCard() != null) {
		foodStorePerformanceLogger.setObjectToContainer(getCreditCard().getId());
	}
	foodStorePerformanceLogger.stopMonitoring(HandlerNames.HANDLE_ADD_PAYMENT_GROUP);
	foodStorePerformanceLogger.sendLog();
	return result;
}

}
