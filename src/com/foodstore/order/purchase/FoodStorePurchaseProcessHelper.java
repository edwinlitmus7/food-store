package com.foodstore.order.purchase;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.foodstore.order.FoodStoreCommerceItemManager;
import com.foodstore.order.beans.FoodStoreCommerceItemImpl;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemManager;
import atg.commerce.order.InStorePickupShippingGroup;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.commerce.pricing.PricingModelHolder;
import atg.commerce.util.PipelineErrorHandler;
import atg.core.i18n.LayeredResourceBundle;
import atg.core.util.NumberUtils;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.dynamo.LangLicense;
import atg.service.pipeline.PipelineResult;
import atg.service.pipeline.RunProcessException;

public class FoodStorePurchaseProcessHelper extends PurchaseProcessHelper {
	
	private static ResourceBundle sResourceBundle = LayeredResourceBundle.getBundle("atg.commerce.order.purchase.PurchaseProcessResources", LangLicense.getLicensedDefault());

	public void updateCommerceItemChilds(Order order, CommerceItem parentItem,
			List<CommerceItem> childItems) throws RepositoryException, CommerceException {
		FoodStoreCommerceItemManager cimgr = (FoodStoreCommerceItemManager) getCommerceItemManager();
		List<CommerceItem> itemsToBeAdded = new ArrayList<>();
		List<CommerceItem> itemsToBeRemoved = new ArrayList<>();
		cimgr.findSideSkusToBeAdded(parentItem, childItems, itemsToBeAdded, itemsToBeRemoved);
		cimgr.updateCommerceItemChilds((FoodStoreCommerceItemImpl) parentItem, itemsToBeAdded);
		cimgr.removeItemsFromOrder(order, itemsToBeRemoved);
		getOrderManager().updateOrder(order);
	}
	  @SuppressWarnings("rawtypes")
	    public PipelineResult runRepricingProcess(String pChainId, String pPricingOperation, Order pOrder,
	            PricingModelHolder pPricingModels, Locale pLocale, RepositoryItem pProfile, Map pExtraParameters)
	            throws RunProcessException {
	        return super.runRepricingProcess(pChainId, pPricingOperation, pOrder, pPricingModels, pLocale, pProfile,
	                pExtraParameters);
	    }

}
