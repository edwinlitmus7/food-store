package com.foodstore.order.purchase;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.ParameterConstants;

import com.foodstore.interfaces.RepeatingRequestMonitorErrorMsg;

import atg.commerce.order.purchase.PurchaseProcessFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.diff.SequenceDifferences.Handler;
import atg.droplet.DropletException;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import sun.misc.Perf;

/***
 * Class to remove Credit card.
 * 
 * @author Anil
 *
 */
public class FoodStoreRemoveCreditCardFormHandler extends PurchaseProcessFormHandler{

	private String removeCreditCardSuccessURL;
	private String removeCreditCardErrorURL;
	private String removeCreditCardNickname;
	private FoodStorePaymentProcessHelper paymentHelper;
	private RepeatingRequestMonitor repeatingRequestMonitor;

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return repeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor repeatingRequestMonitor) {
		this.repeatingRequestMonitor = repeatingRequestMonitor;
	}

	public String getRemoveCreditCardSuccessURL() {
		return removeCreditCardSuccessURL;
	}

	public void setRemoveCreditCardSuccessURL(String removeCreditCardSuccessURL) {
		this.removeCreditCardSuccessURL = removeCreditCardSuccessURL;
	}

	public String getRemoveCreditCardErrorURL() {
		return removeCreditCardErrorURL;
	}

	public void setRemoveCreditCardErrorURL(String removeCreditCardErrorURL) {
		this.removeCreditCardErrorURL = removeCreditCardErrorURL;
	}

	public String getRemoveCreditCardNickname() {
		return removeCreditCardNickname;
	}

	public void setRemoveCreditCardNickname(String removeCreditCardNickname) {
		this.removeCreditCardNickname = removeCreditCardNickname;
	}

	public FoodStorePaymentProcessHelper getPaymentHelper() {
		return paymentHelper;
	}

	public void setPaymentHelper(FoodStorePaymentProcessHelper paymentHelper) {
		this.paymentHelper = paymentHelper;
	}

	/**
	 * Handle method to remove credit card.
	 */
	public boolean handleRemoveCreditCard (DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) 
					throws ServletException, IOException{

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreRemoveCreditCardFormHandler.handleRemoveCreditCard";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			
			if(isLoggingDebug())
				logDebug("Executing handleRemoveCreditCard method");
			
			Transaction tr = null;
			boolean perfCancelled = false;

			PerformanceMonitor.startOperation(HandlerNames.HANDLE_REMOVE_CREDIT_CARD, pRequest.getServletPath());

			String removeCreditCardSuccessURL = pRequest.getParameter(ParameterConstants.REMOVE_CREDITCARD_SUCCESSURL);
			String removeCreditCardErrorURL = pRequest.getParameter(ParameterConstants.REMOVE_CREDITCARD_ERRORURL);


			try {
				tr = ensureTransaction();

				if (!checkFormRedirect(null, removeCreditCardErrorURL, pRequest, pResponse)) {
					return false;
				}

				preRemoveCreditCard(pRequest, pResponse);

				if (getFormError()) {
					if (isLoggingDebug()) {
						logDebug("Redirecting due to form error in preRemoveShippingAddress.");
					}
					return checkFormRedirect(null, removeCreditCardErrorURL, pRequest, pResponse);
				}			

				removeCreditCard(pRequest, pResponse);

				if (getFormError()) {
					if (isLoggingDebug()) {
						logDebug("Redirecting due to form error in removeAddress");
					}
					return checkFormRedirect(null, removeCreditCardErrorURL, pRequest, pResponse);
				}

				try {
					postRemoveCreditCard(pRequest, pResponse);
				} catch (RepositoryException e) {

					if(isLoggingDebug()){
						logDebug("Remove CreditCard from Profile is Failed");
					}
				}

			} finally {
				try {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_REMOVE_CREDIT_CARD, pRequest.getServletPath());
						perfCancelled = true;
					}
				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		// If NO form errors are found, redirect to the success URL.
		// If form errors are found, redirect to the error URL.
		return checkFormRedirect(removeCreditCardSuccessURL, 
				removeCreditCardErrorURL, 
				pRequest, pResponse);
	}

	private void removeCreditCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		String removeCreditCardNickname = pRequest.getParameter(ParameterConstants.REMOVE_CREDITCARD_NICKNAME);
		
		if(isLoggingDebug())
			logDebug("Remove credit card nickname : "+removeCreditCardNickname);
		
		getPaymentHelper().removeCreditCard(getProfile(), removeCreditCardNickname, 
				getPaymentGroupMapContainer());
	}

	private void postRemoveCreditCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RepositoryException {
		String removeCreditCardNickname = pRequest.getParameter(ParameterConstants.REMOVE_CREDITCARD_NICKNAME);
		getPaymentHelper().removeCreditCardFromProfile(getProfile(), removeCreditCardNickname);
	}

	private void preRemoveCreditCard(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		// TODO Auto-generated method stub

	}

}
