package com.foodstore.order.purchase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.RepeatingRequestMonitorErrorMsg;
import com.foodstore.logging.FoodStorePerformanceLogger;
import com.foodstore.logging.PerformanceStatus;
import com.foodstore.order.beans.FoodStoreCommerceItemImpl;
import com.foodstore.util.StoreConfiguration;

import atg.commerce.CommerceException;
import atg.commerce.order.CommerceItem;
import atg.commerce.order.CommerceItemImpl;
import atg.commerce.order.CommerceItemNotFoundException;
import atg.commerce.order.InvalidParameterException;
import atg.commerce.order.Order;
import atg.commerce.order.ShippingGroupCommerceItemRelationship;
import atg.commerce.order.purchase.AddCommerceItemInfo;
import atg.commerce.order.purchase.CartModifierFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.repository.RepositoryException;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;
/**
 * The formhandler to update the quantity of each commerceItem to the Request.
 * 
 * @author akhil
 *
 */
public class FoodStoreCartModifierFormHandler extends CartModifierFormHandler {

	private String updateSuccessURL;
	private String updateErrorURL;
	private String itemId;
	private String currentQuantity;
	private StoreConfiguration storeConfiguration;
	private String addSideItemToOrderSuccessURL;
	private String addSideItemToOrderErrorURL;
	private RepeatingRequestMonitor repeatingRequestMonitor;
	private String commerceItemId;
	private static String ADDING_SIDESKU_INPUT_STRING ="adding_side_sku";
	private static String ADDING_SIDESKU_CHECKBOX_STRING ="add_item_checkbox";
	private static String ADDING_SIDESKU_CATALOGUREFID_STRING ="add_item_catalogRefIds";
	private static String ADDING_SIDESKU_PRODUCTID_STRING ="add_item_productId";
	private static String ADDING_SIDESKU_COMMERCEITEMTYPE_STRING ="add_item_commerceItemType";
	private static String ADDING_SIDESKU_ADDITEMCOUNT_STRING ="add_item_count";
	private Profile profile;
	private FoodStorePerformanceLogger foodStorePerformanceLogger;
	
	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

	public FoodStorePerformanceLogger getFoodStorePerformanceLogger() {
		return foodStorePerformanceLogger;
	}

	public void setFoodStorePerformanceLogger(FoodStorePerformanceLogger foodStorePerformanceLogger) {
		this.foodStorePerformanceLogger = foodStorePerformanceLogger;
	}

	public String getCommerceItemId() {
		return commerceItemId;
	}

	public void setCommerceItemId(String commerceItemId) {
		this.commerceItemId = commerceItemId;
	}

	public String getAddSideItemToOrderSuccessURL() {
		return addSideItemToOrderSuccessURL;
	}

	public void setAddSideItemToOrderSuccessURL(String addSideItemToOrderSuccessURL) {
		this.addSideItemToOrderSuccessURL = addSideItemToOrderSuccessURL;
	}

	public String getAddSideItemToOrderErrorURL() {
		return addSideItemToOrderErrorURL;
	}

	public void setAddSideItemToOrderErrorURL(String addSideItemToOrderErrorURL) {
		this.addSideItemToOrderErrorURL = addSideItemToOrderErrorURL;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return repeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor repeatingRequestMonitor) {
		this.repeatingRequestMonitor = repeatingRequestMonitor;
	}

	public String getUpdateSuccessURL() {
		return updateSuccessURL;
	}

	public void setUpdateSuccessURL(String updateSuccessURL) {
		this.updateSuccessURL = updateSuccessURL;
	}

	public String getUpdateErrorURL() {
		return updateErrorURL;
	}

	public void setUpdateErrorURL(String updateErrorURL) {
		this.updateErrorURL = updateErrorURL;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getCurrentQuantity() {
		return currentQuantity;
	}

	public void setCurrentQuantity(String currentQuantity) {
		this.currentQuantity = currentQuantity;
	}

	public StoreConfiguration getStoreConfiguration() {
		return storeConfiguration;
	}

	public void setStoreConfiguration(StoreConfiguration storeConfiguration) {
		this.storeConfiguration = storeConfiguration;
	}

	/**
	 * Handle method to update the quantity of each commerce item in the cart.
	 */
	public boolean handleSetOrderByCommerceId(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException{
		FoodStorePerformanceLogger foodStorePerformanceLogger = getFoodStorePerformanceLogger();
		foodStorePerformanceLogger.Init(HandlerNames.SET_ORDER_BY_COMMERCE_ID_OPERATION_NAME, getProfile());
		foodStorePerformanceLogger.startMonitoring(HandlerNames.HANDLE_SET_ORDER_BY_COMMERCE_ID);
		
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreCartModifierFormHandler.handleSetorderByCommerceId";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {

			if(isLoggingDebug())
				logDebug("Handle setOrderByCommerceId method");
			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.HANDLE_SET_ORDER_BY_COMMERCE_ID, pRequest.getServletPath());
			try {
				Order order = getOrder();
				List<CommerceItem> commerceItems = order.getCommerceItems();
				String currentId = getItemId();
				for(CommerceItem commerceItem : commerceItems){
					pRequest.setParameter(commerceItem.getId(), commerceItem.getQuantity());
					if(commerceItem.getId().equals(currentId)){
						foodStorePerformanceLogger.setObjectToContainer(commerceItem.getId());
					}
					if(isLoggingDebug())
						logDebug("Commerce Items in the order : " + commerceItem.getCatalogRefId());
				}

				pRequest.setParameter(currentId, getCurrentQuantity());
				setCheckForChangedQuantity(true);
				super.handleSetOrderByCommerceId(pRequest, pResponse);
			}
			finally {
				try {
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_SET_ORDER_BY_COMMERCE_ID, pRequest.getServletPath());
						perfCancelled = true;
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
			
			if(!getFormError()) {
				foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.END);
				foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_SET_ORDER_BY_COMMERCE_ID, PerformanceStatus.END);
			} else {
				foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.FAILED);
				foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_SET_ORDER_BY_COMMERCE_ID, PerformanceStatus.FAILED);
			}
			
			
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		foodStorePerformanceLogger.stopMonitoring(HandlerNames.HANDLE_SET_ORDER_BY_COMMERCE_ID);
		foodStorePerformanceLogger.sendLog();
		return checkFormRedirect(getUpdateSuccessURL(), getUpdateErrorURL(), pRequest, pResponse);
	}

	/*
	 * This function is overridden with a logic that finds the commerce item shipping group relation involving a commerceItem 
	 * and then setting that relations type value to 100.
	 * (non-Javadoc)
	 * @see atg.commerce.order.purchase.CartModifierFormHandler#preAddItemToOrder(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@Override
	public void preAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException 
	{
		// check if trying to add a sidesku
		if(Boolean.valueOf(pRequest.getParameter(ADDING_SIDESKU_INPUT_STRING))) {
			handleAddSideItemToOrder(pRequest, pResponse);
		} else {

			Order order = getOrder();
			List<CommerceItem> commerceItems = order.getCommerceItems();
			String skuId = getCatalogRefIds()[0];
			CommerceItem commerceItemFound = null;
			for(CommerceItem commerceItem : commerceItems){
				if(((CommerceItem) commerceItem).getCatalogRefId().equals(skuId) && ((CommerceItemImpl) commerceItem).getProductId().equals(getProductId())) {
					commerceItemFound = (CommerceItem) commerceItem;
					break;
				}
			}
			if(commerceItemFound != null ) {
				ShippingGroupCommerceItemRelationship rel;
				try {
					rel = getShippingGroupManager().getShippingGroupCommerceItemRelationshipIfPresent(getOrder(), ((CommerceItemImpl) commerceItemFound).getId(),
							getShippingGroup().getId()
							);
					if (rel != null) {
						rel.setRelationshipType(100);
					}

				} catch (CommerceException e) {
					addFormException(new DropletException("Error while processing"));
					e.printStackTrace();
				}
			}

		} 

	}

	/**
	 * Handle method to handle adding of side items to order
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleAddSideItemToOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		if ((rrm == null) || (rrm.isUniqueRequestEntry(HandlerNames.HANDLE_ADD_SIDE_ITEM_TO_ORDER))) {
			Transaction tr = null;
			try {
				tr = ensureTransaction();
				Order order = getOrder();
				synchronized (order) {
					preAddSideItemToOrder(pRequest, pResponse);
					if (getFormError()) {
						return checkFormRedirect(null, getAddSideItemToOrderErrorURL(), pRequest, pResponse);
					}
					addMultipleItemsToOrder(pRequest, pResponse);
					addChildSkusToCommerceItem(pRequest, pResponse);
					if (getFormError()) {
						return checkFormRedirect(null, getAddSideItemToOrderErrorURL(), pRequest, pResponse);
					}
					postAddSideItemToOrder(pRequest, pResponse);
				}
				return checkFormRedirect(getAddSideItemToOrderSuccessURL(), getAddSideItemToOrderErrorURL(), pRequest, pResponse);
			} catch (Exception exc) {
				processException(exc, "errorUpdatingOrder", pRequest, pResponse);
				return checkFormRedirect(null, getAddSideItemToOrderErrorURL(), pRequest, pResponse);
			} finally {
				if (tr != null) {
					commitTransaction(tr);
				}
				if (rrm != null) {
					rrm.removeRequestEntry(HandlerNames.HANDLE_ADD_SIDE_ITEM_TO_ORDER);
				}
			}
		} else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		return false;
	}
	
	@Override
	protected void doAddItemsToOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		if (isLoggingDebug()) {
			logDebug("Starting doAddItemsToOrder");
		}
		Order order = getOrder();
		if (order == null) {
			String msg = formatUserMessage("noOrderToModify", pRequest, pResponse);
			throw new ServletException(msg);
		}
		Map extraParams = createRepriceParameterMap();
		try {
			createInStorePickupShippingGroups(order);
			AddCommerceItemInfo[] filteredItems = filterItems();
			if(filteredItems != null) {
				List<CommerceItem> items = getPurchaseProcessHelper().addItemsToOrder(order, getShippingGroup(), getProfile(), filteredItems, getUserLocale(), getCatalogKey(pRequest, pResponse), getUserPricingModels(), this, extraParams);
				addCommerceItemsToAddedList(items, filteredItems);
				setAddItemsToOrderResult(items);
			} else {
				logWarning("No commerce item created!");
			}
		} catch (CommerceException ce) {
			processException(ce, "errorAddingToOrder", pRequest, pResponse);
		}
	}

	private void addCommerceItemsToAddedList(List<CommerceItem> items, AddCommerceItemInfo[] filteredItems) {
		for(AddCommerceItemInfo item: getItems()) {
			boolean itemInFilteredList = isItemInFilteredList(item, filteredItems);
			if(!itemInFilteredList) {
				try {
					List<CommerceItem> itemsWithId = getOrder().getCommerceItemsByCatalogRefId(item.getCatalogRefId());
					for(CommerceItem itemWithId: itemsWithId) {
						if(((FoodStoreCommerceItemImpl) itemWithId).getParentItem().equals(getCommerceItemId())) {
							items.add(itemWithId);
							break;
						}
					}
				} catch (CommerceItemNotFoundException
						| InvalidParameterException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private boolean isItemInFilteredList(AddCommerceItemInfo item,
			AddCommerceItemInfo[] filteredItems) {
		for(AddCommerceItemInfo filteredItem: filteredItems) {
			if(filteredItem.getCatalogRefId().equals(item.getCatalogRefId())) {
				return true;
			}
		}
		return false;
	}

	private AddCommerceItemInfo[] filterItems() {
		int i = 0;
		List<AddCommerceItemInfo> filteredItems = null;
		if(getItems() != null) {
			filteredItems = new ArrayList<>();
			logInfo("Get Iitems = " + getItems());
			for(AddCommerceItemInfo item: getItems()) {
				try {
					if(!isItemAlreadyInCart(item.getCatalogRefId())) {
						logInfo("Adding " + item.getCatalogRefId() + " to filtered list");
						filteredItems.add(item);
						i++;
					}
				} catch (CommerceItemNotFoundException e) {
					e.printStackTrace();
				} catch (InvalidParameterException e) {
					e.printStackTrace();
				}
			}
			return (AddCommerceItemInfo[]) filteredItems.toArray(new AddCommerceItemInfo[filteredItems.size()]);
		}
		return null;
	}

	private void addChildSkusToCommerceItem(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {
		try {
			CommerceItem parentItem = getOrder().getCommerceItem(getCommerceItemId());
			logInfo("Parent CommerceItem : " + parentItem);
			((FoodStorePurchaseProcessHelper) getPurchaseProcessHelper()).updateCommerceItemChilds(getOrder(), parentItem, getAddItemsToOrderResult());
		} catch (CommerceItemNotFoundException | InvalidParameterException e) {
			addFormException(new DropletFormException("Invalid main dish specified!", "order"));
		} catch (RepositoryException | CommerceException e) {
			addFormException(new DropletFormException("Error while updating order. Please try after some time!", "order"));
		}
	}

	private void postAddSideItemToOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
	}

	/**
	 * validates whether this commerce item is trying to add more than the allowed number of side skus
	 * @param pRequest
	 * @param pResponse
	 * @throws CommerceItemNotFoundException
	 * @throws InvalidParameterException
	 * @throws IOException 
	 * @throws ServletException 
	 */
	private void preAddSideItemToOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws CommerceItemNotFoundException, InvalidParameterException, ServletException, IOException {

		prepareSideSkuDataForAdding(pRequest, pResponse);
		
		// maximum allowed side skus is configured in store configuration
		int maxNumberOfSideSkusAllowed = getStoreConfiguration().getMaxAllowedSideSkus();

		// retrieves the commmerce item from order using commerceItemId passed from jsp
		Order order = getOrder();
		FoodStoreCommerceItemImpl ci = (FoodStoreCommerceItemImpl) order.getCommerceItem(getCommerceItemId());

		// calculates the number of sideSkus already existing + the number of sideSkus trying to be added.
		// if this value is greater than the maxAllowedSideSkus, then exception is thrown
		int numberOfUniqueChildSkus = getAddItemCount();
		logInfo("ChildItems of " + ci.getId() + " : " + ci.getChildItems());
//		if(ci.getChildItems() != null) {
//			numberOfUniqueChildSkus = findUniqueItemsCount(ci.getChildItems(), getItems());
//		}
		if(numberOfUniqueChildSkus > maxNumberOfSideSkusAllowed ) {
			addFormException(new DropletFormException("Only " + maxNumberOfSideSkusAllowed + " side dishes allowed per item!", "order"));
		}
	}

	private int findUniqueItemsCount(List<CommerceItem> childItems,
			AddCommerceItemInfo[] items) {
		int numberOfCommonSkus = 0;
		for(CommerceItem item: childItems) {
			for(int i=0; i<items.length; i++) {
				if(item.getCatalogRefId().equals(items[i].getCatalogRefId())) {
					numberOfCommonSkus++;
				}
			}
		}
		return items.length - numberOfCommonSkus;
	}

	/**
	 *  Inputs from side sku model (in cartItems.jsp) need to be populated into FormHandler variables.
	 *  User can select of n number of sideSkus in fromt end, this data is passed as an array to the formHandler.
	 *  This method populates the corresponding bean values from the array
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 * @throws IOException
	 * @throws InvalidParameterException 
	 * @throws CommerceItemNotFoundException 
	 */
	private void prepareSideSkuDataForAdding(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceItemNotFoundException, InvalidParameterException {
		String[] checkedSideSkus = pRequest.getParameterValues(ADDING_SIDESKU_CHECKBOX_STRING);

		String[] newCatalogueRefId = pRequest.getParameterValues(ADDING_SIDESKU_CATALOGUREFID_STRING);
		String[] newProductId = pRequest.getParameterValues(ADDING_SIDESKU_PRODUCTID_STRING);
		String[] newItemType = pRequest.getParameterValues(ADDING_SIDESKU_COMMERCEITEMTYPE_STRING);
		String[] newItemCount = pRequest.getParameterValues(ADDING_SIDESKU_ADDITEMCOUNT_STRING);
		
		int itemIndfoCountTobeCreated = 0;
		Set<Integer> indeces = new HashSet<>(); 
		for (int i=0;i<checkedSideSkus.length;i++) {
			if(Boolean.valueOf(checkedSideSkus[i])) {
				itemIndfoCountTobeCreated++;
				indeces.add(i);
			} 
		}
		setAddItemCount(itemIndfoCountTobeCreated);

		AddCommerceItemInfo[] items = getItems();

		int j = 0;
		for(int i : indeces) {
			items[j].setCatalogRefId(newCatalogueRefId[i]);
			items[j].setProductId(newProductId[i]);
			items[j].setQuantity(Long.parseLong(newItemCount[i]));
			items[j].setCommerceItemType(newItemType[i]);
			j++;
		}
	}

	private boolean isItemAlreadyInCart(String skuId) throws CommerceItemNotFoundException, InvalidParameterException {
		if(skuId != null && getCommerceItemId() != null) {
			logInfo("Getting child items for commerceItem with skuID" + getCommerceItemId() + " " + skuId);
			List<CommerceItem> childItems = ((FoodStoreCommerceItemImpl) getOrder().getCommerceItem(getCommerceItemId())).getChildItems();
			if(childItems != null) {
				for(CommerceItem item: childItems) {
					if(item.getCatalogRefId().equals(skuId)) {
						return true;
					}
				}
			}
		}
		return false;
	}
	

	@Override
	public boolean handleAddItemToOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		FoodStorePerformanceLogger foodStorePerformanceLogger = getFoodStorePerformanceLogger();
		foodStorePerformanceLogger.Init(HandlerNames.ADD_ITEM_TO_CART_OPERATION_NAME, getProfile());
		foodStorePerformanceLogger.startMonitoring(HandlerNames.HANDLE_ADD_ITEM_TO_CART);
		boolean result = super.handleAddItemToOrder(pRequest, pResponse);
		foodStorePerformanceLogger.setObjectToContainer(getOrder().getId());
		if(!getFormError()) {
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.END);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_ADD_ITEM_TO_CART, PerformanceStatus.END);
		} else {
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.FAILED);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_ADD_ITEM_TO_CART, PerformanceStatus.FAILED);
		}
		if(getCatalogRefIds() != null) {
			foodStorePerformanceLogger.setObjectToContainer(getCatalogRefIds());
		}
	
		foodStorePerformanceLogger.stopMonitoring(HandlerNames.HANDLE_ADD_ITEM_TO_CART);
		foodStorePerformanceLogger.sendLog();
		return result;
	}
	@Override
	public boolean handleMoveToPurchaseInfoByCommerceId(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		FoodStorePerformanceLogger foodStorePerformanceLogger = getFoodStorePerformanceLogger();
		foodStorePerformanceLogger.Init(HandlerNames.MOVE_TO_PURCHASE_INFO_OPERATION_NAME, getProfile());
		foodStorePerformanceLogger.startMonitoring(HandlerNames.HANDLE_MOVE_TO_PURCHASE_INFO);
		boolean result = super.handleMoveToPurchaseInfoByCommerceId(pRequest, pResponse);
		if(!getFormError()) {
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.END);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_MOVE_TO_PURCHASE_INFO, PerformanceStatus.END);
		} else {
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.FAILED);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_MOVE_TO_PURCHASE_INFO, PerformanceStatus.FAILED);
		}
		if(getOrder() != null) {
			foodStorePerformanceLogger.setObjectToContainer(getOrder().getId());
			
		}
		foodStorePerformanceLogger.stopMonitoring(HandlerNames.HANDLE_MOVE_TO_PURCHASE_INFO);
		foodStorePerformanceLogger.sendLog();
		return result;
	}
}