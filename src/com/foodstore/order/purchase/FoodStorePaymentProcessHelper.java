package com.foodstore.order.purchase;

import java.util.Map;

import com.foodstore.profile.FoodStoreProfileTools;

import atg.commerce.order.PaymentGroup;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.order.purchase.PurchaseProcessHelper;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.profile.CommercePropertyManager;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;

/***
 * Contains the common functionalities required by the process helper formhandler.
 * 
 * @author Anil
 *
 */
public class FoodStorePaymentProcessHelper extends PurchaseProcessHelper {
	
	/**
	 * Remove user specified payment group from the payment groups.
	 * 
	 * @param pProfile
	 * @param pRemoveCreditCardNickname
	 * @param pPaymentGroupMapContainer
	 */
	public void removeCreditCard(RepositoryItem pProfile,
			String pRemoveCreditCardNickname,
			PaymentGroupMapContainer pPaymentGroupMapContainer){
		
		if(isLoggingDebug())
			logDebug("Executing removeCreditCard method");
		
		PaymentGroup paymentGroup = pPaymentGroupMapContainer.getPaymentGroup(pRemoveCreditCardNickname);
		
		if(paymentGroup != null){
			
			pPaymentGroupMapContainer.removePaymentGroup(pRemoveCreditCardNickname);
		}
		
	}
	
	/**
	 * Removes the user specified credit card from the user profile.
	 * @param pProfile
	 * @param pRemoveCreditCardNickname
	 * @throws RepositoryException
	 */
	public void removeCreditCardFromProfile(RepositoryItem pProfile, String pRemoveCreditCardNickname) throws RepositoryException{
		
		if(isLoggingDebug())
			logDebug("Executing removeCreditCardFromProfile method");
		
		FoodStoreProfileTools foodStoreProfileTools = (FoodStoreProfileTools) 
				getOrderManager().getOrderTools().getProfileTools();
		
		String creditCardProperty = ((CommercePropertyManager)foodStoreProfileTools.getPropertyManager())
				.getCreditCardPropertyName();
		
		Map creditCard = (Map) pProfile.getPropertyValue(creditCardProperty);
		
		if(creditCard.containsKey(pRemoveCreditCardNickname)){
			
			foodStoreProfileTools.removeProfileCreditCard(pProfile, pRemoveCreditCardNickname);
			
			if (isLoggingError()) {
				logError("Remove CreditCard From Profile is Failed");
			}
			
			if(isLoggingDebug())
				logDebug("Credit card removed : "+pRemoveCreditCardNickname);
			
		}
	}

}
