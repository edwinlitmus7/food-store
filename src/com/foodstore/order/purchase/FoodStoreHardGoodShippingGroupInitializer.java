package com.foodstore.order.purchase;

import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.OrderTools;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.ShippingGroupManager;
import atg.commerce.order.purchase.HardgoodShippingGroupInitializer;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

/***
 * Managing hardgood shipping group functionalities.
 * @author Anil
 *
 */
public class FoodStoreHardGoodShippingGroupInitializer extends
HardgoodShippingGroupInitializer {

	/**
	 * Creating hardgood shipping group for shipment.
	 */
	@Override
	protected ShippingGroup createHardgoodShippingGroup(RepositoryItem pAddressItem, Profile pProfile)
			throws CommerceException, PropertyNotFoundException, RepositoryException
	{
		if(isLoggingDebug())
			logDebug("Executing createHardgoodShippingGroup method");
		
		ShippingGroupManager sgm = getShippingGroupManager();
		HardgoodShippingGroup shippingGroup = (HardgoodShippingGroup)sgm.createShippingGroup(getHardgoodShippingGroupType());
		OrderTools.copyAddress(pAddressItem, shippingGroup.getShippingAddress());

		try
		{
			String addrPropNames[] = DynamicBeans.getBeanInfo(pAddressItem).getPropertyNames();

			for(int i = 0; i < addrPropNames.length; i++){
				vlogDebug(addrPropNames[i] +"-->" + DynamicBeans.getPropertyValue(pAddressItem, addrPropNames[i]));
			}

		}
		catch (Exception e) {
			// TODO: handle exception
			vlogDebug(e+"");
			e.printStackTrace();
		}
		return shippingGroup;
	}
}
