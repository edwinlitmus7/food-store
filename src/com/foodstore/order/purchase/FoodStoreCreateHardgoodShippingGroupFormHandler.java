package com.foodstore.order.purchase;

import java.io.IOException;

import javax.servlet.ServletException;

import com.foodstore.interfaces.HandlerNames;
import com.foodstore.logging.FoodStorePerformanceLogger;
import com.foodstore.logging.PerformanceStatus;

import atg.commerce.order.purchase.CreateHardgoodShippingGroupFormHandler;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

public class FoodStoreCreateHardgoodShippingGroupFormHandler extends CreateHardgoodShippingGroupFormHandler {
	private FoodStorePerformanceLogger foodStorePerformanceLogger;
	
	public FoodStorePerformanceLogger getFoodStorePerformanceLogger() {
		return foodStorePerformanceLogger;
	}

	public void setFoodStorePerformanceLogger(FoodStorePerformanceLogger foodStorePerformanceLogger) {
		this.foodStorePerformanceLogger = foodStorePerformanceLogger;
	}

	

	@Override
	public boolean handleNewHardgoodShippingGroup(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException {
		FoodStorePerformanceLogger foodStorePerformanceLogger = getFoodStorePerformanceLogger();
		foodStorePerformanceLogger.Init(HandlerNames.ADD_SHIPPING_GROUP_OPERATION_NAME,getProfile());
		foodStorePerformanceLogger.startMonitoring(HandlerNames.HANDLE_ADD_SHIPPING_GROUP);
		boolean result = super.handleNewHardgoodShippingGroup(pRequest, pResponse);
		
		if(!getFormError()) 
		{
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.END);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_ADD_SHIPPING_GROUP, PerformanceStatus.END);
		} 
		else {
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.FAILED);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_ADD_SHIPPING_GROUP, PerformanceStatus.FAILED);
			}
		if(getHardgoodShippingGroup() != null) {
			foodStorePerformanceLogger.setObjectToContainer(getHardgoodShippingGroup().getId());
			
		}
		foodStorePerformanceLogger.stopMonitoring(HandlerNames.HANDLE_ADD_SHIPPING_GROUP);
		foodStorePerformanceLogger.sendLog();
		return result;
	}
}
