package com.foodstore.order.purchase;

import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.ServletException;

import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.PropertyConstants;

import com.foodstore.interfaces.RepeatingRequestMonitorErrorMsg;

import atg.commerce.order.CreditCard;
import atg.commerce.order.purchase.PaymentGroupMapContainer;
import atg.commerce.order.purchase.UpdateCreditCardFormHandler;
import atg.commerce.util.AddressValidator;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.core.util.StringUtils;
import atg.diff.SequenceDifferences.Handler;
import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;

/***
 * Class to update the credit card details.
 * 
 * @author Anil
 *
 */
public class FoodStoreUpdateCreditCardFormHandler extends UpdateCreditCardFormHandler {

	private RepeatingRequestMonitor repeatingRequestMonitor;

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return repeatingRequestMonitor;
	}
	
	public void setRepeatingRequestMonitor(RepeatingRequestMonitor repeatingRequestMonitor) {
		this.repeatingRequestMonitor = repeatingRequestMonitor;
	}
	
	/* To assign as default credit card use the below flag */
	private boolean assignAsDefault;
	
	public boolean isAssignAsDefault() {
		return assignAsDefault;
	}
	
	public void setAssignAsDefault(boolean assignAsDefault) {
		this.assignAsDefault = assignAsDefault;
	}
	
	AddressValidator addressValidator;
	public AddressValidator getAddressValidator() {
		return addressValidator;
	}
	
	public void setAddressValidator(AddressValidator addressValidator) {
		this.addressValidator = addressValidator;
	}
	
	boolean validateBillingAddress;

	public boolean isValidateBillingAddress() {
		return validateBillingAddress;
	}
	
	public void setValidateBillingAddress(boolean validateBillingAddress) {
		this.validateBillingAddress = validateBillingAddress;
	}
	
	/**
	 * Validates the user entered billing address.
	 */
	@SuppressWarnings("rawtypes")
	private void validateBillingAddress(){
		
		if(isLoggingDebug())
			logDebug("Executing validateBillingAddress method");
		
		DynamoHttpServletRequest req = ServletUtil.getCurrentRequest();
		DynamoHttpServletResponse res = ServletUtil.getCurrentResponse();
		CreditCard creditCard = getWorkingCreditCard();
		String name = getCreditCardName();
		if(StringUtils.isBlank(name))
		{
			if(isLoggingDebug())
				logDebug("Billing Group Nickname is NULL. I can't process without name.");

			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.NO_VALID_NICKNAME, propertyPath));
			return;
		}
		if(creditCard == null || creditCard.getBillingAddress() == null)
		{
			if(isLoggingDebug())
				logDebug("Payment Group or Billing Address can't be NULL.");


			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.NO_PAYMENTGROUP_OR_ADDRESS, propertyPath));

			return;
		}
		try
		{
			Collection errors = getAddressValidator().validateAddress(creditCard.getBillingAddress(), getUserLocale(req, res));
			if(errors != null && !errors.isEmpty())
			{
				String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
				if(isLoggingDebug())
					logDebug("Errors in the Shipping address validation.");
				Object entry;
				for(Iterator i$ = errors.iterator(); i$.hasNext(); addFormException(new DropletFormException((String)entry, propertyPath)))
					entry = i$.next();

			}
		}
		catch(ServletException e)
		{
			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.ERROR_VALIDATING_CREDITCARD, e, propertyPath));
		}
		catch(IOException e)
		{
			String propertyPath = generatePropertyPath(PropertyConstants.VALIDATE_BILLING_ADDRESS_PROPERTY);
			addFormException(new DropletFormException(ErrorsAndExceptionMessages.ERROR_VALIDATING_CREDITCARD, e, propertyPath));
		}
	}
	
	/**
	 * Update the credit card details.
	 */
	@Override
	public void updateCreditCard(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException
	{
		if(isLoggingDebug())
			logDebug("Executing updateCreditCard method");
		
		if(getFormError())
			return;
		if(isValidateBillingAddress()){
			validateBillingAddress();
		}
		if(getFormError())
			return;
		if(isValidateCreditCard())
			validateCreditCard(getWorkingCreditCard(), pRequest, pResponse);
		if(getFormError())
			return;
		if(isUpdateContainer())
			updateContainer();
		if(getFormError())
			return;
		if(isUpdateProfile())
			updateProfile();
		if(getFormError())
			return;
		if(isUpdateOrder())
			updateOrder();
	}
	
	/**
	 * Updating the credit card container.
	 */
	@Override
	public void updateContainer() {
		
		if(isLoggingDebug())
			logDebug("Entering updateContainer() method");
		
		CreditCard creditCard = getWorkingCreditCard();
		PaymentGroupMapContainer container = getPaymentGroupMapContainer();
		String name = getCreditCardName();
		CreditCard localcc = (CreditCard)container.getPaymentGroup(name);
		if(localcc != null)
		{
			if(isLoggingDebug())
				logDebug("Updating container with latest credit card.");
			copyCreditCard(creditCard, localcc);
			if(isAssignAsDefault()){
				vlogDebug("Assigning card " + name + " as default ");
				container.setDefaultPaymentGroupName(name);
			}

		} else
		{
			if(isLoggingDebug())
				logDebug("This service could not add an non-existing crdit card.");
			try
			{
				String msg = formatUserMessage(ErrorsAndExceptionMessages.NO_CREDITCARD_IN_CONTAINER, ServletUtil.getCurrentRequest(), ServletUtil.getCurrentResponse());
				String propertyPath = generatePropertyPath(PropertyConstants.UPDATE_CONTAINER_PROPERTY);
				addFormException(new DropletFormException(msg, propertyPath, ErrorsAndExceptionMessages.NO_CREDITCARD_IN_CONTAINER));
			}
			catch(Exception exception)
			{
				if(isLoggingError())
					logError(exception);
			}
		}
	}

	/**
	 * Handle method to set the specified credit card details as the default credit card.
	 */
	public void handleSetDefaultCreditCard(){

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreUpdateCreditCardFormHandler.handleSetDefaultCreditCard";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.HANDLE_SET_DEFAULT_CREDIT_CARD, HandlerNames.PERFORMANCE_MONITOR_SET_DEFAULT_CREDIT_CARD_PARAM);

			try {
				if(isLoggingDebug())
					logDebug("Entering handleSetDefaultCreditCard()");
				PaymentGroupMapContainer container = getPaymentGroupMapContainer();
				String name = getCreditCardName();
				CreditCard localcc = (CreditCard)container.getPaymentGroup(name);
				if(localcc != null)
				{
					vlogDebug("Assigning card " + name + " as default ");
					container.setDefaultPaymentGroupName(name);
				} else
				{
					if(isLoggingDebug())
						logDebug("This service could not add an non-existing crdit card.");
					try
					{
						String msg = formatUserMessage(ErrorsAndExceptionMessages.NO_CREDITCARD_IN_CONTAINER, ServletUtil.getCurrentRequest(), ServletUtil.getCurrentResponse());
						String propertyPath = generatePropertyPath(PropertyConstants.UPDATE_CONTAINER_PROPERTY);
						addFormException(new DropletFormException(msg, propertyPath, ErrorsAndExceptionMessages.NO_CREDITCARD_IN_CONTAINER));

					}
					catch(Exception exception)
					{
						if(isLoggingError())
							logError(exception);
					}
				}
			}finally {
				try {
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_SET_DEFAULT_CREDIT_CARD, HandlerNames.PERFORMANCE_MONITOR_SET_DEFAULT_CREDIT_CARD_PARAM);
						perfCancelled = true;
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}else {

			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
	}

}
