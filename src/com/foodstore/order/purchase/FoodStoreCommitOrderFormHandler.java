package com.foodstore.order.purchase;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.transaction.TransactionManager;
import javax.ws.rs.core.MediaType;


import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.interfaces.HandlerNames;
import com.foodstore.logging.ElasticSearchLogging;
import com.foodstore.logging.FoodStorePerformanceLogger;
import com.foodstore.logging.PerformanceStatus;
import com.foodstore.util.FoodStoreEmailTools;
import com.google.gson.Gson;

import atg.commerce.order.Order;
import atg.commerce.order.purchase.CommitOrderFormHandler;
import atg.common.access.Transaction;
import atg.droplet.DropletFormException;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.userprofiling.Profile;

/**
 * Handler class that handles request while placing an order
 * @author Anil
 *
 */
public class FoodStoreCommitOrderFormHandler extends CommitOrderFormHandler {
	private FoodStorePerformanceLogger foodStorePerformanceLogger;
	
	public FoodStorePerformanceLogger getFoodStorePerformanceLogger() {
		return foodStorePerformanceLogger;
	}

	public void setFoodStorePerformanceLogger(FoodStorePerformanceLogger foodStorePerformanceLogger) {
		this.foodStorePerformanceLogger = foodStorePerformanceLogger;
	}

	private FoodStoreEmailTools emailTools;

	public FoodStoreEmailTools getEmailTools() {
		return emailTools;
	}

	public void setEmailTools(FoodStoreEmailTools emailTools) {
		this.emailTools = emailTools;
	}
	private ElasticSearchLogging elasticSearchLogging;
	
	
	public ElasticSearchLogging getElasticSearchLogging() {
		return elasticSearchLogging;
	}

	public void setElasticSearchLogging(ElasticSearchLogging elasticSearchLogging) {
		this.elasticSearchLogging = elasticSearchLogging;
	}

	@Override
	public boolean handleCommitOrder(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
			throws ServletException, IOException {
		FoodStorePerformanceLogger foodStorePerformanceLogger = getFoodStorePerformanceLogger();
		foodStorePerformanceLogger.Init(HandlerNames.SUBMIT_ORDER_OPERATION_NAME,getProfile());
		foodStorePerformanceLogger.startMonitoring(HandlerNames.HANDLE_SUBMIT_ORDER);
		boolean result = super.handleCommitOrder(pRequest, pResponse);
		if(!getFormError()) 
		{
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.END);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_SUBMIT_ORDER, PerformanceStatus.END);} 
		else {
			foodStorePerformanceLogger.changeOperationStatus(PerformanceStatus.FAILED);
			foodStorePerformanceLogger.changeMethodStatus(HandlerNames.HANDLE_SUBMIT_ORDER, PerformanceStatus.FAILED);
			}
		if(getOrder() != null) {
			foodStorePerformanceLogger.setObjectToContainer(getOrder().getId());
			
		}
		foodStorePerformanceLogger.stopMonitoring(HandlerNames.HANDLE_SUBMIT_ORDER);
		foodStorePerformanceLogger.sendLog();
		return result;
	}
	/**
	 * Once the order is placed succesfully, this method gets invoked.
	 * This method sends a confirmation message to the user who placed the order
	 * Also the kibana based order logging is invoked here.
	 */
	public void postCommitOrder(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
			IOException {
		
		if (!getFormError()) {
			
			Transaction tr = null; 
			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = new TransactionDemarcation();

			try{
					if (tm != null) {
							td.begin(tm, TransactionDemarcation.NOT_SUPPORTED);
							logOrder((Profile)getProfile(),getOrder());
							sendConfirmationMessage(pRequest, pResponse);
						}	 
							
				
			
			
			} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						logError("Can't end transaction ", e);
					}
			}
			finally {
				try {
						if (tm != null) {
							td.end();
						} 
					} catch (TransactionDemarcationException e) {
						if (isLoggingError()) {
							logError("Can't end transaction ", e);
						}
					
					}
			}
		}
			
			
			
		
	}
	public void logOrder(Profile profile,Order order) {
		elasticSearchLogging.logOrder(profile, order);
	}

	/**
	 * invokes the emailTools component component for sending email to the user.
	 * @param pRequest
	 * @param pResponse
	 */
	public void sendConfirmationMessage(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	{
		try
		{
			try
			{
				getEmailTools().sendConfirmationEmail(getOrder(), getProfile());
			}
			catch (Exception e)
			{
				processException(e, ErrorsAndExceptionMessages.ERROR_SENDING_EMAIL_CONFIRM, pRequest, pResponse);
				addFormException(new DropletFormException(ErrorsAndExceptionMessages.ERROR_SENDING_EMAIL_CONFIRM, "FoodStoreCommitOrderFormHandler", ""));
			}
		}
		catch (Exception exception)
		{
			if (isLoggingError()) {
				logError(exception);
			}
		}
	}
	
}
