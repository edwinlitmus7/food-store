package com.foodstore.order.purchase;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.transaction.Transaction;

import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.ParameterConstants;

import com.foodstore.interfaces.RepeatingRequestMonitorErrorMsg;

import atg.commerce.order.purchase.ShippingGroupFormHandler;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
/*import jdk.internal.org.objectweb.asm.Handle;
*/

/***
 * Performs Shipping address related operations.
 * 
 * @author akhil
 *
 */

public class FoodStoreShippingGroupFormHandler extends ShippingGroupFormHandler {

	public String removeShippingAddressSuccessURL;
	public String removeShippingAddressErrorURL;
	private FoodStoreShippingProcessHelper shippingHelper;
	private String removeShippingAddressNickname;

	private RepeatingRequestMonitor repeatingRequestMonitor;

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return repeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor repeatingRequestMonitor) {
		this.repeatingRequestMonitor = repeatingRequestMonitor;
	}

	public String getRemoveShippingAddressNickname() {
		return removeShippingAddressNickname;
	}

	public void setRemoveShippingAddressNickname(
			String removeShippingAddressNickname) {

		this.removeShippingAddressNickname = removeShippingAddressNickname;
	}

	public String getRemoveShippingAddressSuccessURL() {
		return removeShippingAddressSuccessURL;
	}

	public void setRemoveShippingAddressSuccessURL(
			String removeShippingAddressSuccessURL) {
		this.removeShippingAddressSuccessURL = removeShippingAddressSuccessURL;
	}

	public String getRemoveShippingAddressErrorURL() {
		return removeShippingAddressErrorURL;
	}

	public void setRemoveShippingAddressErrorURL(String removeShippingAddressErrorURL) {
		this.removeShippingAddressErrorURL = removeShippingAddressErrorURL;
	}

	public FoodStoreShippingProcessHelper getShippingHelper() {
		return shippingHelper;
	}

	public void setShippingHelper(FoodStoreShippingProcessHelper mShippingHelper) {
		this.shippingHelper = mShippingHelper;
	}

	/**
	 * Handle method to remove shipping address.
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	public boolean handleRemoveShippingAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreShippingGroupFormHandler.handleRemoveShippingAddress";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))){
			Transaction tr = null;
			boolean perfCancelled = false; 

			PerformanceMonitor.startOperation(HandlerNames.HANDLE_REMOVE_SHIPPING_ADDRESS, pRequest.getServletPath());

			if(isLoggingDebug())
				logDebug("Executing handleRemoveShippingAddress method");
			
			setRemoveShippingAddressErrorURL(pRequest.getParameter(ParameterConstants.REMOVE_SHIPPING_ADDRESS_ERRORURL));
			setRemoveShippingAddressSuccessURL(pRequest.getParameter(ParameterConstants.REMOVE_SHIPPING_ADDRESS_SUCCESSURL));
			setRemoveShippingAddressNickname(pRequest.getParameter(ParameterConstants.REMOVE_SHIPPING_ADDRESS_NICKNAME));


			try {
				tr = ensureTransaction();

				if (!checkFormRedirect(null, getRemoveShippingAddressErrorURL(), pRequest, pResponse)) {
					return false;
				}

				preRemoveShippingAddress(pRequest, pResponse);

				if (getFormError()) {
					if (isLoggingDebug()) {
						logDebug("Redirecting due to form error in preRemoveShippingAddress.");
					}

					return checkFormRedirect(null, getRemoveShippingAddressErrorURL(), pRequest, pResponse);
				}			

				removeAddress(pRequest, pResponse);

				if (getFormError()) {
					if (isLoggingDebug()) {
						logDebug("Redirecting due to form error in removeAddress");
					}

					return checkFormRedirect(null, getRemoveShippingAddressErrorURL(), pRequest, pResponse);
				}

				postRemoveShippingAddress(pRequest, pResponse);

			} finally {
				try {
					if (tr != null) {
						commitTransaction(tr);
					}
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_REMOVE_SHIPPING_ADDRESS, pRequest.getServletPath());
						perfCancelled = true;
					}
					if (rrm != null) {
	                    rrm.removeRequestEntry(myHandleMethod);
	                }
				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		// If NO form errors are found, redirect to the success URL.
		// If form errors are found, redirect to the error URL.
		return checkFormRedirect(getRemoveShippingAddressSuccessURL(), 
				getRemoveShippingAddressErrorURL(), 
				pRequest, pResponse);
	}

	private void postRemoveShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		getShippingHelper().removeShippingAddressFromProfile(getProfile(), getRemoveShippingAddressNickname());
	}

	/**
	 * Method to remove shipping address.
	 * 
	 * @param pRequest
	 * @param pResponse
	 */
	private void removeAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		getShippingHelper().removeShippingAddress(getProfile(),getRemoveShippingAddressNickname(),
				getShippingGroupMapContainer());
	}

	private void preRemoveShippingAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) {
		// TODO Auto-generated method stub

	}
}
