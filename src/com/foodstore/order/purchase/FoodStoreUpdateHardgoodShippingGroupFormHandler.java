package com.foodstore.order.purchase;

import java.beans.IntrospectionException;

import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.order.beans.FoodStoreContactInfo;
import com.foodstore.order.beans.FoodStoreOrderContactInfo;
import com.foodstore.profile.beans.FoodStoreAddressTools;


import atg.commerce.CommerceException;
import atg.commerce.order.HardgoodShippingGroup;
import atg.commerce.order.RepositoryContactInfo;
import atg.commerce.order.ShippingGroup;
import atg.commerce.order.purchase.ShippingGroupMapContainer;
import atg.commerce.order.purchase.UpdateHardgoodShippingGroupFormHandler;
import atg.commerce.profile.CommerceProfileTools;
import atg.droplet.DropletException;
import atg.repository.MutableRepository;
import atg.repository.MutableRepositoryItem;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.ServletUtil;
/**
 * @author Litmus7
 * FoodStoreUpdateHardgoodShippingGroupFormHandler can be used for ->
 * -> can specify default shipping group in container
 * -> Existing shipping address can be added to profile address list using property addToProfile
 * -> Profile address and shipping address can be updated together using property updtateProfile
 */
public class FoodStoreUpdateHardgoodShippingGroupFormHandler extends
UpdateHardgoodShippingGroupFormHandler {
	
	private final String  NULL_POINTER_EXCEPTION_MESSAGE = "Unknown error occured";

	/* To specify default shipping address */
	private boolean setDefaultShippingGroup;
	public boolean isSetDefaultShippingGroup() {
		return setDefaultShippingGroup;
	}
	public void setSetDefaultShippingGroup(boolean setDefaultShippingGroup) {
		this.setDefaultShippingGroup = setDefaultShippingGroup;
	}
	/* addToProfile if kept true will add the current working shipping group to the user profile */
	private boolean addToProfile;
	public boolean isAddToProfile() {
		return addToProfile;
	}
	public void setAddToProfile(boolean addToProfile) {
		this.addToProfile = addToProfile;

	}


	@Override
	/* This method updates the hardgoodShippingGroup 
	 * (non-Javadoc)
	 * @see atg.commerce.order.purchase.UpdateHardgoodShippingGroupFormHandler#updateHardgoodShippingGroup(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	public void updateHardgoodShippingGroup(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse)
	{
		if(isLoggingDebug())
			logDebug("Entering updateHardgoodShippingGroup()");
		vlogDebug(getWorkingHardgoodShippingGroup().getShippingAddress()+"");
		validateShippingGroup(); // call validations
		if(getFormError())
			return;
		vlogDebug("No form errors");
		if(isUpdateContainer())
		{	vlogDebug("Updating container ");
		ShippingGroupMapContainer container = getShippingGroupMapContainer();
		ShippingGroup shippingGroup = container.getShippingGroup(getHardgoodShippingGroupName());

		if(shippingGroup != null){
			if(isSetDefaultShippingGroup()){
				vlogDebug(container.getDefaultShippingGroupName() + " is the previous default shipping address ");
				vlogDebug(getHardgoodShippingGroupName() + " Making default ");
				container.setDefaultShippingGroupName(getHardgoodShippingGroupName());
				vlogDebug(container.getDefaultShippingGroupName() + " is the default shipping address ");

			} 
			updateContainer();
			vlogDebug("Container updated");

		}


		}
		if(getFormError())
			return;
		vlogDebug("No form errors ");
		if(isAddToProfile()){
			vlogDebug("Adding to profile.");
			addToProfile();
		} else if(isUpdateProfile()){
			vlogDebug("Updating  profile.");
			updateProfile();
		}        
		if(getFormError())
			return;
		if(isUpdateOrder())
			updateOrder();
	}
	
	/**
	 * addeToProfile will check if the address exists in user secondary addresses. If exists then it will update the profile address else it will
	 * add a new address to profile address.
	 */
	public void addToProfile()
	{
		if(isLoggingDebug())
			logDebug("Entering updateProfile()");
		HardgoodShippingGroup shippingGroup = getWorkingHardgoodShippingGroup();
		String name = getHardgoodShippingGroupName();
		MutableRepositoryItem address = null;
		try
		{
			CommerceProfileTools pt = getOrderManager().getOrderTools().getProfileTools();
			address = (MutableRepositoryItem)pt.getProfileAddress(getProfile(), name); // get the user address with the given name

			if(address != null) // such an address exists
			{
				if(isLoggingDebug())
					logDebug("Profile has this address and it is not null.");
				RepositoryItem profile = getProfile();
				MutableRepository repository = (MutableRepository)profile.getRepository();
				pt.updateProfileRepositoryAddress(address, shippingGroup.getShippingAddress());
				repository.updateItem(address);
			} else
			{
				if(isLoggingDebug())
					logDebug("Profile does not have this address.");
				try
				{	/* Creates a new address to user address list */
					getOrderManager().getOrderTools().getProfileTools().createProfileRepositorySecondaryAddress(getProfile(), name, shippingGroup.getShippingAddress());
				}
				catch(RepositoryException repexec)
				{
					try
					{
						DynamoHttpServletRequest pRequest = ServletUtil.getCurrentRequest();
						DynamoHttpServletResponse pResponse = ServletUtil.getCurrentResponse();
						processException(repexec, ErrorsAndExceptionMessages.COULDNOT_ADD_SHIPPING_ADDRESS_TO_PROFILE, pRequest, pResponse);
					}
					catch(Exception exception)
					{
						if(isLoggingError())
							logError(exception);
					}
				}
			}
		}
		catch(RepositoryException repexec)
		{
			try
			{
				processException(repexec, ErrorsAndExceptionMessages.COULDNOT_ADD_SHIPPING_ADDRESS_IN_PROFILE, ServletUtil.getCurrentRequest(), ServletUtil.getCurrentResponse());
			}
			catch(Exception exception)
			{
				if(isLoggingError())
					logError(exception);
			}
		} catch (NullPointerException e) {
			addFormException(new DropletException(NULL_POINTER_EXCEPTION_MESSAGE));
			
		}
	}

}
