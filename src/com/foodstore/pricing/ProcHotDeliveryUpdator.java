package com.foodstore.pricing;

import java.util.Map;

import com.foodstore.order.FoodStoreOrderImpl;
import com.foodstore.order.beans.FoodStoreCommerceItemImpl;

import atg.commerce.order.CommerceItem;
import atg.commerce.order.PipelineConstants;
import atg.commerce.pricing.ItemPriceInfo;
import atg.commerce.pricing.OrderPriceInfo;
import atg.droplet.ForEach;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

/**
 * Changing the amount of commerceitem if user has clicked hotdeliveryCheckbox
 * 
 * @author Vineeth
 *
 */
public class ProcHotDeliveryUpdator implements PipelineProcessor{
	private final int SUCCESS = 1;
    private final int FAILURE = 2;
    
    private Double hotDeliveryCharge;
	public Double getHotDeliveryCharge() {
		return hotDeliveryCharge;
	}

	public void setHotDeliveryCharge(Double hotDeliveryCharge) {
		this.hotDeliveryCharge = hotDeliveryCharge;
	}

	@Override
	public int runProcess(Object pParam, PipelineResult paramPipelineResult)
			throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> params = (Map<String, Object>) pParam;
	       FoodStoreOrderImpl order = (FoodStoreOrderImpl) params.get(PipelineConstants.ORDER);
	       int count =0;
	       for(Object item:order.getCommerceItems()){
	    	   FoodStoreCommerceItemImpl foodItem = (FoodStoreCommerceItemImpl)item;
	    	   if(foodItem.isIshotDelivery()){
	    		   count++;
	    		   ItemPriceInfo info = foodItem.getPriceInfo();
	    		   info.setAmount(info.getAmount()+getHotDeliveryCharge());
	    	   }
	    	   
	       }
	       if(count!=0){
	    	   OrderPriceInfo oinfo=order.getPriceInfo();
	    	   oinfo.setAmount(oinfo.getAmount()+(count*getHotDeliveryCharge()));
	    	   
	       }
	       
		return SUCCESS;
	}

	@Override
	public int[] getRetCodes() {
		// TODO Auto-generated method stub
		return new int[] { SUCCESS, FAILURE };
	}
	
}
