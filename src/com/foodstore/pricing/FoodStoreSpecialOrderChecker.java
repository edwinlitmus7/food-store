package com.foodstore.pricing;

import java.util.Map;

import org.omg.PortableInterceptor.InvalidSlot;

import com.foodstore.order.FoodStoreOrderImpl;
import com.sun.xml.messaging.saaj.util.LogDomainConstants;


import atg.commerce.order.PipelineConstants;
import atg.service.pipeline.PipelineProcessor;
import atg.service.pipeline.PipelineResult;

/**
 * Marking an Order as special order.
 * 
 * @author Vineeth
 *
 */
public class FoodStoreSpecialOrderChecker implements PipelineProcessor {
	
	private final int SUCCESS = 1;
    private final int FAILURE = 2;
	@Override
	public int runProcess(Object pParam, PipelineResult pResults)
			throws Exception {
			
		   Map<String, Object> params = (Map<String, Object>) pParam;
	       FoodStoreOrderImpl order = (FoodStoreOrderImpl) params.get(PipelineConstants.ORDER);
	       if((order.getPriceInfo().getAmount()>=180)&&(order.getPriceInfo().getAmount()<=200)){
	    	   order.setSpecialOrderCloseness(true);
	       }else{
	    	   order.setSpecialOrderCloseness(false);
	       }
	       if(order.getPriceInfo().getAmount()>= 200){
	    	   order.setSpecialOrder(true);
	       }else{
	    	   order.setSpecialOrder(false);
	       }
	       params.put(PipelineConstants.ORDER, order);
	      
		return SUCCESS;
	}

	@Override
	public int[] getRetCodes() {
		// TODO Auto-generated method stub
		return new int[] { SUCCESS, FAILURE };
	}

}
