package com.foodstore.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;

import com.foodstore.interfaces.RepositoryConstants;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
/**
 * 
 * @author Anuraj T S
 *
 */
public class SkuParentProductFinderDroplet extends DynamoServlet {
	private static String PARENT_PRODUCT_STRING = "parentProduct";
	private static String OPARAM_STRING = "OUTPUT";
	private static String SKU_ID_PARAM_NAME ="skuId";
	private Repository catalogRepository;
	public Repository getCatalogRepository() {
		return catalogRepository;
	}
	public void setCatalogRepository(Repository catalogRepository) {
		this.catalogRepository = catalogRepository;
	}

	
	 @Override
	 public void service (DynamoHttpServletRequest request, DynamoHttpServletResponse response) throws ServletException, IOException {
		 			
		 		 	String paramSkuId = request.getParameter(SKU_ID_PARAM_NAME);
		 			List<RepositoryItem> parentProducts = getParentProducts(paramSkuId);
		 			
		 			request.setParameter(PARENT_PRODUCT_STRING,parentProducts.get(0));
		 			request.serviceParameter(OPARAM_STRING, request, response);
		  }
	 
	private String getSideSkuQuery() {
			return "id=?0";
	}
	public List<RepositoryItem> getParentProducts(String paramSkuId) {
			
			Set<RepositoryItem> sideSkus = null;
			try {
				
				RepositoryItem item = getCatalogRepository().getItem(paramSkuId,RepositoryConstants.SKU_VIEW);
				
				sideSkus = 	(Set<RepositoryItem>) item.getPropertyValue("parentProducts");
			} catch (RepositoryException e) {
				if(isLoggingDebug()) {
					logDebug("Exception while getting the sideskus " + e);
				}
				e.printStackTrace();
			}
			
			return new ArrayList<>(sideSkus);

		}
		
}
