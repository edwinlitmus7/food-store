package com.foodstore.catalog;

import com.foodstore.interfaces.RepositoryConstants;

import atg.adapter.gsa.ChangeAwareList;
import atg.nucleus.GenericService;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;

/**
 * The skus of the products are retrieved from database. The id of the product
 * chosen by the user is stored in productId.
 * 
 * @author Thasnim
 * 
 */
public class DisplaySku extends GenericService {

	private Repository catalogRepository;
	private String productId;
	private RepositoryItem[] items;
	


	public Repository getCatalogRepository() {
		return catalogRepository;
	}

	public void setCatalogRepository(Repository catalogRepository) {
		this.catalogRepository = catalogRepository;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * The skus of the products are retrieved using this method.
	 * @return
	 */
	public ChangeAwareList getSku() {
		RepositoryView view;
		ChangeAwareList childSKUs = null;
		try {
			view = getCatalogRepository().getView(RepositoryConstants.PRODUCT_VIEW);
			QueryBuilder queryBuilder = view.getQueryBuilder();
			QueryExpression id = queryBuilder
					.createPropertyQueryExpression(RepositoryConstants.ID);
			QueryExpression value = queryBuilder
					.createConstantQueryExpression(getProductId());
			Query idQuery = queryBuilder.createComparisonQuery(id, value,
					QueryBuilder.EQUALS);
			items = view.executeQuery(idQuery);

			for (RepositoryItem item : items) {
				childSKUs = (ChangeAwareList) item
						.getPropertyValue(RepositoryConstants.CHILDSKUS_PROPERTY_VALUE);
			}
			if (isLoggingDebug()) {

				logDebug("Child skus:" + childSKUs);
			}
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		return childSKUs;

	}

}