package com.foodstore.catalog;

import java.io.IOException;

import javax.servlet.ServletException;

import com.foodstore.interfaces.ParameterConstants;
import com.foodstore.interfaces.RepositoryConstants;

import atg.adapter.gsa.ChangeAwareList;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
/**
 * The sku of the specified index will be retrieved from list of skus
 * @author Thasnim
 *
 */
public class SkuLookUpDroplet extends DynamoServlet {

	@Override
	public void service(DynamoHttpServletRequest req,
			DynamoHttpServletResponse res) throws ServletException, IOException {
		
		if(isLoggingDebug())
			logDebug("Executing SkuLookUpDroplet");
		ChangeAwareList skus=(ChangeAwareList) req.getObjectParameter(RepositoryConstants.SKUS_OBJECT_PARAMETER);
		int index=Integer.parseInt(req.getParameter(RepositoryConstants.INDEX_PARAMETER));
		req.setParameter(ParameterConstants.ELEMENT_PARAMETER, skus.get(index));
		req.serviceParameter(ParameterConstants.OUTPUT_PARAMETER, req, res);
	}
}