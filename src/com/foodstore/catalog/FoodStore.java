package com.foodstore.catalog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;

import com.foodstore.interfaces.RepositoryConstants;

import atg.adapter.gsa.ChangeAwareList;
import atg.nucleus.GenericService;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;

/**
 * The current schedule and current root category of the catalog to be displayed
 * is found out.
 * 
 * @author Thasnim
 * 
 */
public class FoodStore extends GenericService {

	private Repository catalogRepository;
	private RepositoryItem[] scheduleItems, availableItems, availableProducts,
			items;
	private ArrayList<Object> searchResults;
	private HashSet<Object> keys;
	private String message;
	private String productId;
	private ChangeAwareList childCategories = null;

	public Repository getCatalogRepository() {
		return catalogRepository;
	}

	public void setCatalogRepository(Repository catalogRepository) {
		this.catalogRepository = catalogRepository;
	}

	public ArrayList<Object> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(ArrayList<Object> searchResults) {
		this.searchResults = searchResults;
	}

	public HashSet<Object> getKeys() {
		return keys;
	}

	public void setKeys(HashSet<Object> keys) {
		this.keys = keys;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public ChangeAwareList getChildCategories() {
		return childCategories;
	}

	public void setChildCategories(ChangeAwareList childCategories) {
		this.childCategories = childCategories;
	}

	/**
	 * Using the current hour and minute, the current schedule is retrieved from
	 * database. The schedules are breakfast, lunch, tea and dinner.
	 * 
	 * @return
	 */
	public String getSchedule() {
		String id = null;
		try {
			if (isLoggingDebug()) {
				logDebug("Inside getSchedule() method");
			}
			RepositoryView categoryView = getCatalogRepository().getView(
					RepositoryConstants.CATEGORY_VIEW);
			int currentHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY); // Getting
																				// current
																				// hour
																				// of
																				// the
																				// day
			int currentMinute = (currentHour * 60)
					+ Calendar.getInstance().get(Calendar.MINUTE); // Getting
																	// current
																	// minute of
																	// the day
			RqlStatement statement = RqlStatement
					.parseRqlStatement(RepositoryConstants.TIME_QUERY);
			Object params[] = { currentMinute };
			scheduleItems = statement.executeQuery(categoryView, params);
			for (RepositoryItem item : scheduleItems) {
				if (isLoggingDebug()) {

					logDebug("Current schedule id is " + item);
				}
				id = (String) item.getPropertyValue(RepositoryConstants.ID);
			}

		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		return id;
	}

	/**
	 * The root category in the current schedule is found. The child categories
	 * of the root category are stored in the ChangeAwareList childCategories
	 * and is returned by this method.
	 * 
	 * @return
	 */
	public ChangeAwareList getRootCategory() {

		if (isLoggingDebug()) {

			logDebug("Inside getRootCategory() method");
		}
		if (getChildCategories() == null) {

			getFoodMenu();
		}

		return getChildCategories();

	}


	/**
	 * The root category to be displayed at the current time is found using
	 * getRootCataegory() method. The categories and products under the root
	 * category are then retrieved from repository. The products list is then
	 * returned in this method.
	 * @return
	 */

	public Query getProducts() {
		ChangeAwareList childprods = null;
		ArrayList<String> childproducts = new ArrayList<String>();
		Query prdQuery = null;
		try {
			if (isLoggingDebug()) {

				logDebug("Inside getProducts() method");
			}
			ChangeAwareList categories = getRootCategory();
			ArrayList<String> ids = new ArrayList<String>();
			for (Object category : categories) {
				ids.add(category.toString().split(":")[1]);
			}
			if (isLoggingDebug()) {

				logDebug("Root category ids: " + ids);
			}
			String[] idsArray = ids.toArray(new String[ids.size()]); // converting
																		// arraylist
																		// to
																		// array
			RepositoryView categoryView = getCatalogRepository().getView(
					RepositoryConstants.CATEGORY_VIEW);
			QueryBuilder queryBuilder = categoryView.getQueryBuilder();
			Query catQuery = queryBuilder.createIdMatchingQuery(idsArray); // To
																			// find
																			// categories
																			// having
																			// ids
																			// matching
																			// with
																			// those
																			// in
																			// idsArray
			availableItems = categoryView.executeQuery(catQuery);
			for (RepositoryItem item : availableItems) {
				childprods = (ChangeAwareList) item
						.getPropertyValue(RepositoryConstants.CHILDPRODUCTS_PROPERTY_VALUE);
				for (Object obj : childprods) {
					childproducts.add(obj.toString());
				}
			}
			if (isLoggingDebug()) {

				logDebug("child products: " + childprods);
			}
			ArrayList<String> prdIds = new ArrayList<String>();
			for (String product : childproducts) {

				prdIds.add(product.split(":")[1]);
			}
			String[] prdIdsArray = prdIds.toArray(new String[prdIds.size()]); // converting
																				// arraylist
																				// to
																				// array
			RepositoryView productView = getCatalogRepository().getView(
					RepositoryConstants.PRODUCT_VIEW);
			QueryBuilder queryBuildr = productView.getQueryBuilder();
			prdQuery = queryBuildr.createIdMatchingQuery(prdIdsArray); // To
																		// find
																		// products
																		// having
																		// ids
																		// matching
																		// with
																		// those
																		// in
																		// prdIdsArray

		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return prdQuery;
	}


	/**
	 * The keywords of the products to be displayed at the current time is
	 * stored in the hashset 'keys' for handling autocomplete.
	 * @return
	 */
	public HashSet<Object> getAutoComplete() {
		if (isLoggingDebug()) {

			logDebug("Getting available products");
		}
		if (getKeys() == null) {
			getKeywords();

		}
		return getKeys();
	}

	public void getFoodMenu() {
		RepositoryView view;
		ChangeAwareList childCategories = null;
		try {
			if (isLoggingDebug()) {

				logDebug("Before calling  Scheduler........");
			}
			String schedule = getSchedule();
			view = getCatalogRepository().getView(
					RepositoryConstants.CATEGORY_VIEW);
			QueryBuilder queryBuilder = view.getQueryBuilder();
			QueryExpression names = queryBuilder
					.createPropertyQueryExpression(RepositoryConstants.ID);
			QueryExpression value = queryBuilder
					.createConstantQueryExpression(schedule);
			Query nameQuery = queryBuilder.createComparisonQuery(names, value,
					QueryBuilder.EQUALS);
			items = view.executeQuery(nameQuery);

			for (RepositoryItem item : items) {
				childCategories = (ChangeAwareList) item
						.getPropertyValue(RepositoryConstants.FIXEDCHILDCATEGORIES_PROPERTY_VALUE);
			}
			setChildCategories(childCategories);
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * The search keywords are loaded from db
	 */
	public void getKeywords() {
		RepositoryView productView;
		try {
			productView = getCatalogRepository().getView(
					RepositoryConstants.PRODUCT_VIEW);
			Query prdQuery = getProducts();
			availableProducts = productView.executeQuery(prdQuery);
			keys = new HashSet<Object>();
			for (RepositoryItem item : availableProducts) {
				ChangeAwareList keywords = (ChangeAwareList) item
						.getPropertyValue(RepositoryConstants.KEYWORDS_PROPERTY_VALUE);
				for (Object obj : keywords) {
					keys.add(obj.toString());
				}
			}
			setKeys(keys);
			if (isLoggingDebug()) {

				logDebug("keywords : " + keys);
			}
		} catch (RepositoryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
