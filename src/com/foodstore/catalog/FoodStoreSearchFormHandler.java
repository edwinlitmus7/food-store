package com.foodstore.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletException;

import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.RepeatingRequestMonitorErrorMsg;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion.Static;
import com.foodstore.interfaces.RepositoryConstants;

import atg.adapter.gsa.ChangeAwareList;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.droplet.DropletException;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.servlet.SearchFormHandler;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;

/**
 * The form handler for handling product search, faceted search and
 * autocomplete. The data from repository are retrieved here.
 * 
 * @author Thasnim
 * 
 */

public class FoodStoreSearchFormHandler extends SearchFormHandler {

	private Repository catalogRepository;
	private RepositoryItem[] availableProducts, availableSkus, products;
	private HashSet<Object> searchResults;
	private String repositoryKey;
	private FoodStore foodStore;
	private String lowerLimit, upperLimit;
	private HashSet<Object> results;
	private HashSet<Object> keys;
	private String message;
	private RepeatingRequestMonitor repeatingRequestMonitor;

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return repeatingRequestMonitor;
	}

	public void setRepeatingRequestMonitor(RepeatingRequestMonitor repeatingRequestMonitor) {
		this.repeatingRequestMonitor = repeatingRequestMonitor;
	}

	public Repository getCatalogRepository() {
		return catalogRepository;
	}

	public void setCatalogRepository(Repository catalogRepository) {
		this.catalogRepository = catalogRepository;
	}

	public HashSet<Object> getSearchResults() {
		return searchResults;
	}

	public void setSearchResults(HashSet<Object> searchResults) {
		this.searchResults = searchResults;
	}

	public String getRepositoryKey() {
		return repositoryKey;
	}

	public void setRepositoryKey(String repositoryKey) {
		this.repositoryKey = repositoryKey;
	}

	public FoodStore getFoodStore() {
		return foodStore;
	}

	public void setFoodStore(FoodStore foodStore) {
		this.foodStore = foodStore;
	}

	public String getLowerLimit() {
		return lowerLimit;
	}

	public void setLowerLimit(String lowerLimit) {
		this.lowerLimit = lowerLimit;
	}

	public String getUpperLimit() {
		return upperLimit;
	}

	public void setUpperLimit(String upperLimit) {
		this.upperLimit = upperLimit;
	}

	public HashSet<Object> getResults() {
		return results;
	}

	public void setResults(HashSet<Object> results) {
		this.results = results;
	}

	public HashSet<Object> getKeys() {
		return keys;
	}

	public void setKeys(HashSet<Object> keys) {
		this.keys = keys;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * The products to be displayed at the current time is retrieved using
	 * getProducts() method. The keyword entered by the user is stored in the
	 * variable repositoryKey. The products having matching keyword in
	 * keywordsDefaults property of products is found out and are stored in the
	 * hashset searchResults.
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */

	public boolean handleProductSearch(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreSearchFormHandler.handleProductSearch";
		
		if(isLoggingDebug())
			logDebug("Inside handle product search");
		
		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			searchResults = new HashSet<Object>();

			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.PRODUCT_SEARCH_HANDLER, pRequest.getServletPath());

			try {
				// converting arraylist to array
				RepositoryView productView = getCatalogRepository().getView(RepositoryConstants.PRODUCT_VIEW);

				QueryBuilder productQuery = productView.getQueryBuilder();
				Query prdQuery = foodStore.getProducts();

				QueryExpression keywords = productQuery
						.createPropertyQueryExpression(RepositoryConstants.KEYWORDS_PROPERTY_VALUE);

				QueryExpression userKey = productQuery
						.createConstantQueryExpression(getRepositoryKey()
								.toLowerCase());
				Query keywordQuery = productQuery.createPatternMatchQuery(keywords,
						userKey, QueryBuilder.CONTAINS);
				Query[] pieces = { prdQuery, keywordQuery };
				Query andQuery = productQuery.createAndQuery(pieces);
				availableProducts = productView.executeQuery(andQuery);
				for (RepositoryItem item : availableProducts) {

					searchResults.add(item);
				}
				if (isLoggingDebug()) {
					logDebug("Adding results " + searchResults);
				}
				if (searchResults.isEmpty()) {
					setMessage(ErrorsAndExceptionMessages.NO_SEARCHRESULTS);

				}

			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				setMessage(ErrorsAndExceptionMessages.NO_SEARCHRESULTS);

			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally {
				try {
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.PRODUCT_SEARCH_HANDLER, pRequest.getServletPath());
						perfCancelled = true;
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		return checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest,
				pResponse);

	}

	/**
	 * The faceted search is handled in this method. The price range chosen by
	 * the user is stored in the variable 'choice'. The current products are
	 * retrieved using getProducts() method. The skus are fetched from childSkus
	 * property of products. The listPrice property of the skus are checked to
	 * find if it comes under the price range chosen by the user. The parent
	 * products of those skus are then stored in the hashset 'results'.
	 * @param pRequest
	 * @param pResponse
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	public boolean handleFacetedSearch(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreSearchFormHandler.handleFacetedSearch";
		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.FACETED_SEARCH_HANDLER, pRequest.getServletPath());
			if (isLoggingDebug()) {

				logDebug("Inside Handling faceted search method");
			}
			ArrayList<String> skus = new ArrayList<String>();
			results = new HashSet<Object>();
			RepositoryView skuView;
			QueryBuilder skuBuilder = null;
			QueryExpression price = null;
			Query skuQuery = null;
			try {
				RepositoryView productView = getCatalogRepository().getView(
						RepositoryConstants.PRODUCT_VIEW);
				Query prdQuery = foodStore.getProducts();
				products = productView.executeQuery(prdQuery);
				skuView = getCatalogRepository().getView(RepositoryConstants.SKU_VIEW);

				ChangeAwareList childSKUs = null;
				for (RepositoryItem product : products) {

					childSKUs = ((ChangeAwareList) product.getPropertyValue(RepositoryConstants.CHILDSKUS_PROPERTY_VALUE));

					for (Object obj : childSKUs) {
						skus.add(obj.toString());
					}

					ArrayList<String> skuIds = new ArrayList<String>();
					for (String sku : skus) {

						skuIds.add(sku.split(":")[1]);
					}
					String[] skuIdsArray = skuIds
							.toArray(new String[skuIds.size()]); // converting
					// arraylist to
					// array
					skuBuilder = skuView.getQueryBuilder();
					skuQuery = skuBuilder.createIdMatchingQuery(skuIdsArray); // To
					// find
					// skus
					// having
					// ids
					// matching
					// with
					// those
					// in
					// skuIdsArray
					price = skuBuilder.createPropertyQueryExpression(RepositoryConstants.LISTPRICE_PROPERTY);

				}

				QueryExpression lowerRange = skuBuilder
						.createConstantQueryExpression(getLowerLimit());
				Query lowerPriceQuery = skuBuilder.createComparisonQuery(price,
						lowerRange, QueryBuilder.GREATER_THAN_OR_EQUALS);
				QueryExpression upperRange = skuBuilder
						.createConstantQueryExpression(getUpperLimit());
				Query upperPriceQuery = skuBuilder.createComparisonQuery(price,
						upperRange, QueryBuilder.LESS_THAN_OR_EQUALS);
				Query[] pieces = { skuQuery, lowerPriceQuery, upperPriceQuery };
				Query andQuery = skuBuilder.createAndQuery(pieces);
				availableSkus = skuView.executeQuery(andQuery);

				for (RepositoryItem sku : availableSkus) {
					results.addAll((Collection<? extends Object>) sku.getPropertyValue(RepositoryConstants.PARENTPRODUCTS_PROPERTY));
				}
				
				if(isLoggingDebug())
					logDebug("Faceted search results " +results);
				
				if (results.isEmpty()) {
					setMessage(ErrorsAndExceptionMessages.NO_SEARCHRESULTS);

				}
			} catch (RepositoryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NullPointerException e) {
				// TODO Auto-generated catch block
				setMessage(ErrorsAndExceptionMessages.NO_SEARCHRESULTS);

			}finally {
				try {
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.FACETED_SEARCH_HANDLER, pRequest.getServletPath());
						perfCancelled = true;
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		return checkFormRedirect(getSuccessURL(), getErrorURL(), pRequest,
				pResponse);
	}
}