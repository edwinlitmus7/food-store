package com.foodstore.catalog;

import java.io.IOException;

import javax.servlet.ServletException;

import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.ParameterConstants;

import com.foodstore.interfaces.RepositoryConstants;

import atg.adapter.gsa.ChangeAwareList;
import atg.commerce.util.RepeatingRequestMonitor;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;

/**
 * The related products are retrieved from database
 * @author Thasnim
 *
 */
public class RelatedProductsDroplet extends DynamoServlet{

	private Repository catalogRepository;
	private String productId;
	private RepositoryItem[] items;
	
	public Repository getCatalogRepository() {
		return catalogRepository;
	}

	public void setCatalogRepository(Repository catalogRepository) {
		this.catalogRepository = catalogRepository;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public void service(DynamoHttpServletRequest req,
			DynamoHttpServletResponse res) throws ServletException, IOException {

		RepositoryView view;
		ChangeAwareList relatedProducts = null;
		boolean perfCancelled = false;
		
		if(isLoggingDebug())
			logDebug("Executing RelatedProductsDroplet ");
		
		PerformanceMonitor.startOperation(HandlerNames.RELATED_PRODUCT_DROPLET, req.getServletPath());
		setProductId(req.getParameter(RepositoryConstants.ID));
		try {
			view = getCatalogRepository().getView(RepositoryConstants.PRODUCT_VIEW);
			QueryBuilder queryBuilder = view.getQueryBuilder();
			QueryExpression id = queryBuilder.createPropertyQueryExpression(RepositoryConstants.ID);
			QueryExpression value = queryBuilder.createConstantQueryExpression(getProductId());
			Query idQuery = queryBuilder.createComparisonQuery(id, value, QueryBuilder.EQUALS);
			items = view.executeQuery(idQuery);

			for (RepositoryItem item: items) {
				relatedProducts = (ChangeAwareList) item.getPropertyValue(RepositoryConstants.FIXEDRELATEDPRODUCTS_PROPERTY_VALUE);
			}
			req.setParameter(ParameterConstants.RELATEDPRODUCTS_PARAMETER, relatedProducts);
			req.serviceParameter(ParameterConstants.OUTPUT_PARAMETER, req, res);
		} catch(RepositoryException e) {
			e.printStackTrace();
		}finally {
			try {
				if (!perfCancelled) {
					PerformanceMonitor.endOperation(HandlerNames.RELATED_PRODUCT_DROPLET, req.getServletPath());
					perfCancelled = true;
				}
			} catch (PerfStackMismatchException e2) {
				if (isLoggingWarning())
					logWarning(e2);
			}
		}
	}
}
