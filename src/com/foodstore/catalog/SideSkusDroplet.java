package com.foodstore.catalog;

import java.io.IOException;
import java.util.Arrays;

import java.util.List;

import javax.servlet.ServletException;

import com.foodstore.interfaces.RepositoryConstants;

import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.repository.rql.RqlStatement;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.DynamoServlet;
/**
 * 
 * @author Anuraj T S
 *	Droplet finds sideskus and passes to jsp. 
 */

public class SideSkusDroplet extends DynamoServlet {
	private static String SIDE_SKU_STRING = "sideSkus";
	private static String OPARAM_STRING = "OUTPUT";
	private Repository catalogRepository;
	public Repository getCatalogRepository() {
		return catalogRepository;
	}
	public void setCatalogRepository(Repository catalogRepository) {
		this.catalogRepository = catalogRepository;
	}

	 @Override
	 public void service (DynamoHttpServletRequest request, DynamoHttpServletResponse response) throws ServletException, IOException {
		 			List<RepositoryItem> sideSkus = getSideSkus();
		 			request.setParameter(SIDE_SKU_STRING,sideSkus);
		 			request.serviceParameter(OPARAM_STRING, request, response);
		  }
	 
	private String getSideSkuQuery() {
			return "isSideSku=?0";
	}
	public List<RepositoryItem> getSideSkus() {
			RepositoryView productView;
			RepositoryItem[] sideSkus = null;
			try {
				productView = getCatalogRepository().getView(
						RepositoryConstants.SKU_VIEW);
				RqlStatement statement = RqlStatement.parseRqlStatement(getSideSkuQuery());
				Object params[] = new Object[1];
				params[0] = new Boolean(true);
				sideSkus = statement.executeQuery(productView,params);	
			} catch (RepositoryException e) {
				if(isLoggingDebug()) {
					logDebug("Exception while getting the sideskus " + e);
				}
				e.printStackTrace();
			}
			
			return Arrays.asList(sideSkus);

		}
		
}
