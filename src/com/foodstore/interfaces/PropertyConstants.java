package com.foodstore.interfaces;

/***
 * String constants for newly added properties.
 * 
 * @author akhil
 *
 */
public interface PropertyConstants {

	public static final String NICKNAME_PROPERTY="nickname";
	public static final String OLD_NICKNAME_PROPERTY="oldNickname";
	public static final String MOBILE_PROPERTY="mobile";
	public static final String TYPE_OF_ADDRESS_PROPERTY="typeofaddress";
	public static final String GENDER_PROPERTY="gender";
	public static final String REFERRAL_SOURCE_PROPERTY="referralSource";
	public static final String DATE_OF_BIRTH_PROPERTY="dateOfBirth";
	public static final String NEW_CREDITCARD="newCreditCard";
	public static final String CREDITCARD_ID_PROPERTY="id";
	public static final String NAME_ON_CARD_PROPERTY="nameOnCard";
	public static final String UNKNOWN_STATE_CODE = "unknown";
	public static final String BILLING_ADDRESS_PROPERTY = "billingAddress";
	public static final String VALIDATE_BILLING_ADDRESS_PROPERTY = "validateBillingAddress";
	public static final String CREDITCARD_PROPERTY = "creditCard";
	public static final String USER_FIRSTNAME_PROPERTY = "firstName";
	public static final String USER_LASTNAME_PROPERTY = "lastName";
	public static final String DATE_FORMATE_STRING = "yyyy/MM/dd HH:mm:ss";
	public static final String USER_EMAIL_PROPERTY = "email";
	public static final String SCHEDULEID_PROPERTY = "scheduleId";
	public static final String SHIPPINGADDRESS_NICKNAME_PROPERTY = "shippingAddress.nickname";
	public static final String SHIPPINGADDRESS_TYPEOFADDRESS_PROPERTY = "shippingAddress.typeOfAddress";
	public static final String SHIPPINGADDRESS_MOBILE_PROPERTY = "shippingAddress.mobile";
	public static final String UPDATE_CONTAINER_PROPERTY = "updateContainer";
	public static final String COUNTRY_LOCALE = "en_" ;
}
