package com.foodstore.interfaces;

public interface HttpConstants {
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String UTF_8 = "UTF-8";
    public static final String POST = "POST";
}
