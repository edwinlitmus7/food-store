package com.foodstore.interfaces;

/***
 * String constants for Google authentication used in Google login.
 * 
 * @author akhil
 *
 */
public interface GoogleAuthenticationConstants {
	// Error page in case of any unknown error occured.
	public final String LOGIN_ERROR_PAGE = "./error.jsp";
	public final String INVALID_ACCESS_TOKEN = " Invalid access token ";
	public final String GENERAL_SECURITY_ERROR_MSG = " Error occured while processing ";
	public final String UNKNOWN_ERROR_MSG = " Unknown error occured please try again ";
	public final String INVALID_EMAIL_MSG = " Invalid email obtained ";
	// The label of data received from payload.
	public final String PAYLOAD_LAST_NAME = "family_name";
	public final String PAYLOAD_FIRST_NAME = "given_name";
	// public key of google application
	public final String GOOGLE_PRIVATE_KEY = "642167561006-qi2sb3n02su1d4csld6cjp2ussecjnfd.apps.googleusercontent.com";

}
