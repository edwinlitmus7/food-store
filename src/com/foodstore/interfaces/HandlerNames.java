package com.foodstore.interfaces;

/***
 * String constants used to mention handle method names.
 * It is mainly used for Performance monitor and repeat request monitor.
 * 
 * @author akhil
 *
 */
public interface HandlerNames {
	
	static final String HANDLE_LOGIN = "HandleLogin";
	static final String HANDLE_CREATE = "HandleCreate";
	static final String HANDLE_NEWADDRESS = "HandleNewAddress";
	static final String HANDLE_UPDATE_CARD = "handleUpdateCard";
	static final String HANDLE_NORMAL_LOGIN = "HandleNormalLogin";
	static final String HANDLE_UPDATE_ADDRESS = "HandleUpdateAddress";
	static final String HANDLE_REMOVE_ADDRESS = "HandleRemoveAddress";
	static final String PRODUCT_SEARCH_HANDLER = "ProductSearchHandler";
	static final String FACETED_SEARCH_HANDLER = "FacetedSearchHandler";
	static final String RELATED_PRODUCT_DROPLET = "RelatedProductDroplet";
	static final String HANDLE_REMOVE_CREDIT_CARD = "HandleRemoveCreditCard";
	static final String HANDLE_SET_DEFAULT_CREDIT_CARD = "HandleSetDefaultCreditCard";
	
	static final String HANDLE_SET_ORDER_BY_COMMERCE_ID = "HandleSetOrderByCommerceId";
	static final String SET_ORDER_BY_COMMERCE_ID_OPERATION_NAME = "UPDATE QUANTITY OF ITEMS IN CART";

	static final String HANDLE_REMOVE_SHIPPING_ADDRESS = "HandleRemoveShippingAddress";
	static final String HANDLE_ADD_SIDE_ITEM_TO_ORDER = "HandleAddSideItemToOrder";
	static final String PERFORMANCE_MONITOR_SET_DEFAULT_CREDIT_CARD_PARAM = "parameter";
	static final String HANDLE_CREATE_NEW_CREDITCARD_AND_ADDRESS = "HandleCreateNewCreditCardAndAddress";
	
	static final String HANDLE_ADD_SHIPPING_GROUP = "handleNewHardgoodShippingGroup";
	static final String ADD_SHIPPING_GROUP_OPERATION_NAME= "ADD NEW SHIPPING GROUP";
	
	static final String HANDLE_ADD_PAYMENT_GROUP = "handleNewCreditCard";
	static final String ADD_PAYMENT_GROUP_OPERATION_NAME= "ADD NEW PAYEMENT GROUP";
	
	static final String HANDLE_ADD_ITEM_TO_CART = "handleAddItemToOrder";
	static final String ADD_ITEM_TO_CART_OPERATION_NAME= "ADD ITEM TO CART";
	
	static final String HANDLE_MOVE_TO_PURCHASE_INFO = "handleMoveToPurchaseInfoByCommerceId";
	static final String MOVE_TO_PURCHASE_INFO_OPERATION_NAME= "BEGIN CHECKOUT";
	
	static final String HANDLE_SUBMIT_ORDER = "handleCommitOrder";
	static final String SUBMIT_ORDER_OPERATION_NAME= "SUBMIT ORDER";
	
	
	
	
	
}
