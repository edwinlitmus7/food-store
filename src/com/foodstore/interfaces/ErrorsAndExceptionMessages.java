package com.foodstore.interfaces;

/***
 * String constants for errors and exceptions.
 * 
 * @author akhil
 *
 */
public interface ErrorsAndExceptionMessages {

	public static final String NO_EMAIL_INFO_FOR_CONFIRMATION="No email info for confirmation!";
	public final String  NULL_POINTER_EXCEPTION_MESSAGE = "Unknown error occured";
	public final String COULDNOT_ADD_SHIPPING_ADDRESS_TO_PROFILE= "couldNotAddShippingAddressToProfile";
	public static final String UNKNOWN_STATE_CODE = "unknown";
	public static final String USER_NOT_FOUND=" The user was not found please do register.";
	public static final String INVALID_ACCESS_TOKEN=" Invalid access token supplied ";
	public static final String INVALID_TOKEN=" Invalid token supplied .";
	public static final String ERROR_UPDATING_PROFILE="errorUpdatingProfile";
	public static final String ERROR_SENDING_EMAIL_CONFIRM="errorSendingEmailConfirm";
	public static final String NO_PAYMENTGROUP_OR_ADDRESS="Could not find payment Group or address";
	public static final String ERROR_INVALIDATION="errorInValidation";
	public static final String ERROR_VALIDATING_CREDITCARD="Error in validating credit Card ";
	public static final String NO_VALID_NICKNAME = "Could not find a valid nick name";
	public static final String NO_SEARCHRESULTS = "Sorry! No results found.";
	public static final String RESOURCEMAP_KEY = "resourceMapKey";
	public static final String ERRORMAP_KEY = "errorMapKey";
	public static final String NICKNAME_MISSING = "NicknameMissing";
	public static final String BILL_NICKNAME_MISSING = "BillNickNameMissing";
	public static final String TYPE_OF_ADDRESS_MISSING = "TypeOfAddressMissing";
	public static final String BILL_TYPE_OF_ADDRESS_MISSING = "BillTypeOfAddressMissing";
	public static final String MOBILE_MISSING = "MobileMissing";
	public static final String BILL_MOBILE_MISSING = "BillMobileMissing";
	public static final String INVALID_COUNTRY_STATE_COMBINATION = "InvalidCountryStateCombination";
	public static final String SHIP_INVALID_COUNTRY_STATE_COMBINATION = "ShipInvalidCountryStateCombination";
	public static final String NICKNAME_INVALID = "NicknameInvalid";
	public static final String BILL_NICKNAME_INVALID = "BillNickNameInvalid";
	public static final String TYPE_OF_ADDRESS_INVALID = "TypeOfAddressInvalid";
	public static final String BILL_TYPE_OF_ADDRESS_INVALID = "BillTypeOfAddressInvalid";
	public static final String MOBILE_INVALID = "MobileInvalid";
	public static final String BILL_MOBILE_INVALID = "BillMobileInvalid";
	public static final String COUNTRY_STATE_COMBINATION_INVALID = "The country state combination is invalid";
	public static final String SHIP_NICKNAME_MISSING = "ShipNickNameMissing";
	public static final String SHIP_TYPE_OF_ADDRESS_MISSING = "ShipTypeOfAddressMissing";
	public static final String SHIP_MOBILE_MISSING = "ShipMobileMissing";
	public static final String SHIP_NICKNAME_INVALID = "ShipNickNameInvalid";
	public static final String SHIP_TYPE_OF_ADDRESS_INVALID= "ShipTypeOfAddressInvalid";
	public static final String SHIP_MOBILE_INVALID = "ShipMobileInvalid";
	public static final String NO_CREDITCARD_IN_CONTAINER = "couldNotFindCreditCardInContainer";
	public static final String COULDNOT_ADD_SHIPPING_ADDRESS_IN_PROFILE = "couldNotUpdateShippingAddressInProfile";
	public static final String MSG_DUPLICATE_ADDRESS_NICKNAME = "Error Duplicate Nickname Please change the Nickname.";
	public static final String MSG_ERR_CREATING_ADDRESS = "Error in Creating Address.";
	public static final String MSG_ERR_UPDATING_ADDRESS = "Error in Updating Address.";
	public static final String MSG_STATE_IS_INCORRECT = "Error State is Incorrect";
}