package com.foodstore.interfaces;

public interface ParameterConstants {

	public static final String CURRENCYCODE_PARAMETER = "CurrencyCode";
	public static final String RELATEDPRODUCTS_PARAMETER = "relatedProducts";
	public static final String OUTPUT_PARAMETER = "output";
	public static final String ELEMENT_PARAMETER = "element";
	public static final String VALUE_PARAMETER = "value";
	public final String PROFILE = "profile";
	public final String ORDER = "order";
	public final String REMOVE_CREDITCARD_SUCCESSURL = "removeCreditCardSuccessURL";
	public final String REMOVE_CREDITCARD_ERRORURL = "removeCreditCardErrorURL";
	public final String REMOVE_CREDITCARD_NICKNAME = "removeCreditCardNickname";
	public final String REMOVE_SHIPPING_ADDRESS_ERRORURL = "removeShippingAddressErrorURL";
	public final String REMOVE_SHIPPING_ADDRESS_SUCCESSURL = "removeShippingAddressSuccessURL";
	public final String REMOVE_SHIPPING_ADDRESS_NICKNAME = "removeShippingAddressNickname";
	public final String HANDLE_LOGIN = "HANDLE_LOGIN";


}
