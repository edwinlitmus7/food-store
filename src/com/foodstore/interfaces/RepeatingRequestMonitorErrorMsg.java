package com.foodstore.interfaces;

/***
 * String constant used in repeat request monitor.
 * 
 * @author akhil
 *
 */
public interface RepeatingRequestMonitorErrorMsg {

	static final String REPEAT_REQUEST_ERROR = "Repeating Request Error.";
}
