package com.foodstore.interfaces;

/***
 * String constants used repository related operations.
 * 
 * @author akhil
 *
 */
public interface RepositoryConstants {

	public static final String PRODUCT_VIEW="product";
	public static final String ID="id";
	public static final String CHILDSKUS_PROPERTY_VALUE="childSKUs";
	public static final String TIME_QUERY="startTimeMinute<=?0  AND endTimeMinute>?0";
	public static final String CATEGORY_VIEW="category";
	public static final String CHILDPRODUCTS_PROPERTY_VALUE="childProducts";
	public static final String FIXEDCHILDCATEGORIES_PROPERTY_VALUE="fixedChildCategories";
	public static final String KEYWORDS_PROPERTY_VALUE="keywords";
	public static final String SKU_VIEW="sku";
	public static final String LISTPRICE_PROPERTY="listPrice";
	public static final String PARENTPRODUCTS_PROPERTY="parentProducts";
	public static final String FIXEDRELATEDPRODUCTS_PROPERTY_VALUE="fixedRelatedProducts";
	public static final String SKUS_OBJECT_PARAMETER="skus";
	public static final String INDEX_PARAMETER="index";
	public static final String EMAIL_PROPERTY="email";
	
}
