package com.foodstore.interfaces;

import atg.commerce.order.PropertyNameConstants;

public class FoodStorePropertyNameConstants extends PropertyNameConstants{

	public static final String PARENT_ITEM = "parentItem";
}
