package com.foodstore.profile.beans;

import atg.core.util.ContactInfo;
import atg.repository.RepositoryItem;

/***
 * Additional properties added to contactInfo object.
 * 
 * @author akhil
 *
 */
public class FoodStoreContactInfo extends ContactInfo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String nickname = null;
	private String typeOfAddress = null;
	private String mobile = null;
	
	public String getNickname() {
		return nickname;
	}
	
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	
	public String getTypeOfAddress() {
		return typeOfAddress;
	}
	
	public void setTypeOfAddress(String typeOfAddress) {
		this.typeOfAddress = typeOfAddress;
	}
	
	public String getMobile() {
		return mobile;
	}
	
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	
}
