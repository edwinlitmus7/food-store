package com.foodstore.profile.beans;

import java.beans.IntrospectionException;
import java.util.Map;

import com.foodstore.interfaces.PropertyConstants;
import com.foodstore.interfaces.RepositoryConstants;

import atg.beans.DynamicBeanInfo;
import atg.beans.DynamicBeans;
import atg.beans.PropertyNotFoundException;
import atg.core.util.Address;
import atg.repository.RepositoryItem;
import atg.userprofiling.address.AddressTools;

/***
 * Provided a nick name in address as a customization
 * We need to reflect that change in address object
 *  
 * @author akhil
 *
 */
public class FoodStoreAddressTools extends AddressTools {

	/**
	 * Copying address from a map to Address object
	 * 
	 * @param pAddressMap
	 * @param pAddressClassName
	 * @return
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws ClassNotFoundException
	 * @throws IntrospectionException
	 */
	public static Address createAddressFromMap(Map pAddressMap,
			String pAddressClassName) throws InstantiationException,
	IllegalAccessException, ClassNotFoundException,
	IntrospectionException {
		FoodStoreContactInfo address = (FoodStoreContactInfo) Class.forName(pAddressClassName)
				.newInstance();
		copyObject(pAddressMap, address, null);
		return address;
	}

	/**
	 * Copying Address details from address object to another
	 * @param pSrcAddress
	 * @param pDestAddress
	 * @throws IntrospectionException
	 */
	public static void copyAddressIgnoreId(Address pSrcAddress, Address pDestAddress)
			throws IntrospectionException
	{
		String props[] = DynamicBeans.getBeanInfo(pSrcAddress).getPropertyNames();
		DynamicBeanInfo destBeanInfo = DynamicBeans.getBeanInfo(pDestAddress);
		for(int i = 0; i < props.length; i++)
		{
			if(!destBeanInfo.hasProperty(props[i]) || props[i].equalsIgnoreCase(RepositoryConstants.ID))
				continue;
			try
			{
				Object value = DynamicBeans.getPropertyValue(pSrcAddress, props[i]);
				DynamicBeans.setPropertyValue(pDestAddress, props[i], value);
			}
			catch(PropertyNotFoundException e) { }
		}

	}
}
