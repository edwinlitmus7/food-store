package com.foodstore.profile;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.Dictionary;
import javax.servlet.ServletException;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import java.beans.IntrospectionException;
import javax.transaction.TransactionManager;
import atg.dtm.TransactionDemarcation;
import atg.dtm.TransactionDemarcationException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.TreeMap;

import atg.core.i18n.LayeredResourceBundle;
import atg.core.i18n.LocaleUtils;
import atg.commerce.CommerceException;
import atg.commerce.order.Order;
import atg.commerce.profile.CommerceProfileFormHandler;
import atg.commerce.profile.CommercePropertyManager;
import atg.commerce.util.RepeatingRequestMonitor;
import com.foodstore.catalog.FoodStore;
import com.foodstore.interfaces.HandlerNames;
import com.foodstore.interfaces.ParameterConstants;
import com.foodstore.interfaces.PropertyConstants;
import com.foodstore.interfaces.RepeatingRequestMonitorErrorMsg;
import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.interfaces.GoogleAuthenticationConstants;
import com.foodstore.order.FoodStoreOrderTools;
import com.foodstore.profile.beans.FoodStoreAddressTools;
import com.foodstore.profile.beans.FoodStoreContactInfo;

import atg.droplet.DropletException;
import atg.droplet.DropletFormException;
import atg.repository.Repository;
import com.foodstore.profile.FoodStoreProfileTools;
import com.foodstore.profile.FoodStorePropertyManager;
import com.payment.BasicFoodStoreCreditCardInfo;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.methodParamPartsMappingType;

import atg.beans.PropertyNotFoundException;
import atg.core.util.Address;
import atg.core.util.StringUtils;
import atg.payment.creditcard.ExtendableCreditCardTools;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.service.configurationreporter.RepresentationComparatorOutput;

import atg.service.perfmonitor.PerfStackMismatchException;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.RequestLocale;
import atg.userprofiling.Profile;
import atg.userprofiling.ProfileTools;
import atg.userprofiling.PropertyManager;


/***
 * Handle methods for Profile management
 * 
 * @author akhil
 *
 */
public class FoodStoreProfileFormHandler extends CommerceProfileFormHandler {


	private RepeatingRequestMonitor repeatingRequestMonitor;

	// Data member to store the token
	private String token;
	private FoodStore foodStore;

	public String getToken() {
		return token;
	}

	public RepeatingRequestMonitor getRepeatingRequestMonitor() {
		return repeatingRequestMonitor;
	}


	public void setRepeatingRequestMonitor(RepeatingRequestMonitor repeatingRequestMonitor) {
		this.repeatingRequestMonitor = repeatingRequestMonitor;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public FoodStore getFoodStore() {
		return foodStore;
	}

	public void setFoodStore(FoodStore foodStore) {
		this.foodStore = foodStore;
	}

	/**
	 * Login with FoodStore username and password (Without Google login)
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @throws ServletException
	 * @throws IOException
	 */
	public void handleNormalLogin(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = getTransactionDemarcation();
		     try {
		       if (tm != null) { td.begin(tm, 3);
		       }
		if(isLoggingDebug())
			logDebug("Executing handleNormalLogin method");

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreProfileFormHandler.handleNormalLogin";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.HANDLE_NORMAL_LOGIN, pRequest.getServletPath());
			try {

				//calls the handle login in super class.
				super.handleLogin(pRequest, pResponse);
			}finally {
				try {
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_NORMAL_LOGIN, pRequest.getServletPath());
						perfCancelled = true;
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
	}
    catch (TransactionDemarcationException e) {
    throw new ServletException(e);
  } finally {
 try {
  if (tm != null) { td.end();
   }
  }
  catch (TransactionDemarcationException e) {}
}
	}

	/*
	 * (non-Javadoc)
	 * @see atg.userprofiling.ProfileForm#userAlreadyExists(java.lang.String, atg.repository.Repository, atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@Override
	protected boolean userAlreadyExists(String pPossibleLogin,
			Repository pProfileRepository, DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws RepositoryException,
	ServletException, IOException {
		RepositoryItem user = getProfileTools().getItemFromEmail(pPossibleLogin);
		if (user == null) {
			vlogInfo(" user with login " + pPossibleLogin + " does not exists ");

			if(isLoggingDebug())
				logDebug("user with login " + pPossibleLogin + " does not exists");

			return false;
		}
		vlogInfo(" user with login " + pPossibleLogin + " exists ");

		if(isLoggingDebug())
			logDebug("user with login" + pPossibleLogin + "exists");

		return true;
	}

	/*
	 * @param token The access token received from google. 
	 * Extracts the user data from the token string after validating it. 
	 */
	private GoogleIdToken getGoogleIdToken(String token) throws GeneralSecurityException, IOException {

		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
		GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder( httpTransport,jsonFactory)
				.setAudience(Collections.singletonList(GoogleAuthenticationConstants.GOOGLE_PRIVATE_KEY))
				.build();
		return verifier.verify(token); // verifies the token and returns the googleIdToken object
	}

	/* Registers a new atg user. Some of the required data is extracted from the access token.
	 * (non-Javadoc)
	 * @see atg.userprofiling.ProfileFormHandler#handleCreate(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean handleCreate(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreProfileFormHandler.handleCreate";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.HANDLE_CREATE, pRequest.getServletPath());
			try {

				if(isLoggingDebug()) {
					logDebug(" Request to create new user ., ie invoking handleCreate()  ");
				}
				vlogInfo(" Request to create new user ., ie invoking handleCreate()  ");
				String token = getToken(); // gets the token  
				try {
					GoogleIdToken idToken = getGoogleIdToken(token);
					if (idToken != null) {
						Payload payload = idToken.getPayload(); //null check for payload
						if(payload == null){
							addFormException(new DropletException(GoogleAuthenticationConstants.UNKNOWN_ERROR_MSG));
							return false;
						}
						String email = (payload).getEmail(); 
						PropertyManager pManager = getProfileTools().getPropertyManager();
						Dictionary<String, Object> valueDictionary = getValue();
						valueDictionary.put(pManager.getLoginPropertyName(), email); // Sets the login because data supplied by the client cannot be trusted.
						valueDictionary.put(pManager.getEmailAddressPropertyName(), email); // Sets the email

						if(isLoggingDebug())
							logDebug("A valid user email obtained from the token .");

						vlogInfo("A valid user email obtained from the token .");

					} else {
						vlogWarning(GoogleAuthenticationConstants.INVALID_ACCESS_TOKEN);
						addFormException(new DropletException(GoogleAuthenticationConstants.INVALID_ACCESS_TOKEN));
					}
				} catch (GeneralSecurityException e) {
					e.printStackTrace();
					addFormException(new DropletException(GoogleAuthenticationConstants.GENERAL_SECURITY_ERROR_MSG));
				}

			}finally {
				try {
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_CREATE, pRequest.getServletPath());
						perfCancelled = true;
					}               
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}

				} catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		return super.handleCreate(pRequest, pResponse);

	}

	/* Uses the supplied token to validate login request and logins the user to the system if the token is valid .
	 * (non-Javadoc)
	 * @see atg.userprofiling.ProfileForm#handleLogin(atg.servlet.DynamoHttpServletRequest, atg.servlet.DynamoHttpServletResponse)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean handleLogin(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		TransactionManager tm = getTransactionManager();
		TransactionDemarcation td = getTransactionDemarcation();
		     try {
		       if (tm != null) { td.begin(tm, 3);
		       }
		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreProfileFormHandler.handleLogin";
		int status = 0;		
		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {
			vlogInfo("Request to login received.");
			if(isLoggingDebug())
				logDebug("Request to login received.");
			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.HANDLE_LOGIN, pRequest.getServletPath());
			ProfileTools ptools;
			String email = null;
			String token = getToken();
			ptools = getProfileTools();
			PropertyManager pmgr = ptools.getPropertyManager();
			String loginPropertyName = pmgr.getLoginPropertyName();
			try {

				GoogleIdToken idToken = getGoogleIdToken(token); // verifies the token
				if (idToken != null) {
					Payload payload = idToken.getPayload(); // check id payload is null payload 
					// Get profile information from payload

					email = ( payload).getEmail();
					String familyName = (String) payload.get(GoogleAuthenticationConstants.PAYLOAD_LAST_NAME);
					String firstName = (String) payload.get(GoogleAuthenticationConstants.PAYLOAD_FIRST_NAME);
					if(email == null){
						addFormException(new DropletException(GoogleAuthenticationConstants.INVALID_EMAIL_MSG));
						return false;
					}
					if(email != null && isTrimProperty(loginPropertyName)){
						email = email.trim();
					}
					// no need to query the repository

					if(getProfileTools().getItemFromEmail(email) == null) { // check if user with email exists
						/* User not exists so prepare for registration */
						if(isLoggingDebug())
							logDebug(" user with email " + email + " as login donot exists");

						vlogDebug(" user with email " + email + " as login donot exists");
						Dictionary<String, Object> valueDictionary = getValue();
						// The below dictionary values are set to auto populate the registration form
						valueDictionary.put(pmgr.getFirstNamePropertyName(),firstName);
						valueDictionary.put(pmgr.getLastNamePropertyName(),familyName);
						valueDictionary.put(pmgr.getEmailAddressPropertyName(),email);
						valueDictionary.put(pmgr.getLoginPropertyName(),email);
						Enumeration<String> e = valueDictionary.keys();
						while(e.hasMoreElements()) {
							String k = e.nextElement();
							vlogDebug(valueDictionary.get(k)+"");
						}
						addFormException(new DropletException(ErrorsAndExceptionMessages.USER_NOT_FOUND));

					} 

				} else {
					if(isLoggingDebug())
						logDebug(" Invalid token supplied");
					vlogWarning(" Invalid token supplied ");
					addFormException(new DropletException(ErrorsAndExceptionMessages.INVALID_ACCESS_TOKEN));
					setLoginErrorURL(GoogleAuthenticationConstants.LOGIN_ERROR_PAGE); // change the error url to error page
				}
			} catch (GeneralSecurityException e) {
				addFormException(new DropletException(ErrorsAndExceptionMessages.INVALID_TOKEN));

				setLoginErrorURL(GoogleAuthenticationConstants.LOGIN_ERROR_PAGE); // change the error url to error page 
			}
			preLoginUser(pRequest, pResponse);
			status = checkFormError(getLoginErrorURL(), pRequest, pResponse);
			if(status == 2) { // failure
				pRequest.setParameter(ParameterConstants.HANDLE_LOGIN, HANDLE_FAILURE);

			} else if(status == 0) { //success 

				try
				{	
					RepositoryItem authenticatedUser = getProfileTools().getItemFromEmail(email); 
					String passwordPropertyName = pmgr.getPasswordPropertyName();
					String password = getStringValueProperty(passwordPropertyName);
					if(!ptools.upgradePassword(authenticatedUser, email, password)){
						vlogDebug("No need for password upgradation");
						if(isLoggingDebug())
							logDebug("No need for password upgradation");
					}	
					RepositoryItem guest = getProfile();
					/* The properties of anonymous profile is copied to non transient profile of user. */
					copyPropertiesOnLogin(guest, authenticatedUser);
					addPropertiesOnLogin(guest, authenticatedUser);
					setRepositoryId(authenticatedUser.getRepositoryId());
					pRequest.setParameter(ParameterConstants.HANDLE_LOGIN, HANDLE_SUCCESS);

					vlogInfo(" Login success ");
					if(isLoggingDebug())
						logDebug( "Login success");

					checkFormSuccess(getLoginSuccessURL(), pRequest, pResponse);
					if(getAuthenticationMessageTrigger() != null)
						getAuthenticationMessageTrigger().sendAuthenticationSuccessMessage(email, pRequest.getRemoteAddr());

				}
				catch(RepositoryException exc)
				{
					vlogError("Repository Exception ", exc);
					addFormException(new DropletException(formatUserMessage(ErrorsAndExceptionMessages.ERROR_UPDATING_PROFILE, pRequest), exc, ErrorsAndExceptionMessages.ERROR_UPDATING_PROFILE));

				}
				finally {
					try {
						if (!perfCancelled) {
							PerformanceMonitor.endOperation(HandlerNames.HANDLE_LOGIN, pRequest.getServletPath());
							perfCancelled = true;
						}
						if (rrm != null) {
							rrm.removeRequestEntry(myHandleMethod);
						}
					} catch (PerfStackMismatchException e2) {
						if (isLoggingWarning())
							logWarning(e2);
					}
				}

			}
			postLoginUser(pRequest, pResponse);
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}

		return status == 1;
		     }
		          catch (TransactionDemarcationException e) {
		          throw new ServletException(e);
		        } finally {
		       try {
		        if (tm != null) { td.end();
		         }
		        }
		        catch (TransactionDemarcationException e) {}
		      }
	}

	/**
	 * Post login user method
	 */
	@Override
	protected void postLoginUser(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException,
	IOException {
		// TODO Auto-generated method stub
		super.postLoginUser(pRequest, pResponse);
		String schedule = getFoodStore().getSchedule();

		Order order = getShoppingCart().getCurrent();
		((FoodStoreOrderTools) getOrderManager().getOrderTools()).removeUnscheduledCommerceItems(order, schedule);
	}


	// Resource bundle.
	protected static final String RESOURCE_BUNDLE = "atg.commerce.profile.UserMessages";
	private String nicknameValueMapKey = PropertyConstants.NICKNAME_PROPERTY;
	private String oldNicknameValueMapKey = PropertyConstants.OLD_NICKNAME_PROPERTY;
	private Properties countryMap = null;
	private Properties stateMap = null;
	private String newAddressErrorURL;
	private String newAddressSuccessURL;
	private String updateAddressErrorURL;
	private String updateAddressSuccessURL;
	private String removeAddressSuccessURL;
	private String removeAddressErrorURL;
	private String newAddressId;
	private String removeAddressType;
	private boolean useShippingAddressAsDefault;

	/* Map to store the shipping address details */
	private Map<String, Object> mEditValue = new HashMap<String, Object>();


	public String getNicknameValueMapKey() {
		return nicknameValueMapKey;
	}

	public void setNicknameValueMapKey(String nicknameValueMapKey) {
		this.nicknameValueMapKey = nicknameValueMapKey;
	}

	public String getOldNicknameValueMapKey() {
		return oldNicknameValueMapKey;
	}

	public void setOldNicknameValueMapKey(String oldNicknameValueMapKey) {
		this.oldNicknameValueMapKey = oldNicknameValueMapKey;
	}

	protected FoodStorePropertyManager getFoodStorePropertiesManager(){
		return (FoodStorePropertyManager) getProfileTools().getPropertyManager();
	}

	public String getNewAddressErrorURL() {
		return newAddressErrorURL;
	}

	public void setNewAddressErrorURL(String newAddressErrorURL) {
		this.newAddressErrorURL = newAddressErrorURL;
	}

	public String getNewAddressSuccessURL() {
		return newAddressSuccessURL;
	}

	public String getUpdateAddressErrorURL() {
		return updateAddressErrorURL;
	}

	public void setUpdateAddressErrorURL(String updateAddressErrorURL) {
		this.updateAddressErrorURL = updateAddressErrorURL;
	}

	public void setNewAddressSuccessURL(String newAddressSuccessURL) {
		this.newAddressSuccessURL = newAddressSuccessURL;
	}

	public String getNewAddressId() {
		return newAddressId;
	}

	public void setNewAddressId(String newAddressId) {
		this.newAddressId = newAddressId;
	}

	public boolean isUseShippingAddressAsDefault() {
		return useShippingAddressAsDefault;
	}

	public String getRemoveAddressType() {
		return removeAddressType;
	}

	public void setRemoveAddressType(String removeAddressType) {
		this.removeAddressType = removeAddressType;
	}

	public void setUseShippingAddressAsDefault(boolean useShippingAddressAsDefault) {
		this.useShippingAddressAsDefault = useShippingAddressAsDefault;
	}

	public String getUpdateAddressSuccessURL() {
		return updateAddressSuccessURL;
	}

	public void setUpdateAddressSuccessURL(String updateAddressSuccessURL) {
		this.updateAddressSuccessURL = updateAddressSuccessURL;
	}

	public String getRemoveAddressSuccessURL() {
		return removeAddressSuccessURL;
	}

	public void setRemoveAddressSuccessURL(String removeAddressSuccessURL) {
		this.removeAddressSuccessURL = removeAddressSuccessURL;
	}

	public String getRemoveAddressErrorURL() {
		return removeAddressErrorURL;
	}

	public void setRemoveAddressErrorURL(String removeAddressErrorURL) {
		this.removeAddressErrorURL = removeAddressErrorURL;
	}

	public Properties getCountryMap() {
		return countryMap;
	}

	public void setCountryMap(Properties countryMap) {
		this.countryMap = countryMap;
	}

	public Properties getStateMap() {
		return stateMap;
	}

	public void setStateMap(Properties stateMap) {
		this.stateMap = stateMap;
	}


	/**
	 * Creates a new shipping address using the entries entered in the editValue
	 * map. The address will be indexed using the nickname provided by the user.
	 * 
	 * @param pRequest The current HTTP request
	 * @param pResponse The current HTTP response
	 * @return boolean returns true/false for success
	 * @throws ServletException if there was an error while executing the code
	 * @throws IOException if there was an error with servlet io
	 * @throws CommerceException if there was an error with Commerce tools
	 */
	public boolean handleNewAddress(DynamoHttpServletRequest pRequest, 
			DynamoHttpServletResponse pResponse) throws ServletException, IOException, CommerceException {

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreProfileFormHandler.handleNewAddress";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {

			if(isLoggingDebug())
				logDebug("Entering handleNewAddress method");

			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.HANDLE_NEWADDRESS, pRequest.getServletPath());

			// Validate address data entered by user, if its invalid go to the error URL
			if(!validateAddress(pRequest, pResponse)){
				return checkFormRedirect(getNewAddressSuccessURL(), getNewAddressErrorURL(), pRequest, pResponse);

			}

			// Get the current user Profile and the ProfileTools bean and the values
			// entered into the new address form by the user.
			Profile profile = getProfile();
			FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
			Map<String, Object> newAddress = getEditValue();

			// Generate unique nickname if it is not provided by the user
			String nickname = (String) newAddress.get(getNicknameValueMapKey());

			if(isLoggingDebug())
				logDebug("Shipping address nickname : "+nickname);

			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();

			try{
				if (tm != null) {

					td.begin(tm, TransactionDemarcation.REQUIRED);
				}

				try {

					// Create an Address object from the values the user entered.
					Address addressObject = 
							FoodStoreAddressTools.createAddressFromMap(newAddress, profileTools.getShippingAddressClassName());

					//Create an entry in the secondaryAddress map on the profile, for this
					// new address. Set this new Id as the newAddressId so it can be picked
					// up on the success page.
					String newAddressId = 
							profileTools.createProfileRepositorySecondaryAddress(profile, nickname, addressObject);

					if (newAddressId != null) {
						setNewAddressId(newAddressId);
					}

					// Check to see Profile.shippingAddress is null, if it is,
					// add the new address as the default shipping address
					if (isUseShippingAddressAsDefault()) {
						profileTools.setDefaultShippingAddress(profile, nickname);
					}


					// empty out the map
					//newAddress.clear();

				}
				catch (RepositoryException repositoryExc) {
					addFormException(ErrorsAndExceptionMessages.MSG_ERR_CREATING_ADDRESS, new String[] { nickname },

							repositoryExc, pRequest);

					if (isLoggingError()) {
						logError(repositoryExc);
					}

				}
				catch (IntrospectionException ex) {
					throw new ServletException(ex);
				} 
				catch (InstantiationException e) {
					throw new ServletException(e);
				}
				catch (IllegalAccessException e) {
					throw new ServletException(e);
				}
				catch (ClassNotFoundException e) {
					throw new ServletException(e);
				}

				// Success, redirect to the success URL
				return checkFormRedirect(getNewAddressSuccessURL(), 
						getNewAddressErrorURL(), pRequest, pResponse);
			}
			catch (TransactionDemarcationException e) {

				throw new ServletException(e);
			}
			finally {
				try {
					if (tm != null) {

						td.end(); // ending transaction.
					}
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_NEWADDRESS, pRequest.getServletPath());
						perfCancelled = true;
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				} 
				catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						logError("Can't end transaction ", e);
					}
				}catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}
		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		// Failure, redirect to the error URL
		return checkFormRedirect(null, getNewAddressErrorURL(), pRequest, pResponse);
	}

	/**
	 * Update the secondary address as modified by the user.
	 * 
	 * @param pRequest
	 *            the servlet's request
	 * @param pResponse
	 *            the servlet's response
	 * @return true for successful address update, false - otherwise
	 * @throws ServletException
	 *                if there was an error while executing the code
	 * @throws IOException
	 *                if there was an error with servlet io
	 * @throws RepositoryException
	 *                if there was an error accessing the repository
	 */
	public boolean handleUpdateAddress(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse) throws ServletException, IOException, RepositoryException{

		RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
		String myHandleMethod = "FoodStoreProfileFormHandler.handleUpdateAddress";

		if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {

			if(isLoggingDebug())
				logDebug("Entering handleUpdateAddress method");

			boolean perfCancelled = false;
			PerformanceMonitor.startOperation(HandlerNames.HANDLE_UPDATE_ADDRESS, pRequest.getServletPath());

			//Validate address data entered by user
			if (!validateAddress(pRequest, pResponse)) {
				return checkFormRedirect(null, getUpdateAddressErrorURL(), pRequest, pResponse);
			}

			TransactionManager tm = getTransactionManager();
			TransactionDemarcation td = getTransactionDemarcation();
			FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();

			try{
				if (tm != null) {
					td.begin(tm, TransactionDemarcation.REQUIRED);
				}
				Profile profile = getProfile();

				Map<String, Object> edit = getEditValue();
				String oldNickname = (String) edit.get(getOldNicknameValueMapKey());
				String nickname = (String) edit.get(getNicknameValueMapKey());

				if(isLoggingDebug()) {

					logDebug("Previous address nickname : " + oldNickname);
					logDebug("Newly entered address nickname  " + nickname);
				}

				try{				

					//Populate Address object data entered by user
					Address contactInfoObject = FoodStoreAddressTools.createAddressFromMap(edit, 
							profileTools.getShippingAddressClassName());

					//Get address repository item to be updated
					RepositoryItem oldAddress = profileTools.getProfileAddress(getProfile(), oldNickname);

					// Update address repository item 
					profileTools.updateProfileRepositoryAddress(oldAddress, contactInfoObject);


					// Check if nickname should be changed
					if (!StringUtils.isBlank(nickname) && !nickname.equals(oldNickname)) {
						profileTools.changeSecondaryAddressName(profile, oldNickname, nickname);
					}
					// If the user marks the default address checkbox true. 

					if(isUseShippingAddressAsDefault()){
						((FoodStoreProfileTools) getProfileTools()).setDefaultShippingAddress(profile, nickname);
					}
					else if (nickname.equalsIgnoreCase(profileTools.getDefaultShippingAddressNickname(profile))) {
						profileTools.setDefaultShippingAddress(profile, null);
					}

				}
				catch (RepositoryException e) {
					addFormException(ErrorsAndExceptionMessages.MSG_ERR_UPDATING_ADDRESS, new String[] { nickname }, e, pRequest);


					return checkFormRedirect(null, getUpdateAddressErrorURL(), pRequest, pResponse);
				} 
				catch (InstantiationException ex) {
					throw new ServletException(ex);
				} catch (IllegalAccessException ex) {
					throw new ServletException(ex);
				} catch (ClassNotFoundException ex) {
					throw new ServletException(ex);
				} catch (IntrospectionException ex) {
					throw new ServletException(ex);
				}
				edit.clear();
			}
			catch (TransactionDemarcationException e) {
				throw new ServletException(e);
			}
			finally {
				try {
					if (tm != null) {
						td.end();
					}
					if (!perfCancelled) {
						PerformanceMonitor.endOperation(HandlerNames.HANDLE_UPDATE_ADDRESS, pRequest.getServletPath());
						perfCancelled = true;
					}
					if (rrm != null) {
						rrm.removeRequestEntry(myHandleMethod);
					}
				} catch (TransactionDemarcationException e) {
					if (isLoggingError()) {
						logError("Can't end transaction ", e);
					}
				}
				catch (PerfStackMismatchException e2) {
					if (isLoggingWarning())
						logWarning(e2);
				}
			}
		}

		else {
			addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
		}
		return checkFormRedirect(getUpdateAddressSuccessURL(), getUpdateAddressErrorURL(), pRequest, pResponse);
	}

	// Error creating credit card message key.
	protected static final String MSG_ERR_CREATING_CC = "errorCreatingCreditCard";

	// Error removing credit card message key.
	protected static final String MSG_ERR_REMOVING_CC = "errorRemovingCreditCard";

	// Error while updating credit card message key.
	protected static final String MSG_ERR_UPDATING_CREDIT_CARD = "errorUpdatingCreditCard";

	// Missing credit card property message key.
	protected static final String MSG_MISSING_CC_PROPERTY = "missingCreditCardProperty";

	// Duplicate CC nickname message key.
	protected static final String MSG_DUPLICATE_CC_NICKNAME = "errorDuplicateCCNickname";

	// Invalid credit card message key.
	protected static final String MSG_INVALID_CC = "errorInvalidCreditCard";


	// Used in exception messages
	protected static final String EDIT_VALUE = "editValue";

	// Map to store billing address details
	Map<String, String> billingAddress = new HashMap<>();

	private String addEditMode;
	private String creditCardNumberInputType;	
	private String successURL;
	private String errorURL;

	private String shippingAddressNicknameMapKey = "shippingAddrNickname";
	private String newNicknameValueMapKey = "newNickname";


	private Map<String, Object> editValue = new HashMap<>();
	private String createCardSuccessURL;
	private String createCardErrorURL;

	private String removeCard;
	private String removeCardSuccessURL;

	private String updateCardSuccessURL;
	private String updateCardErrorURL;

	private String[] immutableCardProperties = new String[] {"creditCardNumber", "creditCardType"};

	public String[] getImmutableCardProperties() {
		return immutableCardProperties;
	}

	public void setImmutableCardProperties(String[] immutableCardProperties) {
		this.immutableCardProperties = immutableCardProperties;
	}

	public String getAddEditMode() {
		return addEditMode;
	}

	public void setAddEditMode(String addEditMode) {
		this.addEditMode = addEditMode;
	}

	public String getCreditCardNumberInputType() {
		return creditCardNumberInputType;
	}

	public void setCreditCardNumberInputType(String creditCardNumberInputType) {
		this.creditCardNumberInputType = creditCardNumberInputType;
	}

	public String getSuccessURL() {
		return successURL;
	}

	public void setSuccessURL(String successURL) {
		this.successURL = successURL;
	}

	public String getErrorURL() {
		return errorURL;
	}

	public void setErrorURL(String errorURL) {
		this.errorURL = errorURL;
	}

	public String getNewNicknameValueMapKey() {
		return newNicknameValueMapKey;
	}

	public void setNewNicknameValueMapKey(String newNicknameValueMapKey) {
		this.newNicknameValueMapKey = newNicknameValueMapKey;
	}

	public String getRemoveCard() {
		return removeCard;
	}

	public void setRemoveCard(String removeCard) {
		this.removeCard = removeCard;
	}

	public String getRemoveCardSuccessURL() {
		return removeCardSuccessURL;
	}

	public void setRemoveCardSuccessURL(String removeCardSuccessURL) {
		this.removeCardSuccessURL = removeCardSuccessURL;
	}

	public String getRemoveCardErrorURL() {
		return removeCardErrorURL;
	}

	public void setRemoveCardErrorURL(String removeCardErrorURL) {
		this.removeCardErrorURL = removeCardErrorURL;
	}

	private String removeCardErrorURL;

	public String getUpdateCardSuccessURL() {
		return updateCardSuccessURL;
	}

	public void setUpdateCardSuccessURL(String updateCardSuccessURL) {
		this.updateCardSuccessURL = updateCardSuccessURL;
	}

	public String getUpdateCardErrorURL() {
		return updateCardErrorURL;
	}

	public void setUpdateCardErrorURL(String updateCardErrorURL) {
		this.updateCardErrorURL = updateCardErrorURL;
	}

	public String getCreateCardSuccessURL() {
		return createCardSuccessURL;
	}

	public void setCreateCardSuccessURL(String createCardSuccessURL) {
		this.createCardSuccessURL = createCardSuccessURL;
	}

	public String getCreateCardErrorURL() {
		return createCardErrorURL;
	}

	public void setCreateCardErrorURL(String createCardErrorURL) {
		this.createCardErrorURL = createCardErrorURL;
	}

	public String getShippingAddressNicknameMapKey() {
		return shippingAddressNicknameMapKey;
	}

	public void setShippingAddressNicknameMapKey(
			String shippingAddressNicknameMapKey) {
		this.shippingAddressNicknameMapKey = shippingAddressNicknameMapKey;
	}

	public Map<String, Object> getEditValue() {
		return editValue;
	}

	public void setEditValue(Map<String, Object> editValue) {
		this.editValue = editValue;
	}

	public Map<String, String> getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(Map<String, String> billingAddress) {
		this.billingAddress = billingAddress;
	}

	/**
	 * Before get method for editvalue map.
	 */
	@Override
	public void beforeGet(DynamoHttpServletRequest request,
			DynamoHttpServletResponse response) {
		String mode = request.getParameter("addEditMode");

		if(mode != null && mode.equals("edit")) {
			String cardNickname = request.getParameter("nickname");
			logWarning("\n\n " + cardNickname + editValue.get(getNewNicknameValueMapKey()) + " " + editValue.get(getNicknameValueMapKey()));
			setAddEditMode(mode);
			preFillUpdateCardForm(cardNickname);
			// Adding card nickname to map
			editValue.put(getNicknameValueMapKey(), cardNickname);
			editValue.put(getNewNicknameValueMapKey(), cardNickname);
		}
	}


	/**
	 * For filling the credit card and billing address when
	 * edit mode is enabled  
	 * 
	 * @param cardNickname
	 */
	private void preFillUpdateCardForm(String cardNickname) {
		FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
		FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();

		if(isLoggingDebug()) {
			logDebug("Entering preFillUpdateCardForm method" );
			logDebug("Credit card details with nickname : " + cardNickname);
		}
		RepositoryItem creditCard = profileTools.getCreditCardByNickname(cardNickname, getProfile());
		if(creditCard != null) {
			RepositoryItem billAddrs = (RepositoryItem) creditCard.getPropertyValue(propertyManager.getBillingAddressPropertyName());
			editValue = profileTools.getMapFromCreditCardItem(creditCard);
			billingAddress = profileTools.getMapFromAddressItem(billAddrs, propertyManager.getBillingAddressPropertyName());
		} else {
			vlogError(" Invalid nickname : {0}", cardNickname);
			if(isLoggingDebug())
				logDebug(" Invalid nickname : " + cardNickname);
		}

	}

	/**
	 * Determine the user's current locale, if available.
	 *
	 * @param pRequest
	 *            DynamoHttpServletRequest
	 * @return Locale Request's Locale
	 */
	protected Locale getLocale(DynamoHttpServletRequest pRequest) {
		RequestLocale reqLocale = pRequest.getRequestLocale();

		if (reqLocale == null) {
			reqLocale = getRequestLocale();
		}

		if(reqLocale == null){
			return null;
		}
		else {
			return reqLocale.getLocale();
		}
	}

	private ExtendableCreditCardTools creditCardTools;

	/**
	 * @return ExtendableCreditCardTools
	 */
	public ExtendableCreditCardTools getCreditCardTools() {
		return creditCardTools;
	}

	/**
	 * @param pCreditCardTools new ExtendableCreditCardTools
	 */
	public void setCreditCardTools(ExtendableCreditCardTools pCreditCardTools) {
		creditCardTools = pCreditCardTools;
	}

	/**
	 * Validates the credit card information using CreditCardTools.
	 * {@inheritDoc}
	 * @see CreditCardTools#verifyCreditCard(CreditCardInfo)
	 */
	protected boolean validateCreditCard(Map pCard, ResourceBundle pBundle) {

		if(isLoggingDebug())
			logDebug("Entering validateCreditCard method");
		FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
		FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();

		ExtendableCreditCardTools cardTools = getCreditCardTools();

		BasicFoodStoreCreditCardInfo ccInfo = new BasicFoodStoreCreditCardInfo();
		ccInfo.setExpirationYear((String) pCard.get(propertyManager.getCreditCardExpirationYearPropertyName()));
		ccInfo.setExpirationMonth((String) pCard.get(propertyManager.getCreditCardExpirationMonthPropertyName()));

		String ccNumber = (String) pCard.get(propertyManager.getCreditCardNumberPropertyName());

		if (ccNumber != null) {
			ccNumber = StringUtils.removeWhiteSpace(ccNumber);
		}

		ccInfo.setCreditCardNumber(ccNumber);
		ccInfo.setCreditCardType((String) pCard.get(propertyManager.getCreditCardTypePropertyName()));

		if(isLoggingDebug())
			logDebug("Credit card number : " + ccNumber);

		int ccreturn = cardTools.verifyCreditCard(ccInfo);

		if (ccreturn != cardTools.SUCCESS) {
			addCreditCardFormException(pBundle, ccreturn);
			return false;
		}

		return true;
	}


	/**
	 * For validating credit card informations.
	 * 
	 * @param pRequest
	 * @param pResponse
	 * @param pIsNewAddress
	 * @return
	 * @throws ServletException
	 * @throws IOException
	 */
	protected boolean validateCreateCreditCardInformation(DynamoHttpServletRequest pRequest,
			DynamoHttpServletResponse pResponse, 
			boolean pIsNewAddress)
					throws ServletException, IOException {

		if(isLoggingDebug())
			logDebug("Entering validateCreateCreditCardInformation method");

		// return false if there were missing required properties
		if (getFormError()) {
			return false;
		}

		HashMap newCard = (HashMap) getEditValue();
		HashMap newAddress = (HashMap) getBillingAddress();

		//validate credit card fields
		validateCreditCardFields(pRequest, pResponse);

		if (getFormError()) {
			return false;
		}

		// Verify that card number and expiration date are valid
		ResourceBundle bundle = LayeredResourceBundle.getBundle(RESOURCE_BUNDLE, getLocale(pRequest));
		if (!validateCreditCard(newCard, bundle)) {
			return false;
		}

		// Check that the nickname is not already used for a credit card
		FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
		FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();
		if(isLoggingDebug())
			logDebug("Card number : " + newCard.get("creditCardNumber"));
		Profile profile = getProfile();
		String cardNickname = (String) newCard.get(propertyManager.getCreditCardNicknamePropertyName());
		if (profileTools.isDuplicateCreditCardNickname(profile, cardNickname)) {
			addFormException(MSG_DUPLICATE_CC_NICKNAME, new String[] { cardNickname },
					getAbsoluteName() + EDIT_VALUE + ".creditCardNickname", pRequest);
			return false;
		}
		return true;
	}


	private String[] cardProperties = new String[] { "creditCardNumber",
			"creditCardType", "expirationMonth", "expirationYear", "billingAddress" };;

			public String[] getCardProperties() {
				return cardProperties;
			}

			public void setCardProperties(String[] cardProperties) {
				this.cardProperties = cardProperties;
			}

			/**
			 * Validates that all required credit card's fields are entered by user
			 * 
			 * @param pRequest http request
			 * @param pResponse http response
			 * @return true if all required fields are entered
			 */
			protected boolean validateCreditCardFields(DynamoHttpServletRequest pRequest,
					DynamoHttpServletResponse pResponse) {

				if(isLoggingDebug())
					logDebug("Entering validateCreditCardFields methods");

				Map newCard = getEditValue();
				String[] cardProps = getCardProperties();
				FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
				FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();

				boolean missingFields = false;
				Object property = null;
				String propertyName = null;

				// Verify all required fields entered for credit card
				for (int i = 0; i < cardProps.length; i++) {
					propertyName = cardProps[i];

					//not check here billingAddress Property
					if (propertyName.equals(propertyManager.getCreditCardBillingAddressPropertyName())){
						continue;
					}

					if(isLoggingDebug())
						logDebug("Credit card feilds : " + propertyName);

					property = newCard.get(propertyName);

					if (StringUtils.isBlank((String) property)) {
						ResourceBundle bundle = LayeredResourceBundle.getBundle(RESOURCE_BUNDLE, getLocale(pRequest));
						String[] args = new String[]{bundle.getString(propertyName)};
						addFormException(MSG_MISSING_CC_PROPERTY, args, getAbsoluteName(), pRequest);
						missingFields = true;
					}
				}
				return !missingFields;
			}

			/**
			 * Adds form exception with the message according to the specified parameters 
			 * 
			 * @param pBundle Resource bundle
			 * @param pReturnCode The code returned from the validateCreditCard method
			 */
			protected void addCreditCardFormException(ResourceBundle pBundle, 
					int pReturnCode) {

				if(isLoggingDebug())
					logDebug("Entering addCreditCardFormException method");

				ExtendableCreditCardTools cardTools = getCreditCardTools();

				FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
				FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();

				String msg = null;
				if (pBundle != null && pBundle.getLocale() != null) { 
					msg = cardTools.getStatusCodeMessage(pReturnCode, pBundle.getLocale());
				}
				else {
					msg = cardTools.getStatusCodeMessage(pReturnCode);
				}

				StringBuilder path = new StringBuilder(getAbsoluteName());
				path.append(".").append(EDIT_VALUE).append(".");

				// Add the credit card property that failed validation
				switch (pReturnCode) {
				case ExtendableCreditCardTools.CARD_TYPE_NOT_VALID :
					path.append(propertyManager.getCreditCardTypePropertyName());
					break;

				case ExtendableCreditCardTools.CARD_NUMBER_HAS_INVALID_CHARS :
				case ExtendableCreditCardTools.CARD_NUMBER_DOESNT_MATCH_TYPE :
				case ExtendableCreditCardTools.CARD_LENGTH_NOT_VALID :
				case ExtendableCreditCardTools.CARD_NUMBER_NOT_VALID :
					path.append(propertyManager.getCreditCardNumberPropertyName());
					break;

				case ExtendableCreditCardTools.CARD_EXPIRED :
				case ExtendableCreditCardTools.CARD_EXP_DATE_NOT_VALID :
					path.append(propertyManager.getCreditCardExpirationMonthPropertyName());
					break;

				default:
					vlogDebug("Unrecognized return type from validateCreditCard");
				}

				addFormException(new DropletFormException(msg, path.toString(), MSG_INVALID_CC));
			}


			/**
			 * Handle method for creating new credit card and address information.
			 * 
			 * @param request
			 * @param response
			 * @return
			 * @throws ServletException
			 * @throws IOException
			 */
			public boolean handleCreateNewCreditCardAndAddress(DynamoHttpServletRequest request,
					DynamoHttpServletResponse response) throws ServletException, IOException {

				RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
				String myHandleMethod = "FoodStoreProfileFormHandler.handleCreateNewCreditCardAndAddress";


				if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {

					if(isLoggingDebug())
						logDebug("Entering handleCreateNewCreditCardAndAddress method");

					boolean perfCancelled = false;
					PerformanceMonitor.startOperation(HandlerNames.HANDLE_CREATE_NEW_CREDITCARD_AND_ADDRESS, request.getServletPath());

					TransactionManager tm = getTransactionManager();
					TransactionDemarcation td = getTransactionDemarcation();

					try {
						FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();

						preCreateNewCreditCardAndAddress(request, response);


						preCreateNewCreditCardAndAddress(request, response);

						// validate credit card information
						if (!validateCreateCreditCardInformation(request, response, true)) {
							return checkFormRedirect(null, getCreateCardErrorURL(), request, response);
						}

						FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();
						Map newCard = getEditValue();
						Map newAddress = getBillingAddress();

						String secondaryAddrNickname = (String) newAddress.get(getShippingAddressNicknameMapKey());

						if(isLoggingDebug())
							logDebug("Secondary Address nickname : " + secondaryAddrNickname);

						// Get credit card's nickname
						String cardNickname = (String) newCard.get(getNicknameValueMapKey());

						if(isLoggingDebug())
							logDebug("Credit card nickname : " + cardNickname);

						if (tm != null) {
							td.begin(tm, TransactionDemarcation.REQUIRED);
						}

						Profile profile = getProfile();
						logError("\n Logged in profile : " + profile + "\n");
						profileTools.createProfileCreditCard(profile, newCard, cardNickname, newAddress, secondaryAddrNickname, true);
						String newCreditCard = (String) newCard.get(propertyManager.getNewCreditCard());

						if(!StringUtils.isEmpty(newCreditCard) && newCreditCard.equalsIgnoreCase("true")) {
							profileTools.setDefaultCreditCard(profile, cardNickname);
						}

						newCard.clear();
						newAddress.clear();
					} catch (InstantiationException e) {

						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					} catch (RepositoryException e) {
						addFormException(new DropletFormException("", e, MSG_ERR_CREATING_CC));	
						e.printStackTrace();
					} catch (IntrospectionException e) {

						e.printStackTrace();
					} catch (PropertyNotFoundException e) {
						e.printStackTrace();
					} catch (TransactionDemarcationException e) {
						e.printStackTrace();
						throw new ServletException();
					} finally {
						try {
							if (tm != null) {
								td.end();
							}
							if (!perfCancelled) {
								PerformanceMonitor.endOperation(HandlerNames.HANDLE_CREATE_NEW_CREDITCARD_AND_ADDRESS, request.getServletPath());
								perfCancelled = true;
							}
							if (rrm != null) {
								rrm.removeRequestEntry(myHandleMethod);
							}

						} catch (TransactionDemarcationException e) {
							if (isLoggingError()) {
								logError("Can't end transaction ", e);
							}
						}
						catch (PerfStackMismatchException e2) {
							if (isLoggingWarning())
								logWarning(e2);
						}
					}
					postCreateNewCreditCardAndAddress(request, response);
					logError("\n\n " + getCreateCardSuccessURL() + " ^^ " + getCreateCardErrorURL() + "\n");
				}
				else {
					addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
				}
				return checkFormRedirect(getCreateCardSuccessURL(), getCreateCardErrorURL(), request,
						response);
			}

			protected void preCreateNewCreditCardAndAddress(
					DynamoHttpServletRequest request, DynamoHttpServletResponse response) {

			}

			protected void postCreateNewCreditCardAndAddress(
					DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
				editValue = null;
				billingAddress = null;
			}

			/**
			 * Validates new address fields entered by user:
			 * <ul>
			 * <li>all required fields are specified for new address
			 * <li>country/state combination is valid for new address
			 * <li>not duplicate address nickname is used for create address or update
			 *      address operation
			 * </ul>
			 * @param pRequest http request
			 * @param pResponse http response
			 * @return true is validation succeeded
			 * @exception ServletException if there was an error while executing the code
			 * @exception IOException if there was an error with servlet io
			 */
			protected boolean validateAddress(DynamoHttpServletRequest pRequest,
					DynamoHttpServletResponse pResponse) {

				if(isLoggingDebug())
					logDebug("Entering validateAddress method");

				// return false if there were missing required properties
				if (getFormError()) {

					return false;
				}

				HashMap<String, Object> newAddress = (HashMap<String, Object>) getEditValue();
				validateCountryStateCombination(newAddress, pRequest, pResponse);

				if (getFormError()) {

					return false;
				}


				// Validate address nickname or new nickname if it's  update address operation
				String oldNickname = (String) newAddress.get(getOldNicknameValueMapKey());
				String nickname = (String) newAddress.get(getNicknameValueMapKey());

				if (!StringUtils.isBlank(nickname)) {
					// Editing an address, in this case we want to allow them to change the casing of the nickname
					// so we remove the original nickname from the duplicate check by adding it to the ignore list.
					// We still need to perform the check incase they change it to another name thats in the list. 

					if (!nickname.equals(oldNickname)) {
						List<String> ignore = new ArrayList<>();
						ignore.add(oldNickname);
						boolean duplicateNickname = 
								((FoodStoreProfileTools)getProfileTools()).isDuplicateAddressNickname(getProfile(), nickname, ignore);

						if (duplicateNickname) {
							addFormException(ErrorsAndExceptionMessages.MSG_DUPLICATE_ADDRESS_NICKNAME, new String[] { nickname },
									getAbsoluteName(), pRequest);
						}
					}

				}
				else {
					//It's new address so validate nickname against all nicknames
					if (!StringUtils.isBlank(oldNickname)) {

						boolean duplicateNickname = 
								((FoodStoreProfileTools)getProfileTools()).isDuplicateAddressNickName(getProfile(), oldNickname);

						if (duplicateNickname) {
							addFormException(ErrorsAndExceptionMessages.MSG_DUPLICATE_ADDRESS_NICKNAME, new String[] { oldNickname },
									getAbsoluteName(), pRequest);
						}
					}

				}

				if (isLoggingDebug()) {

					logDebug("Previous address nickname : " + oldNickname);
					logDebug("Current address nickname : " + nickname);
				}
				if (getFormError()) {
					return false;
				}

				//all validation passed successfully so return true
				return true;
			}

			/**
			 * This handler deletes a secondary address named in the removeAddress
			 * property. 
			 *
			 * @param pRequest
			 *            the servlet's request
			 * @param pResponse
			 *            the servlet's response
			 * @return boolean true/false for success
			 * @exception ServletException
			 *                if there was an error while executing the code
			 * @exception IOException
			 *                if there was an error with servlet io
			 */
			public boolean handleRemoveAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) 
					throws ServletException,IOException, RepositoryException {

				RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
				String myHandleMethod = "FoodStoreProfileFormHandler.handleRemoveAddress";

				if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {

					if(isLoggingDebug())
						logDebug("Handle Remove Address method started");

					boolean perfCancelled = false;
					PerformanceMonitor.startOperation(HandlerNames.HANDLE_REMOVE_ADDRESS, pRequest.getServletPath());

					preRemoveAddress(pRequest, pResponse);

					// Stop execution if we have form errors
					if (getFormError()) {
						return true;
					}

					TransactionManager tm = getTransactionManager();
					TransactionDemarcation td = getTransactionDemarcation();
					FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();

					try {
						if (tm != null) {
							td.begin(tm, TransactionDemarcation.REQUIRED);
						}

						// Get nickaname of the address to be removed
						String nickname = pRequest.getParameter("removeAddressType");

						if(isLoggingDebug())
							logDebug("Address removed : " + nickname );

						if ((nickname == null) || (nickname.trim().length() == 0)) {
							if (isLoggingDebug()) {
								logDebug("A null or empty nickname was provided to handleRemoveAddress");
							}

							// if no nickname provided, do nothing.
							return true;
						}

						Profile profile = getProfile();

						// Remove the Address from the Profile
						RepositoryItem purgeAddress = profileTools.getProfileAddress(profile, nickname);

						if(purgeAddress != null){
							profileTools.removeProfileRepositoryAddress(profile, nickname, true);
						}    

						return checkFormRedirect(pRequest.getParameter("successURl"), pRequest.getParameter("successURl"), pRequest, pResponse);

					} catch (TransactionDemarcationException e) {
						throw new ServletException(e);
					} catch (RepositoryException repositoryExc) {
						if (isLoggingError()){
							logError("Repository Exception");
						}
					} finally {
						try {
							if (tm != null) {
								td.end();
							}
							if (!perfCancelled) {
								PerformanceMonitor.endOperation(HandlerNames.HANDLE_REMOVE_ADDRESS, pRequest.getServletPath());
								perfCancelled = true;
							}
							if(rrm != null) {
								rrm.removeRequestEntry(myHandleMethod);
							}
						} catch (TransactionDemarcationException e) {
							if (isLoggingError()) {
								logError("Can't end transaction ", e);
							}
						}
						catch (PerfStackMismatchException e2) {
							if (isLoggingWarning())
								logWarning(e2);
						}
					}
				}
				else {
					addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
				}
				return false;
			}

			/**
			 * Validates creditcard information
			 * @param pRequest
			 * @param pResponse
			 * @return
			 * @throws ServletException
			 * @throws IOException
			 */
			protected boolean validateUpdateCreditCardInformation(DynamoHttpServletRequest pRequest,
					DynamoHttpServletResponse pResponse)
							throws ServletException, IOException {

				if(isLoggingDebug())
					logDebug("Entering validateUpdateCreditCardInformation method");

				// return false if there were missing required properties
				if (getFormError()) {
					return false;
				}

				Map card = (HashMap) getEditValue();
				Map billingAddress = (HashMap) getBillingAddress();
				String nickname = ((String) card.get(getNicknameValueMapKey())).trim();

				if(isLoggingDebug())
					logDebug("Updating credit card nickname : " + nickname);

				if (getFormError()) {
					return false;
				}
				FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
				Profile profile = getProfile();
				FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();

				// Verify that card expiry date is valid
				RepositoryItem cardToUpdate = profileTools.getCreditCardByNickname(nickname, profile);

				// only expiry date is needed to be validated because credit card type and number
				// are not updated. But we need to set credit card type and number to map because 
				// they are needed for expiry date validation
				card.put(propertyManager.getCreditCardNumberPropertyName(),
						cardToUpdate.getPropertyValue(propertyManager.getCreditCardNumberPropertyName()));
				card.put(propertyManager.getCreditCardTypePropertyName(),
						cardToUpdate.getPropertyValue(propertyManager.getCreditCardTypePropertyName()));

				ResourceBundle bundle = LayeredResourceBundle.getBundle(RESOURCE_BUNDLE, getLocale(pRequest));
				if (!validateCreditCard(card, bundle)) {
					return false;
				}

				// Check that the new  nickname is not already used for a credit card
				String newNickname = ((String) card.get(getNewNicknameValueMapKey())).trim();
				if (!StringUtils.isBlank(newNickname) && !newNickname.equals(nickname)) {
					List ignore = new ArrayList();
					ignore.add(nickname);
					if (profileTools.isDuplicateCreditCardNickname(profile, newNickname, ignore)) {
						addFormException(MSG_DUPLICATE_CC_NICKNAME, new String[] { newNickname },
								getAbsoluteName() + EDIT_VALUE + ".creditCardNickname", pRequest);

						return false;
					}
				}

				return true;
			}

			/**
			 * Handle method for updating credit card information.
			 * 
			 * @param request
			 * @param response
			 * @return
			 * @throws RepositoryException
			 * @throws ServletException
			 * @throws IOException
			 */
			public boolean handleUpdateCard(DynamoHttpServletRequest request,
					DynamoHttpServletResponse response) throws RepositoryException,
			ServletException, IOException {

				RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
				String myHandleMethod = "FoodStoreProfileFormHandler.handleUpdateCard";

				if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {

					if(isLoggingDebug())
						logDebug("Entering handleUpdateCard method");

					boolean perfCancelled  = false;
					PerformanceMonitor.startOperation(HandlerNames.HANDLE_UPDATE_CARD, request.getServletPath());

					TransactionManager tm = getTransactionManager();
					TransactionDemarcation td = getTransactionDemarcation();
					preUpdateCard(request, response);

					// validate credit card information
					if (!validateUpdateCreditCardInformation(request, response)) {
						return checkFormRedirect(null, getUpdateCardErrorURL(), request, response);
					}

					Map edit = getEditValue();
					Map billAddrValue = getBillingAddress();

					String newNickName = (String) edit.get(getNewNicknameValueMapKey());

					FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
					FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) profileTools.getPropertyManager();

					Profile profile = getProfile();

					String cardNickName = (String) getEditValue().get(getNicknameValueMapKey());

					if(isLoggingDebug())
						logDebug("Nickname of the updating credit card : " + cardNickName);

					// Update credit card
					try {
						if (tm != null) {
							td.begin(tm, TransactionDemarcation.REQUIRED);
						}

						//Get credit card to update
						RepositoryItem cardToUpdate = profileTools.getCreditCardByNickname(cardNickName, profile);
						if(isLoggingDebug()) {
							logInfo("Card Nickname : " + cardNickName);
							logInfo("Card to update : " + cardToUpdate);
						}


						profileTools.updateProfileCreditCard(cardToUpdate, profile, edit, newNickName, billAddrValue, profileTools.getBillingAddressClassName());

						// Update credit card nickname case
						String nickName = profileTools.getCreditCardNickname(profile, cardToUpdate);
						if(!StringUtils.isBlank(newNickName) && !newNickName.equals(nickName) && newNickName.equalsIgnoreCase(nickName)) {
							profileTools.changeCreditCardNickname(profile, nickName, newNickName);
						}

						//save this card to default if needed
						String newCreditCard = (String) edit.get(propertyManager.getNewCreditCard());
						if(!StringUtils.isEmpty(newCreditCard) && newCreditCard.equalsIgnoreCase("true")) {
							profileTools.setDefaultCreditCard(profile, newNickName);
						} else if ("false".equalsIgnoreCase(newCreditCard)) {
							//current card should not be default
							RepositoryItem defaultCreditCard = profileTools.getDefaultCreditCard(profile);
							if (defaultCreditCard != null && cardToUpdate.getRepositoryId().equals(defaultCreditCard.getRepositoryId())) {
								//current card is default, make it not to be
								profileTools.updateProperty(propertyManager.getDefaultCreditCardPropertyName(), null, profile);
								//otherwise we shouldn't change anything more in the profile
							}
						}
					} catch (InstantiationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						vlogError("Exception occurred during updating credit card", e);
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IntrospectionException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (TransactionDemarcationException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						throw new ServletException();
					} finally {
						try {
							if (tm != null) {
								td.end();
							}
							if (rrm != null) {
								rrm.removeRequestEntry(myHandleMethod);
							}
						} catch (TransactionDemarcationException e) {
							if (isLoggingError()) {
								logError("Unable to end transaction ", e);
							}
							if (!perfCancelled) {
								PerformanceMonitor.endOperation(HandlerNames.HANDLE_UPDATE_CARD, request.getServletPath());
								perfCancelled = true;
							}
						}
						catch (PerfStackMismatchException e2) {
							if (isLoggingWarning())
								logWarning(e2);
						}
					}
					postUpdateCard(request, response);
				}
				else {
					addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
				}
				return checkFormRedirect(getUpdateCardSuccessURL(), getUpdateCardErrorURL(), request, response);
			}


			/**
			 * Address that we'd like to remove could be associated
			 * with a gift list. In this case, we'd like to prevent
			 * address removal from the profile.
			 * 
			 * Also on billing page shouldn't be deleted address that 
			 * was chosen as shipping address.
			 * 
			 * @param pRequest Dynamo http request
			 * @param pResponse Dynamo http response
			 */
			public void preRemoveAddress(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) {

				// Get collection of the gift lists that belongs to the profile,
				// iterate over this collection and check addresses
				// If giftlist address is equal to the address we want to remove,
				// add form exceprion
				String addressNickname = pRequest.getParameter("removeAddressType");

				Map<String, RepositoryItem> profileAddresses =
						(Map<String, RepositoryItem>) getProfile().getPropertyValue(getFoodStorePropertiesManager().getSecondaryAddressPropertyName());
				RepositoryItem profileAddress = (RepositoryItem) profileAddresses.get(addressNickname);

				if (profileAddress == null && !getProfile().isTransient()) { //Already removed? Do nothing then.
					return;
				}

			}

			/**
			 * Validate country-state combination.
			 *
			 * @param pAddress - address
			 * @param pRequest - http address
			 * @param pResponse - http response
			 */
			protected void validateCountryStateCombination(Map pAddress, DynamoHttpServletRequest pRequest,
					DynamoHttpServletResponse pResponse) {
				if (pAddress != null) {
					CommercePropertyManager propertyManager = getFoodStorePropertiesManager();
					validateCountryStateCombination((String)pAddress.get(propertyManager.getAddressCountryPropertyName()),
							(String)pAddress.get(propertyManager.getAddressStatePropertyName()), pRequest, pResponse);
				}
			}

			/**
			 * Validate country-state combination.
			 * @param pCountry country code
			 * @param pState county code
			 * @param pRequest dynamo request
			 * @param pResponse dynamo response
			 */
			protected void validateCountryStateCombination(String pCountry, String pState, DynamoHttpServletRequest pRequest,
					DynamoHttpServletResponse pResponse) {
				FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
				String country = "";
				String state = "";
				if(getCountryMap().containsKey(pCountry)){
					country = getCountryMap().getProperty(pCountry);
					if (getStateMap().containsKey(pState)) {
						state = getStateMap().getProperty(pState);
					}
				}
				if (!profileTools.isValidCountryStateCombination(country, state)) {
					Object[] args = new Object[] {LocaleUtils.constructLocale(PropertyConstants.COUNTRY_LOCALE+ country).getDisplayCountry(getRequestLocale().getLocale())};
					addFormException(ErrorsAndExceptionMessages.MSG_STATE_IS_INCORRECT, args, getAbsoluteName(), pRequest);      
				}
			}


			/**
			 * Create a form exception, by looking up the exception code in a resource
			 * file identified by the RESOURCE_BUNDLE constant (defined above).
			 *
			 * @param pWhatException
			 *            String description of exception
			 * @param pRepositoryExc
			 *            RepositoryException
			 * @param pRequest
			 *            DynamoHttpServletRequest
			 */
			protected void addFormException(String pWhatException, RepositoryException pRepositoryExc,
					DynamoHttpServletRequest pRequest) {
				addFormException(pWhatException,null,pRepositoryExc,pRequest);
			}

			/**
			 * Create a form exception, by looking up the exception code in a resource
			 * file identified by the RESOURCE_BUNDLE constant (defined above).
			 *
			 * @param pWhatException
			 *            String description of exception
			 * @param pArgs
			 *            String array with arguments used message formatting
			 * @param pRepositoryExc
			 *            RepositoryException
			 * @param pRequest
			 *            DynamoHttpServletRequest
			 */
			protected void addFormException(String pWhatException, Object[] pArgs, RepositoryException pRepositoryExc,
					DynamoHttpServletRequest pRequest) {   

				String errorStr = pWhatException;
				if (pArgs != null && pArgs.length > 0){
					errorStr = (new MessageFormat(errorStr)).format(pArgs);
				}
				addFormException(new DropletFormException(errorStr, pRepositoryExc, pWhatException));
			}

			/**
			 * Creates a form exception, by looking up the exception code in a resource
			 * file identified by the RESOURCE_BUNDLE constant (defined above).
			 *
			 * @param pWhatException
			 *            String description of exception
			 * @param pArgs
			 *            String array with arguments used message formatting
			 * @param pPath
			 *            Full path to form handler property associated with the exception
			 * @param pRequest
			 *            DynamoHttpServletRequest
			 */
			protected void addFormException(String pWhatException, Object[] pArgs, 
					String pPath, DynamoHttpServletRequest pRequest) {
				String errorStr = pWhatException;

				if (pArgs != null && pArgs.length > 0){
					errorStr = (new MessageFormat(errorStr)).format(pArgs);
				}

				addFormException(new DropletFormException(errorStr, pPath, pWhatException));
			}

			/**
			 * Post method for handle update credit card
			 * 
			 * @param request
			 * @param response
			 */
			private void postUpdateCard(
					DynamoHttpServletRequest request, DynamoHttpServletResponse response) {

				editValue.clear();
				billingAddress.clear();
			}

			/**
			 * Pre method for handle update credit card
			 */
			protected void preUpdateCard(
					DynamoHttpServletRequest request, DynamoHttpServletResponse response) {
				RepositoryItem cardToUpdate = findCurrentCreditCard();
				if(cardToUpdate != null) {
					for (String propertyName: getImmutableCardProperties()) {
						getEditValue().put(propertyName, cardToUpdate.getPropertyValue(propertyName));
					}
				}
			}

			/**
			 * Handle method for removing credit card.
			 * 
			 * @param request
			 * @param response
			 * @return
			 * @throws ServletException
			 * @throws IOException
			 */
			public boolean handleRemoveCreditCard(DynamoHttpServletRequest request,
					DynamoHttpServletResponse response) throws ServletException,
			IOException {

				RepeatingRequestMonitor rrm = getRepeatingRequestMonitor();
				String myHandleMethod = "FoodStoreProfileFormHandler.handleRemoveCreditCard";

				if((rrm == null) || (rrm.isUniqueRequestEntry(myHandleMethod))) {

					if(isLoggingDebug())
						logDebug("Entering handleRemoveCreditCard method");

					boolean perfCancelled = false;
					PerformanceMonitor.startOperation(HandlerNames.HANDLE_REMOVE_CREDIT_CARD, request.getServletPath());

					try {
						preRemoveCard(request, response);

						FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
						String cardNickName = getRemoveCard();

						if(isLoggingDebug())
							logDebug("Card to remove : " + cardNickName);

						if (StringUtils.isBlank(cardNickName)) {
							if (isLoggingDebug()) {
								logDebug("A null or empty nickname was provided to handleRemoveAddress");
							}

							// if no nickname provided, do nothing.
							return true;
						}

						Profile profile = getProfile();
						try {
							profileTools.removeProfileCreditCard(profile, cardNickName);
						} catch (RepositoryException e) {	
							addFormException(new DropletFormException("", e, MSG_ERR_REMOVING_CC));
							e.printStackTrace();
						}

						postRemoveCard(request, response);
					}
					finally {
						try {
							if (!perfCancelled) {
								PerformanceMonitor.endOperation(HandlerNames.HANDLE_REMOVE_CREDIT_CARD, request.getServletPath());
								perfCancelled = true;
							}
							if (rrm != null) {
								rrm.removeRequestEntry(myHandleMethod);
							}
						} catch (PerfStackMismatchException e2) {
							if (isLoggingWarning())
								logWarning(e2);
						}
					}
				}
				else {
					addFormException(new DropletException(RepeatingRequestMonitorErrorMsg.REPEAT_REQUEST_ERROR));
				}
				return checkFormRedirect(getRemoveCardSuccessURL(), getRemoveCardErrorURL(), request, response);
			}

			private void postRemoveCard(DynamoHttpServletRequest request,
					DynamoHttpServletResponse response) {

			}

			private void preRemoveCard(DynamoHttpServletRequest request,
					DynamoHttpServletResponse response) {

			}

			/**
			 * Searches current user's credit card by nick-name from editValue properties.
			 * @return credit card if found else null
			 */
			protected RepositoryItem findCurrentCreditCard() {
				FoodStoreProfileTools profileTools = (FoodStoreProfileTools) getProfileTools();
				String cardNickname = (String) getEditValue().get(getNicknameValueMapKey());
				RepositoryItem cardToUpdate = profileTools.getCreditCardByNickname(cardNickname, getProfile());
				return cardToUpdate;
			}	

			/**
			 * Creates a form exception, by looking up the exception code in a resource
			 * file identified by the RESOURCE_BUNDLE constant (defined above).
			 *
			 * @param pWhatException
			 *            String description of exception 
			 * @param pPath
			 *            Full path to form handler property associated with the exception
			 * @param pRequest
			 *            DynamoHttpServletRequest
			 */
			protected void addFormException(String pWhatException, 
					String pPath, DynamoHttpServletRequest pRequest) {
				addFormException(pWhatException, null, pPath, pRequest);
			}

}