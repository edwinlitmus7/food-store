package com.foodstore.profile;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import atg.commerce.profile.CommerceProfileTools;
import atg.commerce.profile.CommercePropertyManager;
import atg.commerce.util.PlaceUtils;
import atg.core.i18n.PlaceList.Place;
import atg.core.util.StringUtils;

import java.beans.IntrospectionException;

import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.interfaces.PropertyConstants;

import atg.beans.PropertyNotFoundException;
import atg.core.util.Address;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryException;
import atg.userprofiling.Profile;
import atg.userprofiling.address.AddressTools;

/**
 * Methods that supports the profile handle methods.
 * 
 * @author akhil
 *
 */
public class FoodStoreProfileTools extends CommerceProfileTools {

	private String shippingAddressClassName = "com.foodstore.profile.beans.FoodStoreContactInfo";
	private PlaceUtils placeUtils;
	private CommercePropertyManager commercePropertyManager;

	public PlaceUtils getPlaceUtils() {
		return placeUtils;
	}

	public void setPlaceUtils(PlaceUtils placeUtils) {
		this.placeUtils = placeUtils;
	}

	public String getShippingAddressClassName() {
		return shippingAddressClassName;
	}

	public void setShippingAddressClassName(String shippingAddressClassName) {
		this.shippingAddressClassName = shippingAddressClassName;
	}

	public CommercePropertyManager getCommercePropertyManager() {
		return commercePropertyManager;
	}

	public void setCommercePropertyManager(
			CommercePropertyManager commercePropertyManager) {
		this.commercePropertyManager = commercePropertyManager;
	}

	/**
	 * Checking the provided address nickname is duplicate or not
	 */
	public boolean isDuplicateAddressNickName(RepositoryItem pProfile, String pNewNickName){
		return isDuplicateAddressNickname(pProfile, pNewNickName, null);
	}

	/**
	 * Checking the provided address nickname is duplicate or not
	 */
	public boolean isDuplicateAddressNickname(RepositoryItem pProfile, String pNickname, 
			List<String> pExcludedName)
	{
		String secondaryAddressPropertyName =  getCommercePropertyManager().getSecondaryAddressPropertyName();
		Map secondaryAddressMap = (Map) pProfile.getPropertyValue(secondaryAddressPropertyName);
		return checkForDuplicates(secondaryAddressMap, pNickname, pExcludedName);
	}

	/**
	 * Get the default shipping address nickname from the profile.
	 * 
	 * @param pProfile
	 * @return
	 */
	public String getDefaultShippingAddressNickname(RepositoryItem pProfile) {
		RepositoryItem defaultAddress = getDefaultShippingAddress(pProfile);    
		return defaultAddress != null? getProfileAddressName(pProfile, defaultAddress):null;
	}

	/**
	 * Checks the provided country state combination is valid or not
	 * 
	 * @param pCountry
	 * @param pState
	 * @return
	 */
	public boolean isValidCountryStateCombination(String pCountry, String pState) {
		if (StringUtils.isEmpty(pState) || ErrorsAndExceptionMessages.UNKNOWN_STATE_CODE.equals(pState)){
			//State code is empty. Make sure that specified country has no states. 
			Place[] placesForCountry = getPlaceUtils().getPlaces(pCountry);
			return ((placesForCountry == null || placesForCountry.length == 0 ));     
		}    
		return getPlaceUtils().isPlaceInCountry(pCountry, pState);
	}
	
	// Property name
		private static String billingAddressClassName = "com.foodstore.profile.beans.FoodStoreContactInfo";

		public String getBillingAddressClassName() {
			return billingAddressClassName;
		}

		public void setBillingAddressClassName(String billingAddressClassName) {
			this.billingAddressClassName = billingAddressClassName;
		}
		
		/**
		 * Creating credit associated with user profile.
		 * 
		 * @param pProfile
		 * @param pNewCreditCard
		 * @param pCreditCardNickname
		 * @param pBillingAddress
		 * @param pAddressNickname
		 * @param pIsNewAddress
		 * @return
		 * @throws InstantiationException
		 * @throws IllegalAccessException
		 * @throws ClassNotFoundException
		 * @throws IntrospectionException
		 * @throws RepositoryException
		 * @throws PropertyNotFoundException
		 */
		public String createProfileCreditCard(Profile pProfile, Map pNewCreditCard,
												String pCreditCardNickname, Map pBillingAddress,
												String pAddressNickname, boolean pIsNewAddress) throws InstantiationException, IllegalAccessException, ClassNotFoundException, IntrospectionException, RepositoryException, PropertyNotFoundException {
			
			if(isLoggingDebug())
				logDebug("Entering createProfileCreditCard method");
			
			//create new secondary address item or extract existed one
		    RepositoryItem secondaryAddress = null;
		    String addrNickname = pAddressNickname;

		    if (pIsNewAddress) {
		      //generate secondary address nickname if it's not provided
		      if (StringUtils.isBlank(addrNickname)) {
		        addrNickname = getUniqueShippingAddressNickname(pBillingAddress, pProfile, null);
		      }
		      if(isLoggingDebug())
		    	  logDebug("Profile credit card nickname : " + addrNickname);
		      
		      Address addressObject = AddressTools.createAddressFromMap(pBillingAddress, getShippingAddressClassName());
		      
		      createProfileRepositorySecondaryAddress(pProfile, addrNickname, addressObject);

		      // Check to see Profile.shippingAddress is null, if it is,
		      // add the new address as the default shipping address
		      setDefaultShippingAddressIfNull(pProfile, addrNickname);
		    }
		    secondaryAddress = getProfileAddress(pProfile, addrNickname);

		    return createProfileCreditCard(pProfile, pNewCreditCard, pCreditCardNickname, secondaryAddress);
		}

		/**
		 * Get credit card details from profile.
		 * 
		 * @param profile
		 * @return
		 */
		public Map getCreditCards(RepositoryItem profile) {
			FoodStorePropertyManager propertyManager = (FoodStorePropertyManager) getPropertyManager();
			return (Map) profile.getPropertyValue(propertyManager.getCreditCardPropertyName());
		}
		
		/**
		 * Checks whether the provided credit card nickname is duplicate or not. 
		 * 
		 * @param pProfile
		 * @param pNewNickname
		 * @param pExcludedNames
		 * @return
		 */
		public boolean isDuplicateCreditCardNickname(RepositoryItem pProfile, String pNewNickname, 
				List<String> pExcludedNames)  {
			String creditCardPropertyName = ((FoodStorePropertyManager)getPropertyManager()).getCreditCardPropertyName();
			Map creditCardMap = (Map) pProfile.getPropertyValue(creditCardPropertyName);
			return checkForDuplicates(creditCardMap, pNewNickname, pExcludedNames);
		}

		/**
		 * Checks whether the provided credit card nickname is duplicate or not.
		 * 
		 * @param mPropertyMap
		 * @param pKey
		 * @param pExcludedKeys
		 * @return
		 */
	protected boolean checkForDuplicates(Map mPropertyMap, String pKey, List<String> pExcludedKeys){
		//Fetch the keys (ie nick names)
		Collection secondaryAddressKeys = mPropertyMap.keySet();
		List<String> profileNames = new ArrayList<String>();
		Iterator<String> iterator = secondaryAddressKeys.iterator();
		while (iterator.hasNext()) {
			profileNames.add(iterator.next());
		}
		// Remove the names we want to ignore
		if(pExcludedKeys != null){
			profileNames.removeAll(pExcludedKeys);
		}
		// Check for duplicates
		for(String profileNickname : profileNames){
			if(profileNickname.equalsIgnoreCase(pKey)){
				return true;
			}
		}
		return false;
	}

	
	/**
	 * Change secondary address property name.
	 */
	public void changeSecondaryAddressName(RepositoryItem pProfile,
			String pOldAddressName, String pNewAddressName)
			throws RepositoryException {
		if ((StringUtils.isBlank(pNewAddressName))
				|| (pNewAddressName.equals(pOldAddressName))) {
			return;
		}

		CommercePropertyManager cpmgr = (CommercePropertyManager) getPropertyManager();
		Map secondaryAddresses = (Map) pProfile.getPropertyValue(cpmgr
				.getSecondaryAddressPropertyName());
		RepositoryItem address = getProfileAddress(pProfile, pOldAddressName);
		if (address != null) {
			secondaryAddresses.remove(pOldAddressName);
			secondaryAddresses.put(pNewAddressName, address);
			updateProperty(cpmgr.getSecondaryAddressPropertyName(),
					secondaryAddresses, pProfile);
		}
	}

	/**
	 * Get credit card details from profile.
	 * 
	 * @param creditCard
	 * @return
	 */
	public Map<String, Object> getMapFromCreditCardItem(RepositoryItem creditCard) {
		Map<String, Object> editValue = new HashMap<>();
		String[] creditCardProperties = ( (FoodStorePropertyManager) getPropertyManager()).getShallowCreditCardPropertyNames();
		for(String creditCardProperty: creditCardProperties) {
			logWarning("\n - " + creditCard.getPropertyValue(creditCardProperty));
			editValue.put(creditCardProperty, creditCard.getPropertyValue(creditCardProperty));
		}
		return editValue;
	}

	/**
	 * Get address map from addressitem.
	 * 
	 * @param billAddrs
	 * @param address
	 * @return
	 */
	public Map<String, String> getMapFromAddressItem(RepositoryItem billAddrs, String address) {
		Map<String, String> editValue = new HashMap<>();
		FoodStorePropertyManager propertyManager =  (FoodStorePropertyManager) getPropertyManager();
		
		String[] addressProperties = null;
		switch(address) {
			case PropertyConstants.BILLING_ADDRESS_PROPERTY:
				addressProperties = propertyManager.getBilllingAddressProperties();
				break;
		}
		
		for(String property: addressProperties) {
			logWarning("\n - " + billAddrs.getPropertyValue(property));
			editValue.put(property, (String) billAddrs.getPropertyValue(property));
		}
		return editValue;
	}
}
