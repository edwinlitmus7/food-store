package com.foodstore.profile;

import com.foodstore.interfaces.PropertyConstants;

import atg.commerce.profile.CommercePropertyManager;

/**
 * Setters and getters for properties associated with profile.
 * 
 * @author akhil
 *
 */
public class FoodStorePropertyManager extends CommercePropertyManager{

	/** Class version string. */
	//public static final String CLASS_VERSION = "$Id: //hosting-blueprint/B2CBlueprint/version/11.2/EStore/src/atg/projects/store/profile/StorePropertyManager.java#3 $$Change: 1174358 $";

	protected String mAddressNicknamePropertyName = PropertyConstants.NICKNAME_PROPERTY;

	public String getmAddressNicknamePropertyName() {
		return mAddressNicknamePropertyName;
	}

	public void setmAddressNicknamePropertyName(
			String mAddressNicknamePropertyName) {
		this.mAddressNicknamePropertyName = mAddressNicknamePropertyName;
	}

	protected String mMobilePropertyName = PropertyConstants.MOBILE_PROPERTY;

	public String getmMobilePropertyName() {
		return mMobilePropertyName;
	}

	public void setmMobilePropertyName(String mMobilePropertyName) {
		this.mMobilePropertyName = mMobilePropertyName;
	}

	protected String mTypeOfAddressPropertyName = PropertyConstants.TYPE_OF_ADDRESS_PROPERTY;

	public String getmTypeOfAddressPropertyName() {
		return mTypeOfAddressPropertyName;
	}

	public void setmTypeOfAddressPropertyName(String mTypeOfAddressPropertyName) {
		this.mTypeOfAddressPropertyName = mTypeOfAddressPropertyName;
	}
	
	
	
	protected String mGenderPropertyName = PropertyConstants.GENDER_PROPERTY;

	/**
	 * @return the mGenderPropertyName
	 */
	public String getGenderPropertyName() {
		return mGenderPropertyName;
	}

	/**
	 * @param pGenderPropertyName the genderPropertyName to set
	 */
	public void setGenderPropertyName(String pGenderPropertyName) {
		mGenderPropertyName = pGenderPropertyName;
	}
	
	
	protected String mRefferalSourcePropertyName = PropertyConstants.REFERRAL_SOURCE_PROPERTY;

	/**
	 * @return the mRefferalSourcePropertyName
	 */
	public String getRefferalSourcePropertyName() {
		return mRefferalSourcePropertyName;
	}

	/**
	 * @param pRefferalSourcePropertyName the refferalSourcePropertyName to set
	 */
	public void setRefferalSourcePropertyName(String pRefferalSourcePropertyName) {
		mRefferalSourcePropertyName = pRefferalSourcePropertyName;
	}
	
	/**
	 * Date of birth property name.
	 */
	String mDateOfBirthPropertyName = PropertyConstants.DATE_OF_BIRTH_PROPERTY;

	public void setDateOfBirthPropertyName(String pDateOfBirthPropertyName) {
		mDateOfBirthPropertyName = pDateOfBirthPropertyName;
	}

	/**
	 * @return date of birth property name.
	 */
	public String getDateOfBirthPropertyName() {
		return mDateOfBirthPropertyName;
	}

	private static String newCreditCard = PropertyConstants.NEW_CREDITCARD;
	private static String creditCardIdPropertyName = PropertyConstants.CREDITCARD_ID_PROPERTY;

	public String getCreditCardIdPropertyName() {
		return creditCardIdPropertyName;
	}

	public void setCreditCardIdPropertyName(String creditCardIdPropertyName) {
		this.creditCardIdPropertyName = creditCardIdPropertyName;
	}

	public String getNewCreditCard() {
		return newCreditCard;
	}

	public void setNewCreditCard(String newCreditCard) {
		this.newCreditCard = newCreditCard;
	}
	protected String mNameOnCardPropertyName = PropertyConstants.NAME_ON_CARD_PROPERTY;

	public String getmNameOnCardPropertyName() {
		return mNameOnCardPropertyName;
	}

	public void setmNameOnCardPropertyName(
			String mNameOnCardPropertyName) {
		this.mNameOnCardPropertyName = mNameOnCardPropertyName;
	}

}
