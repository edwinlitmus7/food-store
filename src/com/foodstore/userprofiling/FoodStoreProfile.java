package com.foodstore.userprofiling;

import atg.userprofiling.Profile;

/***
 * Provides getter and setter for schedule.
 * 
 * @author akhil
 *
 */
public class FoodStoreProfile extends Profile {
	private String schedule;

	public String getSchedule() {
		return schedule;
	}

	public void setSchedule(String schedule) {
		this.schedule = schedule;
	} 
}
