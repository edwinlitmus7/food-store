package com.foodstore.util;

import java.util.ArrayList;
import java.util.List;

import com.foodstore.interfaces.ErrorsAndExceptionMessages;
import com.foodstore.interfaces.RepositoryConstants;

import atg.commerce.order.Order;
import atg.core.util.ResourceUtils;
import atg.core.util.StringUtils;
import atg.nucleus.GenericService;
import atg.nucleus.ServiceMap;
import atg.repository.RepositoryItem;
import atg.userprofiling.email.TemplateEmailException;
import atg.userprofiling.email.TemplateEmailInfo;
import atg.userprofiling.email.TemplateEmailInfoImpl;
import atg.userprofiling.email.TemplateEmailSender;

/***
 * Class provides e-mail functionality, which sends an e-mail to the user 
 * when the order is successfully placed.
 * 
 * @author akhil
 *
 */
public class FoodStoreEmailTools extends GenericService {

	protected ServiceMap mConfirmationEmailMap;
	protected TemplateEmailSender templateEmailSender;
	protected boolean sendEmailInSeparateThread = false;
	protected boolean persistConfirmationEmails;

	public boolean isSendEmailInSeparateThread() {
		return sendEmailInSeparateThread;
	}

	public void setSendEmailInSeparateThread(boolean sendEmailInSeparateThread) {
		this.sendEmailInSeparateThread = sendEmailInSeparateThread;
	}

	public boolean isPersistConfirmationEmails() {
		return persistConfirmationEmails;
	}

	public void setPersistConfirmationEmails(boolean persistConfirmationEmails) {
		this.persistConfirmationEmails = persistConfirmationEmails;
	}

	public TemplateEmailSender getTemplateEmailSender() {
		return templateEmailSender;
	}

	public void setTemplateEmailSender(TemplateEmailSender templateEmailSender) {
		this.templateEmailSender = templateEmailSender;
	}

	public ServiceMap getConfirmationEmailMap()
	{
		return this.mConfirmationEmailMap;
	}

	public void setConfirmationEmailMap(ServiceMap pConfirmationEmailMap)
	{
		this.mConfirmationEmailMap = pConfirmationEmailMap;
	}

	/**
	 * Sending order confirmation e-mail to the user.
	 * 
	 * @param order
	 * @param profile
	 * @throws TemplateEmailException
	 */
	public void sendConfirmationEmail(Order order, RepositoryItem profile) throws TemplateEmailException {
		
		if(isLoggingDebug())
			logDebug("Inside sendConfirmationEmail method");
		
		//Getting e-mail address of the user.
		String toAddress = (String) profile.getPropertyValue(RepositoryConstants.EMAIL_PROPERTY);

		if (StringUtils.isEmpty(toAddress))
		{
			if (isLoggingDebug()) {
				logDebug("No To Address was specified for the confirmation message. Email is not being sent.");
			}
			return;
		}
		TemplateEmailInfo emailInfo = (TemplateEmailInfoImpl) getConfirmationEmailMap().get("NEW_ORDER");
		if (null == emailInfo)
		{
			if (isLoggingDebug()) {
				logDebug("No EmailInfo property was set. Unable to send the confirmation message.");
			}

			throw new TemplateEmailException(ErrorsAndExceptionMessages.NO_EMAIL_INFO_FOR_CONFIRMATION);

		}
		emailInfo = emailInfo.copy();
		List<String> recipients = new ArrayList();
		recipients.add(toAddress);
		logError(emailInfo + "");
		getTemplateEmailSender().sendEmailMessage(emailInfo, recipients, isSendEmailInSeparateThread(), isPersistConfirmationEmails());
		logError("\n After sending email..");
	}

}
