package com.foodstore.util;

/**
 * Configuration file to store FoodStore specific configurations
 * @author Anil
 */
public class StoreConfiguration {
	int maxAllowedSideSkus;

	public int getMaxAllowedSideSkus() {
		return maxAllowedSideSkus;
	}

	public void setMaxAllowedSideSkus(int maxAllowedSideSkus) {
		this.maxAllowedSideSkus = maxAllowedSideSkus;
	}
}
