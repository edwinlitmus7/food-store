package com.foodstore.logging;

import atg.commerce.order.Order;

public class ElasticsearchOrderLogEvent {
	
	private String userName;
	private String userEmail;
	private Order order;
	private String timeStamp;
	public String getUserName() {
		return userName;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}

}
