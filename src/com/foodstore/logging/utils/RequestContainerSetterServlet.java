package com.foodstore.logging.utils;

import java.io.IOException;

import javax.servlet.ServletException;

import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.DynamoHttpServletResponse;
import atg.servlet.pipeline.InsertableServletImpl;

// imports are omitted
 
public class RequestContainerSetterServlet extends InsertableServletImpl {
 
    private FoodStoreRequestContainer requestContainer;
 
    @Override
    public void service(DynamoHttpServletRequest pRequest, DynamoHttpServletResponse pResponse) throws IOException, ServletException {
        requestContainer.setRequest(pRequest);
 
        try {
            passRequest(pRequest, pResponse);
        } finally {
            requestContainer.setRequest(null);
        }
    }
 
    public FoodStoreRequestContainer getRequestContainer() {
        return requestContainer;
    }
 
    public void setRequestContainer(FoodStoreRequestContainer requestContainer) {
        this.requestContainer = requestContainer;
    }
}