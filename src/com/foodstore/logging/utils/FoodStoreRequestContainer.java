package com.foodstore.logging.utils;

import atg.nucleus.GenericService;
import atg.nucleus.ServiceException;
import atg.servlet.DynamoHttpServletRequest;

public class FoodStoreRequestContainer extends GenericService {
 
    ThreadLocal<DynamoHttpServletRequest> requestContainer;
 
    @Override
    public void doStartService() throws ServiceException {
        super.doStartService();
        requestContainer = new ThreadLocal<DynamoHttpServletRequest>();
    }
 
    public Object resolveName(String nucleusName) {
        DynamoHttpServletRequest request = getRequest();
        if(request == null) {
            return null;
        } else {
            return request.resolveName(nucleusName);
        }
    }
 
    public DynamoHttpServletRequest getRequest() {
        return requestContainer.get();
    }
 
    public void setRequest(DynamoHttpServletRequest request) {
        requestContainer.set(request);
    }
}