package com.foodstore.logging.utils;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import com.foodstore.interfaces.HttpConstants;

/**
 *  This class is used for sending post data to the preconfigured urls.
 * @author Anuraj TS
 */
public class HttpQuerySender {
 
 
    /**
     * @param urlAddress - address to send the post data
     * @param data - String data to be posted as json
     * @param mediaType - The media type as which the data is to be sent.
     * @throws IOException - if the writing fails due to some IO related issues.
     */
    public void sendPost(String urlAddress, String data, String mediaType) throws IOException {
        HttpURLConnection connection = null;
        OutputStreamWriter writer = null;
        try {
            URL url = new URL(urlAddress);
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestMethod(HttpConstants.POST);
            connection.setRequestProperty(HttpConstants.CONTENT_TYPE, mediaType);
            connection.setInstanceFollowRedirects(true);
            writer = new OutputStreamWriter(connection.getOutputStream(), HttpConstants.UTF_8);

            writer.write(data);
            writer.flush();
 
            if (!isResponseCodeOk(connection.getResponseCode())) {
                throw new IOException("Response code is " + connection.getResponseCode());
            }
            
        	} finally {
            if (writer != null) {
                writer.close();
            }
 
            if (connection != null) {
                connection.disconnect();
            }
        }
    }
 
    private boolean isResponseCodeOk(int responseCode) {
        return responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED;
    }
}