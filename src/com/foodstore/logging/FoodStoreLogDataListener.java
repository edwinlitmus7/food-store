package com.foodstore.logging;

import java.util.List;

import com.foodstore.logging.PerformanceData;


import atg.service.datacollection.DataListener;

import java.io.IOException;
import java.lang.reflect.Field;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import com.foodstore.interfaces.PropertyConstants;
import com.foodstore.logging.utils.HttpQuerySender;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;


public class FoodStoreLogDataListener extends GenericService implements DataListener {
	private String logStashUrl;
	private  HttpQuerySender httpQuerySender ;
		
	public String getLogStashUrl() {
		return logStashUrl;
	}
	public void setLogStashUrl(String logStashUrl) {
		this.logStashUrl = logStashUrl;
	}
	public HttpQuerySender getHttpQuerySender() {
		return httpQuerySender;
	}
	public void setHttpQuerySender(HttpQuerySender httpQuerySender) {
		this.httpQuerySender = httpQuerySender;
	}
	
	@Override
	public void addDataItem(Object object) {
		if(object != null) {
			EventLogContainer eventLogContainer = (EventLogContainer) object;
			List<PerformanceData> performanceDatas = eventLogContainer.getPerformanceDataList();
			if(performanceDatas != null && !performanceDatas.isEmpty()) {
				for(PerformanceData performanceData : performanceDatas) {
					EventLogData eventLogData = createLogEvent(eventLogContainer, performanceData);
					logEvent(eventLogData);
				}
			}
		}

	}
	



	/**
	 * createLogEvent creates an object which consists if the user information and the order to be logged to kibana. 
	 * @param profile - the profile object used to collect information about user who submitted the order
	 * @param order - the submitted order object
	 * @return
	 */
	public EventLogData createLogEvent(EventLogContainer eventLogContainer,PerformanceData performanceData) {
		RepositoryItem profile = eventLogContainer.getProfile();
		UserData userData = new UserData();
		EventLogData eventLogData = new EventLogData();
		userData.setEmail((String)profile.getPropertyValue(PropertyConstants.USER_EMAIL_PROPERTY));
		userData.setUserName((String)profile.getPropertyValue(PropertyConstants.USER_FIRSTNAME_PROPERTY));
		userData.setProfileId(profile.getRepositoryId());
		eventLogData.setUserData(userData);
		PerformanceDataForLogging performanceDataForLogging = new PerformanceDataForLogging();
		
		performanceDataForLogging.setMethodExecutionStartTime(performanceData.getMethodExecutionStartTime());
		performanceDataForLogging.setMethodExecutionEndTime(performanceData.getMethodExecutionEndTime());
		performanceDataForLogging.setMethodName(performanceData.getMethodName());
		performanceDataForLogging.setMethodStatus(performanceData.getMethodStatus());
		
		eventLogData.setOperationName(eventLogContainer.getOperationName());
		eventLogData.setOperationEndTime(eventLogContainer.getOperationEndTime());
		eventLogData.setOperationStartTime(eventLogContainer.getOperationStartTime());
		eventLogData.setEventType(eventLogContainer.getEventType());
		
		performanceDataForLogging.setOperationStatus(eventLogContainer.getOperationStatus());
		performanceDataForLogging.setObject(eventLogContainer.getObject());
		
		
		
		eventLogData.setPerformanceData(performanceDataForLogging);
		return eventLogData;
		
	}


	/**
	 * 
	 * Calls the thread for logging the order to kibana
	 * @param profile - the profile object used to collect information about user who submitted the order
	 * @param order - the submitted order object
	 * @return true if logging successful.
	 */
	public boolean logEvent(EventLogData event) {
			
	    
	    	
	    	 FieldNamingStrategy customPolicy = new FieldNamingStrategy() {  // The feild name policy to remove small letters from the beginning like mOrder will be converted to order 
	    		  
					@Override
					public String translateName(Field feild) {
						String feilName = feild.getName();
						String newFeildName = null;
						if(feilName !=null) {
							if(feilName.length() > 1 ) {
								if(Character.isLowerCase(feilName.charAt(0))) {
									 if(Character.isUpperCase(feilName.charAt(1))){
										 newFeildName = Character.toLowerCase(feilName.charAt(1))+feilName.substring(2);
										} else {
											newFeildName  =  feilName;
										}
									
								} else {
									newFeildName  =  feilName;
								}
							} else {
								newFeildName  =  feilName;
							}
						}
						
						return newFeildName;
					}
	    		};

	    	GsonBuilder gsonBuilder = new GsonBuilder(); 
	    	gsonBuilder.setFieldNamingStrategy(customPolicy);  
	    	Gson gson = gsonBuilder.create();

	    	 String jsonString = gson.toJson(event);
	    	 new OrderLoggingThread(jsonString).start();
	    	 return true;
	    
	    
	    
	}
	/*
	 * The thread is used to asynchronously send kibana logs to the kibana server.
	 */
	private class OrderLoggingThread extends Thread{
		private String logString;
		public OrderLoggingThread(String logString) {
			this.logString = logString;
		}
		public String getLogString() {
			return logString;
		}



		@Override
		public void run() {
			 try {
				httpQuerySender.sendPost(getLogStashUrl(),getLogString(),
				            MediaType.APPLICATION_JSON);
			} catch (IOException e) {
				if(isLoggingDebug()) {
					logDebug("Unable to send post request while logging order ");
				}
				e.printStackTrace();
			}
		}
	}
		
	


}
