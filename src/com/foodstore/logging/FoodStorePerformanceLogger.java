package com.foodstore.logging;

import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.xml.internal.resolver.helpers.PublicId;
import com.sun.org.apache.xml.internal.security.Init;
import com.sun.tools.doclets.internal.toolkit.NestedClassWriter;

import atg.dms.mselector.EvaluationContext;
import atg.epub.project.Publishing;
import atg.nucleus.GenericService;
import atg.repository.RepositoryItem;
import atg.service.perfmonitor.PerformanceMonitor;
import atg.servlet.DynamoHttpServletRequest;
import atg.servlet.ServletUtil;
import atg.servlet.security.ServletSecurityUtils;
import atg.ui.scenario.editor.SegmentExpirationTimeDialog;
import atg.userprofiling.Profile;

public class FoodStorePerformanceLogger extends GenericService{
	private FoodStoreLogDataListenerQueue dataListenerQueue;
	public FoodStoreLogDataListenerQueue getDataListenerQueue() {
		return dataListenerQueue;
	}

	public void setDataListenerQueue(FoodStoreLogDataListenerQueue dataListenerQueue) {
		this.dataListenerQueue = dataListenerQueue;
	}
	public final String EVENT_LOG_CONTAINER_CONST = "eventLogContainer";
	boolean enablePerformanceLogger ;

	public boolean isEnablePerformanceLogger() {
		return enablePerformanceLogger;
	}

	public void setEnablePerformanceLogger(boolean enablePerformanceLogger) {
		this.enablePerformanceLogger = enablePerformanceLogger;
	}
	public void Init(String operationName,RepositoryItem profile) {
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
		if(eventLogContainer != null) {
			if(isLoggingDebug()) {
				logDebug(" eventLogContainer != null an action with same name already started monitoring is not ended yet." );
			}
		} else {
			eventLogContainer = new EventLogContainer();
			eventLogContainer.setProfile(profile);
			eventLogContainer.setOperationStartTime(System.currentTimeMillis());
			eventLogContainer.setOperationName(operationName);
			eventLogContainer.setEventType(operationName);
			eventLogContainer.setOperationStatus(PerformanceStatus.STARTED);
			request.setAttribute(EVENT_LOG_CONTAINER_CONST, eventLogContainer);
		}
	}
	public void sendLog() {
		DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
		EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
		if(eventLogContainer != null) {
			eventLogContainer.setOperationEndTime(System.currentTimeMillis());
			getDataListenerQueue().addDataItem(eventLogContainer);

		} else {
			
			if(isLoggingDebug()) {
				logDebug(" eventLogContainer != null an action with same name already started monitoring is not ended yet." );
			}
		}
	}
	public void startMonitoring(String methodName) {
		if(isEnablePerformanceLogger()) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
			if(eventLogContainer != null) {
				PerformanceData performanceData = new PerformanceData();
				performanceData.setMethodExecutionStartTime(System.currentTimeMillis());
				performanceData.setMethodStatus(PerformanceStatus.STARTED);
				performanceData.setMethodName(methodName);
				eventLogContainer.addPerformanceData(performanceData);
			} else {
				if(isLoggingDebug()) {
					logDebug("eventLogContainer was null but method :" + methodName + " was called." );
				}
			}
		}
		
	}
	
	public void setObjectToContainer(Object  object) {
		if(isEnablePerformanceLogger()) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
			if(eventLogContainer != null) {
				eventLogContainer.setObject(object);
			} else {
				if(isLoggingDebug()) {
					logDebug("eventLogContainer was null but set object on container" );
				}
			}
		}
	}
	public void setObjectToMethod(String methodName,Object  object) {
		if(isEnablePerformanceLogger()) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
			if(eventLogContainer != null) {
				PerformanceData lastPerformanceData = eventLogContainer.getLastMethodsPerformanceDataByName(methodName);
				if(lastPerformanceData == null) {
					if(isLoggingDebug()) {
						logDebug("eventLogContainer does not contain a logged  method :" + methodName  );
					}
					return;
				}
				lastPerformanceData.setObject(object);
				
			} else {
				if(isLoggingDebug()) {
					logDebug("eventLogContainer was null but set object monitoring was called on method :" + methodName  );
				}
			}	
		}
	}
	public void stopMonitoring(String methodName) {
		if(isEnablePerformanceLogger()) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
			if(eventLogContainer != null) {
				PerformanceData lastPerformanceData = eventLogContainer.getLastMethodsPerformanceDataByName(methodName);
				if(lastPerformanceData == null) {
					if(isLoggingDebug()) {
						logDebug("eventLogContainer does not contain a logged  method :" + methodName  );
					}
					return;
				}
				lastPerformanceData.setMethodExecutionEndTime(System.currentTimeMillis());
				
				
			} else {
				if(isLoggingDebug()) {
					logDebug("eventLogContainer was null but stop monitoring was called on method :" + methodName  );
				}
			}
		}
	}
	
	public void changeOperationStatus(PerformanceStatus status) {
		if(isEnablePerformanceLogger()) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
			if(eventLogContainer == null) {
				if(isLoggingDebug()) {
					logDebug("eventLogContainer was null but change in status was called" );
				}
			} else {
				eventLogContainer.setOperationStatus(status);
			}
		}
	}
	public void changeMethodStatus(String methodName,PerformanceStatus status) {
		if(isEnablePerformanceLogger()) {
			DynamoHttpServletRequest request = ServletUtil.getCurrentRequest();
			EventLogContainer eventLogContainer = (EventLogContainer) request.getAttribute(EVENT_LOG_CONTAINER_CONST);
			if(eventLogContainer != null) {
				PerformanceData lastPerformanceData = eventLogContainer.getLastMethodsPerformanceDataByName(methodName);
				if(lastPerformanceData == null) {
					if(isLoggingDebug()) {
						logDebug("eventLogContainer does not contain a logged  method :" + methodName  );
					}
					return;
				}
				lastPerformanceData.setMethodStatus(status);
				
			} else {
				if(isLoggingDebug()) {
					logDebug("eventLogContainer was null but change in status was called for method :" + methodName  );
				}
			}
		}
	}

}
