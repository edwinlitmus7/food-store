package com.foodstore.logging;

import java.io.Serializable;

public class PerformanceDataForLogging implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String methodName;
	private PerformanceStatus methodStatus;
	private PerformanceStatus operationStatus;
	private long methodExecutionStartTime;
	private long methodExecutionEndTime;

	private Object object;
	
	
	

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public long getMethodExecutionStartTime() {
		return methodExecutionStartTime;
	}

	public long getMethodExecutionEndTime() {
		return methodExecutionEndTime;
	}

	public PerformanceStatus getMethodStatus() {
		return methodStatus;
	}

	public void setMethodStatus(PerformanceStatus methodStatus) {
		this.methodStatus = methodStatus;
	}



	public void setMethodExecutionStartTime(long methodExecutionStartTime) {
		this.methodExecutionStartTime = methodExecutionStartTime;
	}

	public void setMethodExecutionEndTime(long methodExecutionEndTime) {
		this.methodExecutionEndTime = methodExecutionEndTime;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}


	public PerformanceStatus getOperationStatus() {
		return operationStatus;
	}
	
	public void setOperationStatus(PerformanceStatus operationStatus) {
		this.operationStatus = operationStatus;
	}
	

}