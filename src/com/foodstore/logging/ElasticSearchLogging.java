package com.foodstore.logging;

import java.io.IOException;
import java.lang.reflect.Field;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.core.MediaType;

import com.foodstore.interfaces.PropertyConstants;
import com.foodstore.logging.utils.HttpQuerySender;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import atg.commerce.order.Order;
import atg.nucleus.GenericService;
import atg.userprofiling.Profile;

/**
 * @author Anuraj T S
 * ElasticSearchLogging class is used to send log events to kibana.
 */
public class ElasticSearchLogging extends GenericService {
private String elasticsearchUrl;
	
public String getElasticsearchUrl() {
	return elasticsearchUrl;
}
private  HttpQuerySender httpQuerySender ;

public HttpQuerySender getHttpQuerySender() {
	return httpQuerySender;
}
public void setHttpQuerySender(HttpQuerySender httpQuerySender) {
	this.httpQuerySender = httpQuerySender;
}
public void setElasticsearchUrl(String elasticsearchUrl) {
	this.elasticsearchUrl = elasticsearchUrl;
}
/**
 * createLogEvent creates an object which consists if the user information and the order to be logged to kibana. 
 * @param profile - the profile object used to collect information about user who submitted the order
 * @param order - the submitted order object
 * @return
 */
public ElasticsearchOrderLogEvent createLogEvent(Profile profile,Order order) {
	ElasticsearchOrderLogEvent event = new ElasticsearchOrderLogEvent();
	event.setOrder(order);
	event.setUserName(profile.getPropertyValue(PropertyConstants.USER_FIRSTNAME_PROPERTY) + " " + profile.getPropertyValue(PropertyConstants.USER_LASTNAME_PROPERTY));
	event.setUserEmail((String)profile.getPropertyValue(PropertyConstants.USER_EMAIL_PROPERTY));
	DateFormat dateFormat = new SimpleDateFormat(PropertyConstants.DATE_FORMATE_STRING);
	Date date = new Date();
	event.setTimeStamp(dateFormat.format(date));
	return event;
	
}


/**
 * Calls the thread for logging the order to kibana
 * @param profile - the profile object used to collect information about user who submitted the order
 * @param order - the submitted order object
 * @return true if logging successful.
 */
public boolean logOrder(Profile profile,Order order) {
	ElasticsearchOrderLogEvent event = createLogEvent(profile,order);
    
    	
    	 FieldNamingStrategy customPolicy = new FieldNamingStrategy() {  // The feild name policy to remove small letters from the beginning like mOrder will be converted to order 
    		  
				@Override
				public String translateName(Field feild) {
					String feilName = feild.getName();
					String newFeildName = null;
					if(feilName !=null) {
						if(feilName.length() > 1 ) {
							if(Character.isLowerCase(feilName.charAt(0))) {
								 if(Character.isUpperCase(feilName.charAt(1))){
									 newFeildName = Character.toLowerCase(feilName.charAt(1))+feilName.substring(2);
									} else {
										newFeildName  =  feilName;
									}
								
							} else {
								newFeildName  =  feilName;
							}
						} else {
							newFeildName  =  feilName;
						}
					}
					
					return newFeildName;
				}
    		};

    	GsonBuilder gsonBuilder = new GsonBuilder(); 
    	gsonBuilder.setFieldNamingStrategy(customPolicy);  
    	Gson gson = gsonBuilder.create();

    	 String jsonString = gson.toJson(event);
    	 new OrderLoggingThread(jsonString).start();
    	 return true;
    
    
    
}
/*
 * The thread is used to asynchronously send kibana logs to the kibana server.
 */
private class OrderLoggingThread extends Thread{
	private String logString;
	public OrderLoggingThread(String logString) {
		this.logString = logString;
	}
	public String getLogString() {
		return logString;
	}



	@Override
	public void run() {
		 try {
			httpQuerySender.sendPost(getElasticsearchUrl(),getLogString(),
			            MediaType.APPLICATION_JSON);
		} catch (IOException e) {
			if(isLoggingDebug()) {
				logDebug("Unable to send post request while logging order ");
			}
			e.printStackTrace();
		}
	}
}
	
}
