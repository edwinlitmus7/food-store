package com.foodstore.logging;

import java.util.ArrayList;
import java.util.List;

import com.foodstore.logging.PerformanceData;

import atg.repository.RepositoryItem;
import atg.userprofiling.Profile;

public class EventLogContainer {
	private List<PerformanceData> performanceDataList = new ArrayList<>();
	private RepositoryItem profile;
	private String operationName;
	private PerformanceStatus operationStatus;
	private long operationStartTime;
	private long operationEndTime;
	private Object object;
	private String eventType;
	
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public PerformanceStatus getOperationStatus() {
		return operationStatus;
	}

	public void setOperationStatus(PerformanceStatus operationStatus) {
		this.operationStatus = operationStatus;
	}

	public long getOperationStartTime() {
		return operationStartTime;
	}

	public void setOperationStartTime(long operationStartTime) {
		this.operationStartTime = operationStartTime;
	}

	public long getOperationEndTime() {
		return operationEndTime;
	}

	public void setOperationEndTime(long operationEndTime) {
		this.operationEndTime = operationEndTime;
	}

	public RepositoryItem getProfile() {
		return profile;
	}

	public void setProfile(RepositoryItem profile) {
		this.profile = profile;
	}

	public List<PerformanceData> getPerformanceDataList() {
		return performanceDataList;
	}
	public void addPerformanceData(PerformanceData performanceData) {
		this.performanceDataList.add(performanceData);
	}
	public PerformanceData getLastMethodsPerformanceDataByName(String methodName) {
		List<PerformanceData> eventLogDataList = getPerformanceDataList();
		
		if (eventLogDataList == null) {
			return null;
		}
		for(int j = eventLogDataList.size() - 1; j >= 0; j--){
			PerformanceData eventLogData = eventLogDataList.get(j);
			if(eventLogData.getMethodName() != null && eventLogData.getMethodName().equals(methodName)) {
				return eventLogData;
			}
		}
		return null;
	}
	
}
