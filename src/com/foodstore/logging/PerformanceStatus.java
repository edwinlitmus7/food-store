package com.foodstore.logging;
public enum PerformanceStatus {
		STARTED("STARTED"),
		END("SUCCESS"),
		FAILED("FAILED");
		private String performaceStatus;
		@Override
		public String toString() {
			return performaceStatus;
		}
		private PerformanceStatus(String status) {
			this.performaceStatus = status;
		}
	}