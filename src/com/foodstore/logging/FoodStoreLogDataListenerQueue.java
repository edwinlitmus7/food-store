package com.foodstore.logging;

import atg.service.datacollection.DataListenerQueue;

public class FoodStoreLogDataListenerQueue extends DataListenerQueue {
	private FoodStoreLogDataListener dataListener;
	
	public FoodStoreLogDataListener getDataListener() {
		return dataListener;
	}

	public void setDataListener(FoodStoreLogDataListener foodStoreLogDataListener) {
		this.dataListener = foodStoreLogDataListener;
	}

	@Override
	public synchronized void doStartService(){
		super.doStartService();
		addDataListener(getDataListener());
	}
}
