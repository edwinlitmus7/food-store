package com.foodstore.logging;

import java.io.Serializable;

import atg.commerce.pricing.PricingModelEvaluationSiteVetoer;

public class EventLogData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private UserData userData;
	private PerformanceDataForLogging performanceData;
	private Object object;
	private long operationEndTime;
	private long operationStartTime;
	private String operationName;
	private String eventType;
	
	public long getOperationEndTime() {
		return operationEndTime;
	}

	public void setOperationEndTime(long operationEndTime) {
		this.operationEndTime = operationEndTime;
	}

	public long getOperationStartTime() {
		return operationStartTime;
	}

	public void setOperationStartTime(long operationStartTime) {
		this.operationStartTime = operationStartTime;
	}

	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String operationName) {
		this.operationName = operationName;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public UserData getUserData() {
		return userData;
	}
	
	public void setUserData(UserData userData) {
		this.userData = userData;
	}
	
	public PerformanceDataForLogging getPerformanceData() {
		return performanceData;
	}
	
	public void setPerformanceData(PerformanceDataForLogging performanceData) {
		this.performanceData = performanceData;
	}
	
	public Object getObject() {
		return object;
	}
	
	public void setObject(Object object) {
		this.object = object;
	}
	

}
