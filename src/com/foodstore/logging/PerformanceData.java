package com.foodstore.logging;

import java.io.Serializable;

import com.foodstore.logging.PerformanceStatus;

public class PerformanceData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String methodName;
	private PerformanceStatus methodStatus;
	private String operationName;
	private PerformanceStatus operationStatus;
	private long methodExecutionStartTime;
	private long methodExecutionEndTime;
	private long operationStartTime;
	private long operationEndTime;
	private Object object;
	
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public long getMethodExecutionStartTime() {
		return methodExecutionStartTime;
	}

	public long getMethodExecutionEndTime() {
		return methodExecutionEndTime;
	}

	public PerformanceStatus getMethodStatus() {
		return methodStatus;
	}

	public void setMethodStatus(PerformanceStatus methodStatus) {
		this.methodStatus = methodStatus;
	}

	public long getOperationStartTime() {
		return operationStartTime;
	}

	public void setOperationStartTime(long operationStartTime) {
		this.operationStartTime = operationStartTime;
	}

	public long getOperationEndTime() {
		return operationEndTime;
	}

	public void setOperationEndTime(long operationEndTime) {
		this.operationEndTime = operationEndTime;
	}

	public void setMethodExecutionStartTime(long methodExecutionStartTime) {
		this.methodExecutionStartTime = methodExecutionStartTime;
	}

	public void setMethodExecutionEndTime(long methodExecutionEndTime) {
		this.methodExecutionEndTime = methodExecutionEndTime;
	}

	public String getMethodName() {
		return methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}
	public String getOperationName() {
		return operationName;
	}

	public void setOperationName(String actionName) {
		this.operationName = actionName;
	}
	
	

	public PerformanceStatus getOperationStatus() {
		return operationStatus;
	}
	
	public void setOperationStatus(PerformanceStatus operationStatus) {
		this.operationStatus = operationStatus;
	}
	

}
