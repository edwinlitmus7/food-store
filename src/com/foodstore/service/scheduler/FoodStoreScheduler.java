package com.foodstore.service.scheduler;

import java.util.HashSet;

import com.foodstore.catalog.FoodStore;

import atg.adapter.gsa.ChangeAwareList;
import atg.repository.Query;
import atg.repository.QueryBuilder;
import atg.repository.QueryExpression;
import atg.repository.Repository;
import atg.repository.RepositoryException;
import atg.repository.RepositoryItem;
import atg.repository.RepositoryView;
import atg.service.scheduler.SchedulableService;
import atg.service.scheduler.Schedule;
import atg.service.scheduler.ScheduledJob;
import atg.service.scheduler.Scheduler;
/**
 * The scheduler runs on breaksfast, lunch, tea and dinner timings to load catalog from database.
 * @author Thasnim
 *
 */
public class FoodStoreScheduler extends SchedulableService {
	Schedule schedule;
	private FoodStore foodStore;
	private RepositoryItem[] availableProducts,items;
	private HashSet<Object> keys;

	public Schedule getSchedule() {
		return schedule;
	}

	public void setSchedule(Schedule schedule) {
		this.schedule = schedule;
	}

	public FoodStore getFoodStore() {
		return foodStore;
	}

	public void setFoodStore(FoodStore foodStore) {
		this.foodStore = foodStore;
	}

	@Override
	public void performScheduledTask(Scheduler arg0, ScheduledJob arg1) {
	
		if (isLoggingDebug()) {

			logDebug("Inside getRootCategory()");
			logDebug("SCHEDULER RUNNING!!!!!!!!!!");
		}
		
		getFoodStore().getFoodMenu();
		getFoodStore().getKeywords();
		
	}

}
